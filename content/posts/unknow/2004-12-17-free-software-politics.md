---
title:   "Free Software politics"
date:    2004-12-17
authors:
  - unknow
slug:    free-software-politics
---
Today I've read <a href="http://tsdgeos.blogspot.com/2004/12/firefox-spreads-non-free-pdf-viewers.html">a post</a> on <a href="http://www.planetkde.org">planetkde</a> asking the question: "why is one of the jewels of the Free Software promoting non Free Software pdf viewers?" For those who didn't read the post, the author was referring to the NY Times Firefox ad PDF file not being viewable on any free pdf viewer.

Since this subject (related IMO to the free-software-running-on-win32-is-evil extravaganza) is pretty hot these days, I decided to give my 2¢.

I am certainly not a Firefox developer (nor user btw) but I think I can answer this question.
So, why is one of the free software jewels promoting non-free software? For starters, maybe because the ultimate objective of Mozilla.org is to create a great webbrowser in Firefox, instead of being anybody's agenda's jewel.

Why should they care if people are using their "product" on Windows or Linux or anything else? If people use Firefox, great for them.

If the Mozilla Project sees fit in trying to widen the use of their webbrowser, why would anyone care about it? Let them! This RMS-like attitude of dont-do-anything-without-my-consent makes me mad.

The guys working on KDE win32 are doing The Right Thing by going out and doing it. I do consider the effort to port KDE apps to win32 a waste of time, but who am I to judge?

Of course, I woke up in a bad mood today, so maybe I'll change my mind later in the day :-)