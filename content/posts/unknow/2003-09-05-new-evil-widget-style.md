---
title:   "New evil widget style..."
date:    2003-09-05
authors:
  - unknow
slug:    new-evil-widget-style
---
Well, I said originally that I wasn't going to release this, but the reactions have just been too funny so far to pass it up.

It's not an official release, yet, of course - I still have to implement the rendering code for the sliders and tabbars and probably a few other widgets too. But the sneak preview is out...

http://c133.org/asteroid.png

http://c133.org/qwin-vs-asteroid.png

The new style is called Asteroid, and you can download it today from http://c133.org/asteroid_style.tar.bz2 and do whatever you like with it. There's no README, it's 100% coded from the ground up (I didn't start with any other style), and it's scarily similar to the appearance of Windows 2000.

In order to connive it into building, you'll want to do a 'make -f admin/Makefile.common' from the unpacked source directory. This will generate the 'configure' script. From there on out, it's the standard ./configure && make && sudo make install routine.

Any flames are welcome. :)

-clee