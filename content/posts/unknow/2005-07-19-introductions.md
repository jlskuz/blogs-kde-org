---
title:   "Introductions"
date:    2005-07-19
authors:
  - unknow
slug:    introductions
---
Finally time to write a first entry.

There have been so many things to do lately, which means that I won't get bored - though my head may explode :)

Within KDE, I work with the kde-edu project. I also created the monochrome iconset for KDE 3.4

In the kde-edu realm, nice things are happening. KSimon and Kanagram are getting to a nice stage, and we hope to get them into KDE 3.5 - for those that have tried KSimon, do you think it should be in kde-edu, or kde-games?

I felt the effects of yesterday's plasma slashdotting, the second link of that submission linking to my mockups. As cmk generously hosts various things for me, his site (kde-forum.org) was also taken down. I feel bad about that :(

It seems that I will have to use bittorrent for the plasma *video* that I am preparing.

Also hosted at that server is a project that I am working on, that I hope to show you all very soon. Though this situation has slightly put back my plans, I am now back in business :)

p.s. Enjoy your holiday annma, you deserve it!
<!--break-->