---
title:   "myscreen.org"
date:    2005-09-25
authors:
  - unknow
slug:    myscreenorg
---
So, by writing now I am keeping my last promise that my next entry would be about the project that I have been working on. However, maybe I should have written earlier than exactly 2 months later.

Anyway, as the title of this entry says, my project is myscreen.org - an interactive way to watch various KDE people work and play. As of this entry, there are 14 people from all areas of KDE. As well as viewing the screen of the user, you can also view stats about them, which are also updated live.

The map at http://myscreen.org shows all the users on the site - the red dots are people who are active at this very moment! Some people are more active than others, but you can bet that whenever I am at my computer, I am active at http://myscreen.org/dannya

That's enough about the site for now, I think :)

Also, for you superkaramba users, there is now an applet available now, so you can watch us from your desktop. Grab it here: http://myscreen.org/applet

Oh yes - and if seele asks you to complete a survey in #kde-devel, help our usability people out and say "Yes!"

More soon...

<!--break-->