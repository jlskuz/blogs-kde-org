---
title:   "Proper GPG support for Jabber"
date:    2003-07-25
authors:
  - unknow
slug:    proper-gpg-support-jabber
---
Finally! With a bit of hacking and borrowing from the new libxmpp code, the Kopete Jabber plugin is now able to properly encrypt messages and can thus talk with Psi and other Jabber clients in a secure way.

This was among the most annoying outstanding bugs on the list, I'm glad I was able to make it before a 0.7 release. Testing is welcome! And thanks a lot to Olivier for pointing out how I could reformat the damn message. :)

Other things I did today were minor release fixups, like proper icons and stuff like that. It's not the nicest things of all but polishing for a release is a task that wouldn't be done without someone setting schedules for it. :)