---
title:   "Open source at Apple: Konqueror gets better"
date:    2003-07-27
authors:
  - unknow
slug:    open-source-apple-konqueror-gets-better
---
<br>I attended Apple's Worldwide Developers Conference in San Francisco a month ago. Coming from a Linux and KDE background, I was particularly interested in what Apple would say about open source.

<br><br>If any Konqueror developers are reading, I would like to get their comments on Apple's contributions. I know Apple posted a lot of nice fixes to the mailing list in January, when the initial version of Safari was released. Since then, I've not heard much. How is the cooperation today?</p>
<!--break-->

<br><br>I submitted this little piece to KDE.news a while ago, but it was never posted.</p>

<hr>

<br><br>At the recent <a href="http://developer.apple.com/wwdc/">Worldwide
Developers Conference</a> in San Francisco, Apple released <a
href="http://www.apple.com/safari/">Safari&nbsp;1.0</a>, the browser
based on KDE's HTML and JavaScript libraries. Along with Safari comes
<a href="http://developer.apple.com/darwin/projects/webcore/index.html">Web
Kit</a>, a public API that any Mac&nbsp;OS&nbsp;X application can use
for HTML functionality. Apple has done a lot of work improving the KDE
code, and KDE of course benefits from their work.</p>

<br><br>Apple presents Safari as a fast, standards-compliant and
feature-rich browser. Safari will be the default browser in the
upcoming <a href="http://www.apple.com/macosx/panther/">10.3
&quot;Panther&quot;</a> release of Mac&nbsp;OS&nbsp;X. Apple relies
heavily on the underlying Web Kit API for many other parts of the
operating system, for example Help, Mail, Xcode, and Sherlock. They
also encourage other developers to use the Web Kit, instead of
inventing their own wheel. Since the Web Kit is becoming such a vital
part of Mac&nbsp;OS&nbsp;X, Apple will surely continue their work on
the codebase.</p>


<br><br><b>Apple and Open Source</b></p>

<br><br>The core of Mac&nbsp;OS&nbsp;X is
<a href="http://developer.apple.com/darwin/">Darwin</a>, a FreeBSD based
foundation. Many other components of Mac&nbsp;OS&nbsp;X are open
source as well, such as Postfix, Cups, OpenLDAP, XFree86, and GCC. In
general, Apple has chosen open source wherever they will benefit from
it. A lead developer at Apple stated that open source was like
&quot;shopping&quot;: You can simply pick the project that best fits
your needs.</p>

<br><br>Apple's commitment to open source doesn't stop with just
<em>using</em> the code, they also give something back. If Apple were
to take a piece of codem and maintain it totally separated the
original project, they couldn't as easily pick up future
improvements. Because of this, Apple always tries to keep their branch
synchronized by contributing their improvements back to the main
project.</p>

<br><br>Besides using lots of open source software, some of Apple's own
projects are open source as well. Some examples are the
<a href="http://developer.apple.com/darwin/projects/openplay/">OpenPlay</a>
networking toolkit and
<a href="http://developer.apple.com/macosx/rendezvous/">Rendezvous</a>,
&quot;zero configuration networking&quot;. The motivation for open
sourcing these is presumably to get wider usage. The parts of
Mac&nbsp;OS&nbsp;X that are (still) closed source, for example the
graphical user interface, are parts where Apple don't believe open
sourcing will be beneficial. Perhaps they are afraid of competition,
or perhaps they just don't think the open source community can
contribute.</p>


<br><br><b>Why Konqueror?</b></p>

<br><br>KDE's HTML and JavaScript libraries were chosen for three reasons:</p>

<ul>
  <li>Easy to modify</li>
  <li>Fast, and easy to make faster</li>
  <li>Small codebase</li>
</ul>

<br><br>Using the existing KDE code has given Apple a jump-start on
creating their own browser. Contributing back to KDE is important to
maintain a good relationship with the open source community, but
perhaps the most important reason is that it will save Apple even more
work.</p>

<br><br>The range of standards supported by Safari/KHTML is impressive, but
supporting non-standard web sites is (sadly) very important as
well. Apple has improved KHTML and KJS in many ways, especially with
regards to &quot;real-world&quot; compliance. Being a member of <a
href="http://www.w3.org/">W3C</a>, Apple is strongly focused on web
standards, so we will certainly see more improvements coming from
Apple.</p>


<br><br><b>The Future: Eating their Own Dog Food</b></p>

<br><br>To put it in their own words: Apple is &quot;eating their own dog
food&quot;. They are serious about Safari and the Web Kit, and use the
technology throughout Mac&nbsp;OS&nbsp;X. The web community as a whole
benefits from anything that reduces Microsoft's browser monopoly, and
the KDE project will of course benefit from the improvements made by
Apple's developer team. The future is bright!</p>
