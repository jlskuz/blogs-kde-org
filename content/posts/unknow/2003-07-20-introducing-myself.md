---
title:   "Introducing myself"
date:    2003-07-20
authors:
  - unknow
slug:    introducing-myself
---
Wow, what a great idea! I can only say thanks for such a site - I definitely hope it will start to be a good resource for any user seeking closer contact to the members of the KDE team.

So, for a first post, I'd like to introduce myself. My main focus is Kopete. For well over a year now I've been working on the Jabber plugin, occasionally I also dive into libkopete, the core code behind the UI that ties the protocol plugins together. I must say that it's a very rewarding task, Kopete and KDE in general are the first projects that have fascinated me over a longer period of time and that still feel just as cool as on the very first day.

My way to Kopete was quite indirect: originally, I wanted to create and maintain a Debian package. Any package. Just a package to get my hands on and to work with. Also, at the same time, I was looking for a capable Jabber client. Since no good Jabber client seemed to exist for KDE and I always wanted to get my hands on KDE programming, I started to create my own. Shortly after my first clumsy steps, while hanging out in a KDE channel and asking questions, Nick Betcher pointed me to Kopete with the words "there's no use in creating a Jabber client for KDE, Kopete has everything you need and Daniel Stone almost finished his Jabber plugin for it". Boy was I disappointed.

Still, I joined the Kopete channel and eventually got in contact with Daniel. He said that he'd still be looking for a Debian packager and it got me all excited again. I started to use Kopete and found it to be a quite adorable client, actually much better than most. It's potential surely outweighed the bugs and little features it still had back then.

As I used it daily, I noticed a few things that didn't work out the way they should in the Jabber plugin. At first I kept sending mails to Daniel, at some point I started to look into the code and sent patches. After a while, Daniel encouraged me to apply for a CVS account and I joined the team between the 0.4 and 0.5 releases.

My patches started to get larger and Daniel unfortunately couldn't devote a lot of time to Kopete anymore, so I ended up rewriting most of his code and finally taking over the maintainership of the Jabber plugin.

My aim is to get the Jabber plugin as stable and as feature-rich as possible, while still being easy, intuitive and fun to use. Unfortunately not too many people seem to use Kopete for Jabber, but my self-proclaimed goal is to play at the same level as Psi does, which we share code with (thanks, Justin!) and which I truly admire for its simplicity and effectivity.

Alright, enough for my first post, I'm looking forward to document my development efforts here, especially since there are many things on my todo list for the time after our upcoming 0.7 release!