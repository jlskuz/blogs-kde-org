---
title:   "Ensuring the future of evil"
date:    2003-09-14
authors:
  - unknow
slug:    ensuring-future-evil
---
So, I've spent the last 3 days porting the Redmond KWin decoration to the new kwin_iii style API in my spare time. In case anyone actually cares, here's how it broke down by the day. 

Day 1: Make it compile with the new API. Mostly search and replace. Spent maybe an hour. Didn't compile completely cleanly, so I gave up for the day.

Day 2: Attacked it again, fixing a bunch of stupid problems with actually loading the plugin and making it run without crashing KWin. Spent about half an hour. Once I got the preview to load, I was happy.

Day 3: Make it look just like the KWin-from-HEAD version to the pixel. Errors here were mostly caused by my own stupidity, but I sorted it all out eventually. Total time invested was about an hour and a half, maybe two hours, but it's tough to tell since I kept getting interrupted.

Total amount of time actually required: Somewhere around three or four hours. Yaaay! :)