---
title:   "KBoard, development is going on..."
date:    2006-10-04
authors:
  - ratta
slug:    kboard-development-going
---
We did not advertize it a lot yet, but as a few people already know, kde will have a generic board application :)
We did a 0.9 alpha release a few days ago, immediately after moving to kde svn, but development is going on.
The features in the release include a very generic board game support (at the moment only C++, but lua variants will be supported very soon, games implemented are chess and a few variants, connect four, reversi and chain reaction. Shogi is coming :)), an animation engine that supports contemporary animations, eye candies, etc, a lua-scripted theme loader that can load SVG, PNG, TTF chess fonts, and create images on the fly, and a move list widget that allows for comfortable pgn editing. Support for playing/observing/examinating chess (and chess variants) games on FICS is almost complete, support for chess engines is at a good stage of development (but it is not very usable, yet).
Since the release i've been implementing a generic option framework (with lua bindings), to allow themes, variants, etc to get an integer options from the user (through a spinbox), or a bool (through a check box), as well as a color, font, url, etc.
On the other side Paolo (zhw on IRC) has almost finished the port to kdelibs (we were using Qt4 only, but kdelibs really rocks and we will no longer support the qt-only version), and is rolling out a very generic XML settings class set, that will be much more powerful than the alreay available QSettings or KConfig.
Ah, as you can see in the second screenshot, i could not resist to use KHTMLPart to implement an alternative move list widget, with less eye candies, but more in the chessbase style and that i also hope will make easy printing games (babaschess has two alternative move list widgets too, so i guess that keeping the choice should be ok in kboard too :)).
I think that the option and settings frameworks are pretty good pieces of code that may also be reused in other projects, but we'll have a better idea after using them in kboard for some time.
On the down side, we still haven't decided how the user interface will be, we do not really feel comfortable with QDockWidgets, (for instance, the console when detached will always want to stay on top), so we are planning to give a look at the IDEAL mode, but if some GUI guru can suggest us better ideas any help will be welcome :)
You will find more screenshot on <a href="http://kboard.sourceforge.net">http://kboard.sourceforge.net</a> (ah, the website is another thing that desperately needs to be improved).
[image:2421 size=preview]
[image:2422 size=preview]
[image:2423 size=preview]
