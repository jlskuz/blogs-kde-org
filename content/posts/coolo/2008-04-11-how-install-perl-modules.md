---
title:   "How to install perl modules"
date:    2008-04-11
authors:
  - coolo
slug:    how-install-perl-modules
---
Ever called a perl script and got this?

Can't locate Regexp/Common.pm in @INC (@INC contains: SCRIPT /usr/lib/perl5/5.10.0/i586-linux-thread-multi /usr/lib/perl5/5.10.0 /usr/lib/perl5/site_perl/5.10.0/i586-linux-thread-multi /usr/lib/perl5/site_perl/5.10.0 /usr/lib/perl5/vendor_perl/5.10.0/i586-linux-thread-multi /usr/lib/perl5/vendor_perl/5.10.0 /usr/lib/perl5/vendor_perl .) at SCRIPT line 18.

Well, use zypper:

coolo@desdemona#STABLE>sudo zypper in -C 'perl(Regexp::Common)'
Reading installed packages...

The following NEW package is going to be installed:
  perl-Regexp-Common

Easy, no?
<!--break-->