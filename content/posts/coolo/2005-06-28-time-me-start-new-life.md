---
title:   "Time for me to start a new life"
date:    2005-06-28
authors:
  - coolo
slug:    time-me-start-new-life
---
About time I tell everyone interested about it: Friday, July 1st 2005 I will stop being Mister Kulow, the boy friend and become Mister Kulow, the husband of Mrs. Kulow :)

Nothing more to add, beside: I'll be back from vacation on 11th. Don't expect too much from me till 21st though - as till then I will spend my afternoons in front of the TV set watching cycling. And in case you wonder: my dreambox is there and will record every minute of it, so I don't miss anything while I'm away.

<!--break-->