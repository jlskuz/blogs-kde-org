---
title:   "And the oscar goes to... kpat"
date:    2006-10-31
authors:
  - coolo
slug:    and-oscar-goes-kpat
---
<a href="http://www.surveymonkey.com/DisplaySummary.asp?SID=2748372&U=274837292751">The kdegames survey</a> has shown it: People love kpat. It's the only game that reached an overall score of 4 (with 46% saying it's game play is very good relative to the rest of kdegames)!

And the KDE4 version got some nice lifting... :)