---
title:   "I'm lovin it!"
date:    2005-09-04
authors:
  - coolo
slug:    im-lovin-it
---
Whatever you guys took out of the Malaga conference - for me it's this: I love my wife (knew _that_ part before, but I better list it to avoid misunderstandings), my bed, my couch, the silence in my bedroom and speaking german (having meals without having to guess what it is pretty cool :). 

And the most important part actually: if I have to pick the location where I want to be sick, I surely prefer $HOME. Having seen Boudewijn (copy & paste of text is so much easier than copy & paste of spellings btw :) and Cornelius, I really felt lucky.
<!--break-->