---
title:   "Calling all cross-desktop developers! Let's make crossdesktop colaboration more important in Free/Libre Open Source"
date:    2008-04-25
authors:
  - spstarr
slug:    calling-all-cross-desktop-developers-lets-make-crossdesktop-colaboration-more-important
---
It's been a little while since I've last posted an entry. My own personal life has been busy and hasn't given me much time to work on code. That will change soon though :-)

I post here today as a KDE developer and moderate in the support of cross-desktop collaboration and friendship. A fellow GNOME friend and myself have created a channel on Freenode and irc.gnome.org (Gimpnet) called #freedesktop-cafe. The channel is designed to have GNOME and KDE (and other desktop camps) developers (and users too if you like) come together for casual (non-developer) chat and to help build friendships. It's not meant to be political. The channel relays its messages to both networks. I'd like to know what you all think. 

This channel is not sponsored by either KDE or GNOME. I hope in future we can work together more, despite our language differences and technologies. You can't work with people if you aren't friends with them.