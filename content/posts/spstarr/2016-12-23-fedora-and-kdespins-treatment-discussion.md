---
title:   "Fedora and KDE/spin's treatment - Discussion"
date:    2016-12-23
authors:
  - spstarr
slug:    fedora-and-kdespins-treatment-discussion
---
I think it's important that the Fedora KDE / Spins Community speak out about how Fedora treats KDE and other spins. Given Fedora is about to have  FESCo election, now is the perfect time to get community feedback on what candidates think.

For those who know me, they know I enjoy and support Fedora/Red Hat and have for awhile. However, they also know I strongly dislike how Fedora treats KDE as a 2nd class citizen. Why do I say that? It's well known the history of Fedora/Red Hat has been GNOMEcentric from the very beginning. 

<b>This blog post isn't about GNOME vs KDE Plasma and please do not make it about that</b>, there is room for both desktop environments to collaborate and share ideas.

Fedora is about freedom but as part of that freedom Fedora users being able to select the flavor of Fedora they want to use. We can do better to embrace more people into the Fedora family by improving the visibility of other Fedora projects within the Fedora family.

I get that Fedora wants 'products' for Workstation, Server, Cloud and that's fine, but don't hide the other Fedora projects who want to help Fedora! The goal is to bring more people in Fedora, I don't care if it's for KDE or Mate, Cinnamon but <b>GROW</b> the Fedora community.

See discussions from:
<a href="https://pagure.io/design/issue/411">https://pagure.io/design/issue/411</a>
<a href="https://pagure.io/design/issue/412">https://pagure.io/design/issue/412</a>

Right now, I do not see this, i see an unhealthy split that alienates other projects, so, my question is this to FESCo candidates and Fedora KDE users, Spins users.

"What should Fedora do to encourage, engage and enhance Fedora's  projects and foster a healthy environment of developing the best Linux distribution for everyone to enjoy for productivity, gaming and more?"