---
title:   "The unfortunate events of last week"
date:    2006-03-29
authors:
  - antonio larrosa
slug:    unfortunate-events-last-week
---
The short version: My computer was broken and I haven't had mail since the 19th of March until today, sorry if you were waiting for me to answer some mail, I'll try to do so as soon as possible.

The long version: It all started in a sunny sunday morning...

<b>Sunday, 19th of March</b>

I was using the computer and suddently the display was frozen. I had to press the reset button, but the computer didn't boot up. Not even the BIOS appeared and the speaker made no sound... I opened it, and the chipset fan of the motherboard didn't move at all, which meant that probably the chipset burned due to the fan being stopped :(

On the afternoon, a friend lended me a motherboard to test if it was really a motherboard problem (thanks Alberto!). On monday afternoon I set up the new motherboard with the cpu and graphic card and I could see that the result was the same: no bios, no sound, nothing... so the conclusion was that my good old athlon cpu was broken, and I needed a new motherboard (since the fact that the fan was stopped probably meant that the chipset was broken anyway).

<b>Wednesday, 22nd of March</b>

On wednesday, after 3 days without computer at home (just using the laptop where I have no servers and specially, where I don't have kolab installed in order to read mail!)  I bought a new cpu, motherboard, graphic card and HD. The result is that on Friday I got all the components and could finally set up everything: an Athlon64 X2 3800+ with 1Gb RAM and a GeForce 6500 (the cheapest I could find without a fan). That's something that I guess is seldom seen in shops. I was looking for "the cheapest graphic card that worked in that motherboard and that had no fan at all". I've alway been an "anti-fan" guy, but after this incident, this habit has been reinforced. Now I just have three fans in my computer (the cpu one, the one on the power supply and the one on the case).

<b>Saturday, 25th of March</b>

I've been using SuSE for a lot of years (I think the first version I installed was SuSE 6.2 and since it worked so well for me, I didn't have a reason to change). But I've been wanting to try Debian for some time, so I've created three partitions, one for suse, one for debian and one for a shared /home. So far I've installed both but I'm only using Debian.

Well, on saturday morning I already had debian and suse installed but the pain had just started. It took me the whole weekend and monday evening to compile and get a working kolab installation from sources. I've compiled the whole kolab at least 7 times... all because it seems nobody ever compiled kolab in a 64bit system. Fortunately, I managed to make it work at the end (btw, using the development branch, kolab2.1). I've put some instructions here on <a href="http://www.eforum.de/viewtopic.php?t=487">how to compile kolab on a 64 bit system</a>.

Finally, today I've been able to make it work right, so I've finally been able to get the 4060 mails that were waiting for me since the 19th of March and I've also written the dot story about OSWC and aKademy-es that I promised Jonathan Riddell so long ago (sorry Jonathan).

Tomorrow morning I have to make a presentation at work  about a new design of an application, so I better go to sleep now and sleep at least 4 hours...

Number of tasks in my TODO list: 19


