---
title:   "Debugging the debugger."
date:    2007-12-19
authors:
  - sad eagle
slug:    debugging-debugger
---
I've spent some time recently trying to get Konqueror's JavaScript debugger in a shippable form for 4.0. The debugger was changed heavily from the 3.x version in a SoC project, making ground for much nicer UI. Unfortunately, it also barely worked. 

I've fixed up a chunk of the issues, but there is still a long way to go in fixing up some remaining holes, and doing testing to improve stability. But, it's at least good enough for a screenshot:
[image:3159]
... as well as some simple debugging tasks. Anyway, I am sort of curious: what do readers want from such a debugger? Now, I probably can't incorporate any non-minor suggestions in 4.0 (feature/string freeze), but it'd be good to know what direction to take, and some minor stuff is certainly tweakable.
<!--break-->

