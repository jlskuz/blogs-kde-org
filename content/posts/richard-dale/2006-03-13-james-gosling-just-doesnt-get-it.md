---
title:   "James Gosling just doesn't get it"
date:    2006-03-13
authors:
  - richard dale
slug:    james-gosling-just-doesnt-get-it
---
It really does seem that we're beginning to emerge from the 10 year long Java nuclear winter, when excellent dynamic languages such as Objective-C or Smalltalk were kicked out of the mainstream. I've nothing against Java, and have probably spent a couple of man years or so working on the Qt/KDE Java bindings as some kind of 'public service' combined with a sincere desire to attempt to kill the Swing GUI toolkit. But I could never get at all excited or 'passionate' about it - Java has always just seemed another programming language to me with some pretty serious design flaws, some terrible frameworks and apis (cf Calendar class, the reflection api, and ..umm Swing).
<!--break-->
Recently James Gosling made some <a href="http://java.sys-con.com/read/193146.htm">dismissive comments</a> about what he calls 'scripting languages'. It provoked several excellent responses, but my favourite one was from Ryan Tomayko, <a href="http://lesscode.org/2006/03/12/someone-tell-gosling/">Gosling Didn’t Get The Memo</a>. 

One of the things that makes Ruby great to me is the way it takes ideas from such a large range of languages, everything from Smalltalk and Lisp, to CMU and to Perl. Yuhihiro Matsumoto is amazing 'literate' with his grasp of a wide variety programming idioms and how to incorporate them into Ruby so they 'feel right', and have a degree of usability. 

In contrast what underwhelms me about the design of Java is how it feels like it was done by someone who never really programmed in Smalltalk or Objective-C, and so they just left out any sort of introspection, left out meta-classes, left out dynamic method redirection via #doesNotUnderstand, and left out open classes that you could add methods to via categories. It was obviously designed by someone who had tried C++, found it over-complicated and designed a simpler alternative with a runtime derived from the Pascal p-code interpreter. As a simpler C++, Java has been amazingly successful, but as a be-all and end-all of programming languages it falls a long, long way short. So here we are 10 years on from Java's launch and it seems dynamic languages are back, Ryan Tomayko says:

"This was not the case last year at this time, so what happened? Something must have changed. None of these technologies have underwent huge feature or stability increases in the past year. I’m unaware of any breakthrough in scaling these systems past what they’re already capable of. There have been some improvements in running dynamic languages on the mainstream VMs, which many predicted would lead to quick acceptance, but that’s not it either. So what changed?

Minds changed. Respectful debate, honesty, passion, and working systems created an environment that not even the most die-hard enterprise architect could ignore, no matter how buried in Java design patterns. Those who placed technical excellence and pragmaticism above religious attachment and vendor cronyism were easily convinced of the benefits that broadening their definition of acceptable technologies could bring."

Amen Ryan. Let's all carry on with the turkey shooting!