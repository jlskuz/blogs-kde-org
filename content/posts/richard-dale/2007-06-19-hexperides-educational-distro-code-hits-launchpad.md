---
title:   "Hexperides educational distro code hits launchpad"
date:    2007-06-19
authors:
  - richard dale
slug:    hexperides-educational-distro-code-hits-launchpad
---
<p>The mEDUXa Canary Islands schools Linux project was based on Free Software, and we've finally got round to setting up a community version of it that people can hack. You can read about it on <a href="http://dot.kde.org/1164320348/">this</a> KDE Dot News story and follow the link there for screen shots and more explanation. I'm quite excited by the prospects of Free Software in Education - it just seems inevitable that the Free Software Hacker Ethic will take over and change Education just as much as it has transformed the process of software development. Change education and you can change the World.</p>

<p>We've called it 'Hexperides' which is something Greek to  do with horses I believe. Agustin Benito originally wanted to call it pegasuX, which kind of looked ok until I started to set up an IRC channel and when I saw the name '#pegasux' I just thought 'oh sh*t', that one won't fly (not only have you got 'sux' in there, but 'pega' means 'snag' in Spanish, hmm). Anyhow, I've removed stuff like the artwork which was copyrighted by the Canarian government, and so we will need to create replacement versions of things like the Primary school desktop background that had icons sprinkled over it to start apps. I think it should be possible to do something similar with Plasma and a scaleable SVG background for KDE4. Hopefully that's something we can discuss at this years Akademy, and also look at the OLPC sugar interface for ideas.</p>

<p>There was a lot of work put into setting up five profiles via the KDE Kiosk mode with three types of teacher, and other profiles for primary and secondary school pupils. I'm pretty sure that would be generally useful. In particular the Primary school desktop show just how customizable KDE can be.</p>

<p>Further work went into improving the KDE Menu so it had more sub menus for various categories of educational software. And I think that should be useful for other Edu projects too. Each .desktop file needed to be customised with extra categories so that something like 'GCompris' wouldn't be just lumped together with an electronic circuit design app for those budding Carver Meads. I wrote a few little ruby scripts for doing various things with the .desktop and .menu files that could be adapted and improved.</p>

<p>Finally as schools projects tend to have large numbers of users, you need to to have a lot of advanced configuration to work with central file servers, and push updated packages out, use LDAP or Active Directory and so on. I think the hexperides configuration might be useful to other projects, but not so much as the two categories above.</p>

<p>There is a <a href="http://www.hexperides.org/hexperides/index.php/Portada">Hexperides wiki</a>, the IRC channel is #hexperides on Freenode and the <a href="https://launchpad.net/hexperides">lauchpad project</a> with the code in a bazaar repository.