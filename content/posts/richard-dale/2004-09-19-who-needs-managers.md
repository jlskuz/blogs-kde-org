---
title:   "Who needs managers?"
date:    2004-09-19
authors:
  - richard dale
slug:    who-needs-managers
---
Two of my favourite books about computer programmers are 'The Psychology of Computer  Programming' and 'Understanding the Professional Programmer' by Gerald Weinberg. So I was interested to read this <a HREF="http://www.ayeconference.com/Articles/SDManagerAdvice.html"> short interview</a> with him. 
<!--break-->
Until I started writing Free software in the last year or two, I've always worked for a manager of some sort and often wondered what it is they actually do. If you're self motivated, self trained, know which problems you're personally interested in solving and have suitable computer equipment (these days just a laptop and an internet connection is all you need), then what would you do with a manager. Suppose excellent managers were readily available on the net, and every KDE hacker could have they're own personal manager to direct them, what difference would it make? Would I have been better off focusing on java or C# bindings recently rather than ruby, because they're more popular? Should I have been concentrating on Total Quality Management and ISO 9000 compliance, instead of just trying to get stuff done? I couldn't even manage to think of something to put in the KDE 3.3 release plan, because well.. I didn't actually have a plan. I just hacked away on the ruby bindings and fixed problems as they arose. But the QtRuby/Korundum were reasonabily finished in time for the release, they work well and I've even been doing tutorials and docs recently.

So when I read the interview with Jerry Weinberg I wondered exactly what he meant by this:

Q: <i>If you were to publish a third edition of The Psychology of Computer Programming, what new insights would it include? (Dorset House Publishing released a Silver Anniversary Edition in 1998.)</i>

GW: <i>I might add something about how to make yourself so valuable that your work will never be outsourced -- something about the arrogance and overconfidence that has led to the loss of lots of software development jobs, not just to outsourcing, but to development work that's just not being done because the odds of success are so poor.</i>

Whose fault was this, was it the managers or the developers - who has screwed up? As far as I can see it must be the managers. They don't go telling you 'ignore my training suggestions, just take some time off paid work, and make yourself so valuable you won't be outsourced'. In fact they don't want you getting too valuable because you might just go and work for someone else. Annual appraisals are not about motivation, they're more about getting you to conform to the norms of the project. When managers talk about you becoming more 'business focussed', they actually mean less technically skillful. And so on. 

Paul Graham had a good description for 'curly brackets' languages such as java or C# recently, He described them as languages that 'enable losers to lose less' (he prefers lisp). I wonder if the real function of managers is also to 'enable losers to lose less'.

Q: <i>You once said, "If you can’t manage yourself, you have no business managing others." Could you elaborate on that? What does it mean to manage one's self?</i>

GW: <i>Well, perhaps you can look at Kipling's famous poem,"If." It starts:</i>

If you can keep your head when all about you
Are losing theirs and blaming it on you;
If you can trust yourself when all men doubt you,
But make allowance for their doubting too;
If you can wait and not be tired by waiting,
Or, being lied about, don't deal in lies,
Or, being hated, don't give way to hating,
And yet don't look too good, nor talk too wise;

That poem seems spot on to me. But I think maybe the question to ask is "If you can manage yourself, you have no business in having a manager"..