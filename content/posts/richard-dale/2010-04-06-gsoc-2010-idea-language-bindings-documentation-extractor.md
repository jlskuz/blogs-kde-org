---
title:   "GSOC 2010 Idea - Language Bindings Documentation Extractor"
date:    2010-04-06
authors:
  - richard dale
slug:    gsoc-2010-idea-language-bindings-documentation-extractor
---
<p>I've just added an idea for a Google Summer of Code project to the wiki; a <a href="http://community.kde.org/GSoC/2010/Ideas#Project:_Documentation_Extractor">Language Bindings Documentation Extractor/Generator</a> tool.</p>

<p>For last years GSOC Arno Rehn wrote a tool called 'smokegen' which parses Qt and KDE C++ header files and generates language independent 'Smoke' libraries that are used by several bindings projects for Ruby, C#, Perl, PHP and JavaScript. It is plugin based, and the main part of the documentation extractor project would be to write a new plugin that would parse both headers and sources and extract doc comments and code snippets. For each different language the plugin would translate the C++ docs to a format suitable for the target language. Any embedded code snippets should also be translated as far as possible. Possibly it might be better to have seperate plugins for each language. Part of the project might be to write some sort of documentation viewer tool if one didn't already exist.</p>

<p>The PyQt and PyKDE bindings already have auto generated docs, and so a good place to start might be to see what is involved in creating those. I asked Arno about the project and how much work it would be, and he thought the main thing that would need to be added, was to parse method definitions in the C++ source files, as the smokegen parser doesn't do that at the moment.</p>

<p>Unfortunately there isn't much time as the deadline for the submissions is on the 9th April, so you'll need to get started pretty quickly. Ask any questions on the #kde-bindings IRC channel or the kde-bindings@kde.org mailing list.</p>