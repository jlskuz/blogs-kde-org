---
title:   "Building Ruby on the N810"
date:    2008-08-20
authors:
  - richard dale
slug:    building-ruby-n810
---
I really enjoyed Akademy this year, and blagging a Nokia N810 right on the day of my birthday was one of the highlights of a great week. I'm very keen to port the QtRuby bindings to the N810 as it should make a nice development environment for quickly developing small free standing apps and Plasma applets. Instead of using Scratchbox, cross compiling on a laptop, and then downloading the binaries, I've been building on the device itself. I'll summarize what I've done so far to get my tablet set up with dual booting OSs and native compilation.

The first step is to obtain root access and flash the latest OS - I got instructions for how to do that from a good introductory article <a href="http://www.arachnoid.com/linux/nokia/">Hacking the Nokia N810</a>. Note that you can leave the device in production mode if you enable root by installing 'becomeroot'. Use the http://gronmayer.com/it site to add all the repositories to your Application Manager app. After my first attempt to install openssh I ended up setting a root password that I couldn't seem to type correctly again, in spite of typing it twice when I added it. So it is quite important to know how to re-flash with the flasher-3.0 tool as described on the <a href="http://maemo.org/community/wiki/HOWTO_FlashLatestNokiaImageWithLinux/">Maemo Wiki</a>. Download the latest OS .bin file, as well as the one that came on the tablet. There were a few improvements in the latest OS such as it now gets updates from the internet, and I've used that one. I installed bash2 as the busybox shell seemed to be lacking a few features which made it annoying to use (eg '!!' for do last command). I downloaded and built gnu core utilities and just installed 'ls' at first, so I could have coloured file listings. Later I installed the whole thing as perl wouldn't build without 'comm'. The tar that came with the machine didn't seem to work properly and I installed gnutar via a package. On FAT32, gnutar gives loads of permission errors, but it did seem to actually work.

Next I tried installing Ruby 1.8 and irb with apt-get as .deb packages, and that was nearly a good idea. Unfortunately irb doesn't come with the 'readline' extension which makes it a bit broken as you can't go back through your irb history. Also Ruby didn't have any headers so you couldn't build native extensions with it, and so I thought I'd attempt to build Ruby on the machine. I found instructions in <a href="http://www.internettablettalk.com/forums/showthread.php?t=6737"> HOWTO: C/C++ compiling on the unit itself</a> about what to do with the apparently broken packages, and I did actually end up with a working compiler for both C and C++. Note that you need to manually add sym links for gcc-3.4 to gcc and g++-3.4 to g++. Later I found the perl was expecting the compiler to be called 'cc', and I created another sym link for that too. It doesn't compile that fast compared with my laptop, but it isn't bad. Looking at 'top' when compiling on the Nokia, g++ takes about 100% of the CPU and 25% of the memory and so as long as you don't try to do anything else at the same time it works quite well. I installed make and cmake packages to go with the new compiler.

You need to install gnugrep before Ruby will configure, as the grep that comes with the busybox shell in the Nokia lacks features. The 2GB internal flash drive is formatted as FAT 32 and shell scripts with '#!/bin/sh' didn't work at first, so I added 'exec' to the line in the /etc/fstab for the mmc1 disk (normally the external one) and mounted it as that instead of mmc2. 

Another problem was that you can't create symbolic links on FAT file systems, which meant that building the Qt libs would be possible as they depend on it. The Ruby build expected to be able to create sym links to libruby.so from libruby-1.8.so, and so I worked round that by just copying the libraries. They were installed onto the root partition as proper sym links, and so it didn't make any difference to the final result.

To get the Ruby readline extension to build I needed to download and build both 'readline' and 'ncurses', which seems like an awful lot of code just to get history navigation working. In theory there are .deb packages, but in practice the dependencies were broken for a lot of them, and building them myself seemed the safest option.

Finally I downloaded and built the ruby gem code from Rubyforge, and was in a position to install and build native Ruby extensions. So I installed rails, mongrel etc which worked fine, except that generating the ri and rdoc documentation seems to take forever. Maybe Ruby runs really slowly on the N810 - I haven't done any benchmarks yet.

These are the .debs I installed to get the first root file system working well:
<code>
bash2_2.05b-1maemo2_armel.deb
bzip2_1.0.4-2maemo5_armel.deb
cmake_2.4.7-1mh1_armel.deb
cpp-3.4_3.4.4cs2005q3.2-5.osso8_armel.deb
g++-3.4_3.4.4cs2005q3.2-5.osso8_armel.deb
gcc-3.4_3.4.4cs2005q3.2-5.osso8_armel.deb
gnugrep_2.5.3_armel.deb
gnutar_1.20-1maemo4_armel.deb
libbz2-1.0_1.0.3-2_armel.deb
libc6-dev_2.5.0-1osso7_armel.deb
libc6_2.5.0-1osso7_armel.deb
libdb4.2_4.2.52+dfsg-2_armel.deb
libglade2-0_1%3a2.6.2-1indt1_armel.deb
libreadline4_4.3-10_armel.deb
libstdc++6-dev_3.4.4cs2005q3.2-5.osso8_armel.deb
libxml2_2.6.27.dfsg-1.maemo1_armel.deb
linux-kernel-headers_2.6.16.osso11-1_armel.deb
make_3.80-9osso_armel.deb
makedev_2.3.1-73osso-4_all.deb
wget_1.10.2-2osso2mg1_armel.deb
zlib1g-dev_1%3a1.2.3-9.osso8_armel.deb


</code>
<p>
</p>
<p>
At this point I decided I was fed up with FAT32 and non-functioning sym links, and found a couple of articles, <a href="http://www.thisweekinnuclear.com/KDEonN800.html">here</a> and <a href="http://www.the-little-things.net/blog/?p=15">here</a> about how to reformat the file system and install a second root partition to allow dual booting. If you do exactly what they say you do indeed end up with a dual boot machine, and you hold the menu key down at start up to get to boot options menu. I formatted the internal drive as 500 Mb of FAT32, and 1500 Mb of ext2 (for the second root partition), and that works great with a lot more free space. You can use the original not too hacked internal flash as a back up, which you do more adventurous things on the second root partition. If something goes wrong you can mount the original root partition as read only and copy it across in exactly the same way as the second root partition image was created in the first place.
</p>
I bought a 4Gb mini SD for 26 euros to put the Qt, KDE and other sources on to get KDE4 built, and put it in the tablet's external slot. I think it should be enough for kdelibs, kdebase and kdebindings at least. 

To configure Qt you need a proper working perl, and the one that comes with the Nokia isn't. You can't just add extra perl modules as the dependencies were broken again and so I built Perl 5.8.8, after installing all of gnu core utilities with 'comm' that perl needed. Install pkg-config otherwise Qt won't be able to find dbus. In fact I built the latest dbus, and found it needed the expat xml parser, which again didn't have a working built package.

I've got as far as building QtCore, QtNetworking and QtXml, but have hit a problem with an X-Windows header used by QApplication which I need to look into. I'm quite optimistic though, and think I will end up with a pretty serious KDE development environment that I can put in my pocket - in no way is it a toy! I hope that in the future we can put together a pre-built Qt/KDE solution that will save all messing around needed at the moment..