---
title:   "Who cares about document formats?"
date:    2007-09-11
authors:
  - richard dale
slug:    who-cares-about-document-formats
---
<p>I've loved reading the articles about whether or not the Microsoft OOXML document format should be an ISO standard, as opposed to the ODF ISO standard for word processing documents. In particular, Miguel de Icaza's heroic defence of his position against over 500 rabid anti-microsoft <a href="http://linux.slashdot.org/linux/07/09/10/2343256.shtml">Slashdot posters</a>. I admire someone who can think for themselves against entrenched  opposition (eg Richard Stallman or Miguel de Icaza), and I don't actually care whether or not I agree with them or not.</p>

<!--break-->

<p>But I wonder if people will really care much about these document formats in 50 years. It seems to be an extension of typewritten 20th century documents to me, when it used to be important whether or not this or that sentence appeared in bold/underline format. It doesn't have anything to do with semantics or machine readability of the document. One of my pet hates are word processors, and the culture of sending word processor documents as enclosures in emails, as opposed to putting them online on a wiki or similar, it just really seems out of date, and pisses me off. So why not base your document future on semantic web ideas like RDF, instead of these short lived presentation oriented formats? Instead of bold or italic styles we shouldn't we be more concerned about the foaf (Friend of a Friend) or other ontologies that the document contains?</p>



