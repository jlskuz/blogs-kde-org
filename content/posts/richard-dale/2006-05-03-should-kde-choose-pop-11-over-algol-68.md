---
title:   "Should KDE choose Pop-11 over Algol 68?"
date:    2006-05-03
authors:
  - richard dale
slug:    should-kde-choose-pop-11-over-algol-68
---
<p>
Some things never change, and the recent discussions about a possible 'new VB' for the Linux Desktop reminded me of <a href="http://www.cs.bham.ac.uk/research/cogaff/sloman.pop11.pdf">this excellent article</a> by Aaron Sloman (my philosphy/AI professor from 1976 to 78 while I was a Philosophy undergraduate at Sussex University). He describes the history of teaching Pop-11, and the reasons for choosing it. When he talks about Algol 68, it could just as easily be C# or C++, and when he talks about Pop-11, you can just substitute Ruby (or Python), and the arguments still make perfect sense. Here are some extracts..
</p>

<p>
<i>The most powerful language available on the Sussex Computing Centre machine at that time was Algol-68, and for a while we tried using it for teaching. However it was totally unsuitable for our students, most of whom were complete beginners and quite unused to mathematical formalisms or the kind of rigorous thinking required even to get a small Algol-68 program going</i>
</p>
Yeah, just like getting the C++ compiler to work with automake, or understanding the difference between references, and pointers..
<p>
<i>We did not want students to have to fight with impoverished languages or restricted programming environments, but our resources were very limited. At that time, typical introductory computing courses elsewhere either simply let students play with games or packages, or if they introduced programming they often used numerical programming problems, and impoverished or unfriendly languages like Basic or Pascal. LOGO was an attempt to break away from some of the restrictions of such languages, and for a time we seriously considered buying an Interdata computer and using the LOGO system developed by George Coulouris and colleagues at Queen Mary College London. In order to help me evaluate LOGO, Danny Bobrow arranged for me to have access to the MAXC computer at Xerox PARC (a micro-coded DEC-10 lookalike), Max Clowes allowed me to use his teletype and modem, and University College London accepted me as a user of their experimental transatlantic network service. I spent many hours sitting in Brighton typing in characters that went via a long round-about route to California and then were echoed back. What a way to use communications satellites!</i>
</p>
BASIC is 'impoverished and unfriendly' - surely not? :)
<p>
<i>We considered Lisp but decided its syntax was too impoverished for non-mathematical beginners. I still believe that a rich redundant syntax, although it requires more rules to be learnt, reduces the cognitive load on the user, as well as enabling the compiler to give more useful error messages if you make a mistake. Unfortunately, many designers of computer languages think only about more formal properties of the language, such as the parseability by computer, having a well defined semantics, and so on. Computer Scientists are not (or used not to be) concerned about cognitive processes in users of computer languages.
</i>
</p>
<p>
Take that Paul Graham! And bring on Matz, who designed Ruby based very much on usability and concerns about easing the cognitive processes of Ruby users.
</p>
<i>
We thought about Prolog, but felt that it was too specialised and would give beginners a distorted view of computation, in addition to presenting them with a number of difficult conceptual problems. E.g. why can't you have a rule saying that X is the brother of Y if X is male and Y is the brother of X? In logic there is nothing wrong with this. In most Prolog implementations it leads to infinite loops, as do other fairly natural constructs.
</i>
</p>
<p>
Seconded! Prolog is nice in small doses, but it isn't a good idea to try and use it as a general purpose programming language..
</p>
<i>Eventually, after much discussion and heart-searching, Steve, Max and I decided that POP2 had many advantages over all the alternative languages that were available, both for beginners and for their teachers. It also had some flaws, including the fact that the open stack, while supporting a number of elegant constructs, could lead to obscure bugs. But we felt that the advantages easily outweighed the disadvantages</i>
<p>
Similarly Ruby has those funny blocks which can sometimes lead to obscure bugs.
</p>
<p>
See the wikipedia entry for Pop-11 <a href="http://en.wikipedia.org/wiki/POP-11">here</a>. Here's a small Pop-11 sample from it:
<code>
define RemoveElementsMatching(Element, Source) -> Result;
        lvars Index;
        [ %
           for Index in Source do
              unless Index = Element or Index matches Element then
                 Index;
              endunless;
           endfor;
        % ] -> Result;
enddefine;

RemoveElementsMatching("the", [the cat sat on the mat]) =>      
;;; outputs [cat sat on mat]
RemoveElementsMatching("the", [[the cat] [sat on] the mat]) =>  
;;; outputs [[the cat] [sat on] mat]
RemoveElementsMatching([= cat], [[the cat] is a [big cat]]) =>  
;;; outputs [is a]
</code>
<p>
Lines are terminated by semi-colons, assignments go left to right, and you need to declare local variables. Simple, readable, and usable by non-techies - just like Ruby!