---
title:   "Akademy Qt5 QtQuick course and Nokia N9 fun"
date:    2012-07-04
authors:
  - richard dale
slug:    akademy-qt5-qtquick-course-and-nokia-n9-fun
---
I went to the day long course on Monday given by KDAB, about QtQuick for Qt5, and it was excellent. I had used Qt4 and QML for a Symbian phone project earlier this year, and the combination worked very well. 

The course started with the basics, and in most repects Qt5 QML is pretty similar to the Qt4 version. I learned a few things about the parts of QML that I hadn't tried like state machines and transitions. It got interesting at the end when Kevin Ottens explained how you could include OpenGL fragment and vertex shaders in QML. This <a href="http://labs.qt.nokia.com/2012/02/29/pimp-my-video-shader-effects-and-multimedia/">Pimp my video: shader effects and multimedia</a> blog on Qt Labs show some sample code and a video of what it looks like running on a phone. My personal OpenGL foo isn't up to writing my own shaders, but there is a pretty complete library of canned effects, such as drop shadows, that comes with Qt5.

At lunch time, Quim Gil announced that all the registered attendees would receive Nokia N9 phones, and when we got them it felt like Christmas had come early this year for me. It really is a very fine phone. The UI is polished with plenty of apps, and the industrial design is slick. I tried the SIM from my ancient dumb Nokia phone and unfortunately it was too big to fit. I will have to get the data transferred to another card, or someone told me that it was possible to cut down an old large SIM to make it smaller. I am certainly looking forward to try it out as a phone.

Today I've been getting the N9 working as a development device. It is quite straightforward and you just need to activate 'developer mode' via an option on the Security settings panel. Then you configure Qt Creator with SSH keys to connect via the N9's USB cable (or WLAN works too) and you're done. I got a Hello World app working, and then tried to port the large Symbian app to the N9. The port was a bit more trouble than I was expecting due to a problem with upper case characters in the app name, and other problems related to doing development under Mac OS X. I had developed the Symbian app under Linux and Windows, and I think it would be best to give up on Mac OS X for the N9 and use Linux instead.

I ssh'd onto the N9 and fished around a bit to see what was there. It has perfectly standard Debian packaging, and when you build an app in Qt Creator a .deb is transferred to /tmp and installed from there. The root partition has 2GB free to install apps which should be plenty and there was another partition with 7GB free for pictures, documents, maps and so on.

I did some work on the QSparql library that is used for interfacing Qt apps with the Tracker Nepomuk store, and it was nice to see that the lib came with the phone. But it didn't come with the driver for accessing SPARQL endpoints though, and I couldn't find that driver with an 'apt-cache search' query. I might have to build and install it myself.

I noticed that none of the apps that were pre-installed used QML, and they used the obsolete MeegoTouch libs instead. I asked about installing Qt5 on the N9 at the QtQuick course and apparently it is quite straightforward.

I can confirm what other people have said about the N9, and how great the UI and phyisical design is, with a very nice development environment. I also thought Qt/QML on Symbian works really well. So I feel puzzled by Stephen Elop's unsuccessful Windows Phone 7 strategy. If WP7 isn't taking off now, I can't see how WP8 based on the Windows NT kernel is going to be any more successful. I can't imagine why anyone would want to buy a WP7 device when Microsoft announced the other week that there would be no forward path for existing WP7 phones to run WP8.

I saw Aaron Seigo and Sebastian Kugler give a couple of great presentations about Plasma Active at the Akademy conference last weekend. The highlight was Aaron laying into so called 'Tech Pundits' who said that Plasma Active can't possibly compete with Android. Aaron pointed out that actually Android had Plasma Active had nothing much in common, and the clueless pundits were doing something like criticizing an Italian meal at a restaurant for not being a French meal. An activity based tablet is made of very different stuff to the conventional 'Bucketful of Apps' approach used by Android and iOS. By not competing with that approach head on, and having a very lean operation they can construct a viable business even with relatively small sales.

If the Nepomuk store in Plasma Active can be used as a basis for the activity based app integration then so could the similar (although less powerful) Tracker store in the N9. That is another powerful capability of the N9 that has yet to be exploited by 3rd party apps. The N9 Tracker app data integration is much more powerful than the Windows Phone 7 hubs and active tiles. Perhaps some kind of nice notification center is perhaps the main N9 feature that could be improved as far as I can see.

Oh well, I just hope that Nokia will be able to sort themselves out, and recover from the train wreck that they appear to be at present. And thanks again guys for the N9.