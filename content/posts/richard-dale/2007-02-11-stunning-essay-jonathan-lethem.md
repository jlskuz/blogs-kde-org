---
title:   "Stunning essay by Jonathan Lethem"
date:    2007-02-11
authors:
  - richard dale
slug:    stunning-essay-jonathan-lethem
---
<p>Via slashdot I read <a href="http://www.harpers.org/TheEcstasyOfInfluence.html">this essay</a> by Jonathan Lethem. It is a stunning, jaw dropping piece of work. I don't know what to say to add to it.</p>
<p>
I love vintage country music and he even mentions the similarity between Kitty Well's “It Wasn't God Who Made Honky Tonk Angels