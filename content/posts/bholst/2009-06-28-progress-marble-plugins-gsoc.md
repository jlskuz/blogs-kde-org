---
title:   "Progress on Marble Plugins (GSoC)"
date:    2009-06-28
authors:
  - bholst
slug:    progress-marble-plugins-gsoc
---
Here is a short update about Marble Plugins. Let's have a look at the weather plugin. At the moment it can display the weather condition as well as the temperature, but as the backend already holds all the other weather information it's easy to present more. Every weather stations also got a priority, which results into the displayed stations being nicely selected.

<a href="http://oculusaquilae.de/gsoc/blog/weather_in_sh.png" target="_BLANK"><img src="http://oculusaquilae.de/gsoc/blog/weather_in_sh_small.png" alt="Marble weather" align="center"/></a>

Another thing I worked on is the plugin dialog, which now has buttons for configuration and about dialogs (if available). As you see, only a few plugins have such dialogs, but this will change soon. By the way: The wikipedia plugin got photo support!

<a href="http://oculusaquilae.de/gsoc/blog/wikipedia_configure.png" target="_BLANK"><img src="http://oculusaquilae.de/gsoc/blog/wikipedia_configure_small.png" alt="Marble config dialog"  align="center"/></a>

The Photo and the wikipedia plugin shows the corresponding website with a click on the photo/icon. This is the only feature which found it's way into KDE 4.3. For the above features you have to install trunk (soon) or wait for KDE 4.4

<a href="http://oculusaquilae.de/gsoc/blog/photos.png" target="_BLANK"><img src="http://oculusaquilae.de/gsoc/blog/photos_small.png" alt="Marble photos"  align="center"/></a>