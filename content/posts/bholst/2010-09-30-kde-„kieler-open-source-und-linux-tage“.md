---
title:   "KDE at the „Kieler Open Source und Linux Tage“"
date:    2010-09-30
authors:
  - bholst
slug:    kde-„kieler-open-source-und-linux-tage“
---
I just got the stuff we will need for our booth at the “<a href="http://www.kieler-linuxtage.de" target="_BLANK">Kieler Open Source und Linuxtage</a>”.
The “Kieler Open Source and Linux Tage” will take place on the 1st and 2nd of October at the “Kitz” in Kiel.

If you want to meet up with some of your local KDE fellows, visit us there. There will also be two KDE related talks.
Julian Bäume is going to give a talk about Kontact and Kolab and I will tell you something about Geolocation with Marble.

