---
title:   "OpenCycleMap server changed - don't forget to update"
date:    2010-10-14
authors:
  - bholst
slug:    opencyclemap-server-changed-dont-forget-update
---
<a href="https://blogs.kde.org/files/images/marble_opencyclemap_update1.png target="_BLANK"><img src="https://blogs.kde.org/files/images/marble_opencyclemap_update1.preview.png" /></a>
As you probably know, you can download a lot of additional Marble maps with the “Get hot new stuff” framework. The reason for this entry is that OpenCycleMap is now using a different server for their map storage. For all users of the OpenCycleMap in Marble it means that they will have to update their map configuration if they want to see updated maps.

Fortunately updating maps is pretty easy in Marble thanks to the “Get hot new stuff” framework. Just click on “File->Download Maps...” and look for OpenCycleMap. It should have a little button “Update” next to it. A click on it updates your files automatically.

If you want to update your files manually or if you use the Qt-only version of Marble, just download the new file from <a href="http://edu.kde.org/marble/maps.php" target="_BLANK">the map download page</a>.