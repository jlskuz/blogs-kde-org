---
title:   "Parley Picks up Pace"
date:    2008-04-10
authors:
  - frederik gladhorn
slug:    parley-picks-pace
---
A week ago I discovered a feature so obvious in that plasma thingy, that I just need to share it.
You can put your favorite vocabulary trainer into your favorite panel. You knew it, I bet. Do it 8)
Proof:
[image:3387]
I even poked at it a little so it only takes up a quarter of the panel instead of half of it.
Now I already spent more time on this (I just wanted to see how hard it would be to create a plasmoid... but throwing it away seemed like a waste as well...).
So if you've been looking for something fun and small like improving Parloids, let me know!
Opening files (yes, this includes Parley, KWordQuiz, Kanagram and KHangman ones) should work now.

What is missing (in the config of the applet mostly) is a selection of which languages to display in which order.
Also at CeBit I got the suggestion to let the solution show after a certain timeout automatically (right now it is revealed when the mouse is over the Parloid) to enable even more relaxed passive learning.
Also a first summer of code patch for Parley arrived a bit early today :)
<!--break-->