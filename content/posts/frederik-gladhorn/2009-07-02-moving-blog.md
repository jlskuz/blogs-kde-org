---
title:   "moving blog"
date:    2009-07-02
authors:
  - frederik gladhorn
slug:    moving-blog
---
Just in case anyone is following me on kdedevelopers.org, I moved my blog to <a href="http://blogs.fsfe.org/gladhorn">http://blogs.fsfe.org/gladhorn</a>.