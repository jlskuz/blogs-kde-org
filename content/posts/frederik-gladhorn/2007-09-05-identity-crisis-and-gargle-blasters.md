---
title:   "Identity crisis and Gargle Blasters"
date:    2007-09-05
authors:
  - frederik gladhorn
slug:    identity-crisis-and-gargle-blasters
---
Will it be a <a href="http://en.wikipedia.org/wiki/Gargle_Blaster">GargleBlaster</a>?
(the effect of a Pan Galactic Gargle Blaster is like having your brains smashed out by a slice of lemon wrapped round a large gold brick) Well maybe not. And in the end WORDINATOR gets killed by terminator.
All of these and many more were supposed to be new names for KVocTrain. Some less serious.
But the (KVoc)Train will not roll on for ever.
This is a call for new name suggestions!
Come up with a great new name for the new KVocTrain!
Please add your favorite name for a vocabulary training program to the comments section or visit in #kde-edu!