---
title:   "SoC: Welcome David!"
date:    2008-04-25
authors:
  - frederik gladhorn
slug:    soc-welcome-david
---
David Capel will work on Parley during the Summer of Code.
I'll let him speak for himself:

Hello, I'm David Capel, an 18-year-old from Minnesota in the USA. I'm
going to be a freshman at the University of Wisconsin at Madison this
fall and will likely major in computer science of some sort. For my
summer of code project I'm rewriting Parley's practice interface so
that it uses SVG themes and will be easy to extend in the future, and
I will likely create a few new practice modes along the way (if anyone
has ideas or requests, feel free to email me). Finally, I'm looking
forward to joining the KDE community. :)
<!--break-->