---
title:   "Hobby und Elektronik"
date:    2008-11-15
authors:
  - frederik gladhorn
slug:    hobby-und-elektronik
---
I'll be giving a KDE 4 talk at the Hobby and Electronics fair in Stuttgart tomorrow (Nov, 16th). If you were planing to drop by the fair, come on over at 15:30. You probably won't learn a lot about KDE since I intend to prepare for a rather broad non-technical audience. Let me know if you happen to be in Stuttgart and want to have a coffee or just chat :)
