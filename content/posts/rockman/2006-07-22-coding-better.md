---
title:   "Coding is better.."
date:    2006-07-22
authors:
  - rockman
slug:    coding-better
---
..than studying!
At least, this is what i learned today.. the second exam finished, and i must admit, my way of studying really sucks.
I'd really need a <u><b>real</b></u> job, something really hard to find here..
Anyway, while travelling from university to my city, i wrote a template for a new SerialDevice class, dcode will help me porting it to kde4.
I'm also quite impressed by the increasing number of people interesting in <a href="http://www.kmobiletools.org" target="_BLANK">KMobileTools</a>. My <a href="http://blogs.kde.org/node/2177" target="_BLANK">last blog post</a> brought some fresh new coders, one of them already fixed a bug for Siemens mobile phones. And i'm also really happy to be co-mentor with Stephan Kulow of one of the <a href="http://blogs.kde.org/node/2177" target="_BLANK">Season of KDE 2006</a> projects.
There's still a huge amount of work to do, anyway, so if you feel you can help, even with only small contributions, please contact me.
In the meantime, i'm still thinking about my serial class.. i'm still a bit undecided about how to do threading.. currently we're using ThreadWeaver, a good library, but maintained only for KDE4.
There're a couple of other applications using theyr internal threadweaver modified version, but i'm wondering if it's better to not thread jobs, but only serial-port access.
Which means, anyway, that other engines (like gammu) should develop theyr own threading model.
Perhaps it's better to do some incremental changes.. first implementing a good and clear serialdevice class. Then i'll look around to see what's the best solution. Also now i've some new "heads" for asking ideas ;)
<!--break-->