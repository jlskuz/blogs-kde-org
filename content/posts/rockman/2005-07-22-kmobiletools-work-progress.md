---
title:   "KMobileTools - work in progress"
date:    2005-07-22
authors:
  - rockman
slug:    kmobiletools-work-progress
---
After finishing (well.. kind of) university exams, i'm again working at kmobiletools.
What i'm trying to do is to exit the alpha stage by reprojecting everything, and to provide a complete mobile phone suite, wich can use kaddressbook && kitchensync for addressbook syncing.

What i've done until now is:
<ul>
<LI>total KParts based architecture, so that for istance we can <a href="http://www.kmobiletools.org/kmt/20060422-16-KMobileTools-Kontact.png">integrate with kontact</a> or anything else, without problems</li>
<li>Plugin-like engines, so we can load a device not only with our code, but also using gammu or any other backend (including scripting)</li>
<li><a href="http://www.kmobiletools.org/kmt/">A kool interface</a> (well, why not? this isn't kde? :) ), even if it would be better with a nicer icon and some artwork.. :)</li>
</li>More use of threading, every interaction with the serial devices now is totally transparent (well.. bochi isn't exacly happy using threads, but nobody's perfect :D)</li>
</ul>

With the lastest commit i added the ability to have <a href="http://kmobiletools.berlios.de/kmt/20050720-Multiple Numbers.png">multiple numbers</a> for each contact, decoding this behaviour from motorola and sony ericsson mobile phones.

The only thing i hope now is to have TIME TIME TIME :(

p.s.: more screenshots <a href="http://www.kmobiletools.org/kmt/">here</a>.
p.p.s.: also a bit more time would be really appreciated ;)