---
title:   "Crossfading for GStreamer"
date:    2004-09-03
authors:
  - markey
slug:    crossfading-gstreamer
---
I'm Happy,

..because it's finally working: Crossfading for amaroK's GStreamer-engine. Two weeks of deep hacking, no less, to get this beast stable and smooth. Now we have an implementation of a <i>n-track</i> crossfader design, with unlimited number of channels for mixing, and configurable transition time. Each input pipeline is running in it's own thread, connected to an output pipeline, which is also a dedicated thread with its own scheduler.

The multithreading is really needed to make the transition smooth and prevent dropouts, but it also made the implementation difficult. GStreamer is very flexible - it lets you do pretty much anything you want, but to make it do exciting stuff a little aside the standard path, you need to get a good grip on the internals. Crossfading was especially difficult since this hasn't been done before with gst, so there was no example code available. This meant a lot of hack/crash/grok/fix cycles for me, but finally it works pretty well, and it sure gives me a warm feeling. 

On a related note, amaroK has born a new engine baby; it is called <i>MAS</i> - the engine number 5. Check it out while fresh =)
