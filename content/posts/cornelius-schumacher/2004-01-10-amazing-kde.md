---
title:   "Amazing KDE"
date:    2004-01-10
authors:
  - cornelius schumacher
slug:    amazing-kde
---
This week brought a couple of new exciting things to KDE. Having a closer look at what was announced these days I'm pretty amazed.

It started with <a href="http://kde-apps.org">kde-apps.org</a> on Monday, the followup to the highly popular <a href="http://kde-look.org">kde-look.org</a>. On Tuesday we got <a href="http://www.csh.rit.edu/~benjamin/about/phpquickgallery/photo.php?gallery=KDE/&file=qt-mac-kdepim.png">KDEPIM on Mac</a>. Wednesday brought integration of <a href="http://kde.openoffice.org/nwf/index.html">native KDE widgets in OpenOffice</a>. On Thursday we saw a nice interview about <a href="http://dot.kde.org/1073599985/">Qt styles for Gtk apps</a> and the announcement of <a href="http://lists.kde.org/?l=kde-core-devel&m=107359899930412&w=2">KDE ioslaves for FUSE</a> which basically means that they are now accessible by any non-KDE program, OpenOffice only being one of them. On Friday Zack released his <a href="http://dot.kde.org/1073668213/">QtGtk library</a> which allows to use all the fancy stuff of KDE like the file dialogs or DCOP in Gtk apps. KDE seems to become <i>the</i> integrative desktop. That's good news. Let's see what will happen on the weekend...

You could ask: Why is this happening right now? I personally think because of three reasons: First, it's time. Second, the freeze for KDE 3.2 begins to bore some people and third, the acquisition of SUSE by Novell seems to be some motivation for KDE (You could also see it as a kick in the butt).

Anyway, it's fun being part of this community :-)
