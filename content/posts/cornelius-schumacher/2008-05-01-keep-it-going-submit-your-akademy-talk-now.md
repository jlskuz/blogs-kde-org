---
title:   "Keep it going, submit your Akademy talk now!"
date:    2008-05-01
authors:
  - cornelius schumacher
slug:    keep-it-going-submit-your-akademy-talk-now
---
Yesterday was a busy day on the akademy-talks mailing list. Proposals were rolling in constantly. This is because today is the deadline for submissions of presentations for <a href="http://conference2008.kde.org">Akademy 2008</a>. So you still have a chance. Have a look at the <a href="http://conference2008.kde.org/conference/cfp.php">Call for Presentations</a> and submit your talk now.

There are so many interesting topics we would like to hear about at Akademy:
<ul>
<li>You have ported your application to KDE 4? Tell about your experience.</li>
<li>You run KDE on one of the fancy small devices, be it an Internet tablet, a phone or a tiny laptop? Show us how this works.</li>
<li>You are working on one of the pillars of KDE 4? Tell us how to make use of them.</li>
<li>You are working on a distribution which includes KDE? Present to us what made your life hard and what made it wonderful.</li>
<li>You were a GSoC student last year and are still with the project? Let us know what you have done.</li>
<li>You have written a cool Pasmoid or a rocking Akonadi agent? Submit your talk now. We are also accepting lightning talks, if you feel like five or ten minutes are enough to present your work.</li>
<li>You are working in the community as a non-coder? Tell us about what else than writing code is important for KDE.</li>
<li>You are using KDE in your business? Share your experience.</li>
<li>You are working on a related Free Software project? Give us ideas how to collaborate.</li>
<li>You are doing something completely different which is related to KDE? Submit your talk now.</li>
</ul>

I'm looking forward to another wave of exciting talk proposals. Keep it going.
<!--break-->
