---
title:   "Wow!"
date:    2008-05-18
authors:
  - cornelius schumacher
slug:    wow
---
[image:3467 align=left hspace=16 vspace=5 size="original"]

<a href="http://pim.kde.org/akonadi">Akonadi</a> is hot. I completely realized that when I saw the <a href="http://techbase.kde.org/index.php?title=Projects/PIM/Akonadi/Logo#Nuno_Pinheiro_.26_Thomas_M.C3.B6nicke">fantastic submission by Nuno and Thomas</a> for the <a href="http://techbase.kde.org/index.php?title=Projects/PIM/Akonadi/Logo">Akonadi logo contest</a>. This logo captures the essence of the Akonadi architecture in a very beautiful way. I remember well when I drew the <a href="http://pim.kde.org/development/meetings/osnabrueck4/diagram.jpg">first version</a> of the Akonadi architecture on the whiteboard at the <a href="http://pim.kde.org/development/meetings/osnabrueck4/overview.php">Osnabr&uuml;ck 4</a> meeting more than two years ago. The round shapes made it hard to put it in digital form, though. So after taking a tour through the drawing applications of the free software world without lasting success, I decided to write a <a href="http://www.lst.de/~cs/archibald/">program</a> to create the diagram. When I saw the Akonadi logo contest and some of the submissions there, I thought it would be great to have a three dimensional version of the architecture diagram, and now Nuno and Thomas just did that. Wonderful!


But why is Akonadi hot? I think the main reason is that it's living the KDE 4 vision. It's the condensed experience of ten years of KDE PIM development put into a beautiful architecture. It has portability built in by the platform-independent central storage and the toolkit-neutral access protocol. The <a href="http://websvn.kde.org/trunk/kdesupport/akonadi/">Akonadi backend</a> has recently been moved to kdesupport to be usable by non-KDE projects as well. Finally Akonadi really implements the promise of KDE 3 in terms of functionality of a central PIM storage which is conveniently avaliable all over the desktop.

Akonadi somehow is the missing pillar of KDE 4. With <a href="http://techbase.kde.org/index.php?title=Schedules/KDE4/4.1_Release_Schedule">KDE 4.1</a> this gap will be filled and the Akonadi platform will be available for development of the next generation of PIM applications to advance free software on the desktop one more step in direction to world domination. I'm looking forward to this. Really.
<!--break-->
