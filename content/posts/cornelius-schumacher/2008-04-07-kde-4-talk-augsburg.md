---
title:   "KDE 4 Talk at Augsburg"
date:    2008-04-07
authors:
  - cornelius schumacher
slug:    kde-4-talk-augsburg
---
Last weekend I visited the <a href="http://www.luga.de/Aktionen/LIT-2008/">seventh Linux info day at Augsburg</a>, organized by the <a href="http://www.luga.de/">local Linux User Group</a> and held a talk about KDE 4. It was a nice event and the talk was well received. There is a lot of interest in KDE and people are generally excited and looking forward what we will bring to them with the KDE 4 series. If you are interested have a look at the <a href="http://www.luga.de/Angebote/Vortraege/KDE4">slides of the KDE 4 talk (in German)</a>.
