---
title:   "Blog moved"
date:    2009-07-02
authors:
  - cornelius schumacher
slug:    blog-moved
---
I finally decided to move my blog. <a href="https://blogs.kde.org/blog/54">kdedevelopers.org</a> has served me well, but now I want some more features. <a href="http://blogger.com">Blogger</a> provides some killer features, such as using my own domain, blogging by email, or the powerful comment system. So from now on you'll find my blog at <a href="http://blog.cornelius-schumacher.de">blog.cornelius-schumacher.de</a>. See you there.