---
title:   "Admitting defeat"
date:    2003-11-08
authors:
  - cornelius schumacher
slug:    admitting-defeat
---
Some time before KDE 3.0 David Jarvie, the author of KAlarm, asked me some questions about the KOrganizer alarm daemon. I answered that it would be a great thing, if KOrganizer and KAlarm could share the same daemon, because I thought that by eliminating redundancies development would become easier and we could use our development resources more efficient. So we imported KAlarm into the KDE CVS and David did a great job implementing the code needed for sharing the daemon. The result was that we had a new application and the shared daemon in KDE 3.0. So far so good...

For 3.2 I'm now back to an own alarm daemon for KOrganizer. I had always propagated that the shared daemon would be the right thing, so that's admitting defeat.

Why did I go back to the own daemon?

When looking into adding the alarm daemon functionality to KOrganizer/Embedded I decided to write a new simple daemon for this purpose because the existing daemon was hard to port (mainly because it required DCOP and did some complicated things for the automatic start on login) and I realized that writing a new daemon was less work than porting the old one to Qtopia. 

With introduction of the KResources framework the old daemon stopped working and when trying to fix this, I got lost in the alarm daemon code. It had become a quite complex object because all of the specialities required for the sharing of the daemon by KAlarm, KOrganizer and whatever hypothetical application would also use it. Fortunately I remembered the simple alarm daemon of KOrganizer/Embedded and using the KResources framework it became even simpler. Using this really simple alarm daemon the alarm functionality in KOrganizer was fixed in no time and a lot of code became obsolete. The new daemon might even be simpler than only the code needed for communication with the old daemon.

Apparently I was wrong with assuming that a shared daemon would make something easier. Maybe the alarm daemon functionality is too simple for sharing of it having the desired benefit. But at least this course of action brought us KAlarm which certainly is a good thing, so the effort wasn't completely lost.

Conclusion: I once more learned that sometimes it's better to give up ideas when reality has the better arguments and that you should become suspicious when replacing code by something with "simple" in its name without removing the old code. Oh, and I hope that now I can't be blamed anymore for coolo being late for lunch.