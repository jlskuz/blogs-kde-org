---
title:   "KDE 4 Fun"
date:    2007-10-19
authors:
  - cornelius schumacher
slug:    kde-4-fun
---
Hacking on KDE 4 is fun. There is so much goodness in the development platform.

This begins with all the nice stuff Qt brings, little things like the addictive <a href="http://doc.trolltech.com/4.3/containers.html#the-foreach-keyword">foreach</a>, the beautiful API, or the amazing performance, and big things like the rich-text system <a href="http://doc.trolltech.com/4.3/richtext.html">Scribe</a> or the model-view framework <a href="http://doc.trolltech.com/4.3/model-view-programming.html">Interview</a>. One of my favorites are the <a href="http://doc.trolltech.com/4.3/qmainwindow.html#qt-main-window-framework">dockable toolbars</a>. I could move them around all day and watch the smooth sliding in effects.

[image:3052 width="640" height="360" hspace=100]

But it doesn't stop at Qt. The KDE platform adds so much useful stuff that it's hard to imagine why anyone still would want to write pure Qt applications. For example I'm deeply impressed by the <a href="http://api.kde.org/4.0-api/kdelibs-apidocs/kdecore/html/classKDateTime.html">timezone handling classes</a> David Jarvie has written for KDE 4. They do an incredibly thorough job at addressing the problem of timezones and come with extensive documentation. There is not much left to be wished for if you have to work with timezones. Another thing which make me smile all the time when debugging code is the new incarnation of kDebug, now with sane class names, magically included function names in the debug output and no need to put countless endls or formatting spaces in the code.

Running KDE 4 applications is fun as well. Startup time is now at a level which sometimes made me wonder, if I had correctly closed the app before. I haven't analyzed the reasons for that, but I guess it's due to all the nice
optimizations in Qt and things like the <a href="http://techbase.kde.org/Projects/Summer_of_Code/2007/Projects/Icon_Cache">icon cache</a> which was written during the Google Summer of Code. KDE 4 applications also run flawlessly on a KDE 3 desktop (or any other desktop for that matter). So if you are not ready for Plasma yet you can still get a part of the KDE 4 experience, on your very own desktop, right now, for ultimate pleasure.

Hm, seems I'm getting carried away, and I even haven't mentioned <a href="http://www.oxygen-icons.org/">Oxygen</a> yet. Ok, there are still lots of bugs and some unfinished corners, so I will get back to hacking now.
<!--break-->
