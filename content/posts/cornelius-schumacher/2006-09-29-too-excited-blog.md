---
title:   "Too excited to blog"
date:    2006-09-29
authors:
  - cornelius schumacher
slug:    too-excited-blog
---
In two hours <a href="http://conference2006.kde.org">aKademy</a> will be over for me. I'm going to head back for Germany. The last week was truly awsome. I had planned to blog a bit, but there was so much exciting stuff happening, that I didn't find any time to actually do so. So here are some of my personal highlights of aKademy, all in one.

[image:2407 size=original hspace=80]

In the KDE PIM space we had a BoF where we mainly talked about <a href="http://pim.kde.org/akonadi">Akonadi</a> and there were also some more discussions and coding going on. One of the most interesting aspects was <a href="http://nepomuk.semanticdesktop.org">NEPOMUK</a>, a research project funded by the European Union which was presented in a BoF by <a href="http://people.kde.nl/trueg.html">Sebastian</a>. It comes under the label "Social Semantic Desktop" and has a pretty broad scope. But for KDE it means that we might finally get a way to store relations between desktop objects, like mails, files, addresses, labels etc. and make it possible for the user to capitalise on this information. So the user could get a list of all objects related to a specific person, see where a file was downloaded from or attach tags to mails and all other kind of objects. Sebastian is paid to work on this project, so it's very likely that we will see some results not too far from now.

[image:2405 size=original hspace=80]

Yesterday we had a round of lightning talks with some very interesting contributions, <a href="https://blogs.kde.org/blog/551">Tackat</a> showed his globe viewer program, <a href="http://www.planetmono.org/">segfault</a> presented his KDE modeler for virtual LEGO construction and Jos demonstrated the "Strigi-powered search" for the <a href="http://blogs.kde.org/node/2402">group photo</a> of the attendants of this year's aKademy. It's a great photo, by the way, accurately reflecting the greatness of the KDE community. There was more and today will be another round of lightning talks. Don't miss it. It's fun.

[image:2406 size=original hspace=80]

Dublin was a great place for aKademy. The local organization was close to perfect, thanks to Marcus and his team. Trinity College was a very productive environment and we also had some enjoyable evenings involving Guinness. In the end I only can say, that I'm happy now.
<!--break-->