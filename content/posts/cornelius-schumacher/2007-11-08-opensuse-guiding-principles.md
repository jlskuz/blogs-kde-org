---
title:   "openSUSE Guiding Principles"
date:    2007-11-08
authors:
  - cornelius schumacher
slug:    opensuse-guiding-principles
---
Today the final version of the <a href="http://en.opensuse.org/Guiding_Principles">openSUSE Guiding Principles</a> has been <a href="http://news.opensuse.org/?p=509">announced</a>. The Guiding Principles describe what drives the openSUSE project, our identity, our goals and our values.

It has been a long process to create this document. It was <a href="http://lists4.opensuse.org/opensuse-project/2007-05/msg00208.html">discussed broadly</a> in the community as well as internally at SUSE and Novell. But I'm pretty confident that we have reached a version now which has a very broad acceptance. If you look at the <a href="http://news.opensuse.org/?p=509">initial announcement</a> you will notice that it's signed by community members from inside and outside of Novell including Novell top management. The open <a href="https://users.opensuse.org/">openSUSE user directory</a> holds an extended and constantly growing list of additional supporters. You can go there, express your support for openSUSE and its Guiding Principles. Don't hesitate, become part of this great community.

Together with the Guiding Principles the first <a href="http://news.opensuse.org/?p=498">openSUSE Board of Maintainers</a> has been announced. These group of well-known openSUSE contributors will have the task to channel communication and provide leadership to the project. I'm sure this will help to maintain and hopefully even improve the openness and transparency of the project.

openSUSE as open Linux distribution has a lot of momentum these days. It's incredible to see how much support it gets and all the <a href="http://build.opensuse.org">exciting</a> <a href="http://blogs.kde.org/node/2932">things</a> which happen around it. I'm happy and proud to be part of this community.
<!--break-->
