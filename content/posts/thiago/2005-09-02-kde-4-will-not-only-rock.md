---
title:   "KDE 4 will not only rock..."
date:    2005-09-02
authors:
  - thiago
slug:    kde-4-will-not-only-rock
---
Roberto Cappuccio wrote:

<ul>
After lunch, Zack Rusin, another vegetarian of the KDE community, opened his Powerbook and performed, for our eyes only, the presentation of his improvements to KDE's graphical environment, which did not take place some days ago due to technical problems. It was simply stunning. KDE 4 will rock!
</ul>

However, it's more likely that KDE 4 will also wobble, and turn and shift, etc. :-)
<!--break-->