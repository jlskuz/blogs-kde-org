---
title:   "QtDBus included in Qt 4.2 TP 1"
date:    2006-06-30
authors:
  - thiago
slug:    qtdbus-included-qt-42-tp-1
---
This morning, Trolltech <a href="http://www.trolltech.com/company/newsroom/announcements/press.2006-06-26.0683224314/">released</a> the <a href="http://doc.trolltech.com/4.2/">Technical Preview 1 for Qt 4.2</a>. Along with the many long-awaited features, you can even find the <a href="http://doc.trolltech.com/4.2/qtdbus.html">QtDBus module</a> as part of the standard release.

Yes, yes, I know they're mad :-)

I am sorry I had to keep the suspense of whether it was going to be included in the release.

IMO, this is quite a support for <a href="http://www.freedesktop.org/wiki/Software/dbus">D-BUS</a>. I believe that D-BUS will come to play a big role in the future of the Linux/Unix desktop, which is why I asked and was granted permission to work on it.

So, there it is. I await your feedback.
<!--break-->