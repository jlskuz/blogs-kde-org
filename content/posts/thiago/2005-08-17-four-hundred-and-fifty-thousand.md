---
title:   "four hundred and fifty thousand"
date:    2005-08-17
authors:
  - thiago
slug:    four-hundred-and-fifty-thousand
---
A short while ago, the Subversion <a href="http://lists.kde.org/?l=kde-commits&m=112427751029289">commit 450000</a> happened in the KDE Subversion server. The honour goes to Rafał Rzepecki, while committing to his Google Summer of Code project. This happened not 20 days after the commit number 440000.

It's nice to see everyone working hard.
<!--break-->