---
title:   "It's so weird..."
date:    2007-03-09
authors:
  - thiago
slug:    its-so-weird
---
It's so weird to start writing Yet Another Networking Framework (YANF) for KDE...

It's weird not because it's simply YANF. It's weird because this is the <b>fourth</b> networking framework that I will have written for KDE. So, a little history:

<ol>
  <li>KDE 1.0 came out with a KSocket and a KServerSocket class, by Torben Weis</li>
  <li>During the KDE 2.0 development phase, I started to write something different, to support IPv6. This never made it to mainline KDE because I did not submit it. But here's the proof that I did: <a href="http://www.advogato.org/person/thiagom/diary.html?start=2">my blog from Aug 17th, 2000</a></li>
  <li>Later, when KDE 2.2 was in development, I started to write the <tt>KExtendedSocket</tt> class, that actually made it to the release. Here's the <a href="http://www.advogato.org/person/thiagom/diary.html?start=6">blog from Dec 2nd, 2000</a> to prove</li>
  <li>As a general consensus, KExtendedSocket was found to be bloated and difficult to understand. So I proposed a new framework, simpler and more robust. In fact, I proposed that it become part of Qt, but that didn't happen. Nonetheless, it did influence Qt 4's QtNetwork library. But it turned out to be the current KNetwork framework in kdecore. Here's the <a href="http://www.advogato.org/person/thiagom/diary.html?start=10">blog showing me starting to work</a> and the <a href="http://lists.kde.org/?l=kde-devel&m=104256783230005&w=2">initial proposal email from Jan 14th, 2003</a>
  <li>After <a href="http://labs.trolltech.com/blogs/2007/02/28/ssl-proxies-and-md5-sums/">Trolltech announced support for proxies and SSL</a> in QtNetwork last week, I decided it was time to merge KNetwork and QtNetwork (one could say they are two children of the same idea, above). So, I sent this email entitled <a href="http://lists.kde.org/?l=kde-core-devel&m=117269095807140&w=2">"RFC: Drop notice: KNetwork"</a> to kde-core-devel</li>
</ol>

The new framework will be basically a wrapper around QtNetwork to provide the two missing features: Unix sockets and SVR-based lookups. The rest we can do without. And even then, Trolltech seems to be interested in providing Unix sockets in QtNetwork itself.

<!--break-->