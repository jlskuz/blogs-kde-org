---
title:   "Buzzy Little Bee"
date:    2006-03-02
authors:
  - seele
slug:    buzzy-little-bee
---
I've been a buzzy little bee for the past few weeks.  I would have posted a few days ago, but it ended up turning in to <a href="http://dot.kde.org/1141152947/">a DOT article</a>.

I started my new job a few weeks ago.  Yay for working at home, but I'm having trouble leaving work at work and actually "going" home.  Last night I came home from class at 2300 to make phone calls and write emails until <a href="http://www.neomantra.org/">Justin</a> reminded me I was "home".  Its easy to roll out of bed and go to work, but hard to leave the keyboard at a certain time and stop being a IxD for the rest of the night.

Its possible I am flying to Ohio for a day trip tomorrow.  I was supposed to go last week but the interviews wern't scheduled soon enough, and so far there is only one lined up for tomorrow.  I can't say I'm excited about the day trip to Cleveland (being from Pittsburgh and all), but hey, it could be somewhere in Idaho or someplace.  The trip will give me an opportunity to interview users of my client's software and get a better idea of how their product is actually being used rather than how its supposed to be used.

The Vegas countdown has begun and we leave in 15 days.  As most of you know, I am from Pittsburgh.  Many of the friends I had in college and beyond have moved on to different parts of the country and a big group of us (12 or so people) are going to meet up in Vegas.  

El and Jan are coming to visit me in D.C. before we head to Atlanta for the printing meeting in April.  I'm pretty excited for their visit, especially since El hasn't been to the U.S. yet.  I'm also going to be in Atlanta for Monday and Tuesday (instead of just Monday) so I'll have the chance to catch up with other people too.

The kde-usability and kde-usability-devel lists have been pretty active lately with random threads here and there as well as preparation for the meeting in Berlin as well as future planning for the project.  I have to say I'm happy about how well people are communicating lately rather than the normal troll and rat-hole sessions.

School has been suprisingly tame, but I have been very good about scheduling my time (I hope I can keep it up, I am notorious for 'putting things off').  Now that I got the usability reports up on the site, I can continue with my 'resolutions' list from the beginning of the year. Jan and I have an article forthcoming, and I am in the process of writing another for an industry newsletter.  I would like to get moving on some KDE-Edu work as well (time and sanity permitting).

But first I must worry about Ohio.  I dont even have a plane ticket yet!

<!--break-->