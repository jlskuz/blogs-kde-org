---
title:   "Children and Educational Games"
date:    2006-03-14
authors:
  - seele
slug:    children-and-educational-games
---
<p>Recently I went to Ohio to interview young students and their parent about a client's home schooling education system. They were surprised (although I wasn't) how the students were actually using the system (software, materials, teachers, etc.).  It just reminded me of how important it is to identify your audience in order to have success.  Here are a few things I took away from my trip which could be applied to KDE-Edu (grades K through 6)...</p>

<p><b>Make it fun</b></p>

<p>If possible, make anything and everything a game.  Anything which takes score, counts levels, or gives you a report of success works the best.  Children are very reward-oriented and are more interested in completing a task if there is some kind of incentive (special animation, possibility of recorded high score, printable certificate of achievement).</p>

<p><b>Make it pretty</b></p>

Most adults appreciate a nice looking design, but are almost always annoyed by blinking, flashy, colourful items in an interface (this is a learned aversion to online Ads and spyware).  Children on the other hand enjoy these decorations especially while engaged in play.  You as an adult may find it annoying to have a *bleep* and *flash* every time you clicked a button, but in a way that is a form of positive feedback and reward for a child.  Make it pretty and engaging to hold attention and make it memorable.

<p><b>Make it challenging but not too difficult</b></p>

<p>Children like to be challenged, but they are easily frustrated.  If a puzzle or problem is consistently difficult without success, they will quickly become frustrated and uninterested with the game.  Often it is better to repeat moderately difficult puzzles than solve a single challenge.</p>

<p><b>Make it short and sweet</b></p>

<p>Unless the user has ADD (attention deficit disorder), they're going to get bored, fast.  Games for younger children should have shorter sessions (levels, problems, etc.) with many problems/puzzles than several large problems/puzzles which take up most of the time.  They want instant gratification and are only willing to dedicate a few minutes before expecting to see some kind of progress before giving up or getting bored.</p>

<p><b>Kids are smarter than and not as smart as you think</b></p>

Children are a very interesting user.  They can be clever and inventive, but keep in mind they haven't developed the same affordances or metaphors we have over our much longer lives.  They may be able to identify and quickly grasp the concept of a certain icon, but be completely confused by another.  Just as seniors, children have certain interaction and accessibility needs you need to consider when designing your application.

<hr />

<p>A lot of this might seem like 'no duh', but you would be surprised how often it is overlooked or dismissed.  For example, the client I was working with was under the assumption that most of the students required constant supervision from their parent (or whoever is the 'teacher'), wasn't capable of completing assignments or being responsible for themselves, and spent most of their time with books than the interactive assignments provided by the software.</p>

<p>In reality, the students were quite the opposite.  The majority of students (however I didn't experience the norm; all of the students I interviewed were doing well) were completely independent with their studies other than the 'teaching' sessions with their parent.  They often repeated the interactive portions because they liked them more than studying from a book and had no problems completing assignments and turning them in on time.</p>

<p>I think it would be very beneficial to the KDE-Edu apps to define the scope of their application and come up with realistic user groups (eg: students learning French in 3rd to 6th grade rather than anyone who wants to learn French) and do some user-research backfilling.  This will help a lot when it comes to writing content material and designing the interface and increase the usage and success of the app.</p>

<!--break-->