---
title:   "Plasma on maemo"
date:    2008-07-05
authors:
  - mkruisselbrink
slug:    plasma-maemo
---
As a next step in my SoC project this week I've worked on getting Plasma to run on a maemo based device. Getting KDE compiled for this platform provided some new challenges, mainly caused by the old compiler used by its SDK (gcc 3.4.4), but apart from some compiler issues I didn't have many problems to get it to compile. Overall compiling stuff for maemo seems a lot easier that cross-compiling it for openmoko, as the scratchbox environment maemo uses nicely hides all the complicated cross-compiling issues, so it appears as if you're compiling normally (scratchbox even transparently uses qemu to run any ARM binaries that are build and used in the same build process).

Sebas was so kind to let me borrow his nokia N810, which arrived today, so I could try if my build also actually works. After struggling some hours to get USB networking to work (the n810 doesn't seem to like my wireless network), I installed Qt and my KDE build on it with this result:
<img src="http://koffice.nl/images/slide.php/photos/dsc00438.jpg">

As you can see it runs, although plasma apparently doesn't know there already is a panel on the left from the hildon desktop, so plasma's panel doesn't fit entirely on the screen. After killing hildon plasma already looks a lot better at home:
<img src="http://koffice.nl/images/slide.php/photos/dsc00442.jpg">

One thing I noticed is that Nokia 'solved' the high DPI problem by not telling X the real DPI of the device (which is > 200 DPI), but instead claiming it is a 96DPI display, which makes the size of fonts and other GUI elements match a bit better together. Also this device is quite noticeably faster than my neo1973, and the screen is (physically and by resolution) a bit bigger.

Next week there won't be progress from me, as I'll be away from computer and internet for the entire week, but after that (assuming I'll pass midterm evaluations) I'll have lots of interesting issues to work on.