---
title:   "KDE 4.2.0 and KOffice 2.0 beta6 packages for Maemo"
date:    2009-02-04
authors:
  - mkruisselbrink
slug:    kde-420-and-koffice-20-beta6-packages-maemo
---
After quite a while of not doing much maemo related, I finally found the time to build a bit more up-to-date kde packages for maemo. I've now uploaded kdelibs, kdepimlibs and kdebase from KDE 4.2.0 (more modules will follow), and also the just released koffice 2.0beta6 (unfortunately no kspread yet, as I couldn't get it to compile with the old gcc version in scratchbox). If you want to play with kde and/or koffice on your maemo powered device, add the following line to your /etc/apt/sources.list:<tt>deb http://93.157.1.37/~marijn/maemo binary/</tt>, and install kde and/or koffice related packages (you probably don't want to install them to the small rootfs, as the packages are still a bit big). The state of maemo/hildon integration for kde applications hasn't changed much since my previous packages (for example applications still don't have a correct icon in the taskbar), but I hope to be able to spend some time in that area as well.

<a href="http://93.157.1.37/~marijn/7866067f5ff7c7354bf8aef97def2032.png"><img src="http://93.157.1.37/~marijn/7866067f5ff7c7354bf8aef97def2032_small.png" alt="Plasma on Maemo"></a><br>

I'll also be at fosdem this weekend, so if anyone has problems getting the packages to work, you could try to find me there :)