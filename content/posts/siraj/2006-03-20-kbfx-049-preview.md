---
title:   "Kbfx 0.4.9 Preview."
date:    2006-03-20
authors:
  - siraj
slug:    kbfx-049-preview
---
<h3>Overview</h3>
<p>  
As we know it's been some time since we released a stable version of kbfx. Lot of work has been going on behind the stage..I think users who came to kbfx.org know a lot about the upcoming kbfx version and it's state. but there are lot more who has no idea about 
kbfx 0.4.9 which we will release in 10 to 15 days from now.<br><img src="http://www.kbfx.org/climate56.png">
</p>

<p> In this release we have fixed lot of bugs and processed more than 200 user requests.
made on kde-look, on kbfx.org as well as wishes I got over email.With this version, we have simplified skin development and Nookie is a working on a Wink based Flash tutorial on how to create skin in 10 mins.Lets look at a screen shot and then it's much easy to explain the new features.

<img src="http://www.kbfx.org/climate55.png">
<br>
<img src="http://kbfx.org/23.png">

 OK.. now u have seen it. The First skin is the default of kbfx-spinx bar (by Nookie)
and the second one is some ugly work I did using E17 images source.(all credits to e17 theme team).and the last one is again by Mensur(nick: Nookie) who made a skin to go along the lines of KDE style skins.and I think by the time we release stable..
Nookie will come up with a very Vibrant colored (Orange/Blue/Green/Purple) skin.But he's very busy now with Ur New KHTML based Config Dialog which will replace Qt based config that we had for almost one year..let look ar a screen shot of that too.

<img src="http://kbfx.org/config2.png">

This was designed by Nookie and he's now doing the HTML for ur new config. once this is complete we can release kbfx-0.4.9 . Many asked me on irc as well with mail why we are delaying the release of this version. Well the answerer is very simple. "Quality". 
when we started work on 0.4.9 . we planed the requirements for 0.4.9 and we decided that a stable would not be released until all those requirements are met.Now we have come to the final 10 day's of kbfx release schedule, Hence this blog as planed.

Now here is the list of new features (the most important once )
<ul>
<li>Doubled Buffered Kbfx Button:No more flickers with Large buttons</li>
<li>Smoother Image Translations for kbfx button </li>
<li>very simple Skin spec for kbfx-spinx bar</li>
<li>Animated scrollbars.(Arrows moving opposite Direction</li>
<li>The KDE user Image is automatically picked up:we call it Dude Image </li>
<li>Animated Dude Image </li>
<li>Smooth application Icon Zoom </li>
<li>Mouse Cursor changes</li>
<li>Animated logout and login buttons</li>
<li>KHTML based Config dialog for kbfx</li>
</ul>

u are always wellcome to checkout kbfx from ur sf.net cvs repository. and the
module name is spinx...there are pit falls and might not compile clean..
thanks you all for the good support.
Bye.

....
Siraj Razick
/With all of KBFX team.