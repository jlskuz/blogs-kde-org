---
title:   "Emergent Design Book Preview Available"
date:    2004-05-19
authors:
  - john ratke
slug:    emergent-design-book-preview-available
---
While reading <a href="http://www.joelonsoftware.com">Joel On Software</a>, I found a link to a website with a draft of a book called <a href="http://www.netobjectives.com/emergentdesign/ed_toc.htm">"Emergent Design"</a> by the same authors who wrote "Design Patterns Explained".  Some chapters are still to be added, but the existing material there is very interesting reading, especially <a href="http://www.netobjectives.com/emergentdesign/download/03-CodingPrinciples.pdf">chapter 3</a>, which covers "Coding Principles".
<br>
<br>
If you were to apply this to the internal architecture of something like, say, umbrello, you can see why it would be very difficult to write any unit tests for the code.  The classes in that application are very much linked together. For example, lower level classes send signals to indicate state changes to upper level classes.  In other words, the coupling is too tight.  It makes it very hard to test a class or group of classes in isolation.
