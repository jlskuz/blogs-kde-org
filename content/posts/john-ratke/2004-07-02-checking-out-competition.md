---
title:   "Checking out the Competition"
date:    2004-07-02
authors:
  - john ratke
slug:    checking-out-competition
---
        Since I am somewhat new to C# and .net I decided to follow up on a
        thread I had read on Joel On Software about the new <a href="http://lab.msdn.microsoft.com/express/">Microsoft Visual
        Studio Express</a> product
        line which is targeted at enthusiasts and hobbyists.  So I went, and
        sure enough, Visual C# Express Beta was a free download, and only 50mb.
        Note that they require that you give them a valid email address, and
        also, from what I've read, the evaulation expires next year.
<br><br>
        I find it interesting that Microsoft is releasing their tools like this.
        They probably feel forced to
        because they don't want to lose the hobbyist market completely to
        Open Source offerings.  If people want to learn how to program,
        Microsoft has to do something to get them to try their tools, and this
        is what they are doing.  I remember when I was first starting out
        programming and QBasic came free with DOS.  Then if you wanted to
        compile executables and have more IDE features, you had to buy QuickBasic,
        which I ended up doing.
<br><br>
        Luckily I had just enough harddrive space free in my Windows partition
        for the Visual C# install.  I must say that I was impressed by the
        simplicity and clarity of the setup.  It even had a little check box at
        the very end saying
        that you could send your usage data to Microsoft to help improve the
        setup process (even though the setup process was already only a few
        clicks).  Clicking on the details of that message opened a
        text file showing the click patterns through the various screens of the
        setup wizard, and generic info like the free space on the hard drive and
        the fact that the install was successful.
<br><br>
        I think such functionality would be great for KDE.  We could have the
        apps completely control what got logged using a mechanism similar to
        kDebug.  For example:  
<code>
kUsability << "Button 1 clicked!!" << endl;
</code>
        would log the "Button 1 clicked!!" message to a log file along with the
        time and the date (somewhat important for determining how long users
        are staring at the screen with a confused look on their face).  Then
        something could trigger popping up a dialog to ask the user to send this
        anonymous data to KDE to help us with our usability.  Sending the data
        could be accomplished via email as I believe we do for bug reports today.
        Does any KDE app do anything like this?
<br><br>
        The Visual C# IDE overall is very polished.  I felt that the only Beta
        quality part of it was some missing help files.  With the form editor it
        is very easy to create form layouts, including menu bars, tool bars, and
        status bars.  And just double clicking a button takes you to the code
        that will run when the button is clicked.  There was an example hello
        world type console program as well as a simple windows forms application
        with just one button.  There is also a larger example program which is a
        screensaver that downloads an RSS fead and displays headlines.
<br><br>
        As someone who is relatively new to the C# programming language, it is nice
        to see what looks to be a simple and powerful language, as opposed to the
        C++ that I am used to.  C# is type safe.  It's not too complicated.  It has
        easy to use pass by value and pass by reference.  Everything is in one or
        more classes like in Java.  It has namespaces.  It has easy to use exceptions.
        It has a built in string type.  It has a foreach statement.  It has object
        properties and you can control the accessor methods and don't have to write a
        lot of code for them.  And by virtue of running in the .net environment it has
        automatic memory management and can interoperate with code written in
        other languages.  I used to not understand what they meant when they used the
        phrase "managed code." (They do use that phrase a lot.)  But now it makes
        sense that the environment "manages" the memory, specifically the
        de-allocation.  All of these things combine to make C# useful programming
        language.  I know this may sound like a stupid Microsoft commercial, but it
        really isn't.  Let's hope that C# on Mono with MonoDevelop can bring these
        same qualities to Linux development.
