---
title:   "What I learned at LWE"
date:    2005-02-17
authors:
  - manyoso
slug:    what-i-learned-lwe
---
Presenting KDE as an exhibitor at LWE is an interesting experience.  I've learned a couple tricks that make things more interesting, both for me and for booth visitors:

<ul>
<li>Keep the screens moving.  We are using konq's autoscroll feature and Lubos is setting up a dcop script to set the anchor to the top every minute to keep it from scrolling to the bottom.  Users think this is very cool and it is :)
<li>Don't just take questions to start.  The booth visitors don't usually come to us with specific questions... rather, they want to see what is coming in the latest KDE. That is, if they are familiar with KDE (or Linux for that matter) at all.
<li>Along those lines, I'm ask booth visitors if they want to play a game of "Did you know KDE can do this?"  Examples:
<li>Did you know KDE can select and copy text and images from KPDF?
<li>Did you know you can subscribe and read your news from with Kontact via our new RSS feed tool, akregator?
<li>Did you know Konq can autoscroll, letting you read your news while you drink your coffee?
<li>Did you know you can rip your audiocd in real time and across the network with simple drag'n drop from Konq?
</ul>
<break>
I think KPDF gets the biggest WOW factor.  Several booth visitors have said that they were sold on KDE with just this feature alone.  And then they donated, so they were apparently serious :)

Generally, users like all of the new features and seem genuinely surprised and excited when presented with cool things they never knew existed had been in their KDE the whole time.  

And it's not just booth visitors.  I confess, I didn't know Konq could autoscroll until yesterday when Chris showed me :)
<!--break-->