---
title:   "Go (STEELERS + REFS)!"
date:    2006-02-07
authors:
  - manyoso
slug:    go-steelers-refs
---
Seele, I understand that you are a big Pittsburgh fan and are understandably happy they "won", but I can't imagine how you can call it a "great game to watch."  See, unlike most of the country and unlike most of the people at the stadium in Detroit and unlike all of the referees on the field, I was hoping for a Seahawks win.  <a href="http://www.mercurynews.com/mld/mercurynews/sports/13810017.htm">That game was horrible.</a>  But, don't take my word for it... read just about every account of the game, anywhere.  

The only people truly satisfied are folks like you: Steeler fans who couldn't care less that their team won due to a HUGE assist from the guys in white striped outfits.  You earned your trophy with one of *the* *worst* *quarterback* performance in history.  You earned your trophy despite a phantom touchdown from said quarterback that should never have been called, an offensive pass interference call that should never have been called, a holding call that would have put the Seahawks at the one yard line and ready for the go ahead score and a missed off sides call on the same play, a completely idiotic blocking call on a guy who was making a *tackle* for chris' sakes.

<a href="http://msn.foxsports.com/nfl/story/5310192">But, don't take my word for it.</a>  After all, I'm probably as big a Seahawk fan as you are a Steeler fan.  I grew up with them and have watched them despite 30 years of misery.  I still remember the 1984 championship, crystal clear, even though I was just a little kid at the time.  Trust me, bad officiating against the Seahawks is nothing new.  Go and google Testaverde's "Phantom Touchdown" to see what I mean.  <a href="">However, I know I'm not hallucinating.</a>  If you read ESPN, Foxsports, or many of the national rags it is apparent that <a href="http://www.chron.com/disp/story.mpl/sports/justice/texans/3640886.html">any honest observer can see that this game was either willfully fixed OR subject to some of the worst officiating in Superbowl history.</a>

The Seahawks were clearly the better team.  Problem is, the game wasn't Seahawks VS Steelers... it was Seahawks VS ( Steeler's + Refs )
<!--break-->


