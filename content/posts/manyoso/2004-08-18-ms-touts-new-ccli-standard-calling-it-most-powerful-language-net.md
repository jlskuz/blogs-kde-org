---
title:   "MS touts new C++/CLI standard calling it most powerful language for .NET"
date:    2004-08-18
authors:
  - manyoso
slug:    ms-touts-new-ccli-standard-calling-it-most-powerful-language-net
---
Just came across this article via msdn, <a href="http://msdn.microsoft.com/visualc/default.aspx?pull=/library/en-us/dnvs05/html/VS05Cplus.asp">C++: The Most Powerful Language for .NET Framework Programming.</a>  Microsoft seems especially pleased with their new version of Managed C++.  They've done a complete revamp of their previous .NET support for C++ and submitted it to <a href="http://www.ecma-international.org/news/ecma-TG5-PR.htmECMA">ECMA and ISO as a standard.</a>  This brings C++ inline with C# as far as first class support for the Common Language Runtime.  I really like what they've done to improve the C++ experience for .NET, but still think it will be incredibly hard to adapt the Qt/KDE libraries to fit into this framework.  Consider this annoyance, the new proposed standard mandates PascalCase be used for the core API.  For a technical challenge you'd have to consider how to <a href="http://doc.trolltech.com/3.3/properties.html">integrate MOC</a> and the new C++.NET properties.  Read on.
<br><br>
<b>C++.NET properties:</b>
<code>property String^ Name
{
    String^ get()
    {
        return m_value;
    }
    void set( String^ value )
    {
        m_value = value;
    }
}
</code>
<b>Q_PROPERTY:</b>
<code>    Q_PROPERTY( QString name READ name WRITE setName )
    void setName( QString name );
    QString name() const;
</code>

Well, leave aside the technical challenge of making a Free Managed C++ compiler according to this new spec and check out what it offers:

<ol>
<li>Object construction on the GC heap or regular native
<li>Value types and Reference types just like C#
<li>Boxing and UnBoxing so C++ gets a unified object model
<li>C# style delegates adapted for C++ in addition to normal function pointers
</ol>

There is a handy reference table at the bottom of the article comparing C++.NET syntax for various operations and the equivalent C#.  <a href="http://msdn.microsoft.com/visualc/default.aspx?pull=/library/en-us/dnvs05/html/VS05Cplus.asp">Check it out.</a>
