---
title:   "Update on the C# bindings"
date:    2004-09-07
authors:
  - manyoso
slug:    update-c-bindings
---
I just <a href="http://lists.kde.org/?l=kde-cvs&m=109454867019365&w=2">added features</a> regarding the C# bindings to the 3.4 feature list.  As 95% of the major work towards the new super spiffy Qt# bindings is done, I figured it was time to set some goals for getting all of this in shape for a KDE release.  For those curious about the current state of my bindings (as opposed to the other C# bindings efforts) <a href="http://www.mit.edu/~manyoso/bingestyle.png">this kind of stuff is working now.</a>  For those confused by all the myriad twists and turns of the various C# bindings efforts here is a short history:

<ol>
<li>The original C# bindings for Qt were initially created with the Kalyptus tool.  Richard Dale helped me greatly when I first started out with this approach.  After awhile the obfuscated perl code wore on me and I decided to create a C# generator.</li>
<li>The first (and only) functional Qt# binding was created with this first generation C# generator that has no name.  It generated the C# source by parsing a xml based metadata file that was created with... get ready... kalyptus.  The code really, really bites.  I'm embarrassed by it really.  And to add insult to injury, the kalyptus extension that I wrote to output the xml has been lost entirely.</li>
</ol>

That is the current production state of things and has been for quite sometime.  If you go to the <a href="http://qtcsharp.sourceforge.net/">Qt# webpage</a> you'll find the fruits of this generator.
<br><br>
Now, shortly after getting Qt# in a usable state, I decided to move development out of KDE's cvs because of numerous complaints by users/developers who didn't want to deal with the kde cvs build system.  I've tried to keep the two in sync with updates at various times, but that hasn't worked out to well.  Now, in KDE's cvs you'll find a completely busted C# binding because the QtC library we were using has been removed.  I've learned my lesson and will be moving development back to KDE's cvs as soon as practicable.
<br><br>
Ok, on with our story.  It was apparent very soon that the first generation C# generator was horrible and would need a complete rewrite.  That is when development on BINGE began.  BINGE is the original second generation BINding GEnerator for Qt# that we began working on way back when.  Unfortunately, right around the time when BINGE was becoming quite powerful and full featured, I was pulled away from development as a result of frustrations with: the various free CLI enviroments, the state of bindings in general, and the world going to &*&*# around me ie, the hell that is the Iraq war.  So, I stopped working on the Qt# bindings completely.
<br><br>
With this, three things happened:

<ol>
<li>Development on Binge stopped dead in the water just as it was becoming usable as I was the only one with good knowledge of the tool.</li>
<li>Qt# bindings were maintained by Marcus who also began working on a second generation C# binding generator called <a href="http://qtcsharp.sourceforge.net/background.php">Bugtussle.</></li>
<li>Eventually, Richard Dale took a new look at C# bindings with a new kalyptus generator that would employ SMOKE and realproxies. <a href="http://dot.kde.org/1079750571/">aka kimono.</a></li>
</ol>

Since that time, and until I took up working on BINGE again, development on Bugtussle has moved ahead while Richard's smoke based effort seems to have stopped, at least momentarily.
<br><br>
Which leaves us to the current status in this long winded and boring story.  I believe BINGE is the most advanced second generation C# binding generator at this time.  By quite a bit.  Work on Bugtussle continues by Andreas Hausladen.  He has a replacement for QtC in place, but no working C# bindings.  Richard is using smoke for the intermediate library, but his C# sources are not in a usable form.  BINGE is generating a replacement for QtC and generating an almost complete replacement for the original Qt# right now and I'm working on bindings for the recently released Qt4 beta as well as KDE bindings.
<br><br>
Sorry, if you read all that.  The point is that the C# bindings are quite confused right now, but work continues and hopefully the combined best of all these efforts will emerge.  Oh, and I plan on having initial C# language plugin for kdevelop by 3.4.  Cheers.
<br><br>
PS For those who wish to play with the BINGE bindings to see where they are at... realize that it isn't for the feint of heart right now, <a href="http://sourceforge.net/mailarchive/forum.php?thread_id=5030310&forum_id=8370">and then go here.</a>  Although, cvs is moving quickly so that might be outdated too ;P