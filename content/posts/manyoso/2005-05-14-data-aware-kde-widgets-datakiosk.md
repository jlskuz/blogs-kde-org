---
title:   "Data aware KDE widgets in dataKiosk"
date:    2005-05-14
authors:
  - manyoso
slug:    data-aware-kde-widgets-datakiosk
---
Hey, ruurd, I was going to post this as a comment, but blogger's login crap was giving me problems.  Anyway, I have several data aware KDE widgets in DataKiosk I hope to introduce into KDElibs with KDE4.  They include some widgets that DataKiosk shares with KDEPIM and some that are entirely new to KDE including one that mimics the MS Access Relation Combo.  They are written to be generic enough to use for any QSql driver.  

If you want to have a go at them before the KDE4 timeline ( or if you want to start the port early ) feel free.  The widget I'd most like to see in KDELibs is the multi-column KComboBox derivative I created as a foreign key relation editor.  It comes with completion courtesy of KCompletionBox, but it employs a couple of hacks to overcome deficiencies in the API that should be solved for KDE4.  You could also look into making the entire DataTable class ( which is derived from QTabWidget and features custom QDataTable, QDataBrowser tabs that work with eachother ) into a standalone KDE widget if there is interest.

Here is the <a href="http://extragear.kde.org/apps/datakiosk/datakiosk-relation-combo.png">pic</a> of the multi-column KComboBox derivative embedded in the DataTable widget.<!--break-->