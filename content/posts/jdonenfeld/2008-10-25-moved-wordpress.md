---
title:   "Moved to Wordpress"
date:    2008-10-25
authors:
  - jdonenfeld
slug:    moved-wordpress
---
KdeDevelopers, while a helpful site, did not allow for registration-less comments. Since I'd like to use my blog as a means of easily communicating with the community, I've switched to my own self-hosted <a href="http://blog.jasondonenfeld.com">WordPress blog</a>.