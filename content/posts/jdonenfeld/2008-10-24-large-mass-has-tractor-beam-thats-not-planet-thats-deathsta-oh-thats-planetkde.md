---
title:   "That large mass has a tractor beam! That's not a planet... that's a deathsta--. Oh, that's planetKDE."
date:    2008-10-24
authors:
  - jdonenfeld
slug:    large-mass-has-tractor-beam-thats-not-planet-thats-deathsta-oh-thats-planetkde
---
I've been watching the development of KDE for a while, submitting bug reports and testing trunk, and last night I made my first <a href="http://websvn.kde.org/?view=rev&revision=875009">patch</a>. I received an svn account and decided to start working on Amarok 2, for which I'm currently fixing <a href="https://bugs.kde.org/show_bug.cgi?id=173341">this bug</a>. But I also intend to work on other areas of KDE, after I poke around for a while and learn the general layout of the project. Suggestions? To begin, I think I might start investigating <a href="http://bugs.kde.org/show_bug.cgi?id=168154">this bug</a>. I also work on the <a href="http://www.arora-browser.org">Arora Browser</a>.

Speaking of projects, to learn more about Qt's painting system, I wrote a program that generates the <a href="http://en.wikipedia.org/wiki/Sierpi%C5%84ski_triangle">Sierpiński triangle</a> using a <a href="http://en.wikipedia.org/wiki/Chaos_game">chaos game</a>. Pick three vertices of a triangle. Plot a point at random inside that triangle. Choose a vertex of the triangle at random and plot the midpoint between the chosen vertex and the previous point plotted. Repeat this ad infinitum and you get:
<a href="http://data.zx2c4.com/fractal.jpg"><img src="http://data.zx2c4.com/thumb.php?src=fractal.jpg&width=600" border="0"></a>
I don't think that I've implemented the drawing in the most ideal way, so please leave suggestions. I opted to use a QImage instead of a QPainter, and then just fill pixels and scale. You can <a href="http://git.zx2c4.com/?p=trianglefractalchaos.git;a=tree">browse</a> the source, checkout the repository with <code>git clone http://git.zx2c4.com/trianglefractalchaos.git</code>, or download a <a href="http://git.zx2c4.com/?p=trianglefractalchaos.git;a=snapshot;h=HEAD;sf=tgz">tarball</a>.

Currently I'm running 4.1.2, but I plan to switch completely over to trunk in the next few weeks. Here's my default clean desktop:
<a href="http://data.zx2c4.com/desktop_with_notes.png"><img border="0" src="http://data.zx2c4.com/thumb.php?src=desktop_with_notes.png&width=600"></a>
The large note on the left is about a few ideas for a shared-server-plan based file sharing mini-network. Maybe I'll say more on this another time if I end up implementing it.

Hello planetKDE.
<!--break-->
