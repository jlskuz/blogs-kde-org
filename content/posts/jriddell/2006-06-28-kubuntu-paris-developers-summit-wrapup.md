---
title:   "Kubuntu Paris Developers Summit Wrapup"
date:    2006-06-28
authors:
  - jriddell
slug:    kubuntu-paris-developers-summit-wrapup
---
The Paris summit is now over and by the looks of the blogs the KDE developers are pleased with what happened.  The specs we have approved are:

<ul>
<li>kubuntu-launchpad-integration - Add a menu item to applicaition Help menus to take you to Launchpad</li>
<li>langpacks-desktopfiles-kde - Let .desktop files get their translations from .po files</li>
<li>kubuntu-laptop-buttons - get laptop buttons working with Ubuntu's acpi-support package</li>
<li>kubuntu-power-management - an all new power management frontend, Sebas already has some code for this in Guidance</li>
<li>kubuntu-edgy-docs - Help!,</li>
</ul>

And those that are in review:

<ul>
<li>kubuntu-easy-zeroconf - add a tickbox to turn on zeroconf.</li>
<li>kde-kiosk-profiles - include several kiosk profiles by default</li>
<li>kubuntu-accessibility - make an accessibility profile on the live CD</li>
<li>kubuntu-icons - get Ken to work on Oxygen</li>
<li>kubuntu-printer-sharing - add a tickbox to turn on printer sharing and browsing</li>
<li>kubuntu-system-settings-usability - tidy up system settings</li>
</ul>

We also spend a fun couple of hours looking at goldenear's proposed new UI for kmplayer.

Hopefully we'll get a good number of those implemented by the Edgy release.  Feel free to come along and help.

This week and next are both dedicated to updating all the packages in Kubuntu with the latest versions and merging with Debian.  All help appreciated with that too.

Many thanks to the KDE developers who came to the summit.

I'll be talking at <a href="http://www.ukuug.org/events/linux2006/">UKUUG's Linux 2006</a> conference this weekend, see you in Brighton.

For those not on Planet KDE he's the exclusive first look at the group photo:

<img src="http://kubuntu.org/~jriddell/ubuntu-distro-summit-paris-group-wee.jpg" width="500" height="226" class="showonplanet" />
<a href="http://kubuntu.org/~jriddell/ubuntu-distro-summit-paris-group.jpg">Ubuntu Distro Summit Group Photo</a> and the list of <a href="http://kubuntu.org/~jriddell/group-photo.txt">who's who</a>.
<!--break-->
