---
title:   "Move Along, Nothing to See (except Amarok packages)"
date:    2006-04-10
authors:
  - jriddell
slug:    move-along-nothing-see-except-amarok-packages
---
kubuntu.de published a note today that Canonical had not answered their requests.  Their article makes everything sound much worse than it is.  The problem was that when kubuntu.org moved to a new host the sysadmin request to recreate Amu's account never got answered.  Amu makes the cool Live CDs that get published along with KDE releases so it's obviously very useful for him to have an account.  I should have poked Canonical's sysadmin to remind him but the account has now been added so problem solved.  This doesn't mean, as some people seem to have suggested, that Canonical is in any way dropping support for Kubuntu, they continue to be wonderfully supportive, both to the community of developers and commercially if you want to buy a support contract off them.  

Meanwhile, <a href="http://kubuntu.org/announcements/amarok-1.4beta3.php">Kubuntu packages for Amarok 1.4 beta 3</a> got released.  Pretty new icons!
<!--break-->
