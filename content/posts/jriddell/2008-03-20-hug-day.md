---
title:   "Hug Day!"
date:    2008-03-20
authors:
  - jriddell
slug:    hug-day
---
Come help triage Network Manager related bugs in today's <a href="https://wiki.kubuntu.org/UbuntuBugDay/20080320/KDE">Hug Day</a>.  Join in on #ubuntu-bugs and #kubuntu-devel.
