---
title:   "42,000 schools running Kubuntu derivative"
date:    2011-09-16
authors:
  - jriddell
slug:    42000-schools-running-kubuntu-derivative
---
An <a href="https://lwn.net/Articles/455972/">LWN article from a few weeks ago</a> talks about Userful Corporation's deployment of Linux in Brazillian schools.  

<i>"The Brazil deployment has been rolling out in phases since 2008, and currently includes more than 42,000 schools in 4,000 cities. The base distribution is one created by the Brazilian government, called Educational Linux, which is based on Kubuntu."</i>

Lovely
<!--break-->
