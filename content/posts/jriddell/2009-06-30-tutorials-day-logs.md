---
title:   "Tutorials Day Logs"
date:    2009-06-30
authors:
  - jriddell
slug:    tutorials-day-logs
---
<a href="https://wiki.kubuntu.org/KubuntuTutorialsDay">Tutorials Day rocked</a> and logs are now available for those who missed it.  Talks covered Ruby, Amarok Scripting, Artwork, Packaging and Kubuntu Karmic.
