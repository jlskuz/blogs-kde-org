---
title:   "Qt Licence Update"
date:    2014-08-20
authors:
  - jriddell
slug:    qt-licence-update
---
Today Qt announced some changes to their licence.  The KDE Free Qt team have been working behind the scenes to make these happen and we should be very thankful for the work they put in.  Qt code was LGPLv2.1 or GPLv3 (this also allows GPLv2).  Existing modules will add LGPLv3 to that.  This means I can get rid of the part of the <a href="https://techbase.kde.org/Policies/Licensing_Policy">KDE Licensing Policy</a> which says "Note: code may not be copied from Qt into KDE Platform as Qt is LGPLv2.1 only which would prevent it being used under LGPL 3".

New modules, starting with the new web module QtWebEngine (which uses Blink) will be LGPLv3 or GPLv2.  Getting rid of LGPLv2.1 means better preserving our freedoms (can't use patents to restrict, must allow reverse enginerring, must allow to replace Qt etc).  It's not a problem for the new Qt modules to link to LGPLv2 or LGPLv2+ libraries or applications of any licence (as long as they allow the freedoms needed such as those listed above).  One problem with LGPLv3 is you can't link a GPLv2 only application to it (not because LGPLv3 prevents it but because GPL2 prevents it), this is not a problem here because it will be dual licenced as GPLv2 alongside.

The main action this prevents is directly copying code from the new Qt modules into Frameworks, but as noted above we forbid doing that anyway.

With the new that Qt moved to Digia and there is a new company being spun out I had been slightly worried that the new modules would be restricted further to encourage more commercial licences of Qt.  This is indeed the case and it's being done in the best possible way, thanks Digia.
