---
title:   "Talk and Interview at FOSDEM"
date:    2014-02-10
authors:
  - jriddell
slug:    talk-and-interview-fosdem
---
<a href="http://hackerpublicradio.org/eps.php?id=1452">Hacker Public Radio FOSDEM edition</a> is up now for those wanting a podcast packaged full of geek interviews.  My interview is 27 minutes in and talks of KDE, Kubuntu and how Baloo will make the KDE world slicker.  <a href="http://hackerpublicradio.org/eps/hpr1452.mp3">MP3 file</a>.

And the video of my talk <a href="http://video.fosdem.org/2014/H1302_Depage/Saturday/Do_you_have_to_be_brain_damaged_to_care_about_desktop_Linux.webm">Do you have to be brain damaged to care about desktop Linux</a> is up on FOSDEM's website.  Mixing the highs and lows of severe head trauma with the highs and lows of developing a KDE Linux distribution.
