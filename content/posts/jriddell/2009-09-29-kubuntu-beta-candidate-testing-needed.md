---
title:   "Kubuntu Beta Candidate Testing Needed"
date:    2009-09-29
authors:
  - jriddell
slug:    kubuntu-beta-candidate-testing-needed
---
Nearly Beta time for Kubuntu.  We need testers to grab the ISO images and test them.  Also upgrade testing is needed.  See the <a href="http://iso.qa.ubuntu.com/qatracker/build/kubuntu/all">ISO tester</a> for what needs tested and join us on #kubuntu-devel to help out.

Tests also include the shiny new Kubuntu Netbook edition, which contrary to some reports has been made in full consultation with the lovely people from Plasma Netbook.  It'll be a tech preview rather than a give-to-your-family release but that's why it needs lots of testing now.
<!--break-->
