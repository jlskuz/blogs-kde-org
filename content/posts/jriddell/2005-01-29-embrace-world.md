---
title:   "Embrace the World"
date:    2005-01-29
authors:
  - jriddell
slug:    embrace-world
---
KDE should work with the rest of the software world as much as possible and not just look within itself.  This means having KDE programmes on Windows because that is useful for me and you and all the other people who have to use Windows occasionally.  Its useful having Gimp and Firefox on Windows, it would be useful having Umbrello and Konqueror too.  Of course for that to happen we need a Free version of Qt on Windows.

It also means using D-BUS and HAL and anything else which comes out of freedesktop.org.  We like DCOP, D-BUS is essentially a platform independent DCOP, we should use it.  HAL might not be great to work with (so I hear) but it's the best there is and fantastic for users.
<!--break-->
