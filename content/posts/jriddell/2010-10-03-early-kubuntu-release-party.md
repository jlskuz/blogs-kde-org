---
title:   "Early Kubuntu Release Party"
date:    2010-10-03
authors:
  - jriddell
slug:    early-kubuntu-release-party
---
A couple of weeks ago Thomas McGuire realised his name wasn't very German sounding and came to Edinburgh where it would fit in better.  He is spending the next six months taking a bit of a break from maintaining KMail and instead programming robots to play football.  The robots will be using Akonadi I'm sure.  

To welcomed him into the country the Scottish KDE community had a gathering in Stirling where we introduced Thomas to tartan, shortbread, whisky, Innis and Gunn & Irn Bru.  All the essentials of our culture.  We also had a KDE GB meeting and agreed to turn it into a registered charity, so we can collect tax back from donors, yay for free money.

We also spent the afternoon installing Kubuntu 10.10 RC which was useful to find a bunch of issues that are apparant to KDE devlopers which I would not notice without their focused eyes.  For the most part 10.10 came out quite well which is good news.

<img src="http://people.canonical.com/~jriddell/thomas-jonathan-calton-hill.jpg" width="400" height="300" />
Oh Edin, the Athens of the north

<img src="http://people.canonical.com/~jriddell/DSCF5074.JPG" width="400" height="300" />
Paul is quite into his Laphroig

<img src="http://people.canonical.com/~jriddell/10-10.png" width="176" height="143" />
One week to go
<!--break-->
