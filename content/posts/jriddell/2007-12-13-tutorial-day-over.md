---
title:   "Tutorial Day Over"
date:    2007-12-13
authors:
  - jriddell
slug:    tutorial-day-over
---
That went well.  We even had some of SuSE's finest drop by, were they picking up tips? :)

<a href="https://wiki.kubuntu.org/KubuntuTutorialsDay">Logs available now</a>.
