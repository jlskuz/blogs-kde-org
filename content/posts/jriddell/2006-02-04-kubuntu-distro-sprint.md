---
title:   "Kubuntu Distro Sprint"
date:    2006-02-04
authors:
  - jriddell
slug:    kubuntu-distro-sprint
---
This last week was the Ubuntu distro sprint, where a bunch of core Ubuntu developers get locked in a bland hotel in a bland part of London (which was overcast for the whole week with no variation in weather) and told to work on things in return for Amarulla.

<img src="http://jriddell.org/photos/2006-02-04-distrosprint.jpg" width="400" height="300" />

So here's the first exciting screenshot of the new Kubuntu live CD installer.  Not much else to see just now.

<img src="http://kubuntu.org/~jriddell/espresso.png" width="439" height="451" />

For those who have been complaining on kubuntu-users the media:/ ioslave now shows hard disks thanks to split privilages HAL.

<img src="http://kubuntu.org/~jriddell/media.png" width="560" height="350" />

I also got the KDE frontend to NetworkManager successfully working, and seems to work just as well as the existing Gnome frontend.  Unfortunately it needs CVS NetworkManager so it won't make it into dapper.

<img src="http://kubuntu.org/~jriddell/knetworkmanager.png" width="629" height="265" />

Meanwhile mornfall started work on a simplified app installer for Adept.

<img src="http://kubuntu.org/~jriddell/adept-installer-0.png" width="500" height="194" />

And in New Zealand one time Gnome release dude got a hair cut (or as some person here said on seeing some after photos "who's that").  The story can be found on <a href="http://aseigo.blogspot.com/2006/01/lca-forgotten-stories.html">Aaron's blog</a> on planet.kde.org.

<img src="http://kubuntu.org/~jriddell/jdub.jpg" width="640" height="425" />

And the Ubuntu package build servers got moved from the Katie system to all new Soyuz system.  Latest word is that things seem to be working.  Look.. <a href="https://launchpad.net/distros/ubuntu/+builds">buildlogs!</a>
<!--break-->
