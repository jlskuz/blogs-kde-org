---
title:   "Text Layout Summit at Akademy; Schedule Your App with Gutsy"
date:    2007-04-28
authors:
  - jriddell
slug:    text-layout-summit-akademy-schedule-your-app-gutsy
---
An e-mail out of the blue and within an hour of a reply saying I liked the idea, Akademy is hosting the <a href="http://www.freedesktop.org/wiki/TextLayout2007">Text Layout Summit 2007</a>.  This will be a really exciting event for the specialist but important area of text layout.  The one last year in Boston has an impressive list of attendees from Qt, Pango, KWord, DejaVu, fontconfig, Scribus and others.  It'll be really great to have KDE developers mixing with those from other projects just as we will have on the Tuesday at the <a href="http://akademy2007.kde.org/codingmarathon/schoolday.php">Edu and School Day</a>.  Register by Monday is you want us to book your accommodation.

The <a href="https://wiki.kubuntu.org/GutsyReleaseSchedule">release schedule for Gutsy</a> has been published.  If you are in charge of a project and want to get a release out in around 5 months time, why not align it to the Gutsy schedule?  Some apps including Konversation have done this in the past and it has worked well on both sides.  Make sure you release a beta at least a week before feature freeze and a final at least two weeks before release.  Let me know if you plan to do that or have any releases the Kubuntu team should be aware of in the next 5 months.
<!--break-->

