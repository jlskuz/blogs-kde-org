---
title:   "Kubuntu at Ubuntu Open Week"
date:    2007-10-24
authors:
  - jriddell
slug:    kubuntu-ubuntu-open-week
---
This week is <a href="https://wiki.kubuntu.org/UbuntuOpenWeek">Ubuntu Open Week</a>, a series of talks and Q&A session on IRC.  Yesterday Richard did a top session on Kubuntu and <a href="https://wiki.kubuntu.org/MeetingLogs/openweekgutsy/Kubuntu1">logs are available</a> for those wanting to catch up.  He had another session tomorrow (Thurday) at 2100 UTC.  There are also sessions on using Launchpad, packaging for beginners and pretty much every other part of the Ubuntu world.  At 1600 UTC today is the ever popular Ask Mark session, join #ubuntu-classroom for the chance to put in your questions.
<!--break-->
