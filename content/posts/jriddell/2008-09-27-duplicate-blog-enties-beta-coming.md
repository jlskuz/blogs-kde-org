---
title:   "Duplicate Blog Enties; Beta coming up"
date:    2008-09-27
authors:
  - jriddell
slug:    duplicate-blog-enties-beta-coming
---
The all new Planet KDE is going well.  One hickup was that blog entries would duplicate themselves in RSS readers when anyone edited their post.  I've fixed that now by changing to using the URL to the entry as the ID in the RSS feed (before it was using a hash).  So you may well get duplicates of everything today with the change to the new scheme, but not after that.

<hr />

Kubuntu Intrepid beta is due out next week, if you want to help us test the multiple ways of installing and upgrading to it do join us on #kubuntu-devel and subscribe to ones you are interested in on the <a href="http://iso.qa.ubuntu.com/qatracker/build/kubuntu/all">ISO testing site</a>.

<hr />

It was Edinburgh's Doors Open Day today when various pretty buildings let you come inside and see what they look like.  I went to the Sheriff Court and took part in a mock trial, here's the 10 year old Sheriff sentencing a nasty shoplifter to some weeks in jail.  I then came home to find a letter from the very same Sheriff court on my doormat summoning me to jury duty, how exciting.

<img src="http://farm4.static.flickr.com/3048/2892867378_745a9973f3.jpg?v=0" alt="Edinburgh Sheriff court" />
<!--break-->