---
title:   "Site-ul Kubuntu România a fost lansat"
date:    2013-09-25
authors:
  - jriddell
slug:    site-ul-kubuntu-românia-fost-lansat
---
As previously blogged <a href="http://ro.kubuntu.org">Kubuntu Romania</a> launched.

Site creator Ovidiu-Florin <a href="http://geekaliens.com/blog/2013/09/site-ul-kubuntu-romania-a-fost-lansat/">has a launch ceremony</a> with wine and ribbon to open it.  Now that's style.

<img src="http://geekaliens.com/blog/wp-content/uploads/2013/09/DSC_0301-1024x685.jpg" width="512" height="342" />

<img src="http://geekaliens.com/blog/wp-content/uploads/2013/09/DSC_0303-300x200.jpg" width="300" height="200" />
