---
title:   "Project Neon 5 daily builds, Ubiquity Wireless Setup"
date:    2013-07-31
authors:
  - jriddell
slug:    project-neon-5-daily-builds-ubiquity-wireless-setup
---
<img src="http://3.bp.blogspot.com/-iIFB93ozj-c/TXs43dJYu7I/AAAAAAAAAtk/Rpnn19Hm1LA/s400/banner.png" width="400" height="73" />
<a href="http://techbase.kde.org/Getting_Started/Using_Project_Neon_to_contribute_to_KDE">Project Neon</a> is a fantastic resource for KDE developers giving daily builds for KDE software. It's maintained by the lovely Kubuntu community on the lovely Launchpad infrastructure.  KDE developers can install the various bits they need to develop their part of KDE without having to worry about compiling everything themselves.  It installs everything into /opt so it doesn't touch your normal software installation.

Thanks to the hard work of Harald and others we have the first KDE Frameworks 5 builds on Neon!  I expect this to prove a vital resource in developing Frameworks 5.

As with any daily build bits will break occationally (or quite often until Frameworks 5 settles down).  Take a look at the <a href="https://launchpad.net/~neon/+archive/kf5/+packages">Packages in “Project Neon KDE Frameworks 5”</a> to see the current state.

To give it a try go
<pre>
sudo apt-add-repository ppa:neon/kf5 
sudo apt-get install project-neon5-session
</pre>
and log into a Neon 5 session at LightDM.  (Currently you need to kill lightdm to log out!)

<a href="http://people.ubuntu.com/~jr/plasma2.png"><img src="http://people.ubuntu.com/~jr/plasma2-wee.png" width="600" height="375" title="Plasma 2 with Neon 5" /></a>
Plasma 2 and Frameworks 5 - the first look
<hr />

A lovely little addition to our installer from the lovely Aurelien Le Cake, you can now set up wireless during the installer.  This means if you're running it in installer only mode you can still get a network connection to install Flash or MP3 support
<a href="http://people.ubuntu.com/~jr/ubiquity-nm.png"><img src="http://people.ubuntu.com/~jr/ubiquity-nm-wee.png" width="600" height="386" /></a>
