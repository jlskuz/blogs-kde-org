---
title:   "Akademy 2013 Day 4 in Photos"
date:    2013-07-18
authors:
  - jriddell
slug:    akademy-2013-day-4-photos
---
<a href="http://www.flickr.com/photos/jriddell/9308968601/" title="DSCF8062 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7373/9308968601_ed9412e76d.jpg" width="500" height="375" alt="DSCF8062"></a>
The day started with a long release schedule BoF.  No conclusion but less freezes seemed popular.  Shorter release schedule seemed mostly popular with a 4 month schedule getting a fair bit of approval.  Not ideal of us though as it would get things out of sync with the Ubuntu schedule.

<a href="http://www.flickr.com/photos/jriddell/9311754336/" title="DSCF8063 by Jonathan Riddell, on Flickr"><img src="http://farm6.staticflickr.com/5506/9311754336_6a705da128.jpg" width="500" height="375" alt="DSCF8063"></a>
Kubuntu dudes and Limux Munich rollout dude

<a href="http://www.flickr.com/photos/jriddell/9308978559/" title="DSCF8072 by Jonathan Riddell, on Flickr"><img src="http://farm6.staticflickr.com/5344/9308978559_d063ea4bfe.jpg" width="500" height="375" alt="DSCF8072"></a>
In search of enlightenment KDE trecked along the great wall of Basque country.

<a href="http://www.flickr.com/photos/jriddell/9308984193/" title="DSCF8077 by Jonathan Riddell, on Flickr"><img src="http://farm8.staticflickr.com/7398/9308984193_5db8d39614.jpg" width="500" height="375" alt="DSCF8077"></a>
David finds his zen

<a href="http://www.youtube.com/watch?v=U3jaDkvWgZU" title=""><img src="http://farm3.staticflickr.com/2848/9311784388_ef769cc752.jpg" width="500" height="375" alt="DSCF8095"></a>
KDE gets nekkid <a href="http://www.youtube.com/watch?v=U3jaDkvWgZU" title="">see video</a>

<a href="http://www.flickr.com/photos/jriddell/9311795540/" title="DSCF8113 by Jonathan Riddell, on Flickr"><img src="http://farm6.staticflickr.com/5475/9311795540_2f8d31caff.jpg" width="500" height="375" alt="DSCF8113"></a>
We finished with a large feast of fish and beef
