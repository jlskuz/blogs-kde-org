---
title:   "Kubuntu Banner"
date:    2012-08-20
authors:
  - jriddell
slug:    kubuntu-banner
---
Posted today to the kubuntu-devel mailing list, this handy image for your website

<img src="http://3.bp.blogspot.com/_mZnyDz74x4c/S_2eIQmej6I/AAAAAAAAATQ/Epr5iDGO6Pk/S220/Kubuntu+website+banner.gif" width="220" height="76" />

