---
title:   "Calling all Testers"
date:    2014-04-15
authors:
  - jriddell
slug:    calling-all-testers
---
Candidate images for Kubuntu 14.04LTS are up and need you to test them.  Go to the <a href="http://iso.qa.ubuntu.com/">ISO tracking</a> site to download and mark your testing status.  Check out the <a href="https://tinyurl.com/ovfcj78">milestoned bugs</a> for issues we are aware of and do report any we are not.
