---
title:   "Kubuntu Maverick All Planned Out at UDS"
date:    2010-05-20
authors:
  - jriddell
slug:    kubuntu-maverick-all-planned-out-uds
---
Ubuntu Developer Summit is over, we emerged from the deepest darkest Belgian forest full of plans for the next six months of Kubuntu.  The <a href="https://wiki.kubuntu.org/KubuntuMaverickSpecs">specs are written</a> and we have a lengthy<a href="https://wiki.kubuntu.org/Kubuntu/Todo">Todo list</a>.  If you've ever thought about helping out Kubuntu take a look at the Todo list and see if there's anything that inspires you.

If there's a theme to our plans for the next six months it's filling in the obvious holes we have in our offering.  KPackageKit has plans for usability love, we found a way to have an "App Store" view in KPackageKit, new printer setup tools are in the works, samba file sharing might well get fixed.  We'll also have Kolabsys keeping an eye on our KDE PIM packages.  There's hopes for a "mobile" version of Kubuntu for weeny devices and the Netbook and Desktop CDs will probably merge into one.  Oh and we plan to ship with Rekonq for a web browser, it's getting nicer every day.  Agateau showed his plans for a <a href="
http://agateau.wordpress.com/2010/05/10/getting-menus-out-of-application-windows/">global menu bar</a> which is currently being discussed with the Plasma Netbook guys as a possible way to save screen space.

If it all works out it'll be a lovely release, do come and join us.

<img src="http://people.canonical.com/~jriddell/jonathan-riddell-kubuntu-chef.jpg" width="600" height="450" />
Ubuntu Magazine were handing out copies of their German version featuring "Interview Mit Kubuntu Chef Jonathan Riddell", I quite like the idea of being a Kubuntu Chef mixing up packages in my kitchen.

<img src="http://people.canonical.com/~jriddell/kubuntu-team-uds-maverick.jpg" width="600" height="450" />
Team Kubuntu!  Rodrigo, Aurelien, Scott, Roman, Jonathan, Roderick, Jussi, Another Jonathan, Daniel
<!--break-->
