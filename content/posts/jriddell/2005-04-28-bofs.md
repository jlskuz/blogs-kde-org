---
title:   "The BoFs"
date:    2005-04-28
authors:
  - jriddell
slug:    bofs
---
This conference isn't about talks and it isn't about hacking sessions.  Instead it's about small birds of a feather meetings on a lot of topics.  Everything from KubuntuRoadmap to GraphicalInstaller to GettingInvolvedInUniverse are discussed with groups of 2 to a dozen people.  That's the BrainDump stage.  Then the BoF leader writes down the notes into a Spec which is the DraftedSpec stage.  That then gets edited by the professional proofreaders in a room called Sublime 3 making it an EditedSpec.  After that is has to be checked over by the most elite Ubuntu people, making it an ApprovedSpec.  Finally, after the conference, we go and implement the spec sometime before the next release, making it an ImplementedSpec.  Previous releases have had release goals, the idea behind making specifications like this is so everyone can see exactly what the plans are.  Of course just because there are specifications of what we would like to get done doesn't mean it will get done, which is where the community comes in, Specs should be generally available after the conference I assume, last one to implement is the looser.

This is the BoF wall, draft on the left, implemented on the right.  Kubuntu is currently sitting at EditedSpec, waiting to be checked over into Approved. 

<img class="showonplanet" src="http://jriddell.org/photos/2005-04-29-sydney-bof-wall.jpg" width="400" height="300" />
<!--break-->
