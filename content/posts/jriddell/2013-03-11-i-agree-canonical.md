---
title:   "I Agree with Canonical"
date:    2013-03-11
authors:
  - jriddell
slug:    i-agree-canonical
---
Really I do, dunno why some people think I unfairly critisise them.  They want to fix <a href="http://launchpad.net/bugs/1">bug number 1</a>, "Microsoft has a majority market share".  The obvious way to do that was with the existing community made desktop software but after many years that still had minimal traction.  Actually lots of nice rollouts around the world in educational institutions, every region/nation in Spain made their own Ubuntu derivative and of course Kubuntu has had the world's biggest Linux desktop rollout in Brazil.  But nothing to get it directly into the hands of consumers.  Dell did a half hearted attempt to pre-load Ubuntu on laptops but somehow it never went very far.  

In the mean time Apple did fix Bug No 1 by using shiny design and new form factors.  There's some Free Software in Apple's catalogue including a web browser engine from KDE but it's otherwise more closed than Microsoft.  Netbooks came and went and Linux to the consumer didn't have much success there.

Then Google started fixing Bug No 1 with Android and maybe even Chromebook will go somewhere.  They're a lot more Free Sofware but also not openly community developed.

So it's entirely sensible for Canonical to reason that community developed consumer software isn't going to fix Bug No 1 any time soon and want to start looking at models which do. Hiring a design team to create their own software is more Free Software than Apple and more Open Source then Google.  I wish Ubuntu (Unity) luck in taking over the world and fixing Bug No 1.  

But people who like to work on a community driven project will be a bit put off by that which is why we've seen some high profile departures recently from Ubuntu. But as I say there are many parts of Ubuntu which are very community driven so I'm hanging around to be part of that community.  Come and join us.

<a href="http://www.flickr.com/photos/jriddell/8547806947/" title="DSCF7389 by Jonathan Riddell, on Flickr"><img src="http://farm9.staticflickr.com/8098/8547806947_e9f92e6001.jpg" width="375" height="500" alt="DSCF7389"></a>