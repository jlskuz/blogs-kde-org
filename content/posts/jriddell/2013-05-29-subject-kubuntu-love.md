---
title:   "Subject: Kubuntu love"
date:    2013-05-29
authors:
  - jriddell
slug:    subject-kubuntu-love
---
<img src="https://wiki.kubuntu.org/KubuntuArtwork?action=AttachFile&do=get&target=kubuntu-logo-lucid.png" width="400" height="78" />

<i>I get the best e-mails</i>

Subject: Kubuntu love
Date: Sat, 18 May 2013 12:26:56 +0100
 
Hello Jonathan 
 
I really wasn't sure who to write to about this, but I got your name from the
Kubuntu site as a contact. 
 
Anyway, all I wanted to say was to pass on my thanks to the Kubuntu team for 
producing such a fantastic distro; it really is a pleasure to use. I'm back
with Linux after a long period of using Windows and OS X, and although it
wouldn't be Linux without the occasional gotcha, Kubuntu is so polished, well- 
integrated, and, well, easy that it feels like coming home.
 
Anyway, if you think it's appropriate, please pass on my thanks to the Kubuntu 
team for all their hard work and dedication in making what really is a class 
act. 
 
Best regards,
 
Richard P