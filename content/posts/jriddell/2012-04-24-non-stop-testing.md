---
title:   "Non-stop testing"
date:    2012-04-24
authors:
  - jriddell
slug:    non-stop-testing
---
Ubuntu release date is set for Thursday so it's non-stop testing until then.  All helpers needed - Kubuntu team will test everything in all permutations but it's never a good thing to be the only tester on software for which you are responsible for, third parties always needed.  So join us in #kubuntu-devel and #ubuntu-testing and report results in <a href="http://iso.qa.ubuntu.com/qatracker/milestones/214/builds">the ISO tracker</a>.
