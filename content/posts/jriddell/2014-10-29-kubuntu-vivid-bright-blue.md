---
title:   "Kubuntu Vivid in Bright Blue"
date:    2014-10-29
authors:
  - jriddell
slug:    kubuntu-vivid-bright-blue
---
<a href="https://trello.com/b/3s11MoXD/15-04"><img src="https://www.google.com/webpagethumbnail?c=41&r=4&s=300&d=https://trello.com/b/3s11MoXD/15-04&a=AIYkKU9JCL3Cq-WiEDM8GdZZGCgz254cOg" width="300" height="315" /></a>

Kubuntu Vivid is the development name for what will be released in April next year as Kubuntu 15.04.

The exiting news is that following some discussion and some wavering we will be switching to Plasma 5 by default.  It has shown itself as a solid and reliable platform and it's time to show it off to the world.

There are some bits which are missing from Plasma 5 and we hope to fill those in over the next six months.  Click on our Todo board above if you want to see what's in store and if you want to help out!

The other change that affects workflow is we're now using Debian git to store our packaging in a kubuntu branch so hopefully it'll be easier to share updates.
