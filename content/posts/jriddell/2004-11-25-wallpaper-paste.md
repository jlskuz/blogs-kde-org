---
title:   "Wallpaper Paste"
date:    2004-11-25
authors:
  - jriddell
slug:    wallpaper-paste
---
Today I made desktop wallpapers use .desktop files for their metadata.  Hopefully this means the image names will be translated.

I also made it possible to use SVG images as wallpapers.  

I would like to make a freedesktop.org standard for wallpapers so that gnome's wallpapers can be used by KDE and vice versa.  One possible motivation for this is that gnome has gained <a href="http://cvs.gnome.org/viewcvs/gnome-backgrounds/">some lovely wallpapers</a> recently.

This would require that we agree on a common directory to put wallpapers in, $datadir/wallpapers or $datadir/pixmaps/backgrounds/gnome.  It would also mean we should agree on a common metadata format.  They use an XML file which I think would be difficult to translate using KDE's i18n scripts.  .desktop files mean one description file per image which is much nicer.  So I've no idea how we'll be able to agree.

<img class="rapemewithachainsawthanks" src="https://blogs.kde.org/images/559c599cd0a7d587da352fa513e55e10-738.png" />
<!--break-->
