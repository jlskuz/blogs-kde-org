---
title:   "Falkirk LUG Talk Tonight"
date:    2011-09-06
authors:
  - jriddell
slug:    falkirk-lug-talk-tonight
---
Yesterday's App Developer Week <a href="https://wiki.kubuntu.org/MeetingLogs/appdevweek1109/IntroducingBazaarExplorer">talk on Bazaar Explorer</a> went well.  See the logs if you missed it.

Tonight I'm giving a real life talk at my old user group <a href="http://www.falkirklug.org.uk/">Falkirk LUG</a> about Kubuntu and Bazaar.
<!--break-->
