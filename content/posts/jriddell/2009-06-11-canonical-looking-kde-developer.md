---
title:   "Canonical Looking for KDE Developer"
date:    2009-06-11
authors:
  - jriddell
slug:    canonical-looking-kde-developer
---
Canonical's online services team wants to <a href="http://identi.ca/notice/5077420">hire a KDE developer</a> to work on cool Kubuntu online services integration.  Amazing what you find on identi.ca.
<!--break-->
