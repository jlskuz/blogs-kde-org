---
title:   "FOSSCamp in Prague"
date:    2008-05-17
authors:
  - jriddell
slug:    fosscamp-prague
---
I took the train to Prague, much more relaxed and environmentally friendly than flying.  Went through Berlin where I found <a href="http://www.flickr.com/photos/jriddell/2498860123/">a curious memorial to the Glorious Soviet Union</a> and stopped off for an executive power breakfast at Trolltech's new office, which seems to still be in the process of being built around them.

In Prague we have just finnished <a href="http://www.fosscamp.org/">FOSSCamp</a>, the Canonical organised general free software unprogrammed conference.  A good showing from KDE with Jos of Strigi, Inge of KOffice/Marble, Lydia of Amarok, Robert of Konsole and Peter of Adept.  The local Novell people came along too, with Lubos of KWin doing a valiant job of running the KDE and Gnome Collaboration sessions.  We had sessions on KDE, Free Music, Mobile interfaces, OpenOffice, upstream bug reporting, packagekit and many more.  Next week is UDS, down to the serious business of planning the next six months of Kubuntu development.

<a href="http://www.flickr.com/photos/jriddell/2498860129/"><img src="http://farm3.static.flickr.com/2008/2498860129_5e9000b17b.jpg?v=0" width="500" height="375"/></a>
Qt, programmed with table football and electric drums.

<a href="http://www.flickr.com/photos/jriddell/2498860137/"><img src="http://farm3.static.flickr.com/2153/2498860137_3af4ccb6e0.jpg?v=0" width="500" height="375"/></a>
Blurry KDE developers hit the Prague nightlife
<!--break-->
