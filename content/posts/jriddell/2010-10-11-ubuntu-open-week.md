---
title:   "Ubuntu Open Week"
date:    2010-10-11
authors:
  - jriddell
slug:    ubuntu-open-week
---
<a href="https://wiki.kubuntu.org/UbuntuOpenWeek">Ubuntu Open Week</a> is a week of talks about how to get involved in the many parts of Ubuntu.  I'm doing a talk at 16:00UTC today which is titled "Kubuntu is Awesome".  Join us in #ubuntu-classroom and #ubuntu-classroom-chat on Freenode IRC

