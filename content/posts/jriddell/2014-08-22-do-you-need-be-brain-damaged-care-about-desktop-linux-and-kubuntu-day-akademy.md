---
title:   "Do you need to be brain damaged to care about desktop Linux? and Kubuntu day at Akademy"
date:    2014-08-22
authors:
  - jriddell
slug:    do-you-need-be-brain-damaged-care-about-desktop-linux-and-kubuntu-day-akademy
---
After sell out dates in Glasgow and Belgium the tour of my dramatic talk <b>"Do you need to be brain damaged to care about desktop Linux?"</b> is making a stop in Brno for the KDE Conference Akademy.  In it I'll talk about the struggles of recoving from a head injury mixed with creating a beautiful and friendly Linux distro: Kubuntu.  It'll have drame, it'll have emotion, it'll have a discussion of the relative merits of community against in-house development. Make sure you book your tickets now!

Also at Akademy is the Kubuntu day on Thursday, <a href="https://notes.kde.org/p/kubuntu-akademy">sign up now</a> if you want to come and talk about your ideas or grumble about your problems with Kubuntu.  Free hugs will be in store.

<img src="https://community.kde.org/images.community/2/22/Banner400.going.png" width="400" height="178" />

