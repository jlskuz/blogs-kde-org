---
title:   "Nice things in the post"
date:    2009-02-24
authors:
  - jriddell
slug:    nice-things-post
---
"That's not our usual postman at the door, and why is she carrying a plant?"  What a fun thing to have delivered in the morning, and whoever the mystery sender was full marks on potted plant over wasteful disposable non-potted one.

<img src="http://www.kubuntu.org/~jriddell/dscn2684.jpg" width="400" height="533" />

Also arrived is a batch of Kubuntu stickers.  Canonical is sending these out to Kubuntu contributors as a wee thank you.  Now my laptop can be complete in showing my geeky loyalties.

<img src="http://www.kubuntu.org/~jriddell/dscn2685.jpg" width="400" height="300" />
<!--break-->
