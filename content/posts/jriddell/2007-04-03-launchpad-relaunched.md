---
title:   "Launchpad Relaunched"
date:    2007-04-03
authors:
  - jriddell
slug:    launchpad-relaunched
---
<a href="http://launchpad.net">Launchpad</a> today got a shiny new makeover for the 1.0 beta. If you haven't looked at Launchpad yet I highly recommend it for hosting free software projects. Nifty features include linking bugs to different distributions, team management and web based translations. But the killer feature is integration with <a href="http://bazaar-vcs.org/">Bazaar (bzr)</a> which makes it super easy to make branches. If you are working on something which is at all experimental just branch then merge back in to mainline once its done. The other killer feature is that unlike other free software hosting sites you don't have to ask to set up a project, just fill in the name and it's done, project registered, it gives the freedom to be able to set up a project for any random script you think would be useful to others.
<!--break-->
