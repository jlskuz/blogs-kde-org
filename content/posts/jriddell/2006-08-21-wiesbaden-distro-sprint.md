---
title:   "Wiesbaden Distro Sprint"
date:    2006-08-21
authors:
  - jriddell
slug:    wiesbaden-distro-sprint
---
This week I'm at the <a href="https://wiki.kubuntu.org/UbuntuDeveloperSprintWiesbaden">Ubuntu distro sprint</a> in Wiesbaden near Frankfurt. If you're in the area do drop by and say hi.

<img src="http://static.flickr.com/72/221138809_12feabb443.jpg?v=0" width="500" height="375" class="showonplanet" />
<!--break-->
