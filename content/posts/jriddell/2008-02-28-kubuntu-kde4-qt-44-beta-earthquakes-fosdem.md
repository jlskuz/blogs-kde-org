---
title:   "Kubuntu-KDE4, Qt 4.4 beta, Earthquakes, FOSDEM"
date:    2008-02-28
authors:
  - jriddell
slug:    kubuntu-kde4-qt-44-beta-earthquakes-fosdem
---
<a href="https://wiki.kubuntu.org/HardyHeron/Alpha5/KubuntuKDE4">Kubuntu-KDE4 got its first alpha</a> today.  Lots of bits to tidy up but for the most part it works.

Qt 4.4 beta is in <a href="https://edge.launchpad.net/~jr/+archive">my PPA</a> for hardy, thanks to fdoving (still liable to break existing compiles of Plasma).

Was rudely awoken last night to the shaking of the bedside cabinet.  Turns out to have been an earthquake, which would have been exciting if I hadn't gone back to sleep.

FOSDEM happened and was crammed packed with good talks, great people and finest free beer.  (I never did find out who was paying for the free beer, but thanks.)  My talk went surprisingly well, I only expected half a dozen people to want to learn about .deb packaging but the room was pleasingly filled, I'm still waiting for someone to follow through and supply me with an updated package for GNU Hello.  The KDE room in general was packed full most of the time, which was great, although I tried to get out and visit Gnome, SuSE, Debian and others.  

<a href="http://www.flickr.com/photos/jriddell/2292262222/sizes/o/"><img src="http://farm3.static.flickr.com/2407/2292262222_d77a24610e.jpg?v=0" width="500" height="333" alt="group photo" /></a>
KDE Group photo from FOSDEM.  You're doing good if you can name them all.

<!--break-->

