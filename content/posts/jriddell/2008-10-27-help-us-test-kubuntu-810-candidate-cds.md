---
title:   "Help us test the Kubuntu 8.10 Candidate CDs"
date:    2008-10-27
authors:
  - jriddell
slug:    help-us-test-kubuntu-810-candidate-cds
---
Kubuntu 8.10 is due out this week.  If you want to help make that happen we need people to test the CDs, DVDs and upgrades.  

Grab the <a href="http://cdimage.ubuntu.com/kubuntu/daily/current/">Alternate CDs</a>.  Desktop CD and DVDs should appear shortly.  Follow <a href="https://help.ubuntu.com/community/IntrepidUpgrades/Kubuntu">these upgrade instructions</a>.

Use the <a href="http://iso.qa.ubuntu.com/qatracker/build/kubuntu/all">ISO tracker</a> for recording results.

Join us on #kubuntu-devel and #ubuntu-testing on freenode.

<img src="http://aplg.kollide.net/kubuntu/counter/medium.en.png" border="0"/>
<!--break-->
