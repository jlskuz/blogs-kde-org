---
title:   "Kubuntu 12.04 LTS is out"
date:    2012-04-27
authors:
  - jriddell
slug:    kubuntu-1204-lts-out
---
<img src="http://www.kubuntu.org/files/12.04-lts-banner.png" width="700" height="230" />
A shiny new Kubuntu release all for you.  You can install it safe in the knowledge that the Kubuntu community will make security updates and major bug fixes for 5 years along with Canonical's lovely resources doing the same on much of the rest of the Ubuntu packages.

There's a few <a href="https://wiki.kubuntu.org/PrecisePangolin/ReleaseNotes/Kubuntu#Known_Issues">Known issues</a> in this release I'm unhappy with, kmix crashes, akonadi upgrades are still not pleasant, Plasma activity manager likes to crash too, and users of 11.10 need to update to the latest packages before starting an upgrade.  Sorry about those, we'll get onto them.

In terms of new features we've kept it light to be able to fit with the requirements of Long Term Support, but all the packages have been updated.  I especially like what OwnCloud are doing, they've seen a big improvement and are getting some useful apps for providing from a server.

New stuff I like is Calligra and KDE Telepathy, I use these both a lot.  It's really handy to be able to stay on Facebook chat without being distracted by the aimless news on Facebook webpages and Calligra has saved my day more than once when Libreoffice has crashed (on its own documents!).

<img src="http://www.kubuntu.org/files/kubuntu-active-logo-idea.png" width="429" height="200" />
Kubuntu Active is a shiny new Ubuntu flavour for tablets.  It's a technology preview for now because it's not a very smooth experience yet and it's i386 only but.. ooh what shinyness!

<img src="http://people.canonical.com/~jriddell/tmp/canonical.jpg" width="489" height="90" />
Kubuntu is in interesting times, we're trying to branch out from one sponsor of Canonical to several.  Those sponsors will include Canonical of course.  Kubuntu as a project has had a long history of working with Canonical. Canonical have invested in sponsoring staff and producing CDs, as well as providing infrastructure and release management. Although Canonical has decided to adjust where it focuses it's resources, I am delighted that Canonical is continuing to support the Kubuntu project with infrastructure and other resources that help us as a project to build Kubuntu. I am also delighted that Blue Systems have stepped forward to fund my continued work on Kubuntu so that I can continue to give my full attention to the project and build the best possible KDE-based Operating System for our users
<!--break-->
