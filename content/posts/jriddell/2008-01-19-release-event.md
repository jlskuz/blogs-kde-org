---
title:   "Release Event"
date:    2008-01-19
authors:
  - jriddell
slug:    release-event
---
Much exciting happenings here at the KDE 4.0 Release Event.  Best of which is Trolltech adopting GPL 3 for Qt (and future GPL versions as approved by them and KDE Free Qt Foundation).  That'll make the life of distros much easier.

More news later, but most exciting is managing to get a picture  with the first couple of KDE, Katie and Konqi.

<a href="http://jasmine.19inch.net/~jr/away/2008-18-01-kde-release/dscn0092.jpg"><img src="http://jasmine.19inch.net/~jr/away/2008-18-01-kde-release/index.php?thumb=true&name=dscn0092.jpg" width="133" height="100" /></a>
<!--break-->

