---
title:   "Writing"
date:    2005-04-27
authors:
  - jriddell
slug:    writing
---
Most of this morning was spent writing the plan for the next Kubuntu release.  It's a very long plan as we have a large number of ideas for making Kubuntu into the finest operating system there is.  With any luck that will get approved tomorrow.  Somewhere amongst the writing I went to a session learning about HCT, Canonical's groovy tool built on top of their version control system Baz which treats patches as if they were branches.  It can also pull in sources from CVS or SVN so I think I'm right in saying that it can branch CVS, which is fun.  

Just to clarify my blog of yesterday, devious doesn't mean wrong, organising a project in a devious way can be just the right way to do it, look at Ubuntu for the first year of its existance, or Firefox, or Appeal.  It can also be the wrong way to do it too.  

In the evening the Breakout project decided to go and see the sights of Kings Cross but ended up breaking up when some people wanted a more classy curry than others.  And for all their laid back beach lifestyle the night clubs in Australia are just as descriminating against sandals as elsewhere.

Just to finish on a picture here's the silent takeover of Ubuntu by Quakers.  They're always the quiet ones.

<img class="showonplanet" src="http://jriddell.org/photos/2004-04-26-sydney-quaker-free-software-cabal-paul-jonathan-daf.jpg" width="400" height="300" />
<!--break-->