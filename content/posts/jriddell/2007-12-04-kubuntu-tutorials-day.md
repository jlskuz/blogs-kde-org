---
title:   "Kubuntu Tutorials Day"
date:    2007-12-04
authors:
  - jriddell
slug:    kubuntu-tutorials-day
---
<p><a href="https://wiki.kubuntu.org/KubuntuTutorialsDay"><img src="http://kubuntu.org/~jriddell/kubuntu-tutorials-day-2.png" width="557" height="150" alt="kubuntu-tutorials-day-2" border="0" /></a> </p>

<h2><a href="https://wiki.kubuntu.org/KubuntuTutorialsDay">Kubuntu Tutorials Day</a></h2>

With KDE4 only a month away, now is the time to get involved in becoming a Kubuntu developer. We've set up these IRC sessions below to get you started. 

Who's it for?  Coders who want to get their apps and themes in the Kubuntu archives; learn about distributed revision control; get into KDE programming or just ask about us.

When is it?  Thursday December 13th from 1500UTC

Where is it?  On freenode IRC chat network in the #kubuntu-devel channel

<ul>
<li>15:00 UTC - 16:00 UTC: <b>Packaging 101</b><br />
How to turn some code into a .deb, one of the best ways of getting into distro development or for getting your code out there.</li>
<li>16:00 UTC - 16:30 UTC: <b>Kubuntu bug triage</b><br />
Not a coder but want to help?  We show you how to make good quality bug reports and triage existing ones. </li>
<li>16:30 UTC - 17:00 UTC: <b>Branch your svn with bzr</b><br />
 How to commit your changes when you don't have an account?  How to work experimentally on some code?  Distributed revision control is the way forward and bzr is the simplest yet most fully featured system there is. </li>
<li>17:00 UTC - 18:00 UTC: <b>Get programming with PyKDE 4</b><br />
Scared by C++?  Quite right too.  Learn how to code graphical apps in an hour with PyKDE 4.</li>
<li> 18:00 UTC - 19:00 UTC: <b>Get your work into Kubuntu</b>: PPA, REVU, how to pass New queue<br />
Now you know how to make a .deb, get it into the archives for millions to use.</li>
<li> 19:00 UTC onwards: <b>Kubuntu and KDE Q&amp;A</b> <br />
Want to know how to get into Kubuntu development?  Want to ask why we love KDE so much?  The Kubuntu developers are here to answer any question you have.</li>
</ul>
<!--break-->
