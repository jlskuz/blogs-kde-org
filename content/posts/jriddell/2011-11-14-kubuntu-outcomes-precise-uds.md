---
title:   "Kubuntu Outcomes from Precise UDS"
date:    2011-11-14
authors:
  - jriddell
slug:    kubuntu-outcomes-precise-uds
---
<a
href="http://www.flickr.com/photos/jriddell/6326109344"><img
src="http://farm7.static.flickr.com/6227/6326109344_25485acc73.jpg" width="500"
height="375" /></a>
Hacking between sessions, Quintasan, fregl, afiestas

The Ubuntu Developer Summit was in Florida again for a week of sessions, specs,
work items, discussions and mouse burgers.  We had a lot of useful Kubuntu
sessions and came up with a long list of things to do over the next six months.

<a href="https://blueprints.launchpad.net/sprints/uds-p?searchtext=kubuntu">The
list of specs</a> gives the work items, our Todo list for the Precise cycle.  

In <a
href="https://blueprints.launchpad.net/ubuntu/+spec/desktop-p-kubuntu-packaging">Kubuntu Precise Packaging</a> we discussed what we should package for the
forthcoming LTS release.  Whereas Ubuntu Desktop will not be upgrading to the
latest Gnome we decided that upgrading to the latest KDE releases is safe enough
for an LTS.  There's no new library version coming (because of KDE Frameworks 5)
and some packages such as Kontact really need the latest version.  We're looking
forward to formal releases from Calligra, qt-at-spi, Plasma Networkmanagement
and more.  Integration of bluetooth keyboard on boot and Oxygen-gtk3 should make
some use cases smoother.  There are also grand plans to make awesome ninja
scripts which will automate a lot of the Software Compilation packaging which
would allow us to focus on testing and assurance rather than the boring stuff of
updating changelogs.

<a href="http://www.flickr.com/photos/jriddell/6305865109/" title="DSCF6325 by Jonathan Riddell, on Flickr"><img
src="http://farm7.static.flickr.com/6101/6305865109_07b60191c2.jpg" width="500" height="375" alt="DSCF6325"></a>
Chat and beer in the evenings, agateau, afiestas, claydoh, rbelem

In <a
href="https://blueprints.launchpad.net/ubuntu/+spec/desktop-p-kubuntu-
defaults">Kubuntu Precise Defaults</a> we want to look at low-fat settings by
default for low powered machines, we'd like to get LightDM up to scratch for
KDE and use that by default and we'll consider using KDE Telepathy instead of
Kopete (but only if it's really ready).

Being a Long Term Support edition what we care about is <a
href="https://blueprints.launchpad.net/ubuntu/+spec/desktop-p-kubuntu-quality">
Quality</a>.  So our quality sessions looked at ways to ensure no new problems
creap in and old problems get squished out.  We'd like to be part of a KDE
papercuts initiative that David Edmundson has recently suggested.

In the <a
href="https://blueprints.launchpad.net/ubuntu/+spec/desktop-p-kubuntu-muon">
Muon spec</a> we have some fixes to our shiny new package manager.  And is that
the sound of Ubuntu One being ported to PyQt I hear?

<a
href="https://blueprints.launchpad.net/ubuntu/+spec/desktop-p-kubuntu-active">Kubuntu Active</a> is the name of our morning exercise programme and also hopefully a new variant of your favourite distro focused on tablets and low powered consumer devices featuring the shiny Plasma Active.

Finally we have important fixes to <a
href="https://blueprints.launchpad.net/ubuntu/+spec/desktop-p-kubuntu-cjk">CJK</a>, to <a
href="https://blueprints.launchpad.net/ubuntu/+spec/desktop-p-kubuntu-
filesharing">Samba filesharing</a> and to <a
href="https://blueprints.launchpad.net/ubuntu/+spec/desktop-p-kubuntu-
accessibility">Qt Accessibility</a>.

All together a nice little package.  Should be a fun six months.  Do come along and help us make it happen, we're in the #kubuntu-devel IRC channel.

<a href="http://www.flickr.com/photos/jriddell/6326109350"><img
src="http://farm7.static.flickr.com/6053/6326109350_61299da916.jpg" width="500" height="375" /></a>
Group Photo: Frederik, Lynoure, Clay, Michel, David, Maco, Luke, Jonathan, Rodrigo (missing Alex for some reason, but he's in <a href="http://www.flickr.com/photos/jriddell/6326109346">this one</a>.

Now I'm off on a little adventure and self improvement exercise as I
move to a little bit of France in the Antillies pour parler Français, see my personal blog <a href="http://jriddell.org/diary/?p=99">Moving to Guadeloupe</a> for the story.

<a href="http://www.flickr.com/photos/jriddell/6335545027/" title="Rum and sun by Jonathan Riddell, on Flickr"><img
src="http://farm7.static.flickr.com/6238/6335545027_60f2a9b6ef.jpg" width="500" height="375" alt="Rum and sun"></a>
French Rum in my new home
<!--break-->
