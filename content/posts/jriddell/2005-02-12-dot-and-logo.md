---
title:   "dot and logo"
date:    2005-02-12
authors:
  - jriddell
slug:    dot-and-logo
---
Recently I became part of the exclusive invite only club of KDE Dot News editors.  It's surprisingly time consuming to proofread and filter all the articles into a constant stream of high quality news for the world to learn about KDE from.  The submitions queue gets some oddities from spam to rants to an article about how to start kppp, and various completely off-topic stories.  Then there's questions like do we link to theKompany when they release a new proprietary product (if it's KDE related) or a new commercial GPLed product (yes) or to <a href="http://www.linare.com/">Linare</a> with their cheap laptop which happens to come with KDE (I let that one through but has anyone heard of these Linare people?  what is their distro based on?).  The Dot doesn't tend to cover program releases, that's what kde-apps is for, the trick is to accompany it with an article or review.  Anyone who wants to write an article is very welcome, we're always looking for more.

Eagle eyed CVS watchers may have noticed a new logo creeping through KDE.  You can find it at my <a href="http://www.kde.org/stuff/clipart.php">KDE clipart page</a>.

<img class="showonplanet" src="http://muse.19inch.net/~jr/klogo.png" />
<!--break-->