---
title:   "long time no blog"
date:    2003-08-26
authors:
  - mattr
slug:    long-time-no-blog
---
it's been awhile since I've written one of these. Mostly because I've been working on porting Kopete's oscar protocol to KExtendedSocket. It's used QSocket which is very limited in certain things. KExtendedSocket gives more control, as well as built in IPv6 support and proxy support. I wonder how many AIM and ICQ proxy related bugs I can close now. :D Hopefully it'll be in CVS in the next couple of days.
<!--break-->
In other news, Kopete 0.7.2 comes out on the 30th of August, right as N7Y is finishing up. Fall classes also started today (bleh). My first class isn't until tomorrow though, so yay!