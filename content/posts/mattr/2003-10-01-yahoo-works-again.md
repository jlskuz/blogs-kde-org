---
title:   "yahoo works again"
date:    2003-10-01
authors:
  - mattr
slug:    yahoo-works-again
---
Well, I got the kopete yahoo plugin to work again. Yahoo decided to change their authentication methods and so that left all of us in the dark.  From the looks of it, I tihink they're using an SHA style hash now, but I'm not all that sure since I don't know enough about the protocol internals as I should. Personally, I don't think they're documented well enough, so maybe I'll document them when I have the chance.  Anyways, time for bed, have to be at work early tomorrow. ;(
<!--break-->
oh yeah, i'm not #2 on the bugs list anymore. oh well. ;)