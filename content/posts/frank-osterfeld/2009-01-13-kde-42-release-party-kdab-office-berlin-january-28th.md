---
title:   "KDE 4.2 Release Party at KDAB Office Berlin (January, 28th)"
date:    2009-01-13
authors:
  - frank osterfeld
slug:    kde-42-release-party-kdab-office-berlin-january-28th
---
<a href="http://www.kdab.net/">KDAB</a> and the <a href="https://wiki.fsfe.org/FellowshipGroup/Berlin">FSFE Fellowship Group Berlin</a> invite you all to celebrate the 4.2 release with us.

<b>Date:</b> January 28th, 2009 (Wednesday)
<b>Time:</b> 20:00
<b>Place:</b> KDAB Office Berlin, Adalbertstr. 7/8, U Kottbusser Tor (<a href="http://maps.google.de/maps?f=q&hl=en&geocode=&time=&date=&ttype=&q=adalbert+str+7,+berlin&sll=51.124213,10.546875&sspn=14.085092,34.189453&ie=UTF8&z=16&iwloc=addr&om=1">map</a>)

If you want to join us, please send us a mail to <a href="mailto:kde-release-party@kdab.net">kde-release-party@kdab.net</a>, so we can plan accordingly.
<!--break-->
