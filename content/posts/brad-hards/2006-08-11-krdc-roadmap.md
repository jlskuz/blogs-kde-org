---
title:   "KRDC Roadmap"
date:    2006-08-11
authors:
  - brad hards
slug:    krdc-roadmap
---
KDE Remote Desktop Connection now has a roadmap for KDE4 development.

We still have a lot of work to do (well, just about everything on the roadmap :-)), but at least we have a plan for what that work is going to be.

If you have a specific need / problem, then please continue to use http://bugs.kde.org, but if you have a more generic idea request for KRDC, or you want to get involved in development, then feel free to drop by <a href="http://wiki.kde.org/tiki-index.php?page=Krdc+Roadmap">the KRDC roadmap</a> and wiki-away.

