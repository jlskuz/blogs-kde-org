---
title:   "OpenChange BoF - LCA 2010"
date:    2010-01-20
authors:
  - brad hards
slug:    openchange-bof-lca-2010
---
I'm not at Camp KDE, but instead at LCA 2010 (in Wellington, NZ).

Andrew Tridgell, Andrew Bartlett, Jelmer Vernooij and I will be running "birds of a feather" (BoF) sessions during the last part of the conference (Friday 22 January 2010 starting at 1430 in the "Civic 3" room, which is over in the Town Hall building).

The first part of the BoF session will focus on Samba 4 and its ability to replace (or work with) Active Directory. I've been working on an installer GUI for Samba4 and OpenChange, and if you are a sysadmin who might be interested in installing this stuff yourself, then you can influence the design of the tool.

There will also be some demos. 

If nothing else, you'll get to see Tridge hack, which is always fun. 

So come along (remember 1430 to 1630 on Friday), hang out, observe, contribute and enjoy!