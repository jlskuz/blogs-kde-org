---
title:   "Logitech mouse support"
date:    2004-09-27
authors:
  - brad hards
slug:    logitech-mouse-support
---
I've finally started work on the special support for Logitech mice (eg 400cpi/800cpi switching, wireless status reporting). I'm integrating it into the Mouse KCM (kdebase/kcontrol/input), with a conditional build on <a href="http://libusb.sf.net">libusb</a>.

The autoconf stuff is OK, but the automake substitution is borked. If anyone can see why this doesn't work, let me know?
<code>HAVE_USBLIBRARY=
if (test "$LUSBHEADER" == "yes" ) && (test "$LUSBLIB" == "yes" ); then
   AC_DEFINE(HAVE_USBLIBRARY, 1, Define to 1 if libusb is available)
   AC_SUBST(HAVE_USBLIBRARY)
fi
AM_CONDITIONAL(have_usblibrary, test -n "$HAVE_USBLIBRARY" )
</code>

<code>if have_usblibrary
USB_LIBADD = -lusb
endif
kcm_input_la_LIBADD = $(THEMEPAGE_LIB) $(XTESTLIB) $(LIB_KIO) $(LIB_XCURSOR) $(USB_LIBADD)
</code>

In the mean time, I just manually add -lusb to the end.

The other thing I need to figure out is how to manage the security side of things. The module needs to be able to write to the usb filesystem (under Linux, at least). Even getting status requires control transfers, which are a write. Is is just a distro problem to make sure that the mouse device gets to be owned by the user who is "logged in"? Or do I forget it, and make the kcm setuid root? Or do I need to write a helper daemon and communicate with that? Whatever I do, I am really sure I don't want to be responsible for the next KDE security advisory.