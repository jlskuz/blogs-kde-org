---
title:   "OpenChange / Akonadi talk available on video..."
date:    2009-02-28
authors:
  - brad hards
slug:    openchange-akonadi-talk-available-video
---
My talk from linux.conf.au 2009 is now available for everyone to see on video:
<a href="http://mirror.linux.org.au/pub/linux.conf.au/2009/Friday/72.ogg">http://mirror.linux.org.au/pub/linux.conf.au/2009/Friday/72.ogg</a>

The slides are also available in <a href="http://www.frogmouth.net/lca2009-hards.pdf">PDF</a> and <a href="http://www.frogmouth.net/lca2009-hards.odp">ODF</a>.
<!--break-->
There are a lot of other interesting videos also available - see 
- <a href="http://mirror.linux.org.au/pub/linux.conf.au/2009/Wednesday/">http://mirror.linux.org.au/pub/linux.conf.au/2009/Wednesday/</a>
- <a href="http://mirror.linux.org.au/pub/linux.conf.au/2009/Thursday/">http://mirror.linux.org.au/pub/linux.conf.au/2009/Thursday/</a>
- <a href="http://mirror.linux.org.au/pub/linux.conf.au/2009/Friday/">http://mirror.linux.org.au/pub/linux.conf.au/2009/Friday/</a>

There are still some in work - you might like to check back occasionally.

I'd like to say a huge "love your work" to all those involved in the conference, and especially to the AV team, for making this available.

[Also, hello Planet Linux Australia. For those I haven't met yet, I'm a KDE and OpenChange hacker, based in Canberra.]