---
title:   "Encryption support for okular OOo reader"
date:    2007-12-14
authors:
  - brad hards
slug:    encryption-support-okular-ooo-reader
---
You may (or may not) know that Okular can read the Oasis Open Document format (aka OpenOffice.org text format or <a href="http://en.wikipedia.org/wiki/Opendocument">OpenDocument</a>). It doesn't render the document exactly as oowriter would, but it is quite a bit faster to start up :-) I tried to open an encrypted document with it though, and it failed. A couple of evenings later, and I have a patch that "fixes" Okular.

The patch basically adds support for parsing the manifest file, and uses <a href="http://delta.affinix.com/qca/">QCA</a> to provide the necessary cryptographic primitives. QCA is supposed to be an optional compile-time dependency.

I don't think this type of thing really qualifies as a bug fix (and it does introduce quite a few new strings), so I'm not going to apply it so late in the KDE4.0.0 release cycle. 

I would appreciate some help with testing though. You can get the patch from the <a href="http://mail.kde.org/pipermail/okular-devel/2007-December/000251.html">okular-devel mailing list archives</a>.

It will only actually decrypt the file if you have built QCA from subversion, or have a beta3 (or later, but there aren't any) release of the plugins. Not having QCA or only having an old version of the plugins would still be a valid test though - the patch is supposed to deal with that sort of issue.