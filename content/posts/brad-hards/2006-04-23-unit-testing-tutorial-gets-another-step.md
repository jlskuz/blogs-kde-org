---
title:   "Unit testing tutorial gets another step"
date:    2006-04-23
authors:
  - brad hards
slug:    unit-testing-tutorial-gets-another-step
---
I've just added another section to an example-based tutorial I've been working on. The tutorial aims to describe what you need to know to be able to write unit tests for Qt and KDE, using the QTestLib test framework.

The latest section covers testing of QObject subclasses using signals and slots. Slots are easy (because they are really just normal methods that you can call using some special syntax). Signals requires a bit more work, although Qt provides the elegant QSignalSpy class which makes things look fairly neat. You can read the tutorial at http://developer.kde.org/documentation/tutorials/writingunittests/writingunittests.html

In the longer term, I'd like to use this as part of a on-line training course. More about that when I figure out whether it is going to work or not.

I also liked <a href="http://people.kde.nl/englich.html">Frans'</a> idea of using the material on http://developer.kde.org to write a KDE developer's book. Of course, my tutorials are already in docbook. Such an activity would also give us a chance to clean up and update the various tutorials to match the current state of the art in KDE.