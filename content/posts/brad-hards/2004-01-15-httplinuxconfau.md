---
title:   "http://linux.conf.au"
date:    2004-01-15
authors:
  - brad hards
slug:    httplinuxconfau
---
I'm at http://linux.conf.au in Adelaide. 

Yesterday I went to two tutorials - Keith Packard's talk on cairo ( http://www.cairographics.org ) and Malcolm Treddnick's talk on the Gnome Libraries. Cairo is a really interesting concept - lots of possibilities for 2D graphics rendering. I'll have to blog on that seperately.

By contrast to Keith's talk, Malcolm's talk was mostly a "these are all the libs" - more evangelism than content. I've heard of gtk, glib, libgnomeui and so on. So I wrote a kfile_plugin for the ILM EXR file format ( http://www.openexr.org ). This (along with the
image/x-exr.desktop file I wrote in the airport lounge waiting to fly to Adelaide - no translations of course) is a step towards solving bug:58170. I now know that the autoconf/automake magic is working, and can start working on the image display part. I have the kimgio outline, but it doesn't work yet.

I have a screenshot attached to the bug:58170 - see http://bugs.kde.org/attachment.cgi?id=4174&action=view

I've asked the OpenEXR devel list (and the original reporter) to provide feedback on the meta-info. If anyone here is into EXR, please let me know if you want any changes.

There were a lot of other discussions around the conference - one of the more interesting was a suggestion to relicense the X server code under a more restrictive license. The concept was that this would allow the incorporation of certain code (eg VNC server without the ugly screenscraping idea) and also would reduce the ability to fork the project.

BTW: This is the second version - not sure why the first version got lost