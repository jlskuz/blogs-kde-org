---
title:   "Progress on an Arthur backend for Poppler"
date:    2005-06-19
authors:
  - brad hards
slug:    progress-arthur-backend-poppler
---
I've had to travel for work, and during a particularly long flight[1], I managed to get stuck into a Qt4 renderer backend for Poppler[2].

I think I'm about a quarter of the way into it - my Arthur[4] backend can render some pretty complex PDF files[8], but the text/font handling is terminally broken, and I'm having good success with some images, but not with others. There are also some things (like patterned/tiled fill) that none of the backends currently appear to do completely, so I'm not sure how important they are. Certainly the next big step is to get the font selection and character positioning sorted out, so typical PDFs will run.

I'm keenly awaiting my fd.o CVS account to get some of this committed. Then I can put it up and starting getting some suggestions on stuff that needs to be reworked, and also perhaps get some help.

It was interesting to see how the new QPainter API turned out, and certainly it wasn't obvious exactly how I should pass around the QPainter and/or QPaintDevice to be used. However once I had an interim API done, the process to implement the functionality wasn't that hard. In particular, I noticed that the Cairo API and Arthur API have very similar levels of functionality. That helped a lot, because I could refer to the (in work) Cairo backend while I figure out something on the Arthur version.  There are also a couple of noticeable cases[16] where applying a bit of AntiAliasing made everything look a lot better. 

[1] Sydney->Los Angeles, just over 12 hours.
[2] Poppler is a PDF library, forked from xpdf. See <a href="http://www.poppler.org">the web site</a>
[4] Arthur is the name of the new Painter system in Qt4 - see <a href="http://doc.trolltech.com/4.0/qt4-arthur.html">http://doc.trolltech.com/4.0/qt4-arthur.html</a> for the <http://www.trolltech.com>Trolls'</a> overview of it.
[8] I'm doing tests with some of the samples from Michael Still's <a href="http://www.stillhq.com/pdfdb/db.html">PDF Database</a>. The interesting pattern from the first page of the PDF at http://www.stillhq.com/pdfdb/000002/info.html made for an interesting test case.
[16] See <a href="http://www.stillhq.com/pdfdb/000001/data.pdf">http://www.stillhq.com/pdfdb/000001/data.pdf</a> for one example.