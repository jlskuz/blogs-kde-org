---
title:   "In bad mood and annoying people"
date:    2003-08-15
authors:
  - uga
slug:    bad-mood-and-annoying-people
---
Yeap, I was in really bad mood today, and I think I even managed to annoy some koffice developers today :-(  Sorry Beineri! I hope the comments I added in the lists will help fixing the issues in KPresenter.

Anyway, the work in Krecipes is progressing well. The help from the new recruits Jason & Cyril is being very useful, although Jason got me out of my nerves today undoing some of my commits..... Well, calm down... ;-)
