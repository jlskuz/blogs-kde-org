---
title:   "New Oxygen category in bugs.kde.org"
date:    2008-01-22
authors:
  - boemann
slug:    new-oxygen-category-bugskdeorg
---
I've just created a new product called "Oxygen" in bugs.kde.org.

It has 4 components: style, window decoration, icons and sound. So please use these in the future if you know the bug belongs there. I'll be adding known bugs from our wiki in the coming days

I'll use this opportunity to also ramble a bit about bug reports. Because though in general we love reports and wishes there is one kind that isn't very useful, and that is the category I'd call "I don't like the look of.."

Graphics and sound is a very subjective matter, so bugs.kde.org just isn't the place for this. And the reason is quite simple: There will never be an agreement.

Instead add comments about like/dislike to the blogs of our artists when they present new stuff. But please do it with taste and respect as you would like to be treated yourself.
<!--break-->