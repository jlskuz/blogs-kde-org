---
title:   "How to make a new layout engine for Calligra Words in 4 months "
date:    2011-05-18
authors:
  - boemann
slug:    how-make-new-layout-engine-calligra-words-4-months
---
First you need a lot of dedication and time. I've spent like 15 hours a day every day since February, and since April Sebastian Sauer have spent full days on it as well. It also takes a lot of spare time plus someone to pay for my living, and this is where Nokia comes in hiring my employer KO GmbH to let me do this.

I should also thank Pierre, Pierre, Gopala, Stuart, Lukas, Elvis, Matus, Matus, Thorsten, Boudewijn and Pavol for their contributuions to Words in this period.

What we have achieved is really really great. A whole new layout engine, that is close to a complete rewrite of the old junk that was in KWord. We still use the same loading, saving and editing code, but what you see on screen is all displayed by new code. It benefits from the old code giving us some valuable lessons on how not to do it, but we have also copied small chunks of code here and there.

I started out with disabling Words completely and for the next month or so I experimented with moving code around, changing dependencies, and so on, so that we would have a dedicated library for textlayout. It took a couple of tries, in more than 50 local branches, but in the end I got it right and was able to move on. At this point the textlayout library did nothing, but all the old applications were able to compile, but obviously not show any text.

The next month I made rapid progress, thanks to the sound design I had created the previous month. Quickly it was able to layout plain text, with fonts and line spacing etc. And although it was designed with pages in mind, Words was still completely disabled from compilation at this point. Then tables were added and again thanks to the sound design it only took me a little over a month. Okay to be fair I did reuse the old <a href="http://blogs.kde.org/node/4048">borderdrawing</a> code I carefully created 1½ year ago.

Along came April and the Calligra Sprint and Sebastian came back from his long vacation and within days he had gotten Words compiling again and started using the new textlayout library. And this would be the first time we would see more than one page being drawn by the engine.

The next month we fixed bugs and added small features, Thorsten started throwing hundreds of test documents at the engine revealing many issues which we solved, almost as quick as he found them, and special attentinon was put to testing the Stage application which also uses text rather heavily. I didn't want to break Stage but breaking Words was acceptable to me Sebastian and my comaintainter Pierre, so the decision was made to merge our branch into master. That sparked many of the people I mentioned in the beginning to begin contributing fixes, and reporting bugs, so it turned out to be a good decision to merge as we have moved forward ever since at an incredible pace. At this point I added text-runaround back in (which was almost a verbatim copy af the good work by Pavol Korinek). Adding anchoring proved a bit more problematic with lots of endless loops needing to be fixed.

And in the middle of this month we also tagged a snapshot alpha release. Which if you try out will see is taken right in the middle of the anchoring work. So your milage may vary and we have not been idle since then so many bugs are already fixed and will look even better in the next snapshot in a month or so.

The next many months will be just as exciting where the focus will shift to reworking the ui.

As for the wider topic of Words in general I've added buttons so it's now possible to manipulate tables from the ui. We also merged a huge work by Ganesh who paid by nlnet implemented the next generation changetracking which will probably end up being the new standard in odf.

So stay tuned for a the snapshot release any day now.
<!--break-->
