---
title:   "KWord font metrics"
date:    2010-07-16
authors:
  - boemann
slug:    kword-font-metrics
---
So it's no secret many people are not liking the font rendering in KOffice (they often say KWord, but it applies to all of KOffice). As it has been suggested a couple of times you need to turn the global font hinting down to slight to improve the rendering. I know this is unacceptable to some as they feel the system fonts look worse. Now, hinting or not is a religion and personally I like no hinting, but I respect that other people feel differently. So what we need is a way to make font rendering in KOffice only slightly hinted while respecting the users wishes for every other thing. This is however not possible in Qt as we speak, but it might be in the future.

Right now what we can do is improve the font metrics, meaning the text on screen will look like what you see when printed and like it looks in various other word processors. This is is not about how the letters look, but about how they are spaced. But for your information, in the below screenshots, the hinting is set to slight (and obviously the subpixel rendering is off as all people should do when creating screenshots)

First a shot of how KOffice currently looks:

<img src="https://blogs.kde.org/files/images/kofficefont1.png">

And then a shot of how KOffice will look with improved font metrics. Notice, how the letters are more evenly space in the word "look", and how the line length scale proportionally with the font size. To me this a big win. On the downside the distance between the IIIs are a bit more uneven (but really who writes a lot of IIIs)

<img src="https://blogs.kde.org/files/images/kofficefont2.png">

So what do you think. Is this an improvement or not. It for sure is an improvement of WYSIWYG (interoperability with other word processors, printing and export to pdf). Now the question is if it has a downside to the editing user. So what do you think?
<!--break-->
