---
title:   "News Feed now as RSS"
date:    2006-08-05
authors:
  - blackie
slug:    news-feed-now-rss
---
It didn't take long from me setting up my RSS reader til KPhotoAlbum's news section could be read using such a guy,

Now point your RSS reader at http://www.kphotoalbum.org/news.rss
