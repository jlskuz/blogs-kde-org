---
title:   "KPhotoAlbum splash screen contest over!"
date:    2006-09-28
authors:
  - blackie
slug:    kphotoalbum-splash-screen-contest-over
---
The KPhotoAlbum splash screen contest ended today, and a winner has been found, namely Jaroslav Holan's submission. Congratulation and thanks for all the nice splash screen submitted.

<img src="http://www.kphotoalbum.org/splashscreen/9.jpg"/>
<!--break-->