---
title:   "The Linux Show"
date:    2003-12-10
authors:
  - aseigo
slug:    linux-show
---
The Linux Show went rather well tonight. George was a great co-guest, and I think we handled the questions well in general. We maintained a positive and factual yet enthusiastic air throughout. Even the SUSE/Novell/Ximian and User Linux queries went well. They had a record number of people attending tonight; even the IRC channel had ~80 people in it (they usually only have ~50). So, they were happy with it too. Good fun and a great way to get KDE some positive exposure. I'm feeling rather spent now, though. Time to find some wine and a good book.<!--break-->