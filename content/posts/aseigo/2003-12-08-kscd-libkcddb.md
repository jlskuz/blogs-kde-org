---
title:   "kscd, libkcddb"
date:    2003-12-08
authors:
  - aseigo
slug:    kscd-libkcddb
---
working on kscd... fixed several bugs over the weekend... only several more to go =P

auditing sucks. but it's so necessary. i read over libkcddb tonight and caught several issues, including classes used in a QValueList that didn't have copy ctors or operator=s and a potential buffer underrun. kids, indexing with ints that can be negative isn't cool. anyways. auditing sucks. it's tedious, boring and not much fun. but it's just so easy to miss things in your own code, that having someone else look through it can make quite a difference.

p.s. i can't believe how expensive out-of-print boardgames are.<!--break-->