---
title:   "Of KControl and Other Needs"
date:    2003-07-12
authors:
  - aseigo
slug:    kcontrol-and-other-needs
---
Alright, this time less philosophy and more "what I'm doing and thinking about doing, as if you care" ...
<!--break-->
I am nearly ready to start working on some major KControl issues, but am stalled on a couple of fronts. Hoping inspiration (and some bits of code i'm waiting on) will arrive soon.

I've got a couple of patches to some smaller KDE apps that were sent to me and then abandoned. I'll have to fix them up some, and just discard others. Not sure what prompts someone to start in on something and then just leave it halfway?

Someone on theDot went on about Nat Friedman's Dashboard app. It would be pretty trivial to implement in KDE, especially using DCOP signals. I don't know if it's worth it though. Personally I probably wouldn't use it since web shortcuts and a well chosen set of apps semicoherently arranged on my virtual desktops makes it trivial to pop around. It may make an interesting addition to Klipper. I'd probably implement it using passive popups or a perhaps an applet. This biggest mistake currently in Nat's UI design is that it takes up so much friggin' space that it had better be damn useful and replace the need to launch most applications.

I also need to fix the applet handle menu to allow merging in an applet-specific menu. And I'm continuing to mess around with Konqi menu stuff.

Not that anyone really cares. At least it's fun for me... =)