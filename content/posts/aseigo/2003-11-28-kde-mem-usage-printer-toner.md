---
title:   "KDE mem usage, printer toner"
date:    2003-11-28
authors:
  - aseigo
slug:    kde-mem-usage-printer-toner
---
Crazy things happening around Mahlah's computer this week. First she complains to me that her KDE (3.1 on SUSE 8.2) is acting up: it's slower than usual and rather flaky. It started up after she rebooted two weeks ago. I went downstairs to check it out and sure enough, it's much slower than it should be. She's only got 8 konqi windows open, kmail, a konsole with three tabs (one of which was ssh'd into a machine to irc from a screen session) and some text document open. That shouldn't slow the system down. The CPU isn't showing much usage but there's a distinct slowness and disk churning when switching from the desktop with all the browser windows on it to the one with kmail on it. So I take a look at the output of `free` assuming something must have gone run-away on the mem usage. To my astonishment I see that, for whatever reason, the system thinks there's only 32MB of RAM! There's many times that amount actually installed, but on the last reboot the system took a left turn somewhere and figured it would only use 32MB. It was only 8MB into swap, despite running sshd, cups, apache and the afformentioned KDE session. No wonder it was "slower than usual"! I'm just amazed that it was usable at all. 

After a quick reboot (this time the kernel decides to use all the installed RAM), Mahlah tells me that our printer (a networked HP 4050N) is low on ink. Indeed, the little LCD display has the worrying "Toner Low!" message on it. So I have it print out a config page, which contains the toner level on it:

[image:247]

Yep, we're low. Or... are we? Sure, the toner cartridge is ~1.5 years old but something in me (probably the cheapskate) says it shouldn't be so low. Then I remember a trick I had employed with great success on the previous cartridge: I pop out the toner cartride and smack it soundly all around the sides and top, then slide it back into the guts of the printer. A few seconds later the printer is powered back up and I have it print out a config page again and here's what it said about the toner level:

[image:248]

Not bad for a couple smacks.