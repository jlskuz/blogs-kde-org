---
title:   "booooring"
date:    2003-08-12
authors:
  - aseigo
slug:    booooring
---
so after attacking the clock applet i convinced myself to fix another long time annoyance of mine: no way for an applet to pop a custom menu in the applet handle menu. well, no more! now i'm going through all the applets in CVS and fixing 'em up to Do The Right Thing(tm). not exactly exciting, though i'm sure all 2 people who notice in the next release will be happy about it. at least they better be. ;-)

once i'm done the 15 pieces applet i'll send a patch to Elter for his kicker applet writing tutorial. then maybe i'll get to working on the KMail reader window and/or the address widget. or maybe i'll finally add that Autostart manager to the session manager control panel.

in any case, Tabasco kicks ass. in case you were wondering.