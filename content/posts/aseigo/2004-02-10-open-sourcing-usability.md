---
title:   "open sourcing usability"
date:    2004-02-10
authors:
  - aseigo
slug:    open-sourcing-usability
---
usability is one of the last avenues of software development to have it's methodologies challenged/changed using open source mechanisms. even many practicing/promoting usability in design in Free Software projects usually attempt to do so using draconian and/or closed methods. let's stop the insanity and open source usability. and here's your chance to join the revolution. read on to see how you can change the face of usability studies by doing what so many have jabbered on about but so few (if any) are actually doing. and it'll only take a few minutes of your time.
<!--break-->
i'm studying usage patterns of Konqueror's toolbars when in web browser mode. i've got a nice little spreadsheat (in KSpread, of course) that holds the statistics and provides some nice sums. i need more stats. from real users, of all sorts: from beginners to advanced. 

if you would like to contribute data, please email me (aseigo at kde dot org) the following information: user category (beginner, advanced, expert), browser used (Konqueror preferred, IE and Opera acceptable, Mozilla and Safari are almost useless since their toolbars are devoid of options), and a list of which elements on the toolbars they use hourly, daily, weekly or less often during web surfing sessions. you can include yourself, but i'd encourage you to go track down one other person and get the information from them as well.

there is no hurry to complete this (well, a couple months at most, i suppose) so please take your time and allow yourself a week or more to collect _real_ data. once i have "enough" data (whatever i deem that to mean), i'll publish the URL where the spreadsheet can be found.

this is not a voting system. this is a means to collect data. it is an experiment...

... and an opportunity to participate in change.