---
title:   "Impressed by openSUSE 10.3"
date:    2007-10-13
authors:
  - amantia
slug:    impressed-opensuse-103
---
openSUSE 10.3. I couldn't follow its development as I did with previous releases and tested only after the final version appeared. First I upgraded my desktop from 10.2 and it wasn't a pleasure as it made the system unbootable and I had to fix using a rescue console. But after having it running I was quite satisfied with it. Software management has improved (but still not fast enough in my opinion), the possibility to add the community repositories from YaST is nice, as it is the search page and the one click install (well one click on the webpage, several clicks later). I found some bugs and annoyances and I reported them, in the hope that they will be fixed in an update or in future releases.
I also downloaded the 1 CD KDE version and decided to try it on my laptop. This laptop run Kubuntu since the beginning of 2006, when I switched it from SUSE in order to try it and learn it. Kubuntu was OK, but I missed some things, like YaST, I was not happy with all the modifications made to KDE and suspend to disk worked, but strangely (took a long time to come back and had to play with CTRL-ALT-Fx to get back the X screen). But I was happy with the boot speed, I even <a href="http://blogs.kde.org/node/1739">blogged about it</a>.
 So I replaced Kubuntu with openSUSE 10.3. All I can say is wow. 
This time the installation went fine as it was a clean install, not an upgrade. The system feels quite good for this computer (PIII 550Mhz laptop, 192MB RAM, Trident video card with shared memory, 30GB IBM HDD). Compared to my previous tests only the HDD is different, but works at about the same speed (13MB/s).
 Booting until the KDM screen takes only 45 seconds and until I can use my system is 1:15seconds. Yes, the system is usable at that time when my optimized Kubuntu did not finish loading the login screen. The only optimization I did was to disable services I don't need from the current runlevel and disable arts. 
 Now I don't want to say openSUSE is better, faster than Kubuntu. The new Kubuntu might also be just as fast and good. What I'm saying is that I'm impressed by the speed of the latest version of a Linux distribution. Software tends to be slower in time, while this time it became faster. 
 I also know that booting speed is not enough or really relevant. Right now all I can say is that for general use (not for development) the system feels to be responsive and fast enough.
 Suspend to disk takes 25 seconds, resume 36. That's quite OK, I didn't meassure yet, but I believe it takes just as much on my desktop system is which is very fast compared to this laptop.
 I had until now two issues with the laptop:
- I had to add acpi=force to the kernel parameters, otherwise shutdown/suspend would not turn off my laptop
- <strike>for some reason when I start a konsole I get a bunch of "bash: /dev/null: Permission denied" messages. I have to report this. </strike>  EDIT: Turned out that this is already fixed, I just had to install the available updates.
- Install Software prefers xdg-su, altough kdesu is installed. Starting the complete YaST uses kdesu though.

More figures:
- OpenOffice took 40 seconds on first start, 20 seconds on the second
- YaST software management: you need to wait almost 3 minutes before you can use it. :(
