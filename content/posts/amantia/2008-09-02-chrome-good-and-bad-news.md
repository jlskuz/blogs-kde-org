---
title:   "Chrome: good and bad news"
date:    2008-09-02
authors:
  - amantia
slug:    chrome-good-and-bad-news
---
Maybe you already know, maybe you don't: Google created its own browser, called Chrome. The good is that it is based on WebKit, thus contains KDE technology. That's is another recognition for the work of KDE developers. The bad is that they mention Apple's WebKit and nothing about KDE/khtml. Sad. :(
See http://googleblog.blogspot.com/2008/09/fresh-take-on-browser.html .