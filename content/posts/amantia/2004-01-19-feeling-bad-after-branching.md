---
title:   "Feeling bad after branching"
date:    2004-01-19
authors:
  - amantia
slug:    feeling-bad-after-branching
---
We've reached an important point: branching the CVS for 3.2. After complaining a little that things didn't went as smooth as they could I've started to work again. Now that HEAD is open and we have a 3.2 branch, it was time to get rid of development branches. So let's merge them into HEAD. I've tried to do is carefully. Made a copy of my local HEAD checkout, merged (using CVS merge) the development branch into that. So far, so good. Compiled, committed that one. Great, it worked perfectly, I only had to fix minor issues. Committed also those ones. According to the kde-cvs mails they went to HEAD. Some minutes later I've changed the formatting of a file and,
surprise, the mail to the kde-cvs indicated that it went to the 3.2 branch. I've become suspicious and verified my local copy. It looked that some files have the KDE_3_2_BRANCH tag and some other don't have. This also means that I committed some files to HEAD and some to branch. Great. Not that I break every freeze rule, but for sure it won't even compile. I've tried to figure out what has happened, and the only thing I can imagine is that the connection went down (by itself, or I canceled) while a cvs command was not completely finished, and this messed up the local copy. This may happen, as I usually connect->commit/update->disconnect. Many times I also download my mails during this time and sometimes I disconnect after the mails are downloaded (especially if there are lot of mails) and forget to check that the commit/update was finished. Or the merge itself caused the problem, I don't know... Or the cvs server had problems. 
 After struggling a while and trying to figure out what can be done, I've decided to repair thing manually. Namely update my local branch copy (which wasn't easy either, as the updates were often interrupted (signal 9?), although the connection was up and running and I haven't killed cvs) to the place where cvs was branched and commit this code again to the branch. This wasn't so simple, but I could do: locally. I couldn't commit, now due to some locking problems. This was the point when I gave up. It was around 2 AM. 
 Now it's morning: I couldn't even update from CVS. I get 
cvs [update aborted]: connect to cvs.kde.org(195.135.221.67):2401 failed: Connection refused

Great. :-( I hope everything will be back and restored during the day. I have the correct versions locally, now I just have to put to the server somehow.
I feel bad. I feel bad because I complained and now I was the one who broke things. Not intentionally and maybe due to independent causes of me, but the damage is there. So don't update Quanta now... HEAD is just as broken as BRANCH...