---
title:   "KDE code changes for ARM"
date:    2008-09-16
authors:
  - tstaerk
slug:    kde-code-changes-arm
---
When I heard that Nokia was giving away <a href=http://en.wikipedia.org/wiki/N810>N810 devices</a> on aKademy, I wondered how long it would take till I saw the first code changes. So, code changes to support the ARM architecture or the use of KDE on a PDA. Today I saw three (and wrote two of them):
<ul>
<li><a href=http://websvn.kde.org/?view=rev&revision=850153>Require at least Strigi 0.6.0 (SVN trunk) because a BC change was needed in libstreamanalyzer to support ARM architecture.</a></li>
<li><a href=http://websvn.kde.org/?view=rev&sortby=date&revision=861442>Allow configuration for a PDA</a></li>
<li><a href=http://websvn.kde.org/?view=rev&revision=861663>Show/Hide the searchbar immediately</a></li>
</ul>
I really like this - the idea from Nokia was super, the device is super, and soon there shouldn't be any reason not to run KDE on it!