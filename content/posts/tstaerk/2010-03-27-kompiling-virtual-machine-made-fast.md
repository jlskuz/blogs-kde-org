---
title:   "Kompiling in a virtual machine made fast"
date:    2010-03-27
authors:
  - tstaerk
slug:    kompiling-virtual-machine-made-fast
---
<p>As you may have followed, I <a href="http://blogs.kde.org/node/3675" class="external text" title="http://blogs.kde.org/node/3675" rel="nofollow">compiled my KDE in a virtual machine</a>, <a href="http://techbase.kde.org/Getting_Started/Build/KDE4/on_virtual_machines" class="external text" title="http://techbase.kde.org/Getting_Started/Build/KDE4/on_virtual_machines" rel="nofollow">described an example how it goes</a> and even <a href="http://www.staerk.de/thorsten/index.php/My_hobby_benchmarks#Results" class="external text" title="http://www.staerk.de/thorsten/index.php/My_hobby_benchmarks#Results" rel="nofollow">compared compile times in a mini-benchmark</a>. I stopped using a virtual machine later because VirtualBox only gives you one virtual CPU, QEmu as well, and VMware Server only two. The good thing is I found KVM virtualization and learned <a href="http://www.linuxintro.org/wiki/Kvm" class="external text" title="http://www.linuxintro.org/wiki/Kvm" rel="nofollow">how to set it up</a>. It is a bit tedious and you will have to create a network bridge after every reboot, but the result is astonishing - I could compile Qt 4.6 in 20 minutes instead of 80.
</p>
