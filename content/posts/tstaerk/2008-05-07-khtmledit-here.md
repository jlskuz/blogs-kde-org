---
title:   "khtmledit is here"
date:    2008-05-07
authors:
  - tstaerk
slug:    khtmledit-here
---
I have had so much struggle finding a decent html editor that I finally wrote one on my own:

<ul>
<li>mozilla composer cannot handle the fish://-protocol</li>
<li>openoffice dito</li>
<li>quanta crashes when it sees me</li>
<li>I finally started using <b>kword</b>, which cost me several months of fighting and finally even <a href="http://websvn.kde.org/branches/koffice/1.6/koffice/filters/kword/html/import/">contributing code</a>, but there are too many problems for using it where I work.</li>
</ul>

OK, so I started khtmledit - the wysiwyg html editor that has everything you need (if it does not have anything - you don't need that ;) First, here is how it looks:
<img src=http://www.staerk.de/files/khtmledit.png />

The following was important for me during design:
<ul>
<li>Work with the fish:// protocol so I can finally change pages directly on my server. No more struggling with up/downloading</li>
<li>Have <a href=http://en.wikipedia.org/wiki/Wysiwyg>WYSIWYG</a> so I see if I run into problems.</li>
<li>Have a plain text <a href="http://en.wikipedia.org/wiki/GUI_widget">widget</a> so I can do any change allowed by html</li>
<li>Follow the <a href="http://en.wikipedia.org/wiki/80/20_rule">80/20 rule</a> - 20 percent of your actions are done in 80% of the cases, so make them easy</li>
<li>Make that <a href="http://www.staerk.de/thorsten>I</a> can use it</li>
</ul>

I like my new editor a lot, however it requires some work. E.g. the fish://-protocol does not seem to work with KDE 4. Anyway, this is the first editor I can use at work. Download it at http://www.staerk.de/thorsten/index.php/Khtmledit