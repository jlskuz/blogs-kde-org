---
title:   "only kdevelop in a virtual machine"
date:    2008-09-10
authors:
  - tstaerk
slug:    only-kdevelop-virtual-machine
---
Many of us know this: You are on KDE version "from yesterday" and suddenly, everything breaks. Maybe someone broke the kompile or it is just a bunch of bad code that went in before your checkout and prevents the window manager from starting.

You learn from this mistake, you no longer install to /usr, but you <a href=http://techbase.kde.org/Getting_Started/Build/KDE4>create a dedicated user for your KDE programming fun</a>. Soon, the next painpoints arise:
<ul>
<li>you cannot test the display manager</li>
<li>you cannot test the latest system-wide dbus stuff</li>
<li>the complexity is just annoying</li>
</ul>
I kicked both ways to program KDE. I am now using a VMWare Server virtual machine for my programming. Got rid of all problems above. And the best is: I just broke KDE. No problem! I just revert back to the latest snapshot. I am so happy I took this way!