---
title:   "Dead Trees"
date:    2006-05-08
authors:
  - scott wheeler
slug:    dead-trees
---
After seeing Seele's <a href="http://weblog.obso1337.org/?page_id=317">list of books</a> I decided that I liked the idea.  There's something in my personality that likes collecting physical media -- books, CDs, records.  It's obvious when I move and I have a tiny amount of furniture, a box of clothes, a few things from the kitchen and then like 10 boxes of books and a pile of pro-audio gear.

I went through my Amazon list of orders and my bookshelf to figure out 2005.  2006 has been a slow year on reading thusfar, but to be fair to myself I've spent a lot of time reading bits and pieces of non-fiction and scientific papers.

Here's <a href="http://developer.kde.org/~wheeler/books.html">my list</a>.  Since I like the idea of tracking my reading, I'll probably keep it up to date as I finish things.

I've been sick (flu) for the last few days, so I've been reading a good bit again.  Unfortunately being sick meant missing out on LinuxTag for the first time in five years.  I've done a little coding, but that's mostly been on some local hacks that I'm kicking around.  Most the time I've been holed up in bed.
<!--break-->