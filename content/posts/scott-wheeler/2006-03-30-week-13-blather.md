---
title:   "Week 13 Blather"
date:    2006-03-30
authors:
  - scott wheeler
slug:    week-13-blather
---
So, in typical blog-o-riffic fashion, since I seem to be back in the world of the blogging, here I go with a set of largely unrelated KDE/geek-ish notes:

<ul>
<li>Have a HDD about to die.  200 GB.  Mostly media.  Doing a bad cluster scan on it took about 18 hours.  At then end what did I know?  That the files that I already couldn't read are corrupted.  Woohoo.</li>
<li>I've taken three days off just to relax, watch a few movies, do a bit of coding.  First time I've used vacation days for such in a long time.</li>
<li>Another thing that I haven't done in a while -- just picked something annoying me <a href="http://lists.kde.org/?l=kde-core-devel&m=114368252628101&w=2">and fixed it</a>.  I've hated it that every time I go to empty the trash on my desktop that the right mouse menu takes like half a second to come up, so I implemented menu caching and preloading.  Seems to work well; we'll see if I can get it into the (frozen) 3.5 branch.</li>
<li>The kde-usability list seems to be on the mend.  There's been a concerted effort in the last couple of weeks to try to give the list a bit of direction (and less of a comment box / peanut gallery) and it seems to be working a little.  Let's hope for the best.  I've written up some guidelines and run them by the usability folks, and those will probably go live in the next couple of days.</li>
<li>I've moved the whiteboard back into the livingroom.  Yes, I own a 90 cm x 120 cm whiteboard.  And it's now in my living room again.  I've been coding on a few prototype sorts of things that will probably at some point find their way into KDE and sometimes you just need space for sketching and I'm definitely not a UML man.</li>
<li>During the time off I watched a few films.  <i>Crash</i> was disappointing; it helped to once again invalidate the Oscars in my view.  It's a collection of semi-contrived racial memes with a conflict, transposition, redemption, rinse, repeat cycle to it.  Afterwards I watched <i>Bulworth</i> again for the first time in years; <i>much</i> better commentary on race and politics in the US and still one of my favorite films.  <i>Jackie Brown</i> disappointing too, but mostly because I'd seen all of Tarantino's other films.</li>
</ul>

This concludes the test of the emergency blather system.  Had this been an actual emergency, well, you hopefully wouldn't be reading my blog.  ;-)
<!--break-->