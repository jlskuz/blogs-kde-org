---
title:   "On Being Unresponsive"
date:    2006-08-23
authors:
  - scott wheeler
slug:    being-unresponsive
---
Since I'm on a blogging kick, here we go again:

There's a hard balance in the Open Source world in answering "I'm interested in...but where do I start?" mails.  I think one thing that invariably people that write them don't realize is that you're doing really good if 10% of those turn into contributors.  10% actually sounds way too high.  2% is probably closer.  Actually, thinking about it, I've probably written a few hundred of those and I can only think of maybe 3-4 that translated to patches coming in.

And they take a long time to write.  You always would like to tailer them to the people writing you -- to their questions, interests, skills.  But at the end of the day, they take a long time to write and it's hard to know when it's worth it to spend that time.  I'd guess a good responce is 20-30 minutes.

As a result, sometimes they just don't happen.  Sometimes they're truncated and rude seeming to those showing interest.  If you've ever gotten one of those mails and thought, "Wow, F/OSS developers are asses."  Really -- we're not.  It's an efficiency thing, nothing personal.

The simple fact is, the more general the question, the less likely it is to turn eventually return as code.  I think this is mostly because it can be hard to discipline yourself to work on something even when you know exactly what you'd like to do.  The inquiries that are most likely to be fruitful are generally the ones with pretty specific questions -- people that are already digging into stuff and are stuck somewhere.

So, if you're stumbling across this and have thought about sending one of those questions, my advice is dig.  See if you can find stuff and if you can't, write and ask where to look.  That's usually easy to answer.  If you get stuck, send another quick question, those are probably easy to answer to.

I went back and looked at some of the people that I have turn into useful contributors and also noticed something in common -- most of their first mails had patches attached.  Often they were wrong, but it's a pretty clear indication of, "I'm serious about contributing and I've already started familiarizing myself with the code."  F/OSS always needs help; it's just sometimes hard to see who's serious about it and who just like the idea of helping.  You, dear readers, can make that easier to see.  :-)
<!--break-->