---
title:   "Nicer Background Image Selection"
date:    2004-05-28
authors:
  - scott wheeler
slug:    nicer-background-image-selection
---
Ok, I got tired of the old file-name based background image selection, so I hacked up something to use the (first line of) comment in the JPEG meta data.

[image:477]

Nothing too exciting, but now the selection looks a bit nicer.  At some point it might be nice to actually have desktop files for these things so that the names could be translated, but I'm too lazy for that at the moment and this is a step in the right direction.  :-)