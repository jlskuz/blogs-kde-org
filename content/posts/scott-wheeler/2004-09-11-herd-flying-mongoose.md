---
title:   "off, like a herd of flying mongoose"
date:    2004-09-11
authors:
  - scott wheeler
slug:    herd-flying-mongoose
---
Ok, so as has been mentioned a handful of times, I'm headed to Chile for a while.  I'm looking forward to it and my <a href="http://www.kde.cl/index.php?module=pagemaster&PAGE_user_op=view_page&PAGE_id=5">talk</a> there (summary is slightly out of date, the talk won't be identical to my aKademy talk).  See everyone in October!
<!-- break -->