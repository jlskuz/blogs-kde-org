---
title:   "Qt 4 - \"Interview\""
date:    2004-08-11
authors:
  - scott wheeler
slug:    qt-4-interview
---
Ok, so Qt 4's listview / table / iconview / listbox replacement is called <a href="http://doc.trolltech.com/4.0/interview-overview.html">"Interview"</a> collectively.  I started playing around with it last weekend and well, there's some good and some bad.  The good is that at least from an initial take on things the performance seems much better.  This could just be the double buffering tricking me and making it look smooth since I haven't actually done any really intensive tests on it, but it certainly feels more responsive.

On the other hand, wow -- it's pretty hard-core over-designed.  Even for me -- and note that I've done most of the KListView maintenance for the last year or two -- it was really difficult to get a handle on things.  I spent about two hours just to produce <a href="http://ktown.kde.org/~wheeler/images/qt4-listview.png">this</a> which still doesn't work properly.  It requires reimplementing an abstract base class, implementing 5 pure virtual methods, and coming up with some sort of content mapping scheme.  I still haven't figured out list headers, full row selection -- not to mention adding feature like we have now with row highlighting and searching.

The API seems pretty powerful, but it's going to alienate a lot of the "show these ten items in a list" guys.  Also it seems like a pretty poor decision to have reused the class name QListView, when in fact a QListView is just a view type that seems to roughly correspond to an iconview currently.  There are no columns, column headers, etc.  It would have been better to just not have a class by that name.  I blindly assumed that it worked something like a listview does currently.

Anyway -- I'll pull together a semi-cohesive rant -- once I've got a bit more figured out -- and send it to the Qt 4 feeback list.  Failing that I'll just corner one of the Trolls at aKademy and see if I can talk some sense into them.  ;-)