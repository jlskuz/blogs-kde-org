---
title:   "The In-Good-Faith License?"
date:    2006-08-19
authors:
  - scott wheeler
slug:    good-faith-license
---
I've got some code around that I'm thinking about shuffling a bit and putting out in a library.  But I can't find the right license.  I basically want to say, "Try to give back any changes that you make in a sufficiently demonstable way."  (i.e. patch to the publicly archived project list with a template of information that needs to be there)

Requirements for source distribution, a la [L]GPL, aren't really all that effective in actually making projects move forward and  they're annoying for users of the library.  At best, you end up with improved source on some random server or CD somewhere and the (proprietary) user is forced to distribute the library sources for.  Despite RMS's take on the issue, I don't personally think that does users a lot of good.

What I'd like is to get the changes -- a patch, basically, in a good faith effort to contribute them back and for that to be an acceptable substitute for source distribution.  What I haven't seen is a pre-existing license for that.  LGPL is cumbersome for proprietary use.  BSD is too permissive.  Artistic comes pretty close but then says, "Or you can just change the name."

Thoughts?

(Note:  Anything that I would release on such a license would be dual-licensed with one of the more vanilla licenses like LGPL, so there wouldn't be any license conflicts.)
<!--break-->