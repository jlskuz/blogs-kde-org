---
title:   "Visual Assault"
date:    2006-03-03
authors:
  - scott wheeler
slug:    visual-assault
---
<b>Pretty Things</b>

One of my coworkers (Thorsten -- also does some work on KArm) saw my background today and suggested that I should put some of those that I use (which are generally from my own photos) up online.  I've since obliged him.  The thumbnails from a couple of my favorites are below:

<a href="http://developer.kde.org/~wheeler/images/wallpapers/Ephesus_at_Dawn.jpg"><img class="showonplanet" src="http://developer.kde.org/~wheeler/images/wallpapers/thumbs/Ephesus_at_Dawn.jpg"></a> <a href="http://developer.kde.org/~wheeler/images/wallpapers/The_Other_Road_Ahead.jpg"><img class="showonplanet" src="http://developer.kde.org/~wheeler/images/wallpapers/thumbs/The_Other_Road_Ahead.jpg"></a> <a href="http://developer.kde.org/~wheeler/images/wallpapers/Mountains_of_Mendoza.jpg"><img class="showonplanet" src="http://developer.kde.org/~wheeler/images/wallpapers/thumbs/Mountains_of_Mendoza.jpg"></a>

The 21 that I picked out as my favorite background-ish photos are <a href="http://developer.kde.org/~wheeler/wallpapers.html">here</a> (notably not my favorite photos, but these mostly work as backgrounds).

<b>Ugly Things</b>

<a href="http://developer.kde.org/~wheeler/images/juk-first-qt4-compile.png"><img src="http://developer.kde.org/~wheeler/images/juk-first-qt4-compile-thumb.png"></a>

JuK actually sort of compiles against Qt 4 now, but it ain't exactly pretty.  I did kind of a marathon of commits last weekend to synch up the changes from the 3.5 branch to trunk and then worked towards getting it to build.  David came and finished things off the next day.  It still doesn't actually do anything interesting, but it's encouraging seeing it run.  There will likely be a visual overhaul to the interface and various redesign of the internals to work with Qt 4's Interview, but well, lot's-o-warm-fuzzies seeing it build and start.

Also in the 3.5 branch I recently checked in support for <a href="http://gstreamer.freedesktop.org/">GStreamer</a> 0.10, which I've been using at home for the last couple of weeks and seems quite stable.

In the last bit of kde-multimedia news, Matthias and I have been cleaning up the kdemultimedia module in Trunk and have removed many of the deprecated aRts plugins and moved the maintained stuff out of kdemultimedia and into an artskde module.  It's nice to get things tidied up a bit.

<b>Invisible Things</b>

TagLib has also seen quite a few bug fixes of late, a few new features, along with a lot of new folks working on <a href="http://developer.kde.org/~wheeler/taglib.html#bindings">bindings</a>.  Even though I haven't updated the application on the page in a while, I'm aware of over 20 applications using it these days and have heard rumors of it being used in some mp3 players.   It's also now the top listed ID3v2 implementation on <a href="http://www.id3.org/implement.html">id3.org</a>.  A 1.5 release will probably show up in the next month or two.

<b>Intangible Things</b>

For the last few months I'd been doing what I call "dancing in my room".  I call it "dancing in my room" because, you know how sometimes when there's some really music you just catch yourself dancing in your apartment?  Not as a social or alcohol or flirting induced phenomenon, but just because, well, it seems like the thing to do?  That's kind of been how I've been coding for the last few months.  I've been coding on stuff not so much to release stuff or have things to show, but just because I wanted to understand a problem or well, just because coding is an interesting means of exploring certain classes of things.  I've hacked up a couple of small applications for my own use which will likely never see the light of day, but it kind of reminded me why I got into OSS in the first place -- because I like writing code.  I was disconnected from November to January which gave me an opportunity to really step away from some of the more bureaucratic bits of the whole scene and get back to the fun parts.

My hacker mojo is returning.  I've been writing more code at work in the last few weeks than I've had a chance to in a long time; my commit rate in KDE isn't breaking any personal records, but it's starting to reenter the range where I feel active again, which is nice.

The last few months had been pretty stressful for me, and the last month has involved some pretty difficult personal decisions (nothing like I'm selling my kidneys or moving to Zimbabwe), but as some clarity has started to surface I'm kind of getting back to my roots -- coding, reading (I've read Tom Robbin's <i>Villa Incognito</i>, Neal Stephenson's <i>Snow Crash</i> and am about half-way through Chomsky's <i>On Nature and Language</i> as well as picking up the MIT Press <i>Computer Music Tutorial</i> -- 1200 pages -- in the last month.) and music.  Hopefully this trend will continue.  :-)
<!--break-->
