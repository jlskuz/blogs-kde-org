---
title:   "Some peoples don't quite grasp some easy to"
date:    2006-03-05
authors:
  - chouimat
slug:    some-peoples-dont-quite-grasp-some-easy
---
understand concept like "use the right tool for the job" 
after spending a few weeks (2) trying to  salvage a project (shouldn't have accepted this contract)
I come to realise once again that some persons still choose the technologies for their
project based on how high the buzzword is on the "Pointy-haired boss" scale

They wanted to port 2 applications to a small arm9 based port so they 
could have a smaller device ... ok that nice when you have something
as powerfull as a desktop with an amd64 to run your  java and/or mono
applications ... and they wanted those applications to be realtime :)

I asked them why they choose those languages. I got for answer that
their programmers only know java and C# and they will never have to learn anything else
since it's the future ... but they didn't understand why their applications crashed randomly

so after a few days (10 iirc) trying to reproduce the problem (and I did a few time), looking at the code
I finally found the source of the problem. the source was their favorite feature of java and c#, the 
memory management and the garbage collection was the culprit ... it some times started in some time 
critical section of the application ... And at this point I went to Hell, the worse day of my life 
(ok I won't talk about my 5 year marriage ... this is a completely another story) I spent about 10 hours trying 
to make them understand that they were using the wrong tool for the job, and the memory manager was the source of the
problem ... let just say that in the end I wasn't diplomatic at all, it was like explain to religious fanatics that they were wrong ...

Mow I wonder how much time they will take to send me my check ...
ok now time to take sometime to rest before the next contract ... I think I will need some ;)
<!--break-->
