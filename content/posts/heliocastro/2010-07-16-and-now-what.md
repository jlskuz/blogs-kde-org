---
title:   "And now what  ?"
date:    2010-07-16
authors:
  - heliocastro
slug:    and-now-what
---
<a href="http://www.boingboing.net/2010/07/10/brazils-copyright-la.html?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed:+boingboing/iBag+(Boing+Boing)&utm_content=Google+Reader">Brazil's copyright law forbids using DRM to block fair use</a><br>

<a href="http://www.engadget.com/2010/07/15/chile-becomes-first-country-to-guarantee-net-neutrality-we-star/">Chile becomes first country to guarantee net neutrality</a><br>

<a href="http://yro.slashdot.org/story/10/07/15/143210/Software-Now-Un-Patentable-In-New-Zealand?from=rss&utm_source=feedburner&utm_medium=feed&utm_campaign=Feed:+Slashdot/slashdot+(Slashdot)&utm_content=Google+Reader">Software Now Un-Patentable In New Zealand</a><br>

<b>What is your country is doing for you ?</b>
<!--break-->
