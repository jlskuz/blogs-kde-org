---
title:   "cmake && kdelibs4_ss work!"
date:    2006-04-21
authors:
  - khindenburg
slug:    cmake-kdelibs4ss-work
---
Well, I'm impressed... cmake-2.4 and kdelibs4_ss actually compile and works (and doesn't seg. fault).  Grats to the hard working people on kde-buildsystem.

Systems: x86 and sparc... 

'make -j6' is a joy to watch... :-)

Now to kdebase...
<!--break-->