---
title:   "I Blog Too"
date:    2005-10-23
authors:
  - khindenburg
slug:    i-blog-too
---
Finally got around to fixing some issue with Konsole with the latest kdelibs4_snapshot.  The menus work and you can create multiple sessions/tabs again.  The actual terminal widget still needs some TLC.
Anyway, Konsole is at least useable.  I haven't really looked at Qt4/KDE4 much.

Went and saw Doom, the movie.  It was about as good as I expected.  On other new I finally got DSL.  This after 10+ years on dial-up.  Much better :-))