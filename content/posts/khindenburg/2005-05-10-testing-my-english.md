---
title:   "testing my English"
date:    2005-05-10
authors:
  - khindenburg
slug:    testing-my-english
---
<table style="color: black;" width=400 align=center border=1 bordercolor=black cellspacing=0 cellpadding=2>
<tr><td align="center" bgcolor="#A8FFB3">
<h3>Your Linguistic Profile:</h3>
</td></tr><tr><td bgcolor="#D9FFD8">
75% General American English</td></tr><tr><td bgcolor="#A8FFB3">
10% Midwestern</td></tr><tr><td bgcolor="#D9FFD8">
10% Yankee</td></tr><tr><td bgcolor="#A8FFB3">
5% Upper Midwestern</td></tr><tr><td bgcolor="#D9FFD8">
0% Dixie</td></tr></table>

<div align="center">
<a href="http://www.blogthings.com/amenglishdialecttest/">What Kind of American English Do You Speak?</a>
</div>
