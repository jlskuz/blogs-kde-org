---
title:   "Nightwish and Tristania"
date:    2005-02-16
authors:
  - njaard
slug:    nightwish-and-tristania
---
Last night I saw Nightwish and Tristania in Birmingham.  Nightwish was absolutely spectacular and put on a great show!

Tristania, the band I actually want to see was actually a disappointment. They seemed to forget how to play some of their older songs.  They definitely are better mastered.

Nightwish, which I did like some in past as well, I now have a new appreciation for. They're incredibly talented, what you hear on their albums is what they actually play, not merely the result of lots of mastering.

So, Chris, Jeff, enjoy them Saturday, and try to not get pushed around too much!
<!--break-->