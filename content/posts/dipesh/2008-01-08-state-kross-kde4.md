---
title:   "State of Kross in KDE4"
date:    2008-01-08
authors:
  - dipesh
slug:    state-kross-kde4
---
<a href="http://kross.dipe.org">Kross</a>, the scripting framework, is one of the pillars new with KDE4. While the project started already a while back in 2004 it enveloped over the time to a rather big codebase. During the KDE4-process one of the main-goals was to decrease the codebase what can be even more difficult then writting new code.

After leaving <a href="http://www.koffice.org">KOffice</a> to join kdelibs in the early KDE4-days, the progress increased a lot during the integration into <a href="http://techbase.kde.org/Development/Tutorials/SuperKaramba">SuperKaramba</a> and by all the feedback provided over the time by people all around the world.

Beside the <a href="http://techbase.kde.org/Development/Tutorials#Kross">tutorials</a> that got assimilated by our brand new <a href="http://techbase.kde.org">techbase</a> to offer a good starting point, there are some more Kross related highlights in 2007 outlined below.

<b>Embed PyQt and QtRuby</b>

I remember back a talk about integration of widgets created with QtRuby into a C++ application with an <a href="http://amarok.kde.org/">amarok</a> developer in 2005 what is reality now. It is easy to create widgets with PyQt, QtRuby, KjsEmbed or QtScript and to have them embedded somewhere into your KApplication.

KSpread does as example use that for it's <a href="http://techbase.kde.org/Development/Tutorials/KSpread_Scripting#Scripting_Docker_Widgets">Scripting Docker Widgets</a>. While hitting the topic of scripting in KSpread, you may also note, that KSpread does offer access to the <a href="http://techbase.kde.org/Development/Tutorials/KSpread_Scripting#The_R_Project">R programming environment for data analysis and graphics</a> and implements 45 R-Project functions using the python backend and comes with support for <a href="http://techbase.kde.org/Development/Tutorials/KSpread_Scripting#Yahoo.21_Web-services">Webservices</a> as well now.

<b>Connecting Kross and QtScript together</b>

Just today a larger patch got committed into the just open KDE 4.1 trunk that connects QtScript and Kross closer together. This was done two ways;

1) QtScript as Kross interpreter backend
We support now both. Kjs+KjsEmbed as "javascript" (mimetype application/javascript and file-extension *.js) and QtScript as "qtscript" (mimetype application/ecmascript and file-extension *.es).

2) Kross in QtScript as QScriptExtensionPlugin
Each KDE-application using QtScript will be able to transparently access Kross and all supported scripting backends now.

The <a href="http://websvn.kde.org/trunk/KDE/kdelibs/kross/qts/main.cpp">main.cpp</a> is a simple test-application that does nothing else then
<pre>
QScriptEngine* engine = new QScriptEngine();
engine->importExtension("kross");
</pre>
to load the Kross QScriptExtensionPlugin. This plugin does allow us then to use Kross functionality direct within a QtScript like demonstrated in the <a href="http://websvn.kde.org/trunk/KDE/kdelibs/kross/qts/test.es?view=markup">test.es</a> file;
<pre>
var action = Kross.action("MyPythonScript");
action.addQObject(action, "MyAction");
action.setInterpreter("python");
action.setCode("import MyAction ; print 'This is Python. name=>',MyAction.interpreter()");
action.trigger();
</pre>
You will be able to use this without any additional line of code and even without linking to Kross explicit. Now, isn't that cool?

<b>KWord, Kopete and Kile scripting plugins</b>

Beside porting and extending the scripting support for <a href="http://techbase.kde.org/Development/Tutorials/Krita_Scripting">Krita</a>, <a href="http://techbase.kde.org/Development/Tutorials/KSpread_Scripting">KSpread</a> and <a href="http://www.koffice.org/kexi/">Kexi</a>, the KOffice word processor <a href="http://techbase.kde.org/Development/Tutorials/KWord_Scripting">KWord</a> got also a scripting plugin. Then end of the last year a scripting plugin for Kopete landed in <a href="http://websvn.kde.org/trunk/extragear/network/kopete-scripting/">extragear</a> and just few days ago <a href="http://kile.sourceforge.net/">Kile</a>, _the_ integrated LaTeX environment, got some initial code to support scripting using Kross. Taken the massive feedback into account during the KDE4 development, it is impressing how fast the codebase growed, went into a stable state and got adopted. The FOSS-world is amazing.

<b>Java and Falcon backends</b>

For the just started year 2008 there are multiple items on the todo-list. While the biggest point of them was to connect QtScript and Kross together, something that is mostly done already and sits in the KDE 4.1 codebase now, there are at least two more points on that list.

In the spirit of the <a href="http://code.google.com/soc/2007/">Google Summer of Code 2007</a> the <a href="http://websvn.kde.org/trunk/playground/bindings/krossjava/">Java plugin</a> was written and reached a pretty good state. One of the top-goals for 2008 will be to finish that plugin and once done ship it together with kdebindings. There is huge potential here taken into account that all the needed work to have the java-backend working transparent was already done. Class-files are written on demand aka matching to a QObject we throw in. Another hot-topic will be to connect krossjava together with the <a href="http://trolltech.com/products/qt/jambi">Qt Jambi Rich Client Java Development Framework</a> to optional access it's functionality just like we did with PyQt and QtRuby.

The <a href="http://websvn.kde.org/trunk/playground/bindings/krossfalcon/">falcon plugin</a> implements access to the <a href="http://www.falconpl.org">The Falcon Programming Language</a> and while started very late this year, the master-mind and code-guru Giancarlo Niccolai did land already impressing work what made me believe in beeing able to have in the fall of 2008 a rock stable falcon plugin available.
