---
title:   "MSOOXML and the german vote"
date:    2007-09-04
authors:
  - dipesh
slug:    msooxml-and-german-vote
---
[<b>disclaimer: this is a personal opinion and meaned to be sarcastic!</b>]

So, germany was voting with "yes" for the Microsoft XML2007 format. That may not new for those that did follow the worldwide ISO-adventure and that believe in the power of black suitcases.

But it may new to you, that germany was again miles ahead there compared to every other country. Right, while some countries really started to look at those MSOffice2007 DNA-specs, we already answered with a clear "yes" in 2005!

Let's look what the <a href="http://www.fokus.fraunhofer.de/fokus/fokus/presse/meldungen_fokus/2007/05/DIN-E.pdf">institute</a> that was leading the <a href="http://www.din.de/cmd;jsessionid=2135DF808FB807A58AD5D84BAB954644.2?level=tpl-artikel&menuid=47567&cmsareaid=47567&cmsrubid=57318&menurubricid=57318&cmstextid=57821&2&languageid=en">NIA34</a> gang that is responsible for the "yeah-why-not vote" wrote in the <a href="http://www.fokus.fraunhofer.de/fokus/fokus/presse/PM-pdf/MSinterop2-2-05e.pdf">MSinterop2-2-05e.pdf</a> press-release dated March 2005;

<pre>
Intensive evaluation of Microsoft technologies in the vendor-
and technology-independent Fraunhofer FOKUS eGovernment
Laboratory has shown that Microsoft technologies and products
do function smoothly with technologies and products from other
vendors, including those from the open source sector.
</pre>

Right, hmmm... isn't there still an ongoing monopoly-case re that
topic? anyway... It's even more surprising to note, that

<pre>
Interoperability was convincingly demonstrated in terms of data
exchange from documents of various Office programs
</pre>

very interesting. It continues with;

<pre>
To test document interoperability, Office solutions (MS Office,
OpenOffice) were linked up and run with specialist applications
[...] Transferability is based on Microsoft Office 2003’s
support for open standards – in this case XML technologies.
</pre>

So, as you probably know those MSOffice 2003 XML != MSOffice 2007 XML but even there germany did vote
with "yes, it's an open standard" already. Man, we even named it "open" long before the MS-PR team
came up with it for there 2007-format. Wow. Now I really don't wonder anymore that all details re
the decision are secret and hidden from the public. Any country able to top that?

And for those that may like to read some more details, the with Word 2007 (Beta) created <a href="http://www.fokus.fraunhofer.de/projekte/Dokumentenaustauschformate/pdfs/ECMA.pdf">ECMA.pdf</a> does contain even some more details written by someone working at <a href="http://www.nextpage.com/about/bios/tom.htm">a R&D organization funded by Microsoft</a> - and it seems, that's the official paper they provide on the topic "Transmission ODf - OOXML"