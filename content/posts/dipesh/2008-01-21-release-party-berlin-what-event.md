---
title:   "Release-party in Berlin; what an event"
date:    2008-01-21
authors:
  - dipesh
slug:    release-party-berlin-what-event
---
<b>disclaimer: as usual this is a personal opinion and I am not that sure that everything was like I describe it now cause of some side-conditions.</b>

The last friday started just like one of those usual normal january-days not worth to be mention. Over the day I even got the impression its still one of those months where it's not worth to wake up in the morning and leave the warm and bright home to move into the cold and dark world outside there.

I am still not sure at what time it switched to an amazing and funny day though I guess it was already the case I got my first free beer (free as in beer, not free as in software) at the KDE4 release-party here in the Berlin <a href="http://www.kdab.net">KDAB</a> office - thanks again here to KDAB for supporting once more a social-event like this, you really rock KDAB!

It's a big advantage to monitor the daily svn-commits since just some days before I saw a commit that moved the starttime of the release-party to 7pm with a comment like "to be sure there is at least someone around at 8pm". What was resulting in me and a friend being there 9pm and missing the free food (free as in food, not free as in beer since the later may provide longer impressions as I was able to confirm the next morning).

Looking the live-stream from google's headquater I somehow got a nostalgia feeling remembering back movie screeners filmed with a handcam. Maybe cause of the heads in front of me? Or maybe cause the n+1 free beer already started to squeeze the reality? No idea there...

With so much known faces all over the place and with even more to me unknown faces it was pretty easy to don't remember all the productive and unproductive talks that filled next few hours. Though I remember being once more impressed how much FOSS-developers (not only KDE ones since we also had guests from a few other projects) are using MacOS taking into account that Mac is even more closed then Microsoft ever was. Something to investigate more into detail once I did got my own Mac :-)

I still remember sitting at some point outside of the office in the rain enjoying even more talks. That was probably the first time I lost my jacket that evening somewhere and re-found it later somewhere else. My thesis there is, that the KDAB office was either build upon a bone yard or Marty McFly used my jacket on his trip through a wormhole. Aliens would be an alternate but I guess that's somewhat unrealistic cause why should they need jackets?

And then the evening (or maybe already morning, no idea there since both times it's dark here) turned around again into something funny with a touch of bizarre. The area the KDAB office is located on has 2 rather big outgoings closed with 2 big doors. While it's normaly needed to have a key to go in or out, this evening one of the doors was opened. But even then we had the luck to enjoy multiple people trying to find there way out and failing horrible. One of them even had a bike with him and did drive more then 1/2 hour around between the doors (it's a huge area) and with each minute even more people joined that group that tried to figure there way out of the light into the dark.

There was a moment I was able to stop laughing (sorry here guys, that was mainly cause I remember from the last meeting to have exactly the same problems with the doors and even did tried it much longer + the free beer and with it the dark force in me was a bit to strong that evening/morning). Remembering back that I solved it last time with a secret weapon, I tried to re-find that secret weapon by asking one of the KDAB-wizards for the magic key to open the hexed door that leads to an ensorcelled city-wood.

That's where the situation got bizarre. At my way upstairs to the office I came across the elevator and believe it or not: multiple people got imprisoned in there. That means, that the elevator stopped between two levels and as Arne told me, those group of 10 people was in there since a few hours. 30 minutes later it was solved and they got freed (free as in freedom, not as in beer though I guess they had at least the same level I reached with the difference that I didn't tried the elevator cause running around the stairs did helped me a few times to re-find my jacket before already).

All in all, it was an impressing release-party && there is no way to forget about it. At least those parts I still remember... Thank you so much for it KDAB, thank you google and thank you KDE!

p.s. I didn't changed the names cause nobody is innocent (just a refer to Aaron's great talk :)
