---
title:   "gsoc: Sometimes size doesn't matter, really."
date:    2008-05-26
authors:
  - dipesh
slug:    gsoc-sometimes-size-doesnt-matter-really
---
While working on <a href="http://blogs.kde.org/node/3457">Improving OpenDocument in KWord</a> for gsoc2008 we enjoyed today again one of this very productive pair-programming days. The todays goal was not that difficult: get those svg:x and svg:y positions for text-anchors (which are used to position frames like e.g. an image relative to there anchor) working.

It turned out it was difficult. Not the job itself but to find the proper solution and to do it the right(TM) way. First we had to investigate how anchors are working atm what includes loading, saving and the behavior during editing. We compared this with how it should work and what is needed to let both match a bit more (as in where to set/update the x/y-position). Till we reached that state a few hours of reading and interpretation of the OpenDocument-specs, running through our sources and loading+saving various ODT-testcases passed while no single line of code was written.

Just some moments after the old day closed and the new one opened the solution was jumping at us and within 5 minutes around 10 lines of code got written to do the job.

Result for today;
* ~5 hours of investigation without any line of code followed by
* ~5 minutes coding with
* ~10 lines of C++ code and
* ~60 lines of doxygen-documentation

Where the last point is the most important one to turn those hours we had to spend at the beginning into something very productive that does decrease the entry-barrier for others who may need to deal with that code-base in the future.

Code-size doesn't always matter but the solution and the documentation and with it the possibility to get fast into something to solve a specific task does. To write some code is not the hard thing but to maintain it later is and that's where those hours will pay-out long term.

p.s. ok, we don't only solved that defined task but also filled our TODO with everything we discovered on our long way to solve that task what is another good result of the investigation-phase ;)
