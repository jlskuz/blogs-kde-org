---
title:   "KMLDonkey on KDE4"
date:    2007-09-11
authors:
  - dipesh
slug:    kmldonkey-kde4
---
The image below shows the next generation KMLDonkey 2.0 from <a href="http://websvn.kde.org/trunk/extragear/network/kmldonkey">SVN</a> running on KDE4. The whole port was done within around 3 days - KDE4 rocks :)

<img src="http://kross.dipe.org/kmldonkey.jpg" />
