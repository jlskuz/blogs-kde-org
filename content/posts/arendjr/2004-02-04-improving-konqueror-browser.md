---
title:   "Improving Konqueror the Browser"
date:    2004-02-04
authors:
  - arendjr
slug:    improving-konqueror-browser
---
As I downloaded KDE 3.2 RC1 some while ago and KDE 3.2.0 today, I started using Konqueror as my primary browser as it has improved immense since 3.1. Now I want to share some ideas for improving Konqueror the Browser.

First of all, it would be really nice if Konqueror could instantly switch between the filemanager and webbrowsing profiles when a URL of the other type is entered. So, if I have Konqueror open as filemanager in my home directory and I click a bookmark which points to a website, it would be nice if Konqueror could automatically load the webbrowsing profile; the toolbar/sidebar configuration for filemanaging will only hinder browsing.

To extend on this idea, some more settings could be added to the profiles, like whether the 'Up' icon should be displayed. This icon is only useful when when in filemanager mode and is confusing when browsing.

Finally (and maybe this fits better in a separate request) it would be nice if Konqueror (and the webbrowsing profile in specific) could offer a Google bar in the way Firebird and Safari do this.

I also filed this as bug #74097 but I wanted to know your reactions on this. Btw, I'm trying to implement the Google bar myself right now, if some useful comes out of it, I'll let you know ;)