---
title:   "QtWebKit 2.3 beta 1 tagged."
date:    2013-01-21
authors:
  - carewolf
slug:    qtwebkit-23-beta-1-tagged
---
Now that Qt 5.0 has been released, it is time to think about the release of QtWebKit 2.3. For those who do not remember, QtWebKit 2.3 is QtWebKit 3.0 (QtWebKit from Qt 5.0) ported to Qt 4.8.

Beta 1 was tagged yesterday, so go nuts and try it out.

Since the announcement of QtWebKit 2.3, a lot of crash fixes have gone in, printing, webgl, css animations and css shaders have been fixed so they work in most configurations, plus build fixes for a lot of system. Disclaimer: CSS shaders is still a little experimental and will fail in some complex cases if accelerated compositing is enabled, but it will fail gracefully now.

I have had a great deal of feedback from a  people trying to enable more than the default features using the build-webkit script. Not to discourage experimentation, but please note that if something hasn't been enabled by default, it is usually because it is not fully functional or simply broken. You can disable features, but enabling features that are default off, will often be a waste of time. If you really want to pimp your QtWebKit, I recommend these few settings to play with : css3-text, css-variables, microdata, styled-scope, web-audio and legacy-web-audio. I won't promise they will work though.

Finally for packagers: If you are making packages for i386 you might want to build with --no-sse2, otherwise we currently default to using SSE instead of i387 math, and that means a minimum of Pentium-M, Pentium4 or Athlon64.

For more information, see <a href="http://blogs.kde.org/2012/11/14/introducing-qtwebkit-23">my introduction of QtWebKit 2.3</a>.