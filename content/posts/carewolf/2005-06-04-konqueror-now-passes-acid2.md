---
title:   "Konqueror now passes Acid2"
date:    2005-06-04
authors:
  - carewolf
slug:    konqueror-now-passes-acid2
---
As the title says, Acid2 is now passed.
<a href="http://blogs.kde.org/node/view/1127"> <img src="http://developer.kde.org/~danimo/screenies/konq_acid2.png" border=no class="showonplanet" align="right" width="30%" style="margin-left: 2em" /></a>
With the help of Hyatt's Safari patches, Ivor Hewitt(*) and I have finally made Konqueror pass Acid2 as only the second browser.

Roughly half of the Safari Acid2 patches could be merged in some form or another, and another two bugfixes could be merged from Safari 1.3 once found and isolated. The rest was written from scratch. All in all not a bad reuse of code, allthough slow. 

The fixes are at the moment in KDE 3.5 branch, but will soon be merged into KDE4, and evaluated for KDE 3.4.2 inclusion.

(*) Ivor is a new KHTML developer, and author of the adblock feature.
<!--break-->