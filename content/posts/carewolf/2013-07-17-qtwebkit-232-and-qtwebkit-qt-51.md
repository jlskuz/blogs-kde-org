---
title:   "QtWebKit 2.3.2 and QtWebKit for Qt 5.1"
date:    2013-07-17
authors:
  - carewolf
slug:    qtwebkit-232-and-qtwebkit-qt-51
---
Thursday I tagged QtWebKit 2.3.2. To avoid confusion I will present both  QtWebKit for Qt 5.1 and QtWebKit 2.3.2.

The new features in QtWebKit for Qt 5.1 can be seen on http://qt-project.org/wiki/New-Features-in-Qt-5.1 . Let me expand a few interesting ones below:

<b>Windows 64bit JIT:</b> In QtWebKit 5.0 and QtWebKit 2.3.1 we only supported interpreted JavaScriptCore on 64bit Windows. The issue is that unlike 32bit x86, x64 has different calling conversions on Windows and Linux, so the it requires different JIT implementations to work on Win64, and on top of that. Note that x64 is still not as fast as x86 on windows or x64 on linux. Only first generation JIT has been implemented, the second generation DFG JIT is not available. This work was also cherry-picked forQtWebKit 2.3.2

<b>MIPS JavaScriptCore:</b> MIPS have had second level DFG JIT implemented. This work was also cherry-picked for QtWebKit 2.3.2. Note the at WebKit MIPS implementation is limited to the O32 binary format and calling convention, and requires 64bit floating-point any other MIPS configuration will default to interpreted.

<b>Improved garbage collection:</b> Garbage collection have been improved by implemented the activity triggered garbage collected and the incremental sweeping garbage collector. The first means that garbage collection can now be triggered by new allocations. This is more likely to keep the memory usage constant, by spending time to find garbage each time more memory are allocated. The second means that garbage collection will now run incrementally which means JS will now cause shorter CPU stalls when under pressure. Also in QtWebKit 2.3.2.

<b>Improved font rendering:</b> In Qt 5.1 QtWebkit now defaults to enabling font kerning  Previously kerning could be enabled using '-webkit-font-kerning: normal' but  doing so would incur a big performance hit because a complex font rendering path would be used (the slowl path used when complex shaping is required). In Qt 5.1 we can now renderer kerned text in the fast font rendering path, which means we can now enable it when '-webkit-font-kerning' is set to 'auto' which is default. This is Qt 5.1 only because it depends on new features added there.

<b>Support for WOFF fonts:</b> We have added support for the WOFF web-font format. This feature depends on the presence of the zlib library when building WebKit. This work was also cherry-picked for QtWebKit 2.3.2.

Qt 5.1 is available at http://qt-project.org/downloads

QtWebKit 2.3 for Qt 4.8 is maintained at https://gitorious.org/+qtwebkit-developers/webkit/qtwebkit-23 and the tarball 2.3.2 is https://gitorious.org/webkit/qtwebkit-23/archive-tarball/qtwebkit-2.3.2

Best wishes here from Bilbao (Akademy + QtCS)

Edit: correct tarball link