---
title:   "Business talk in La Serena"
date:    2004-11-22
authors:
  - duncan mac-vicar
slug:    business-talk-la-serena
---
I am invited to give a talk to a Business Summit on Thursday, in La Serena city. As I am used to, the audience are business people and my talk will have to be very general. My current plan right now is to talk about technology as a tool for entrepeneurs, opensource, copyright, licenses, creative commons, and other cool stuff, and of course, can't wait to decide where to put a big KDE logo somewhere in a slide to tell them about the project. Known city for me since it is where I lived before coming to University in Santiago.

[image:730,middle,4,4,1]

I hope I can make some interesting contacts in University of La Serena to talk about using Linux/KDE in the University Labs.



