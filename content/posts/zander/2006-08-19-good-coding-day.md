---
title:   "Good coding day"
date:    2006-08-19
authors:
  - zander
slug:    good-coding-day
---
For KWord I have progressed nicely in the layout department.  In my last blog I started researching linespacing. It turns out that are two accepted methods of doing linespacing and the different results was (partly) due to the different models. Funny thing is, the OpenDocument Format spec has a configure setting for it. So I just made KWord do both to honour that config setting. :-)

Doing proper layout on a large body of text is pretty heavy on the processor, though. And the way KWord 1.x does things is a bit outdated ;)  Since Qt4 multithreading became socially accepted.

So, when I saw the 0.6 release of ThreadWeaver <a href="http://blogs.kde.org/node/2268">this morning</a>, I thought I'd take a look at it and see how I can use it to do some heavy processes in a different thread.  I used today to write a little library on top of ThreadWeaver; a Make It Simple class for many of your multithreading usages. Feels nice to have a finished product and 590 new LOC at the end of the day :)

For everyone that wants to enjoy multithreading, without much thinking about it here, I put the result in svn; under the TheadWeaver repo.

In my case I have one method that needs to be called from the main thread and run in a different thread, and I need to be able to call it from different places and different threads; the simple concept is something most programmers have seen sometime or other;
<pre>
    void MyClass::addSomething() {
        // do the add
        updateData();
    }
    void MyClass::removeSomething() {
        // do the remove
        updateData();
    }
    void MyClass::alterItem(Foo bar) {
        // do the alter
        updateData();
    }

    void MyClass::updateData() {
        // do the heavy lifting, lots of lines of code.
    }
</pre>

This is the simple version that sadly breaks down completely when the updateData starts to take
a lot of time because when your mouse/key events don't return very quickly the user is looking
at unchanging screens a long time and quality goes doen really quickly.  Even 200ms delay between
user action and visible response is noticed.

To make this multithreading you have to replace the updateData with a way to start a new thread. Or a new Job in ThreadWeaver.  And then you will have to make sure only one job is actually run at the same time as having the updateData being run by two threads will surely lead to bugs.
I'd love to see a 'create and forget' job that would just take all administrative work out of my hands.

Introducing the new Action object I created you can just alter the updateData() to a <code>updateDataAction.execute();</code>.  Plus you can call it from any thread, including the updateData, while its running an another thread. Something I need to 'chunk' the rendering.

Have fun!

ThreadWeaver as well as this extention can be found in KDE SVN (kdenonbeta/threadweaver).
<!--break-->