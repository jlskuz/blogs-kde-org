---
title:   "Birthdays"
date:    2006-10-15
authors:
  - zander
slug:    birthdays
---
Last weekend we had a nice celebration based on the date of the first post about KDE.
When we think about birthdays of humans we don't take the first "lets create one" message, but the date its "released" into the world. Which is more practical for several reasons.

For KOffice I failed to find the first message that started the project, and while I know that it took about 2 years from first commit to first release (yeah, big baby ;) I also could not find the first commit date anymore. (svn longs don't go back all the way to the beginning, it seems).
So, I'll take the easy route and just use the first release.  Which coincided with the KDE2 release and that was on 23-Oct-2000

Just 8 days until KOffice is 6 years old. And look how she has grown!  11 applications, 101Mb for a clean checkout and lots of quite innovative technology.

I'll surely grab a cake and put up some candles on the 23th :)<!--break-->