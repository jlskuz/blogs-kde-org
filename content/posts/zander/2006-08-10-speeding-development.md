---
title:   "Speeding up development"
date:    2006-08-10
authors:
  - zander
slug:    speeding-development
---
If you develop like me, you will most likely have a lot of 'change source' 'compile' 'test' roundtrips. Each roundtrip will need a 'make install' to actually be able to see your changes.  So, after we optimized linking by dumping libtool, installing is the place to look.
In KDE4 we use cmake, and its slow on installing. See <a href="http://bugs.kde.org/show_bug.cgi?id=132074">this</a>  bugreport.  Apparently someone had the right idea to not install things twice, but for some reason that got implemented using a diff.  So my 6Mb kwordprivate.so gets ready byte by byte and compared with the installed one.  Ugh, that can't be fast if they are indeed the same.

So, I sat down over lunch today and extended my unsercmake app to no longer call the cmake install target, but do it ourselves. CMake makes it easy because it basically writes out a script with source and target lines. So it really was just a couple of hours hacking.
For finding out what I have to actually install I did what I think is the norm; check modified date and filesize and determine on those grounds if the installed one is old or incorrect.

Doing a unsercmake install twice now makes the second one return immediately.  Which saves me quite some time and makes me even more productive :)

Oh, and at the same time I moved the 'install lib' to be immediately after linking.  Solving another annoyance I had where you have to wait for everything to be compiled and installed before you can start your app to see the changes.

Please download and test.  I won't claim this is perfect; but more people that like these changes and tell the cmake developers will help us all to get better tools.  For now, I like the fact that I have a simpler to use and more powerful tool.  Oh, don't forget to type: <code> unsercmake --help</code>

Download the script <a href="http://members.home.nl/zander/unsercmake">here</a> <!--break-->