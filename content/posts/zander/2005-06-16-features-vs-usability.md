---
title:   "Features v.s. Usability"
date:    2005-06-16
authors:
  - zander
slug:    features-vs-usability
---
I stubled upon a post about features/usability curve.  This <a href="http://headrush.typepad.com/creating_passionate_users/2005/06/featuritis_vs_t.html">article</a> made be laugh; its a very good, highly recommended :)<br>
Choice quotes:
<ul>
   "Of course you'll lose customers if you stop adding as many new features. <br>
   "Or will you? <br>
   "What if instead of adding new features, a company concentrated on making the service or product much easier to use? [] In a lot of markets, it's gotten so bad out there that simply being usable is enough to make a product truly remarkable."
</ul>
And this one:
<ul>  "Most of you here know that Don Norman talked about this forever in the classic The Design of Everyday Things, but why didn't the designers and manufacturers listen?"
</ul>

Note that just adding features is not a guarentee of making software unusable (the target link covers more then software); as always software is more complicated then that.  Its more the lack of maintainance and attention to workflow that makes things hard in software.  But software that just adds features tend to ignore that, so its a save bet :)