---
title:   "Research into KDE"
date:    2005-06-23
authors:
  - zander
slug:    research-kde
---
KDE is growing.  And I don't mean in the way programmes think; but we are receiving contributions from non programmers more and more.  In the last year we have seen the artists sites grow tremendously, <a href="http://dot.kde.org/1119293365/">this interview</a> on the dot proves again that more usability experts are stepping into the open source waters and with companies like <a href="http://blogs.kde.org/node/view/1046">Apple</a> and <a href="http://dot.kde.org/1118683407/">Nokia</a> seriously picking up, and working with technologies like KHTML that will only increase.  Its baby steps, but watching this is very exciting!

I had a talk with Evangelia recently; she is a PhD student and we got into the subject of quality control.  In KDE we have the <a href="http://quality.kde.org/">quality team</a>, but we both know that that very much need some more manpower.  So we asked outselves why some areas of expertise have only a few persons being active.  We have a lot of coders, translators and graphics artists.  Although more is always good. Areas like usability, documentation and quality control are notoriously understaffed.  How come?

I came up with the idea that it takes a different kind of people to do coding then from quality control, for example.  Quality control is a subject that is not really tought in school and only big companies tend to have them. This then leads to the conclusion that we have not been reaching a big group of people that may just want to do opensource work in the area of their expertise.  Reaching coders seems to be the easy part, how about reaching professional testers and technical writers?
With KDE (and Linux) keeping the growth it has known for the last years, will they eventually come?  Or should we follow the <a href="http://aseigo.blogspot.com/2005/06/newfoundland-neurobiology-networks.html"> example of Aaron</a> and just try to find experts in the local pub?

I'm not sure what the answer is; research might be needed :)

And this brings me to the subject of todays blog.  Evangelia Berdou, a PhD student is doing research into various aspect of cooperation and coordination in KDE and GNOME.  In our talk she said she will be sending out email-questionaires to the members of KDE eV asking them some simple questions about their contribution in KDE.
An Economics student to look at coordination in KDE, now that might get interresting in all sorts of ways!
And, she was quick to add; "It will only take a minute and will be great for my research, the results of which I will share with you all"

Evangelia was at Fosdem 2004 where I first met her and we had a nice talk about these and various other subjects back then already (yeah; doing a PhD takes years).  Here she is; between <a href="https://blogs.kde.org/blog/175">Fabrice</a> and <a href="https://blogs.kde.org/blog/72">Wheels</a>, all saying FOSDEM very loudly :)

<img border=0 class="showonplanet" src="http://members.home.nl/zander/fosdem2004.jpg">

<!--break-->