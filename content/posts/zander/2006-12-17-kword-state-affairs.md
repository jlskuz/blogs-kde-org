---
title:   "KWord state of affairs."
date:    2006-12-17
authors:
  - zander
slug:    kword-state-affairs
---
For KOffice2 we had a huge todo list. Where various core technologies are in need of basically a rewrite. For KWord the most interresting one is the text-engine. The old one had serious problems and using Qt4 would allow me to write a better one in not too much time.
The progress on the new text engine is proceeding nicely, with just some advanced features missing like variables and inline images.
The text engine will now support styles much better then the previous one did; with character styles being available as the biggest change.

One core-koffice component I have been working on is a <a href="http://wiki.koffice.org/index.php?title=ShapeSelector">Shape Selector</a>.  This will be a cross between a scratchboard and a clipart-selector.
In effect, it will show you a list of all items you can insert into your document.  This ranges from preset items like arrows or the flowcharting stencilset.  The shape selector can also be used to store templates for your current project.  Like a logo which you can then drag & drop into any KOffice application.
A future extention is that we should be able to move a shape from the document to the shape selector as a sort of clipboard area.

We need a couple of ways to actually copy such a template from the shape selector into the document.  First one is clicking a template and then creating an outline on the canvas where it will be inserted.  We have a really nice way to do that now since Jan Hambrecht extended the code to show a shaded version of the actual shape you are about to insert. For optimal feedback.
A new way is to use drag and drop.  I implemented that last week.  I was really surprised at how easy it is to use drag & drop in Qt4.  You just create a new QDrag object, set an icon and you 'start' it.  You don't even have to take eventqueue details into account!
The (semi) hard part is then to put the data you actually want to drag into the QDrag object and to get that out on the other side. As I mentioned, the shape selector holds templates. Where a template is basically a set of name=value pairs combined with an Id of which plugin is capable of using the template and create a real object.  See the template as a patato you can stamp with, and the Id as the person that has that patato in his collection.
This means that when I drag an object from the shape selector to a KWord canvas, I'm just dragging a properties object and the canvas will use the flake-plugin to actually build a shape it can put on screen.
This ended up being amazingly easy in KOffice as I could just serialize the KoProperties object that stores the template and add that to the drag object.  Since the KoProperties class has a QString KoProperties::store() method;

So, with time to spare I thought the idea of Jan to show the object before I'm finished creating it would be usefull here as well.  In KOffice we have a class that extends the QScrollArea and therefor will provide scrollbars. It also provides blank area around your canvas when its smaller than the window.
Now, it would be great if the shape I'm about to insert would not be clipped to the canvas, which is easier said then done due to the two being two different widgets. And two widgets get drawn completely separately.  What I did to allow this is basically implement the drawing of the same shape in both the canvas and the canvasController (which draws the surrounding area). Its a little tricky making the offeset and scaling to properly done, but the effect is pretty realistic :)

<a href="http://www.koffice.org/kword/pics/200612-shapeSelector.png"><img src="http://www.koffice.org/kword/pics/200612-shapeSelector_thumb.png" border="0" /></a>

Now all I need to figure out is how to make the pixmap set on the icon disappear when its hovering over the canvas so you don't see the gray pixmap while the real thing is being drawn as well!

Another image; to show the things we can do in KWord now.  Note how shapes formerly only available in Karbon (circle-part) or KPresenter (arrows) are now available in KWord as well. Flake rules :)
<p><a href="http://www.koffice.org/kword/pics/200612-kword.png"><img src="http://www.koffice.org/kword/pics/200612-kword_thumb.png" border="0" /></a>
<!--break-->