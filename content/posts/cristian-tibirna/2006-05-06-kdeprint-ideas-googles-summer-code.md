---
title:   "KDEPrint ideas for Google's \"Summer Of Code\""
date:    2006-05-06
authors:
  - cristian tibirna
slug:    kdeprint-ideas-googles-summer-code
---
The <a href="http://printing.kde.org">KDEPrint team</a> is willing to mentor <a href="http://wiki.kde.org/tiki-index.php?page=KDE%20Google%20SoC%202006%20ideas#id105578">two projects</a> in the frame of <a href="http://code.google.com/summerofcode.html">Google's Summer of Code"</a>.

We would like a student to work on streamlining and improving, usability-wise, the KDEPrint user interfaces. Then there is the very inciting idea of a common, cross-platform, generic description of printer driver and application-side plug-ins to the KDEPrint dialog.

Here is your opportunity for a great experience: enter the greatest desktop FOSS project of these times -- KDE -- and be part, in a same stroke, of one of the coolest social experiments ever -- SoC. 

Submit a project related to KDEPrint (based on the <a href="http://wiki.kde.org/tiki-index.php?page=KDE%20Google%20SoC%202006%20ideas#id105578">the two mentioned</a> or of your own creation) or  to KDE. We want you! <a href="http://code.google.com/soc/student_step1.html">Join today!</a>
<!--break-->
