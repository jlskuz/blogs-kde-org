---
title:   "The first mockups of an eventual future main KDEPrint dialog"
date:    2007-07-13
authors:
  - cristian tibirna
slug:    first-mockups-eventual-future-main-kdeprint-dialog
---
<a href="http://rgcgsoc.wordpress.com/">Rutger Claes</a>, KDEPrint's GSoC student for the new UI design, published <a href="http://rgcgsoc.wordpress.com/2007/07/11/version-02-of-my-report/">his report</a> on today's most common forms of printing interfaces. He also offers the fruit of his analysis of how the main dialog should appear in KDE.

Work is still incipient, but I'm pleased that things move ahead. There's big hopes for out printing platform, in spite of my lackluster maintainership...
<!--break-->