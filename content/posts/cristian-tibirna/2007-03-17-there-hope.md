---
title:   "There is hope"
date:    2007-03-17
authors:
  - cristian tibirna
slug:    there-hope
---
I'm a <a href="http://developer.kde.org/~wheeler/juk.html">juk</a> kind of guy.

Yet a few weeks ago, frustrated with my inability to make akode or arts play the 50 mp3s from my collection of 2000-odd pieces (mostly ogg, personal CDs-ripped), I gave <a href="http://amarok.kde.org">Amarok</a> a new try. I still feel (very) awkward with the interface. But I kinda like a) the default classifications available in "Collection"; b) the automatic refresh/update of the collection and c) the statistics tool.

What I find impressive at different (socio-cultural) level is the cool integration with <a href="http://magnatune.com/">Magnatune</a>. I just hope more music artists of this world will wake up some day and will realise in what magnificent ways Magnatune is another sign of the future social revolution of which the surely want to be part.

Thanks, KDE (and Amarok), for getting me accustomed to another great concept.
<!--break-->

