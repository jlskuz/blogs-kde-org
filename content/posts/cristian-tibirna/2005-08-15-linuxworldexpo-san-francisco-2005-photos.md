---
title:   "LinuxWorldExpo San Francisco 2005 - photos"
date:    2005-08-15
authors:
  - cristian tibirna
slug:    linuxworldexpo-san-francisco-2005-photos
---
KDE and LinuxPrinting.org at LWE, now in color:
<!--break-->

Setting up the booths in the .org pavilion
<img src="http://ktown.kde.org/~ctibirna/photos/lwe-sf-2005/images/IMG_2292.JPG.jpg">

Visitors in the first day:
<img src="http://ktown.kde.org/~ctibirna/photos/lwe-sf-2005/images/IMG_2299.JPG.jpg">

Charles Samules (right) and David Johnson at the KDE booth:
<img src="http://ktown.kde.org/~ctibirna/photos/lwe-sf-2005/images/IMG_2300.JPG.jpg">

Kitch:
<img src="http://ktown.kde.org/~ctibirna/photos/lwe-sf-2005/images/IMG_2303.JPG.jpg">

Charles Samules (left) and Jason Katz-Brown at the KDE booth:
<img src="http://ktown.kde.org/~ctibirna/photos/lwe-sf-2005/images/IMG_2312.JPG.jpg">

Commercial floor:
<img src="http://ktown.kde.org/~ctibirna/photos/lwe-sf-2005/images/IMG_2319.JPG.jpg">

.org pavilion floor:
<img src="http://ktown.kde.org/~ctibirna/photos/lwe-sf-2005/images/IMG_2330.JPG.jpg">

Nathan Krause stirring the masses:
<img src="http://ktown.kde.org/~ctibirna/photos/lwe-sf-2005/images/IMG_2334.JPG.jpg">

Some of the 9 linuxprinting.org booth staffers: Uli Wehner (Lanier), me, George Liu (Ricoh), Patrick Powell (LPRng) and Till Kamppeter (linuxprinting.org and Mandriva):
<img src="http://ktown.kde.org/~ctibirna/photos/lwe-sf-2005/images/IMG_2339.JPG.jpg">

All things have an end:
<img src="http://ktown.kde.org/~ctibirna/photos/lwe-sf-2005/images/IMG_2405.JPG.jpg">

Even the commercial ones:
<img src="http://ktown.kde.org/~ctibirna/photos/lwe-sf-2005/images/IMG_2408.JPG.jpg">
