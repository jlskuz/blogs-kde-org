---
title:   "Work on support for online OpenPrinting drivers progresses"
date:    2007-07-13
authors:
  - cristian tibirna
slug:    work-support-online-openprinting-drivers-progresses
---
<a href="http://gsoc2007gavinbeatty.blogspot.com/">Gavin Beatty</a>, GSoC student for the KDEPrint's online OpenPrinting drivers fetching and installation, provided his <a href="lhttp://mail.kde.org/pipermail/kde-print-devel/2007-July/001815.html">latest report</a> today. It's refreshing to see enthusiasm and excitement from new young faces joining our community. It's perhaps this kind of effervescence that makes me eager to meet my students in class each autumn. Or to try to put some time in projects like GSoC. We need new energy. We need evolution.
<!--break-->