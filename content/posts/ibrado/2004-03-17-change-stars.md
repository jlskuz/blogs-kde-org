---
title:   "Change the stars"
date:    2004-03-17
authors:
  - ibrado
slug:    change-stars
---
I recently watched <i>A Knight's Tale</i> for the Nth time, which got me thinking about improbable dreams...

Like many open source developers, my fondest wish is to be able to sit at the computer all day and all night hacking KDE and other free code -- no punishing deadlines, no extraneous worries, just me pushing my limits doing what I love best... and getting paid for it. 

Alas, reality dictates that I have to work for a living, make the most out of whatever little time is left over for free software development, and squash that dream down to a vague buzzing at the back of my head.

Vague, but still disconcerting.

I live in the so-called Third World, and I sometimes wonder... Would corporate Open Source supporters be willing to retain, at a minimal expense, some skilled developers from these parts expressly for free software development?

A "normal" year's salary of US$60k or so would likely fund up to 20,000 hours of open source development -- with happy developers probably voluntarily coding during nights, weekends, and holidays as well, throughout the year. No, I'm not smoking crack. ;)

Twenty thousand hours a year just working on free software is a lot of fixed bugs, new features, enhancements, and fresh code. Imagine what could be done with twice or thrice that...

Anyone care to change the stars?
