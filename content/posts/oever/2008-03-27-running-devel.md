---
title:   "Running with the devel"
date:    2008-03-27
authors:
  - oever
slug:    running-devel
---
Google Summer of Code has started taking student proposals. This year Strigi is joining <a href="http://techbase.kde.org/index.php?title=Projects/Summer_of_Code/2008">the KDE project in Google Summer of Code</a> again. For students looking for a nice project in information technology in the true sense of the word, Strigi is the project to join.

Last year we had a great project by Alexandr Goncearenco which resulted in giving Strigi the ability to extract information from a plethora of chemical datatypes.
You can read all about his work in his <a href="http://neksa.blogspot.com/">blog</a>.

<img src="http://bp3.blogger.com/_wh2gcHiH63c/Rr3Zlbs3q5I/AAAAAAAAAAk/lmpX5qFgBU4/s400/aKademy2007_strigi-chemical_slide2.png"/>

This year, there are many possibilities to do great work. Here is a list of suggestions.

<dl>
<dt>KDE4 Search program using Xesam</dt>
<dd>Strigi is part of the Xesam project which also includes Beagle, Tracker and Pinot. Because we now have a unified query language, we can have one search client that is a front-end to all of these search clients. It would be important to let this client integrate well with the KDE4 desktop.</dd>
<dt>Live search in Plasma</dt>
<dd>Wouldn't you like to have a plasma widget that you can give a Xesam query and which will stay up to date? It could show the last emails you received or your most popular songs. Or the latest pictures of your cat.</dd>
<dt>Integration of Strigi into K*</dt>
<dd>Take your favourite KDE application and make it completely Strigi-aware. This means: make sure the file types it produces can be read by Strigi and add a search box to the program in the relevant places.</dd>
</dl>

More suggestions can be found <a href="http://techbase.kde.org/index.php?title=Projects/Summer_of_Code/2008/Ideas#Strigi">here</a>.
Of course you are more than welcome to come up with your own ideas too. But hurry up, the deadline is approaching.

<!--break-->