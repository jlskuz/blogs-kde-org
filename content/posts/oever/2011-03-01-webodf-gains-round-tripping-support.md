---
title:   "WebODF gains round-tripping support"
date:    2011-03-01
authors:
  - oever
slug:    webodf-gains-round-tripping-support
---
In my <a href="http://blogs.kde.org/node/4392">previous blog</a> I talked about converting ODF files to PDF files with <a href="http://webodf.org/">WebODF</a>. This is a functionality that is generally useful, but is also one that lets <a href="http://officeshots.org">OfficeShots</a> compare WebODFs ODF rendering to that of other office suites.

Another useful feature is round-tripping of ODF. Round-tripping is the process of loading an ODF file in a program and subsequently saving it again. It is an ODF to ODF conversion. OfficeShots uses round-tripping to see if an office suite generates valid ODF. In WebODF, the original ODF file is barely modified. The XML contents of the ODF is parsed and serialized in this step. Any bugs in this process would be exposed by roundtripping.

After <a href="http://blogs.kde.org/node/4392">building</a>, the round-tripping can be performed like this:
<tt>   qtjsruntime <a href="http://gitorious.org/odfkit/webodf/blobs/master/webodf/lib/runtime.js">lib/runtime.js</a> <a href="http://gitorious.org/odfkit/webodf/blobs/master/webodf/roundtripodf.js">roundtripodf.js</a> myfile.odp</tt>

The file will be roundtripped in-place, so make sure to make a copy before trying this.

In the next blog entry I'll talk about <a href="http://gitorious.org/odfkit/webodf/trees/master/programs/android">WebODF on Android</a>, editing ODF or unhosted.org. Please vote in the comments, come to the irc channel <a href="http://webchat.freenode.net?nick=visitor.&channels=webodf">#webodf</a> or post <a href="http://www.webodf.org/redmine/projects/webodf/issues">bugs or comments</a>.
<!--break-->
