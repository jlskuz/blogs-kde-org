---
title:   "WebODF at FOSDEM "
date:    2011-02-17
authors:
  - oever
slug:    webodf-fosdem
---
The yearly FOSDEM was excellent as always. I could not attend all talks; mine was on sunday afternoon and as usual I was still improving it at the conference itself. Nevertheless, I spoke with many people and saw some very good presentations. Now that the videos are online, I will mention some of them with a link to the video footage.

<a href="http://www.fosdem.org/2011/schedule/event/software_freedom">Why Political Liberty Depends on Software Freedom More Than Ever</a> (<a href="">video</a>).

Eben Moglen rallied the FOSDEM troops with early morning politics. He warns that we need not just Free Software, but also a Free Internet. If it is possible to turn off the internet, it is flawed.

<a href="http://www.fosdem.org/2011/schedule/event/calligra">Calligra Under the Hood</a> (<a href="http://video.fosdem.org/2011/maintracks/calligra.xvid.avi">video</a>).

Boudewijn Rempt gave a technical overview that shows that Calligra is a good starting point if you want to write your own custom office suite.

<a href="http://www.fosdem.org/2011/schedule/event/free_cloud">Building a free, massively scalable cloud computing platform</a> (<a href="http://video.fosdem.org/2011/maintracks/openstack.xvid.avi">video</a>)

Soren Hansen talked about the 'Apache of cloud solutions' OpenStack. 

<a href="http://www.fosdem.org/2011/schedule/event/firefox4_new_features">Firefox 4: new features for users and developers</a> (<a href="http://video.fosdem.org/2011/maintracks/firefox4.xvid.avi">video</a>)

Tristan Nitot talked about the improvements in Firefox 4. It's nice to see them summarized in one talk. I particularly like the improvements in speed.

<a href="http://www.fosdem.org/2011/schedule/event/cloud9_ide">Cloud 9 IDE</a> (<a href="http://video.fosdem.org/2011/lightningtalks/cloud9.xvid.avi">video</a>) A development environment for JavaScript in the browser: awesome!.

<a href="http://www.fosdem.org/2011/schedule/event/kdevelop">KDevelop: Rapid C++ Programming</a> (<a href="http://video.fosdem.org/2011/lightningtalks/kdevelop.xvid.avi">video</a>)
For those not entirely in the cloud, KDevelop is a great IDE. Milian Wolff gave a nice overview of some of the coolest features.

and of course the amazing talk on
<a href="http://www.fosdem.org/2011/schedule/event/webodf">WebODF: an office suite built on browser technology</a> (<a href="http://video.fosdem.org/2011/maintracks/webODF.xvid.avi">video</a>)
 where I showed how to add WebODF to a website, how to write an Android application using WebODF and simply explained how it works. If this talk does not answer all your questions, come to #webodf on freenode, or post a question on <a href="http://www.webodf.org/redmine/projects/webodf">forums</a>.
<!--break-->
