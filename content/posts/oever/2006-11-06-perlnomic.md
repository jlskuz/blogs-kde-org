---
title:   "PerlNomic"
date:    2006-11-06
authors:
  - oever
slug:    perlnomic
---
<p>Feel like upping your l33t Perl skills? <a href='http://perlnomic.org/'>PerlNomic</a> is a very funny game that is a community effort in making a world where all laws are laid down in <em>Perl code</em>. The fuzzyness of natural languages is replaced by the illegibility of Perl code. Lawyers cannot help you here. You get points by having patches to the book of laws accepted by your fellow citizens. Everybody is in it for the points but without getting supporters for your patches you will get nowhere.</p>

<p>You can provide patches for the wildest of ideas, e.g. a patch for setting up an insurance company that protects you against having your propopals rejected. Or you can try to get a patch passed that allows citizens to start a pol(ly)itical party that gets permission from users to vote in their interest.</p>

<i>
<p>PerlNomic is an experiment in rule-making and enforcement in digital
shared spaces.  It is based upon the game <a href="http://nomic.net">Nomic</a> by
<a href="http://www.earlham.edu/~peters/">Peter Suber</a>.  It is written
entirely in <a href="http://www.perl.org">Perl</a> (which is by
<a href="http://www.wall.org/~larry">Larry Wall</a>), using the
<a href="http://hoohoo.ncsa.uiuc.edu/cgi/">Common Gateway Interface</a>.</p>

<p>The basic idea behind PerlNomic is to avoid the flimsy vagueness of
natural-language rules which must always be interpreted by fallible
humans.  All of the "rules" of PerlNomic are written in Perl, and
interpreted by perl.  This complete specificity obviates the need for
any human "judge", though it sometimes allows for unintended loopholes!</p>
</i>

<p>You can start your career in Perl legislature by <a href='http://www.perlnomic.org/8/adduser.cgi'>submitting a proposal</a> to allow yourself into the community.</p>