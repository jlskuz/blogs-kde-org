---
title:   "Strange UDF DVD"
date:    2008-07-22
authors:
  - oever
slug:    strange-udf-dvd
---
My sister-in-law got married and the wedding photographer made a DVD with 412 pictures. Unfortunately, my parents-in-law could not read the DVD, either with Linux (which is their main OS) nor with Windows XP. So they gave the DVD to me and I had a look.

The DVD is a DVD-R and my Linux version (Hardy Heron) tried to open it: it showed the dialog asking if it should mount the DVD. A failure message opened asking me to have a look at <tt>dmesg</tt>. Hmm.

So I did and it said:
<code>[ 2981.006899] UDF-fs: No partition found (1)
[ 2981.125072] ISOFS: Unable to identify CD-ROM format.</code>

So I tried to install some kernel patches  for udf.ko, but this did not help. The disc was still not recognized. In the end I decided to call upon the trusted program <tt>hexdump</tt>. Hexdump should be compulsory at kindergarten!

Browsing through the data with hexdump, I quickly recognized JPEG headers, so I wrote a small program (70 lines) to extract all JPEGs from a binary blob. You can <a href="http://ktown.kde.org/~vandenoever/blogfiles/jpeg.c">download</a> it.

The first megabyte of the DVD is <a href="http://ktown.kde.org/~vandenoever/blogfiles/strangeudf">here</a>.


<code>00000000  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00008000  00 42 45 41 30 31 01 00  00 00 00 00 00 00 00 00  |.BEA01..........|
00008010  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00008800  00 4e 53 52 30 33 01 00  00 00 00 00 00 00 00 00  |.NSR03..........|
00008810  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
00009000  00 54 45 41 30 31 01 00  00 00 00 00 00 00 00 00  |.TEA01..........|
00009010  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|
*
0000a000  01 00 03 00 48 00 00 97  21 87 f0 01 14 00 00 00  |....H...!.......|
0000a010  01 00 00 00 00 00 00 00  08 55 44 46 20 56 6f 6c  |.........UDF Vol|
0000a020  75 6d 65 00 00 00 00 00  00 00 00 00 00 00 00 00  |ume.............|
0000a030  00 00 00 00 00 00 00 0b  01 00 01 00 02 00 02 00  |................|
0000a040  01 00 00 00 01 00 00 00  08 31 33 33 41 33 31 32  |.........133A312|
0000a050  43 20 55 44 46 20 56 6f  6c 75 6d 65 20 53 65 74  |C UDF Volume Set|
0000a060  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  |................|</code>

<!--break-->