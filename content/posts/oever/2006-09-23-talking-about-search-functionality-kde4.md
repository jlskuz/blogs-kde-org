---
title:   "Talking about search functionality in KDE4"
date:    2006-09-23
authors:
  - oever
slug:    talking-about-search-functionality-kde4
---
So Akademy started and the atmosphere is great. The talks so far are very nice and if the talks that are up next hold up to their title the next days will provide a lot if listening pleasure.

In Aaron's second talk (very unusual, he really doesn't talk that much :-), he mentioned Nepomuk in relation to searching on the desktop. I'm very curious to hear what these guys are up to. If you are so excited about searching tools on the desktop that you cannot wait until the Nepomuk BoF on Tuesday evening, you can join the first BoF on Akademy 2006, the <a href="http://akademy.kde.org/codingmarathon/bof.php">Strigi Desktop Search BoF at 13.00 o'clock</a>, also on Tuesday.<!--break-->