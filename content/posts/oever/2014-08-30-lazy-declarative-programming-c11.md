---
title:   "Lazy declarative programming in C++11"
date:    2014-08-30
authors:
  - oever
slug:    lazy-declarative-programming-c11
---
<p>
<tt>make</tt> does it, Haskell does it, spreadsheets do it, QML can do it and below I explain how to do it with C++11: <a href="http://en.wikipedia.org/wiki/Declarative_programming">declarative programming</a>. And not just any declarative programming, but my favorite kind: <a href="http://en.wikipedia.org/wiki/Lazy_evaluation">lazy evaluation</a>.
</p>

<p>I have written <a href="http://paste.kde.org/psgmyhyqh">a few C++ classes</a> that wrap imperative C and C++ functions into functional expressions. A simple example illustrates the difference between the common imperative programming and declarative programming.</p>

<pre>
/**
 * This is our business logic.
 */
int sum(int a, int b) {
   return a + b;
}
/**
 * In the imperative example, c and d are just storage locations for the results.
 */
void imperative() {
    int a = 3;
    int b = 4;
    auto c = sum(a, b);
    auto d = sum(a, c);
    // correct
    std::cout &lt;&lt; a &lt;&lt; " + " &lt;&lt; b &lt;&lt; " = " &lt;&lt; c &lt;&lt; std::endl;
    std::cout &lt;&lt; a &lt;&lt; " + " &lt;&lt; c &lt;&lt; " = " &lt;&lt; d &lt;&lt; std::endl;
    a = 4;
    // error: c and d have not been updated
    std::cout &lt;&lt; a &lt;&lt; " + " &lt;&lt; b &lt;&lt; " = " &lt;&lt; c &lt;&lt; std::endl;
    std::cout &lt;&lt; a &lt;&lt; " + " &lt;&lt; c &lt;&lt; " = " &lt;&lt; d &lt;&lt; std::endl;
}
/**
 * In the lazy example, c and d are defined by the function sum()
 * and the actual values of c and d are determined when these variables
 * are accessed. Any C or C++ function can be used, but the outcome
 * should depend only on the input values.
 */
void lazy() {
    InputValue&lt;int> a = 3;
    InputValue&lt;int> b = 4;
    auto c = makeLazy(sum, a, b);
    auto d = makeLazy(sum, a, c);
    std::cout &lt;&lt; a &lt;&lt; " + " &lt;&lt; b &lt;&lt; " = " &lt;&lt; c &lt;&lt; std::endl;
    std::cout &lt;&lt; a &lt;&lt; " + " &lt;&lt; c &lt;&lt; " = " &lt;&lt; d &lt;&lt; std::endl;
    a = 4;
    std::cout &lt;&lt; a &lt;&lt; " + " &lt;&lt; b &lt;&lt; " = " &lt;&lt; c &lt;&lt; std::endl;
    std::cout &lt;&lt; a &lt;&lt; " + " &lt;&lt; c &lt;&lt; " = " &lt;&lt; d &lt;&lt; std::endl;
}
</pre>

<p>The function makeLazy() which turns imperative functions to functional use, relies on a new feature in C++: <a href="http://en.wikipedia.org/wiki/Variadic_templates">variadic templates</a>. Variadic templates give C++ great flexibility in the use of functions in templates. The example above shows that the functions can be chained.</p>
<p>The biggest advantage of this style of programming is that the programmer does not have to worry about whether the results are up to date. Programming like this feels like writing a spreadsheet: you give the logic and the computer takes care of how and when to get the result. It is nice to see that clean lazy functional programming can be done in C++ too.</p>