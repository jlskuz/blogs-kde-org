---
title:   "Touched by an owl"
date:    2008-01-11
authors:
  - oever
slug:    touched-owl
---
Today at <a href="http://www.panalytical.com">work</a>, a new building was opened. To celebrate the happy event, <a href="http://en.wikipedia.org/wiki/Hugo_Rietveld">Hugo Rietveld</a> came to open the building. To do this, he needed keys and these were brought to him by a <a href="http://en.wikipedia.org/wiki/Barn_Owl">barn owl</a> (<a href="http://nl.wikipedia.org/wiki/Kerkuil">church owl</a> in dutch). The bird flew through the cafetaria and landed in front of Mr Rietveld with the keys.

But the relatively small barn owl was not the only owl present. There was also a specimen of the largest european owl: the <a href="http://en.wikipedia.org/wiki/Eurasian_Eagle-owl">eagle-owl</a>. This is an enormous bird. And this bird too flew across the room. It almost touched my head as it aimed for the small mouse that was held up for it at the other end of the room.

Both of these birds are from the same order: the <a href="http://en.wikipedia.org/wiki/Owl">strigiformes</a> (owl), the bird that gives Strigi its name.

I consider this event on the day of the KDE4 release a wonderful omen. KDE4 has been released and <a href="http://strigi.sf.net">Strigi</a> is flying!

<a href="http://www.kde.org"><img border='0px' src="http://kde.org/img/kde40.png" alt="KDE4 rocks!"/></a>
<a href="http://strigi.sf.net"><img src="http://upload.wikimedia.org/wikipedia/commons/6/60/Uhu-2.jpg" width='427px' alt='Strigi is flying!'/></a>

The owls came from the medieval shop and owl center <a href="http://www.dragonheartflyingteam.nl/index2.html">Dragonheart</a>.
They are really wonderful creatures and it was great experience to see these animals up close.
<!--break-->