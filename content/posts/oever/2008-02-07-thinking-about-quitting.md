---
title:   "Thinking about quitting"
date:    2008-02-07
authors:
  - oever
slug:    thinking-about-quitting
---
When logging out of your desktop session, you want all programs to shut down quickly. Strigi is one of the programs that can linger while it is analyzing a file. I've done some work to improve this latency and have measured the current latency after some improvements to analyzers that can potentially take long on some files.

Here is a graph with the analyzer latencies of my current version:

<img alt='strigi latency' src="https://blogs.kde.org/files/images//x_0.png"/>

You can see that there is a 5% chance that stopping an analysis takes more than one second.

This graph is just an example. Regardless of statistics, it should never take more than one second to shutdown an analysis. So I'm going to try to eliminate all delays longer than one second. Detecting the delays can be done with a simple <tt>assert(timeSinceLastCheckpoint() &lt; 1)</tt> in the checkpoint function. Finding which analyzer should do more checkpointing might be a bit harder.



<!--break-->