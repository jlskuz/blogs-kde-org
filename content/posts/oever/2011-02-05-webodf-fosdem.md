---
title:   "WebODF at FOSDEM"
date:    2011-02-05
authors:
  - oever
slug:    webodf-fosdem
---
Currently I am enjoying FOSDEM, the excellen Free Software conference in Brussels. Tomorrow I will give a presentation "<a href="http://fosdem.org/2011/schedule/event/webodf">WebODF: an office suite built on browser technology</a>" about <a href="http://webodf.org/">WebODF</a>. If you want a preview, you can look at <a href="https://demo.webodf.org/demo/video.html">a screencast</a> about it.

<img src="https://blogs.kde.org/files/images/going-to_0." alt="I'm going to FOSDEM"/>

Office suites for the cloud are becoming more popular. All of them are closed source and, worse, running on a server that is outside of the control of the user. A Free Software solution for this problems is urgently needed.

WebODF is a library for adding OpenDocument Format (ODF) support to applications, regardless of whether they are running on the web or on the desktop. WebODF is a small JavaScript library that can display ODF documents in browsers and HTML widgets. Currently, simple editing support is being added. WebODF can be used in web applications and desktop applications.

WebODF is extremely innovative because it is the first FOSS implementation of an office suite based on HTML5. Using HTML5 means that the code will run on nearly all modern computing systems. On top of that, it uses CSS in such a way that the ODF document is used nearly unaltered as the run-time presentation. This simplification allows us to develop fast and with little code.
<!--break-->
