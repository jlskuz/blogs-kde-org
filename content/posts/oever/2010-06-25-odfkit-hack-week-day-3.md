---
title:   "OdfKit Hack Week day 3"
date:    2010-06-25
authors:
  - oever
slug:    odfkit-hack-week-day-3
---
It's Friday and day three of the <a href="http://blogs.kde.org/node/4249">OdfKit Hack Week</a>. So what did we do all day besides folding balloons, talking to men in wooden shoes and eating pancakes? We actually implemented the style inheritance I <a href="http://blogs.kde.org/node/4254">blogged about yesterday</a>. Background images are now supported too. There was some philosophizing over APIs and <a href="http://gitorious.org/odfkit/webodf/">we published some code</a> (recommended if are interested in (Qt)WebKit or ODF).

Since the weekend is here we'll not go into details too much, after all you can <a href="http://gitorious.org/odfkit/webodf/">download the Qt client code</a> or try <a href="https://demo.webodf.org/demo/">the online demo</a> in a WebKit or Firefox browser, but we will show some images.

The first image shows that background images are working in the Qt client now and the second screenshot shows the first part of the ODF 1.2 specification odt format opened in OpenOffice, our WebKit based viewer and KOffice.


<img src="https://blogs.kde.org/files/images/odf-bgimage-norep.png" style="max-width: 1280px;" width="100%"/>
<a href="https://demo.webodf.org/demo/odf.html#./kofficetests/specs/OpenDocument-v1.2-part1-cd04.odt"><img src="https://blogs.kde.org/files/images/compare.png"   style="max-width: 1161px;" width="100%"/></a>
<!--break-->
