---
title:   "7/22/2003"
date:    2003-07-22
authors:
  - zack rusin
slug:    7222003
---
I'm trying to make a habit out of those entries. 
I've spent a little time today working on and closing <a href="http://bugs.kde.org/show_bug.cgi?id=12384">12384</a>, which by now had 423 votes. Felt good to close it. Also worked on <a href="http://bugs.kde.org/show_bug.cgi?id=4202">4202</a> a little more. I want to have the html capable editor in libkdepim after OLS. Hopefully everyone is going to like it as the editor itself is a kpart plugin. In the beginning kedit, krichtextpart and ktexteditor based will be there. Hopefully before 3.2 I also get a KoText editor part there so that you can edit emails with it. But that's after I'll port KMail and KNode to it, especially that I need to work on html export in KWord a little more. 
George emailed me today saying that he wants to work on KConfEdit, which is great because I was planning on doing that for a while now. KConfEdit was an app I wrote right after KDE 3.0 came out. I never had enough time or energy to move it out of kdenonbeta but with Waldo's proposed KConfig changes for 3.2 (comments and types in entries) I started thinking about it again. During OLS we'll, most probably, have a few nice hacking sessions during which we'll bring that app up and running. 
I also took over KSpell and am the maintainer of it as of today. Few minutes after announcing that I got a few emails pointing me to Dom Lachowich's email on the freedesktop list about <a href="http://www.abisource.com/enchant">Enchant</a> which seems like a good idea. I'll decide what to do about it when Dom's going to answer some of my questions about it. One of the usability people sent a mock-up screenshot of the new, proposed KSpell dialog layout. It looks really good and I like it a lot. If some other people won't object then that's how it's going to look for 3.2.
I also started working on bluetooth integration in KDE. As Ian can assure you I'm addicted to my phone (Nokia 3650). I just think it's neat. More on that later.
Oh, and I started looking into integrating KDE api docs in Qt Assistant. More on that also later.
I don't know. I'm rather productive and I think I'll start limiting those entries to some bare minimum to not scare people of with their size. It all comes down to a simple fact: I'm well, coding, closing bugs and a lot of cool things is coming your way - just trust me on that.