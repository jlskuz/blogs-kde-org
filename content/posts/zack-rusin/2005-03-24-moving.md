---
title:   "Moving"
date:    2005-03-24
authors:
  - zack rusin
slug:    moving
---
So, as some of you may already know in the coming weeks I'll be moving to Norway. I'll be taking a fulltime position at Trolltech. A large part of my responsibilities there will be making sure that X11 is again a state of the art technology. Trolltech is committed to making the Open Source desktop stand out among all other solutions.

So during work I will be hacking on the X server itself, extensions, Mesa, DRI/DRM and fbdev. Some might say that we'll be working on getting the world of graphics in the Open Source (mostly of course GNU/Linux) up to speed with OS X and the coming Microsoft Longhorn but the reality is that we want it better! A lot better. So with that in mind I hope to see many of you in Spain on aKademy 2005 where I'll be presenting many of the things we'll be working on and if you think that some of the recently announced solutions/hacks look cool, you haven't seen anything yet... :)
<!--break-->