---
title:   "Desktop Composition"
date:    2005-01-23
authors:
  - zack rusin
slug:    desktop-composition
---
What I find missing quite often is ability to take context specific notes. For example:
- when I'm reading some spec, html documentation or emails I often want to create a transparent box over something noting "this is important", "this is not true" or something along those lines,
- taking some personal notes as such when editing code,

So "desktop composition" is something that an application author could put on top of any widget which could induce the above described behavior. 

We're talking about a simple text editor, plus some basic paintaing operations sitting on top of our parent widget. Our widget would have an alpha channel so I could draw semi transparent lines on top of whatever I'm marking underneath. 

Painting isn't the hardest thing, it's the association of data that's going to be a real killer. Of course seeing extensive usage of alpha-channel while painting over a parent widget is going to be a very interesting stress test for Qt4.

Let get back to the data though. How do you bind a semi-transparent, blue rectangle with a text "this is important" over the fourth sentence of the email from Mr. Foo from February 29th 2004, 8:31:10am? Forcing application writers to figure out the schema might be a solution (message-id for emails, location for pdf files). Ideally though we would have some kind of Klink implementation in place. So that the compositor could just say "hey, give me composition data associated with this document". It would be especially interesting in a larger environment where when editting a file I could say "show me only annotations made by John in this document". 

I don't know, I like the idea, I'll probably implement something like this next week just to see how it feels.
<!--break-->