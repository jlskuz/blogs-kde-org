---
title:   "\"What can Apple do?\""
date:    2005-05-01
authors:
  - zack rusin
slug:    what-can-apple-do
---
Dave Hyatt asks this question in his <a href="http://weblogs.mozillazine.org/hyatt/archives/2005_04.html#008054">blog</a>. 

First of all let me just say this: KDE developers who worked on KHTML are simply really attached to it because historically it was the "rendering engine done right" and for people who worked on it, well, it's their baby. 

At some point the Open Source ideals which we apply to KHTML and commercial setup in which you emerge yourself went in two different directions. At this point we have two completely separate groups developing two different versions of KHTML. We have absolutely no saying in the way you develop your version of KHTML and you don't participate at all in the way we develop KHTML. 

Whatever solution we can come up will probably revolve around the following two: either we'll have some say in the way you develop WebCore's KHTML or you will start participating in the way we develop KDE's KHTML. It's basically doing whatever we can to somehow build a bridge between both teams. So, in no particular order. 

1) We start sharing bugs database. Where each closed bug/wish is accompanied by a patch from the team that fixed it first. Which might be impossible from your side because IIRC many of your testcases had code that belonged to commercial sites that you were not allowed to disclose. Unfortunately that might be the best solution right now. 

2) We start discussing the changes again. Which doesn't work that well because you seemed to be getting frustrated when none of us had enough time to respond in a timely manner, we were getting upset when the patches you were sending while saying that you committed them already to your tree weren't as good as we'd like them and then you were getting upset because you had a deadline and the given patch could in fact be fixing the issue that you were trying to solve while we were complaining so you were ignoring our reviews and we ended up where we're right now - meaning with a lot of code that  KDE developers simply refuse to ever merge back and both trees have the same issues fixed in different ways. 

3) Apple hires someone whose responsibility is simply to be merging patches between the trees. Not much to say here - just one guy/gal who keeps both trees more or less in sync with each other. 

4) You get involved in development of KHTML head at some level. You obviously do follow it because you do take patches with new features from our tree. Instead of being silent, start being vocal. 

5) You simply commit and work on your version of KHTML in our CVS/SVN. That is so unrealistic I just made myself laugh so I'm not even going to explain why that would be a great thing for KHTML.

6) You make the CVS (or whichever vcs) with your version of KHTML (not even whole WebCore just KHTML) publicly available. Same thing as in 4. 

So, unfortunately, I think that the above represents a set of the more realistic solutions. 
But you know what? If you really care about fixing the situation between both teams and making KHTML better then lets organize a phone conference and lets talk about it. I think the question of "what can Apple do" can be answered a lot better by KDE's very own KHTML developers rather than random people posting comments on your blog. There's about 8 or 9 KHTML developers on the KDE side right now and one of them might have an idea that I haven't thought about and that might work you.   

You have our email addresses, we'll be glad to provide you our phone numbers and to be honest it's high time we had a real conversation anyway.

<!--break-->