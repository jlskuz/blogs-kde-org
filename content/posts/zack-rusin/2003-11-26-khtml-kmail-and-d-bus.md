---
title:   "khtml, kmail and d-bus"
date:    2003-11-26
authors:
  - zack rusin
slug:    khtml-kmail-and-d-bus
---
<p>Thanksgiving is in two days. I'm a vegetarian so I'm definitely not going to be eating turkey.</p>
<p>Anyway, I've spent most of today with Coolo and Dirk talking about khtml, safari and regression testing. Coolo did a great job with regression testing. The rendering tree output is going to be a pain. Baseline output has to be pretty much regenerated after most changes to the rendering. Coolo thinks this will go away as we stabilize. We'll see. Dirk fixed the  td p margin problem that was plaguing us. Cool stuff.  </p>
<p>Dirk also told me about a "serious" problem with POP3 on-server-filters in KMail. Apparently if you have around 50000 emails in your POP3 inbox,getting the top few while leaving the rest on the server is unacceptably slow. Who finds those things? I thought only Ian would have a crazy enough setup to do such a thing. Anyway, I fixed it. The algorithm was horrible to be honest. It was a O(n^n) so it obviously didn't scale too well. I talked to Don and we decided that maybe we'll skip it for 3.2 since it's hardly a crucial bug.</p>
<p>I took a few hours off coding on KDE and working and I wrote and committed D-BUS Qt bindings. The DBusConnection wrapper doesn't yet support some sending types and I haven't bothered to write any examples, besides it's done. Code generation has to be of course still done.</p> 
Tomorrow I'm going over to Ian's to work on KConfigEditor a bit. Ian gave me a great idea of integrating kjsembed into KConfigEditor. So thanks to that with trivial scripts admins will be able to configure their whole networks. Also it's going to make writting custom configuration dialogs trivial in js. Very cool. 