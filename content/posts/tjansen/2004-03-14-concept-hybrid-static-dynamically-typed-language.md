---
title:   "Concept for a hybrid static-/dynamically typed language"
date:    2004-03-14
authors:
  - tjansen
slug:    concept-hybrid-static-dynamically-typed-language
---
I am watching the static vs dynamic typing wars with some curiosity. On the one hand, I can't 
understand how to write any large application without the help of static typing. The lack of information
in the code, especially the imprecise and fuzzy specification of APIs, reduces the confidence 
that my code will work in all situations. It also does not fit my usual coding style for large
programs and applications: I tend code for days, weeks or even months until I have a usable state, 
without executing the code even once. I <b>RELY</b> on the compiler's ability to find all typos during that time. 
<br>
On the other hand, I see that there are many people who prefer dynamic languages. Most of them have
a write-a-little/test-a-little style, which I know from writing JSPs, so I can understand the style
at least somewhat.<!--break-->
<br><br>
I think I found a very simple concept to allow dynamic typing in a Java-like statically typed language.
The following examples are based on Java, but with two additional features:
<ol>
<li>Everything is an Object. Java 1.5 has auto-boxing, but that's not enough. For instance basic
math operations are not supported for the Number classes.
<li>Support for fully dynamic method dispatching, aka <a href="http://www.cyberdyne-object-sys.com/oofaq/oo-faq-S-1.19.html">multi-methods</a>: if a method is overloaded, the actual
type of the arguments is used and not the type of the reference. For example 
<pre>
void printMe(Object o) {
	System.out.println("I am an Object.");
}

void printMe(String s) {
	System.out.println("I am a String.");
}

// ....

String s = "Some string";
Object o = s;
printMe(o);
</pre>
would return "I am an Object." in Java, but "I am a String." with multi-method support.
</ol>
<br><br>
Three following steps are needed to add Python- and Ruby-like dynamic typing in such a language:
<br>
<b>1. Auto-Variable Declaration</b><br>
The first step is to allow implicit declaration of local variables. If a value is assigned
to an undeclared variable, it is automatically declared with the assigned type. The target
of a foreach statement can be auto-declared in the same way:
<pre>
void printStringArray(String strs[]) {
	i = 0;
	for (s: strs) {
		System.out.println("String number "+i+" is: "+s);
		i++;
	}
}
</pre>
There are two limitations: after the assignment the type can't be changed anymore, and the
declaration is only valid within the scope of the first assignment. Thus following functions are 
not allowed:
<pre>
String errorFunction1() {
	i = 0;
	i = "Some string"; // error, i is already int
	return i;
}

int errorFunction2(bool b) {
	if (b) {
		i = 10;
	}
	else {
		i = 6;
	}
	return i; // error: i is not defined in this scope
}
</pre>
Implicit local variables should not be on by default, but be enabled either using a compiler 
switch or by a short declaration in the compilation unit. It is a trade-off between type safety
(or in other words: letting your compiler check that you are using your types correctly) and
being too lazy to declare variables.
<br>
<br>
<b>2. The <i>any</i> type</b><br>
The key concept for dynamic typing is the <i>any</i> type. It is a reference to Object that, unlike a 
regular Object reference, disables all checks for member fields and implicit casts at compile time 
and executes them at runtime instead. In a JavaVM this can be implemented using Java's reflection 
APIs. Multi-methods are important for all function invocations with a <i>any</i> reference as argument, because otherwise the least specific overloaded function would be called. <br>
The first function with the any type for its local variables looks like:
<pre>
void printStringArray(String strs[]) {
	any i = 0;
	for (any s: strs) {
		System.out.println("String number "+i+" is: "+s);
		i++;
	}
}
</pre>
Any could be used anywhere, so if you want to obscure the function you could use <i>any</i>
for the argument:
<pre>
void printStringArray(any strs) {
	any i = 0;
	for (any s: strs) {
		System.out.println("String number "+i+" is: "+s);
		i++;
	}
}
</pre>
If the 'strs' argument is not a <i>Iterable</i> (the type needed for Java's foreach statement),
the function will abort with an exception in the <i>for</i> loop. <br>
(Sidenote: In this example the <i>any</i> variant has
the advantage that it can print any Object in any <i>Iterable</i>, but that's only because of the more 
restrictive original example - you could achieve the same with static typing by using <i>Iterable</i> for 'strs' and <i>Object</i> for 's')<br><br>
<i>Any</i> can change the type at any time, so the following is legal:<br>
<pre>
String validFunction1() {
	any i;
	i = 0;
	i = "Some string";
	return i;
}

int validFunction2(bool b) {
	any i;
	if (b) {
		i = 10;
	}
	else {
		i = 6;
	}
	return i;
}
</pre>
Any could also be used to write C++ template-like functions (but IMHO, if you need it for that purpose in a language with multi-methods and operator overloading, it only shows that you aren't using interfaces correctly).
<br><br>
<b>3. Combine them</b><br>
Step 3 does the obvious and combines both: if a value is assigned to an undeclared, local variable
then this variable is declared as <i>any</i>. Because it can have any type, it's no problem
the declare it for the whole method. Here the three example functions using implicit auto-<i>any</i>-declaration:
<pre>
void printStringArray(String strs[]) {
	i = 0;
	for (s: strs) {
		System.out.println("String number "+i+" is: "+s);
		i++;
	}
}

String validFunction1() {
	i = 0;
	i = "Some string";
	return i;
}

int validFunction2(bool b) {
	if (b) {
		i = 10;
	}
	else {
		i = 6;
	}
	return i;
}
</pre>
Like step 1 it should be enabled optionally. You would have the choice between real static typing (like in Java or C++), step 1's auto-declaration and step 3's auto-any-declaration.<br><br>
Here's the question to the dynamic typing zealots: would that be enough to make you happy? :)
