---
title:   "Thought Experiment: XML integrated into a C-like language"
date:    2003-12-16
authors:
  - tjansen
slug:    thought-experiment-xml-integrated-c-language
---
In recent days I made the following thought experiment: how can XML processing be made easier by integrating XML support into a Java/C#-like programming language.
<!--break-->

I created the code snippet below to try out what such a language could look like. The syntax of this theoretical language:
<ul>
        <li>Adds two hybrid-base types called Node and NodeList to the language. Hybrid means that they are Objects like java.lang.String in Java, but have their own literals and operators.
        <li>Node is similar to a DOM node, but uses the XPath data model (no DTD/Doctype, no entities, no CData sections, everything normalized)        
        <li>Using the index (&#91;]) operator an XPath expression can be executed on a Node, the result is a NodeList
        <li>A node has the operators += (add as a child), + (create a node list of the two nodes), -= (remove node from children) and &#60;&#60; (replace the node)
        <li>A NodeList is a list of references to nodes. It has operators like +, += (append a node list) and &#60;&#60; (replace all nodes)
        <li>A normal XML node literal is contained in &#91;&#91; ]] brackets. To avoid uneccessary escaping, you can use more than two brackets, e.g. &#91;&#91;&#91;&#91; &#60;element/> ]]]]. 
        <li>A perl-string-like XML node expression that allows the insertion of base types is enclosed in single brackets &#91; ]. This would be a simple node with content: &#91; &#60;text>Blabla &#36;{somevariable} &#36;anothervariable&#60;/text> ]  . Variables can be Nodes, NodeLists, Strings, numbers..
<li>You can cast any Node to NodeList. NodeLists can be casted to Node, but when the list has more than one member it throws an exception
       <li>Nodes can be implicitly casted to Strings
        <li>Strings can be implicitly casted to (text) nodes
        <li>the keyword <i>prefix</i> is used to define a XML namespace prefix to be used in XML node literals and XPath expressions. It can be used in all places you can declare a const variable, and has the same scoping rules
</ul>

The example assumes that you are familar with XPath. Dont expect the code to be really useful, it's just to get a feel for the syntax. I think I could get used to something like this...

<pre>
class Test {
        prefix ageext "urn:mascot-age-extension"; 

        static void main() {
                Node mascots = &#91;&#91;
        &lt;mascotList>
                &lt;mascot>
                        &lt;name>Tux&lt;/name>
                        &lt;species>Penguin&lt;/species>
                        &lt;project>Linux&lt;/project>
                        &lt;ageext:age>8&lt;/ageext:age>
                &lt;/mascot>
                &lt;mascot>
                        &lt;name>Konqi&lt;/name>
                        &lt;species>Dragon&lt;/species>
                        &lt;project>KDE&lt;/project>
                        &lt;ageext:age>3&lt;/ageext:age>
                &lt;/mascot>
        &lt;/mascotList> 
]];

                workWithMascots(mascots, 4);
        }

        void workWithMascots(Node mascots, int mimimumAge) {
                mascots&#91;&#47;mascotList&#47;mascot&#91;ageext:age &#60; &#36;minimumAge&#93;&#93; &lt;&lt; minimumAge;

                NodeList n = mascots&#91;&#47;mascotList&#47;mascot];
                foreach Node i in n {
                        Node summary = 
&#91;
&lt;summary>&#36;{i&#91;name]} is a &#36;{i&#91;species]} and the mascot of &#36;{i&#91;project]}&lt;/summary>
];
                        i += summary;
                }
        
                // print all mascots
                int num = 0;
                foreach Node i in n {
                        num++; 
                        Console.println(&#91;Mascot Number &#36;num: &#36;{i&#91;summary]}]);
                }
        }
};

</pre>

