---
title:   "Automating joining&leaving mailing lists"
date:    2003-07-26
authors:
  - tjansen
slug:    automating-joiningleaving-mailing-lists
---
The lack of comfort when joining mailing lists is annoying. If anybody wants to make me a little bit less grumpy, try the following:
- design a XML Scheme / file format to describe a mailing list (and especially how to join and leave it)
- register a corresponding mime type in KDE
- add a handler for that file type in KMail, or write a stand-alone app that uses the KMail backend
- when somebody clicks on a link with that mime type in Konqui, start the handler
- the handler will, after some GUI to explain it to the user and asking for permission, create a folder for the mailing list in KMail, create a filter for the mailing list, write a mail to the registration address to join and reply automatically to the confirmation mail
- KMail should have a 'unsubscribe' context menu entry that lets a user unsubscribe automatically

That would be useful... The next step, of course, would be a more comfortable way of browsing and finding mailing lists. KMail could have a build-in list of mailing lists (or download one) so people don't need to search for them.