---
title:   "New features"
date:    2003-07-08
authors:
  - admin
slug:    new-features
---
Hi,
	Today I added modules that will allow us to use "Fancy Quotes" and (tm) & (r).  (tm) can be made by placeing tm in parenthases and (r) can be done by placeing a r inside.

	Also links are now created automagicly from urls so http://www.kde.org and ftp://ftp.kde.org can appear as links with no extra markup.

	Im also working on installing buddy lists, a set of community bookmarks, and the ability to attach images to posts.  These should be up later today, as I need to make some changes to the database for these plugins.