---
title:   "Successful Akonadi Hack Sprint in Berlin"
date:    2007-08-27
authors:
  - bille
slug:    successful-akonadi-hack-sprint-berlin
---
So after 2 days of frantic hacking we made some good progress on Akonadi - including sorting out the database schemas with professional help for performance, producing benchmarking tools to find areas to improve in other layers, fixing MIME parser bugs, improving KMail in KDE 4.0 and KOrganizer's layouting.

[image:2953 align=center border=0 size=preview]

Thanks to KDAB for hosting us, Kris from MySQL for spending his weekend on our database, and KDE e.V. and Novell for their support.

