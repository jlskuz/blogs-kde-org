---
title:   "KDE NetworkManagement Sprint Day One"
date:    2009-06-05
authors:
  - bille
slug:    kde-networkmanagement-sprint-day-one
---
I pried my eyes open at 0430 and stumbled to the airport.  This all started about a month ago when we had the idea of having a developer sprint to get Network Management into shape in time for KDE 4.3's release.  Now I'm sitting in a meeting room in Oslo listening to the progress report of 3 Norwegian students Peder, Sveinung and Anders who are investigating ways to make setting up mobile broadband connections easier.  Thanks to the KDE eV's sponsorship, six of us are meeting this weekend.  TODOs include cleaning up UI glitches, fixing some exotic VPN types and auth types and deciding how to abstract different backends like wicd and ConnMan. 

If you want to help out or just rubberneck, we're in #solid.
<!--break-->
