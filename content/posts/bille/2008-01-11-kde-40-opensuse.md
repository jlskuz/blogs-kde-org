---
title:   "KDE 4.0 @ openSUSE"
date:    2008-01-11
authors:
  - bille
slug:    kde-40-opensuse
---
The nice thing about a community event is the way it brings everyone together.  I just gave a presentation to all my colleagues at the SUSE office here in Nuernberg on KDE 4.0, what it brings to the table and where it's going in the future.  It was great to have our two largest meeting rooms joined together, with a capacity audience.  For the last few weeks Dirk, Stephan, Lubos and I on the KDE Team have been working all the hours we can to add the final polish to 4.0 and to make sure the openSUSE packages of KDE 4.0 are the freshest and highest quality KDE binaries available.  

KDE 4 gives us an opportunity to demonstrate the power of the openSUSE Build Service.  By visiting this <a href="http://download.opensuse.org/repositories/KDE:/KDE4:/STABLE:/Extra-Apps/openSUSE_10.3/KDE4-DEFAULT.ymp">one click KDE 4 default pattern</a>, you can install a complete KDE 4 desktop onto your openSUSE 10.3 - without any typing at all.  Other patterns make it possible to update Qt, the KDE4 platform, or the entire desktop.   A special KDE4-DEVEL pattern contains only the KDE 4 build requirements.  In this way we aim to make openSUSE the best platform for <u>developing</u> KDE 4.

Some notes about KDE 4 from openSUSE:

KDE 3 and KDE 4 are co-installable
<ul>
<li>Start a KDE 4 session using 'Session Type' in your display manager</li>
<li>KDE 3 apps store settings in $HOME/.kde as usual</li>
<li>KDE 4 apps store settings in $HOME/.kde4</li>
<li>Caution: If you set KDEHOME (it is unset by default) both KDE3 and KDE4 apps will respect this value.</li>
<li>KDE 3 versions are clearly indicated in the menu in the description field eg "kwrite - Text Editor/KDE3"</li>
</ul>
Also
<ul>
<li>KDE 4 is the first openSUSE KDE where the applications live under /usr, for Filesystem Hierarchy Standard compliance.</li>
<li>You can easily update all packages if you know the name of your KDE4 repository.  If you used the one-click installer it is "KDE:KDE4".  Then do "zypper update -t package -r KDE:KDE4".</li>
<li>Our KDE 4 packages are frequently updated and include branch diffs relative to the tagged version for recent fixes.</li>
</ul>

That's it - <a href="http://en.opensuse.org/KDE/KDE4">read more about KDE4 on openSUSE</a> and then go start something amazing."<!--break-->