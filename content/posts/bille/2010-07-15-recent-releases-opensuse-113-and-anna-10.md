---
title:   "recent releases: openSUSE 11.3 and Anna 1.0"
date:    2010-07-15
authors:
  - bille
slug:    recent-releases-opensuse-113-and-anna-10
---
<p><img src="https://blogs.kde.org/files/images/11.3_out_now.png"/>

<p>Today <a href="http://en.opensuse.org/Portal:11.3">openSUSE 11.3</a> is released, concluding 8 months of intense and enjoyable work.  This release has been especially enjoyable for me, as it was the first openSUSE release where the community KDE team really took the driving seat and made decisions about what to include, updated packages and intensively tested.  Instead of just being a slave to a feature list this release, I was more occupied in enabling, advising and reviewing others' contributions.  I'd like to say "Excellent work!" to the whole openSUSE team here in Nuremberg, Prague, the rest of Novell and to every openSUSE contributor who has tested milestones, reported bugs, learned how to use <em>osc</em> and <a href="http://build.opensuse.org>build.opensuse.org</a> and made a difference.  

<p>By our recent standards, openSUSE 11.3 is a conservative release; it doesn't have bleeding edge features shoehorned in at the last minute by product management or novelty for its own sake.  Instead it's what a Linux distribution should be: a careful composition of the all the newest stable elements from upstream.  The KDE SC 4.4.4 that we ship is now stable and feature complete, by most accounts, and contains many tweaks and upstream patches to be an elegant, good-looking but also stable and usable desktop.  I hope that it will provide its users a lot of pleasure and utility, inspire many of them to jump on board at the openSUSE project, and others to explore all the possibilities and smoking hot new stuff available in the openSUSE Build Service.


<p>Oh and in case you wondered why I wasn't at Akademy in Tampere, here's why:

<p><a href="https://blogs.kde.org/files/images/4752532397_f30fdffe57_b_0.preview.jpg"><img src="https://blogs.kde.org/files/images/4752532397_f30fdffe57_b_0.thumbnail.jpg"/></a>

<p>After 9 months of enjoyable and intense work, our daughter Anna was released a couple of weeks ago.  At the moment she's quite unimpressed by computers, desktops and operating systems, but I hope that Free Software will be of benefit to her life as it already is to millions around the world.
<!--break-->
