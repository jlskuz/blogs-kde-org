---
title:   "Desktop memory usage"
date:    2006-09-12
authors:
  - lubos lunak
slug:    desktop-memory-usage
---
<p>
This was actually supposed to be a follow-up to <a href="http://blogs.kde.org/node/1663">my</a> <a href="http://blogs.kde.org/node/1664">tests</a> of startup performance of various desktop environments, primarily KDE of course :). In fact I even did most of the benchmarks some time after the startup ones, but, alas, I'm much better at writing things that computers are supposed to read than at writing things that people will read :-/ (some volunteer to write good user documentation for KWin's window specific settings, BTW ;) ?) I even meant to make a somewhat more extensive analysis of the numbers, but having never found time to write that, I decided I should publish at least a shorter variant with all the numbers and some conclusions. You can do your own analyses of the numbers if you will.</p>
<p>
These memory benchmarks are meant to measure various cases of desktop configuration and compare KDE to some other desktop environments ... and since it's actually a bit too long for a blog entry, I've put the complete version <a href="http://ktown.kde.org/~seli/memory/">here</a>. I'm not going to put any numbers here, but let me say that some of the numbers comparing KDE, GNOME, Xfce and Window Maker are quite interesting (and they come from one of my favourite <a href="http://blogs.kde.org/node/1567">hero tools Exmap</a>, so they shouldn't be completely off the track). However, one thing I'm going to copy here is the final section titled <i>The things we should learn from this:</i></p>
<ul>
<li>
Exmap can be very useful when doing certain memory usage analyses. In comparison the usual tools like <i>free</i> are often next to useless. Some of the <i>free</i> numbers above are simply absurd - although they're included because I measured them, they're not used anywhere.</li>
<li>
We are not significantly worse than competition. In few cases we are slightly worse, sometimes we are about the same, but often we are better, sometimes even noticeably better. If somebody tells us that we are lame, that C++ or KDE are inefficient or similar things, they should first check with our comparable competition.</li>
<li>
"Desktops" like Window Maker are not comparable competition. There are large differences in feature sets and even in concepts. Their users are happy with what they have and don't care about KDE, or, in the worse case, they badmouth KDE but would never switch anyway. The same way our users are unlikely to switch because Window Maker is not a desktop from our point of view.</li>
<li>
Although our libraries cause us some overhead, like the initial large requirements, they are our advantage. They allow us to write good applications that are often more lightweight than competition and of course there are many other benefits like large code reuse, many features and so on.</li>
<li>
However, there are still areas where we can improve. There are limits, of course, but we are not near them.</li>
<li>
Processes are in desktops are usually relatively heavy. This is caused by the various initializations done for GUI applications. Some of them are inefficiencies in our code and should be improved, some of them are problems of non-KDE code we depend on (like the already-mentioned <a href="http://blogs.kde.org/node/1495">fontconfig problems</a>), but some of them cannot be avoided (it's simply unrealistic to have specific code for each KDE application). Since such inefficiencies affect every KDE application, their impact should be minimized. The number of running KDE applications should be also kept generally low - either techniques like KDED modules and similar should be used, or, when possible, some processes should be avoided altogether.</li>
</ul>
<p>So, if you want to see the numbers and the rest, <a href="http://ktown.kde.org/~seli/memory/">here are the full details</a>.</p>
<!--break-->
