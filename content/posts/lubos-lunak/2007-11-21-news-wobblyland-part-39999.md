---
title:   "News from the Wobblyland, part 3.9999"
date:    2007-11-21
authors:
  - lubos lunak
slug:    news-wobblyland-part-39999
---
(I suppose using such number for this part makes the previous blog entry, part IV, written already quiiiiteee log ago, to have a wrong number, but who cares about that nowadays, it's just numbers, right? Anyway, it was so long ago that even I don't remember what it was about, which means I now have to think what to write about now. Hmm.)

I suppose I should maybe show some videos or screenshots again, but hey, we need testing for almost-KDE4.0, so if you want to see them, you need to use your own eyes for that. Besides, I don't remember what's new since then anyway, and I'm too lazy to check SVN. Ah, well, maybe one comes to mind:

[image:3108 size=original]

It is not a broken image but an effect called ShowPaint, which with various colors shows parts of the desktop that need redrawing. One can for example see that the KPluginSelector widget in the dialog redraws just on mouse being moved, that taskbar items repaint at least for 10 seconds after an animation has already stopped or that the Shadow effect is a bit too generous with causing repaints around windows (well, you obviously quite can't, since it's just a static picture, but in reality those parts flicker). Those are hardly critical right now, although such things can reduce performance of the composited desktop, and I also suppose the tool can be useful even for other developers.

But most work recently has been going under the hood. Although there have been also new visual features like the Login and Logout effects, most work has gone into the usefulness and stability areas. The sooner being e.g. config dialogs or keyboard usage in <a href="http://www.youtube.com/watch?v=SWaSz4smYlg">PresentWindows</a> and <a href="http://www.youtube.com/watch?v=LMnmGdk1ODs">DesktopGrid</a> effects, not shown in these old linked videos of course, but simply try hitting keys that make sense, like arrows or F&lt;n&gt; (BTW, there's one thing I'd like to get in other places in KDE/Qt - wrapping around works only without key autorepeat, that is, hitting Down once on the lowermost item will go to the topmost, but keeping Down pressed will stop at the lowermost - I really hate what holding Down pressed currently does e.g. in Konqueror's iconview).

However the testing and stabilizing work is now the most important one. It looks like we've made it for 4.0, with KWin compositing being usable and in quite good shape, although, indeed, it'll be still marked experimental and with few glitches (sorry if you'd like to use window shading with compositing in 4.0). The biggest question now seems to be whether to have it in 4.0 enabled by default, if possible. Even with Compiz having been around for like, what, almost two years, and XCompmgr even longer, support for compositing still has some rough edges (check, for example, <a href="http://websvn.kde.org/*checkout*/trunk/KDE/kdebase/workspace/kwin/COMPOSITE_HOWTO">kwin/COMPOSITE_HOWTO</a>, and I've run personally into every single of the issues listed there - I really don't want to know what David Reveman's buglist sometimes looks like). I still haven't decided on that one - we certainly want testing, but people unlucky enough to end up staring at a screen full of white squares with shadows around them probably wouldn't appreciate it.

Well, and while I have the attention :), few things:
- Bug reports go to <a href="http://bugs.kde.org">bugs.kde.org</a>. Including useful details like X version, driver and whether it works with Compiz (where applicable) would be nice.
- The API for the effects, just like rest of the compositing stuff, will be marked as experimental and unstable for 4.0. We even intend to document it in time for final 4.0 release :).
- I've said this one enough times to make it really boring, but just in case somebody has missed it yet for an unknown reason, people interested in helping with the development are welcome on the <a href="https://mail.kde.org/mailman/listinfo/kwin">kwin@kde.org</a> mailing list.
- People complaining about problems caused by them not having read the above-linked COMPOSITE_HOWTO will be assaulted by a hired specially trained mercenary commando of lethal chipmunks and will be publically spanked. I have a large storage of nuts and I mean it.
<!--break-->
