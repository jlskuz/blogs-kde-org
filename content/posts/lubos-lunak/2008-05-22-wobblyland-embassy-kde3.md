---
title:   "Wobblyland embassy in KDE3"
date:    2008-05-22
authors:
  - lubos lunak
slug:    wobblyland-embassy-kde3
---
I would post a screenshot of what this is going to be about, but the screenshot would look remarkably similar to other KDE3 screenshots I could post. Unless I switched the decoration to Oxygen/Ozone, but then yours truly is still quite happy with the KDE2 decoration (and then, also not quite happy with all those people who think that anything that's older than a year, especially if it's not shiny, must be oh-so-bad), so let's just skip that. You can try yourself after all.

SVN trunk (KDE-to-be-4.1) now has a selection of several window managers for KDE to use in the 'Session Manager' module in the control center (AKA systemsettings), which serves as a more friendly alternative to $KDEWM (although that one remains supported). The benefits are several:
<ul>
<li>People who don't know about $KDEWM should be still able to handle this and change their window manager as they like.</li>
<li>Compiz users will hopefully stop using the ugly Autostart hack and then even occassionally complain about how it breaks things in subtle ways. Updating to a Compiz version that will include my few fixes for its rather broken session management support may be useful, although I think I've already worked them around in ksmserver too. The default command to launch Compiz is 'compiz cpp', but you can create a custom one too (beats me why launching Compiz needs to be such a voodoo magic when for pretty much any other window manager Foo the right way to launch it is just 'foo'). Together with KDE4 mapping Compiz' viewports transparently to virtual desktops there should be hopefully no problem with running Compiz in KDE for those who want that.</li>
<li>People can add more window managers to the list to $KDEDIR/share/apps/ksmserver/windowmanagers (kdebase/workspace/ksmserver/windowmanagers in SVN).</li>
<li>I backported the patch to KDE3 and the latest packages in the openSUSE KDE:/KDE3 repository now have it (well, slightly older ones have it too, but broken, the correct ones have a changelog entry about fixing a patch for bnc#332079 from May 19th). And <a href="http://en.opensuse.org/OpenSUSE_11.0">openSUSE11.0</a> is going to ship it (users of other distros will probably have to dig it out of the package or apply some other kind of magic).</li>
</ul>

The last item of course means that those not brave enough to use KDE4 can still use at least KWin from KDE4 and get compositing, without regressions in the feature set or window management area (hmm, is that a very cheap jab at Compiz?). Unlike with other KDE4 components, we were too busy with compositing and didn't have time to break KWin ;) , which means KDE4's KWin in non-composited mode should be as good as the one from KDE3 and you won't be missing any features you're used to. The only problem I'm aware of is that the dialog for assigning a specific shortcut to a window (i.e. Alt+F3/Advanced/Window Shortcut) does not work as a result of the somewhat problematic shortcuts rewrite for KDE4, and, of course, some small bugs might have slipped in somewhere. Whether it works for you even with compositing enabled, well, that's up to you to try. And, pretty please, read the <a href="http://techbase.kde.org/Projects/KWin/4.0-release-notes#Setting_up">section on setting up compositing</a>. When KWin refuses to activate compositing, it's for a reason, so if it doesn't do anything for you, it's a problem between the chair and ... erm, configuration, I'd bet my hair color on that (oh wait, that's <a href="http://blauzahl.livejournal.com/3675.html">already taken</a>).

KWin from KDE4 of course will not work as well in KDE3 as it does in KDE4, but that's mostly details like Kicker's taskbar not having support for taskbar thumbnails, so that effect does not work there. Also, the settings are separate, so you may need to set KWin up again (I suggest copying kwinrc and kwinrulesrc from ~/.kde/share/config to ~/.kde4/share/config before switching). But generally it seems to work quite fine ... ok, I give up, maybe one picture after all.

[image:3477 size=original]

In other KWin-related news, there are now two new things that should help when something goes wrong with compositing, more or less obsoleting the more manual fixes from the <a href="http://techbase.kde.org/Projects/KWin/4.0-release-notes#Troubleshooting">troubleshooting section</a>. First one should be a special KDE4 failsafe session in KDM, which should launch KWin with compositing forced off. Second is a shortcut, Shift+Alt+F12 by default, that temporarily suspends compositing, until the shortcut is pressed again. That should help with finding the checkbox to turn off compositing again in case it goes awry :), but it can be also quite convenient when e.g. running a game that needs all the power the graphics card has.

Unfortunately neither of these has made it into openSUSE11.0 due to freezes :(, however in case of trouble blindly pressing Alt+F2 and typing 'kwin --replace' should start KDE3's KWin, which should do in most cases.

In fact, there is probably a third thing helping with compositing problems too. When I was in Nürnberg some time back to see the rest of the team, I noticed Dirk running KWin in XRender mode. What a surprise, does somebody actually use that :) ? So I checked that mode again and fixed/implemented the most visible problems. Which means, if OpenGL compositing does not work for you, you can still go to KWin compositing settings and there in the Advanced settings try switching the mode to XRender. Some of the more complex features don't work with XRender (and few are not implemented yet, there should be XRender shadows from Thomas Lübking soon though), but in general XRender may work better for you.

<!--break-->
