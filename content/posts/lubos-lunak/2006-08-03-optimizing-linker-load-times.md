---
title:   "Optimizing Linker Load Times"
date:    2006-08-03
authors:
  - lubos lunak
slug:    optimizing-linker-load-times
---
LWN has a very interesting <a href="http://lwn.net/Articles/192624/">article</a> summing up Michael Meeks' work on improving shared libraries loading as a part of his work to improve OpenOffice.org startup times (his <a href="http://go-oo.org/~michael/OOoStartup.pdf">paper</a> linked from the article is worth reading as well). And the dynsort and hashval optimizations that improve the data used while dynamically linking are finding their way into glibc as the --hash-style option.

These two are only relatively small optimizations though and there is no show of promise for a substantial improvement :(. Michael's -Bdirect was offhand <a href="http://sourceware.org/ml/binutils/2005-10/msg00437.html">dismissed</a> by Drepper by claiming that prelink should be used instead, apparently not knowing that OOo's startup time is dominated by dlopen() where prelink currently helps by doing exactly nothing. And while prelink could help with dlopen by treating interposing similarly like -Bdirect does, if that's not liked with -Bdirect it probably wouldn't be liked as part of prelink either, thus making prelink unable to ever help with dlopen(), despite dlopen() generally getting used more and more. Not to mention that prelink has other problems, like causing disk fragmentation (and there's currently no useful defragmenting tool) and has apparently seen only two snapshot <a href="ftp://people.redhat.com/jakub/prelink">releases</a> since the beginning of 2005. Although SUSE has shipped prelink for quite some time already it's never been enabled by default and now there are even suggestions it should be simply dropped. And finally in KDE's case it's in practice simply better to stay with kdeinit than to use prelink.

Don't get me wrong, I actually like prelink better than -Bdirect (my <a href="http://blogs.kde.org/node/1650#comment-4090">post</a> linked from the article got a bit misinterpreted) and I'd really like it see it really usable (because it ultimately can perform better than -Bdirect). However, the thing I'd like the most would be a solution that works, so with the current situation I hope distributions will start using -Bdirect regardless of what Drepper thinks about it.
<!--break-->
