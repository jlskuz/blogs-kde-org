---
title:   "kio_locate -- my little KDE gem of the week"
date:    2004-11-01
authors:
  - pipitas
slug:    kiolocate-my-little-kde-gem-week
---
<br>
My favourite little KDE gem of the week is <a href="http://arminstraub.de/browse.php?doc=kio_locate&lang=en">kio_locate</a>. Its maintainer, <a href="http://arminstraub.de/">Armin</a> has now added support for regular expressions and piping results through  <A href="/man:/grep">grep</A>. This increases usefulness by at least a factor of 10...

<br><br>
Ahhh -- you didn't know kio_locate <b>at all</b> up to now? Don&#039;t fear: until last Saturday not even distinguished KDE core hackers such as <A href="https://blogs.kde.org/blog/15">danimo</A> did. After I showed it to him, he was sold. (In return, he promised to burn a CD with legal sound files for me to test the new <A href="http://amarok.kde.org/">amaroK</A> with... danimo, I&#039;m waiting.).

<br><br>
kio_locate is one very small, but oh!, so useful little addition to KDE. It utilizes the powerful, yet still underrated KIO Slave architecture of KDE. KIO slaves add "protocols" to your desktop and let each application use these. A good first introduction you can gain from a <A href="http://osdir.com/Article2159.phtml">recent article</A> published on O'Reilly's OSDir.com website.

<br><br>
So what about kio_locate? We all know the good old <tt>locate</tt> command. Some of us, who haven't organized their directory hierarchy too well, type <tt>"locate sylvia.jpg"</tt> (or whatever we happen to search for) every day into a konsole window. The result we got, we then highlighted and pasted into the address field of <A href="http://gwenview.sourceforge.net/">gwenview</A> (or whatever cool image viewer program we happen to prefer). Quite an effort.

<br><br>
With kio_locate you can do (nearly) the same now in a Konqueror address field. Type in <i>"locate:sylvi.jpg"</i> and you&#039;ll get a list of matching pathnames in a konqueror window. Now you can do whatever you wanted to: click on one of the results to open it in &nbsp;your imaging application, drag it to a new location, copy it to another place, cram all matching pics into a *.tar.gz-archive... No detour needed via konsole -- do all at one place.

<br><br>
  As with all KIO Slaves, kio_locate isn&#039;t just for the Konqui address field. It can be used in each and every KDE application that understands URLs. Most important of all, you can use it directly in all &quot;File Open&quot; dialogs. Again, this safes lots of detours, clicks and typing effort. &nbsp;&nbsp;&nbsp;

<br><br>
 kio_locate isn't part of KDE currently. Hopefully it will be in 3.4. But to download it, untar/-zip and type <tt>./configure; make; sudo make install</tt> didnt take more than 5 minutes. Achim even provides the required documentation. After doing ALT+F2, typing <tt>khelpcenter</tt>, I easily found the little manual in the "kioslaves" section of the documentation. It is also <A href="http://arminstraub.de/browse.php?page=programs_kiolocate&lang=en">online</A>. And if you are keen for screenshots,  <A href="http://arminstraub.de/browse.php?page=programs_kiolocate_screenshots">look here</A>. 

<ul>
<li>Can you make the search case-insensitive?    
    <br>
    Yes, you can. Use <i> "ilocate:sYlVi.JPG" </i>.</li>
<li>Can you get the matching result of, say <tt>locate kdeprint | grep -v kde-unstable | grep lib</tt> &nbsp; ?
    <br>
    Yes, you can. Use <i> "locate:kdeprint !kde-unstable lib" </i>.</li>
</ul>
This last item took only <A href="http://www.kde-apps.org/content/show.php?content=17201">a little question</A> on KDE-kapps.org to ask. And 3 days later Achim had implemented it. Cheers, Achim!

<!--break-->
