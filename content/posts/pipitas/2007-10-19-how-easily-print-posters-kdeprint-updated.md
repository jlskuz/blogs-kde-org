---
title:   "How To Easily Print Posters With KDEPrint [UPDATED]"
date:    2007-10-19
authors:
  - pipitas
slug:    how-easily-print-posters-kdeprint-updated
---
<p>What a coincidence today happened. In the morning I used KDEPrint's 'poster' frontend to create a "poor man's poster" in A1 size from 4 A3 printouts.</p>

<p>In the afternoon, a lady mailed me, asking why her KDE print dialog on Solaris didn't show the poster dialog, while her husband's openSUSE KDE did show it.</p>

<p>I took the time, mailed her back what I knew about the question, and included a few screenshots.</p>

<p>Two hours later I thought for myself: <i>"WTF -- you took more than half an hour to write back to this lady and explain everything to her... Why not put another 30 minutes effort into it and convert the mail into a little tutorial to be published in my blog?"</i>.</p>

<p>I slightly changed my earlier mail in a few sentences, re-arranged it a bit, and posted a <a href="http://blogs.kde.org/node/3042">screenshot with a long comment</a> to kdedevelopers.org.</p>

<p>Hardly I was ready with this when I saw a <a href="https://lists.linux-foundation.org/pipermail/printing-summit/2007/001394.html">posting by Hin-Tak</a> on the 'printing summit' mailing list over at linux-foundation.com, asking about ... poster again (without remembering the name of the utilitiy). Happily I mailed the link to said image with comments back to him.</p>

<p>And now it's <i>"Heck! I may as well make a real blog post from it, add a few more screenshots and declare it a tutorial...."</i></p>

<p>So here we go.</p>

<hr>

<p>You [image:3042 align="left" width=408 height=444 hspace=6 vspace=4 border=0 class="showonplanet"] may already have come across the "Poster" tab in KDE's printing dialog. The one the screenshot to the left shows. It should be there for each printer you select from the drop-down list, even the virtual ones, that "Print to File" or "Send to Fax" or "Mail PDF File".
However, the poster tab of kprinter will *NOT* show up if you don't have the "poster" utility installed and in your $PATH. So if you want it, simply install the 'poster' package.</p>

<p><i>(<b>UPDATE:</b> Seems after Michael Goffioul's patches from 2002 there were more new features added to poster (which I wasn't aware of). There's a  <a href="http://bugs.kde.org/show_bug.cgi?id=132916">bug report 132916</a> which was pointed out to me in a comment below by jlp. Given that the bug reporter says "version 20060221 doesn't work, while <a href="ftp://ftp.kde.org/pub/kde/printing/poster-20050907.tar.bz2">version 20050907</a> does", it is probably saver to download and use the latter. BTW, openSUSE ships the version 20020826 which works as well. This bug may explain why Gentoo and Debian have reverted to a 1999 version of poster, which does not work with KDEPrint.) </i></p>

<p>Obtain "poster" from here: <a href="ftp://ftp.kde.org/pub/kde/printing/poster-20050907.tar.bz2">ftp.kde.org/pub/kde/printing/</a>.</p>

<p><b>Important:</b> you need to use the version from the link above, should your distro's version not function properly! It contains some patches to make it work with KDEPrint (poster's commandline abilities don't suffer from these patches!). The patches (written by our deerly missed Michael Goffioul, who currently does have too little time for active KDEPrint development) have also been accepted by the upstream poster developer, years ago.</p>

<p>Unfortunately, some recent distro releases (Debian?, *buntu?) for some reason seem to ship an older version which makes the kprinter poster tab display an error message.</p>

<p>As soon as you install the patched version (compiling it is easy), kprinter will start work with it.</p>

<p>If you figure your distro is using a b0rken version (or no poster package at all), you should contact its respective packager and/or submit a bug report or feature request. Ask them to use the patched version of poster to make it work with KDEPrint.</p>

<!--break-->

<p>Poster is meant to scale up a printout beyond available media sizes of your printer. You print [image:3045 align="left" size="thumbnail" hspace=6 vspace=4 border=0 class="showonplanet"] small tiles [image:3046 align="left" size="thumbnail" hspace=6 vspace=4 border=0 class="showonplanet"] of the final image [image:3047 align="left" size="thumbnail" hspace=6 vspace=4 border=0 class="showonplanet"] on your available paper (you do not even need to print all of them, as the line "tiles to be printed" shows). These pages you can glue together to form a "poor man's poster".</p>

<p>The actual printouts will slightly overlap, in order to allow some minor image shifting by the actual print devices, and by the person who uses scissors or cutters to trim the paper sheet towards the actual page image.</p>

<ul>
<li>Above screenshot shows what you can tweak with the settings (click thumbnails full size). </li>
<li>Determine the final poster size by selecting from the drop down listbox.</li>
<li>Determine the size of the printouts as "Page Size" on the "General" tab. </li>
<li>The number of tiles shown dynamically adapts to these size selections.</li>
<li>To select which tiles you want at all use the mouse and hold the shift key. </li>
<li>Printout will occur in the order you clicked the tiles (or typed their number into the line edit). </li>
<li>Alternatively, just type a comma-separted list of tile numbers into the line edit field.</li>
<li>Change the "Cut Margin" as needed.</li>
</ul>

<p>What benefit is it to make it selectable which tiles print, not not do all at once?</p>

<p>Well, you may want to try with two tiles first, and see if they fit and match what you expect. If they do, continue with more tiles, different ones this time. If they don't, change you settings and try again.
<p><b>Tip 1:</b> do not waste too much paper with experiments whose results you will not like. Instead you may first want to <i>"Print to File (PDF)"</i> by selecting such a printer in the first place.</p>

<p>Or enable the checkbox <i>"Preview"</i> on the main kprinter dialog, and cancel the printout if the preview doesn't look like you expect; then try again with different settings.</p>

<p><b>How it works "under the hood":</b> poster is utilized as a "prefilter" by KDEPrint. When KDEPrint receives a PostScript for printing (as is the case when you print from any KDE3 application), it sends this file to poster first, using appropriate commandline options (which you do not need to know if you use the GUI shown in the screenshot -- they are a bit awkward), receives the pre-filtered file from poster and sends it on to the real print subsystem (or to the preview application you may be using).</p>

<p>Of course, poster (the utility) isn't perfect, and if it fails, KDEPrint can't do much about it....</p>

<p><b>What you can do when it doesn't seem to work <i>at all</i>...</b></p>

<p>The utility and the kprinter tab do also work if you start kprinter as a standalone application (i.e. not from the <i>'Print...'</i> menu entry of an application) and load a PostScript file into it.</p>

<p>However, [image:3048 align="left" size="thumbnail" hspace=6 vspace=4 border=0 class="showonplanet"] if you start [image:3050 align="left" size="thumbnail" hspace=6 vspace=4 border=0 class="showonplanet"]  kprinter standalone, [image:3049 align="left" size="thumbnail" hspace=6 vspace=4 border=0 class="showonplanet"]  but load  a <i><b>non</b></i>-PostScript file (text, image, PDF), and use "poster" on it, you may see an error message, or it may simply not work as expected. If you are lucky, you'll see a dialog pop up that asks if KDE should convert the original file for you to create the correct format.</p>

<p>Why is this?</p>

<p>The poster utility can only work with PostScript files and does require PostScript as input format. (To be more precise: it expects a PostScript file that follows the <a href="http://en.wikipedia.org/wiki/Document_Structuring_Conventions">DSC recommendations</a>, the <i>Document Structuring Conventions</i> for PostScript files).</p>

<p>Is there no hope then?</p>

<p>Yes, there is.</p>

<p><b>Stacking different pre-filters</i>...</b></p>

<p>Just plug one more pre-filter into the pre-filtering chain of KDEPrint! One that creates the PostScript and feeds it to 'poster'.  ;-)</p>

<p>How to do that?</p>

<p>Click on the right-most tab, labelled <i>"Filters"</i>. Click on the top icon showing a funnel symbol. Select a pre-filter from the top-down list that takes your loaded input format, and converts it into PostScript:</p>

<ul>
<li>"Generic Image to PS Filter",</li>
<li>"PDF to PostScript Converter", or</li>
<li>"Enscript Text Filter"</li>
</ul>

<p>Make sure the stacking order of the two pre-filters (the "Poster" one may be active already)
is as needed. (If it's wrong, you'll see a unequivocal complaint in the user interface -- you can sort the order with the help of the "Up" and "Down" arrows.)</p>

<p><b>Tip 2:</b> The "Preview" checkbox is not available, if you run kprinter from the commandline. If you are not confident about the results that will go on paper, and if you don't want to waste precious resources, you may want to <i>"Print to File"</i> first instead of the real printer. Then you can verify if the result comes at least close to what you expect by using KPDF to view it (KPDF also works with PostScript files, should you have printed to PS).</p>

 <p>[image:3044 align="left" size="thumbnail" hspace=6 vspace=4 border=0 class="showonplanet"] <b>Tip 3:</b> While the "Preview" checkbox is not there, another one is, when you run kprinter from the commandline: it is on the bottom left corner of the dialog, labled "Keep this dialog open after printing", and does what it says. So as long as you are experimenting with the different print settings, you do not need to restart kprinter every time you want to change an option....</p>

<p>Voila! Poster printing with KDE. </p>
<p>From any KDE application. </p>
<p>With preview of results. </p>
<p>To any print device. </p>
<p>Even to the "PDF printer" that ships with KDE.</p>
<p>And also for any printable file format (PDF, image, PostScript, text),...<br/>
 ...when loaded into a kprinter started from the commandline (remember to enable an additional prefilter; one that consumes your original file, produces PostScript and pipes its output into the Poster pre-filter...)</p>

<p>How do you like that?</p>

<p>(And all this works in KDE since the days of KDE 2.2, released more than 6 years ago. But unfortunately, it was never really documented, remains a rather unknown little gem within KDEPrint, and in general is pretty under-appreciated.)</p>

<!--break-->