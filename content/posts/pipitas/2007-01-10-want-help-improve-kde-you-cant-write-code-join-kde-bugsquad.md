---
title:   "Want to help improve KDE? But you can't write code? Join the KDE Bugsquad!"
date:    2007-01-10
authors:
  - pipitas
slug:    want-help-improve-kde-you-cant-write-code-join-kde-bugsquad
---
<p>Last weekend I took part in the <A href="http://developernew.kde.org/Contribute/Bugsquad/Konqueror_Weekend_Jan2006">"Konqueror Bugsquad Days"</A>. We had a few handful of KDE contributors taking part, AFAIK all of them non-C++/Qt coders. Knowing nearly nothing about HTML rendering, or JavaScript and what-not, I picked to sift through all bug reports that contained the string "print".</p>

<p>Why do we do it at all? We want to cleanse the bug data base.</p>

<p>Why do we want to cleanse it? To get rid of duplicate + invalid + solved + un-verifi-able entries. To verify and improve bug descriptions if the original reporter didn't supply enough details.</p>

<p>Why is that nice? Because then the real developers of the respective application (here: Konqueror/KHTML) can better concentrate on *fixing* bugs. And because this is a job that can easily be done to a large part by non-coders.</p>

<p>If you want get in touch, join the <A href="irc://freenode.net/kde-bugs">#kde-bugs</A> channel on <A href="http://www.freenode.org/">Freenode</A>. You'll find people hanging out there most of the time.</p>

<p>Will <i>you</i> take part in the next round of KDE Bugsquad attacks?</p>

<!--break-->