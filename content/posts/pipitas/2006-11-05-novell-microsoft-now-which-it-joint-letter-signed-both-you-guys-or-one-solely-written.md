---
title:   "Novell, Microsoft: Now which is it -- a \"joint\" letter signed by both of you guys, or one solely written by Novell?"
date:    2006-11-05
authors:
  - pipitas
slug:    novell-microsoft-now-which-it-joint-letter-signed-both-you-guys-or-one-solely-written
---
<hr><i><dl>
<dt>UPDATE:</dt>
<dd>Meanwhile, the difference in the headings of the "Joint Letter to the Open Source Community" (described below) is gone. (Screenshots of original versions linked in the comment by RangerRick, below). Microsoft <a href="http://www.microsoft.com/interop/msnovellcollab/open_letter.mspx">updated</a> their respective webpage and now uses the same heading as Novell uses. Looks like this change is <a href="http://blogs.msdn.com/jasonmatusow/archive/2006/11/07/joint-or-separate-the-content-matters.aspx">due to this blog entry</a> :-)</dd>
</dl>
</i><hr>

<p>While we are still a good few days away from a daily need to seriously watch out for arial pork (so that they don't shit on our heads), nevertheless strange things are happening already. Just look at this headline: <i>"Fox marries chicken; honeymoon to be spent in henhouse."</i> Naah, just kidding...</p>

<dl>
<dt>However, this one is for real (<a href="http://www.microsoft.com/interop/msnovellcollab/open_letter.mspx">www.microsoft.com</a>):</dt>
    <dd>"<b>An Open Letter to the Community from Novell</b><br><small><b>published: November 2, 2006 | Updated: November 2, 2006</b></small><br>As part of the announcement activities, Novell is reaching out to the open source community with an open letter regarding the agreement.<br><i>Today's announcement of the collaboration between Microsoft and Novell marks the beginning of a new era: Microsoft is coming to terms with Linux.<br>Over the past six years, we've seen the effect that the open source community has had on Microsoft.</i>"<br>[..... (here follows the text of the Open Letter) .....]</dd>
</dl>

<dl>
<dt>Now compare it with this (<a href="http://www.novell.com/linux/microsoft/openletter.html">www.novell.com</a>):</dt>
    <dd>"<b>Joint letter to the Open Source Community</b><br><b>From Novell and Microsoft</b><br><i>Today's announcement of the collaboration between Microsoft and Novell marks the beginning of a new era: Microsoft is coming to terms with Linux.<br>Over the past six years, we've seen the effect that the open source community has had on Microsoft.</i>"<br>[..... (here follows the text of the Open Letter) .....]</dd>
</dl>

<p>Now which one is it? A joint Open Letter signed and taken responsibility for by both companies? Or one that is solely drafted, written and signed by Novell?</p>

<p>As it looks, our happy honeymooners seem to experience their first matrimonial disagreement already. That doesn't bode well for any successfull raising of their common brood. Novell tells the bewildered wedding party (which was already stunned by surprise marriage coup that came without any timely put up of banns) what we ought to think about their new bed partner Microsoft, and calls the piece a <b>joint</b> letter, even labelling it as <i>"From Novell and Microsoft"</i>. Microsoft meanwhile calls it a "to the Community from Novell" thingie, washing its hands in innocence....</p>

<p>I'm taking bets:</p>

<ul>
   <li>Which one of the two websites will be modified to bring it into line with the other, and when will it happen?</li>
   <li>Will Microsoft admit it had indeed undersigned a "Joint Letter", but were too much of a coward to be seen with it in public?</li>
   <li>Or will Novell tell us, reluctantly, "Ah, just an unfortunate mistake by our webmaster, may Microsoft forgive him"?</li>
</ul>

<p>Uhmmm... and how come, Novell could have it messed up like that? Did Microsoft lawyers have to give their secret nod to the wording of an "it's-a-Novell-only" document first, while Novell's uppermost tier stupidly mistook this pouch check for an official endorsement that best be sold to us as a common statement? </p>

<p><b>In any case: Novell -- you are expecting we are still gonna trust you? Trust Microsoft's honesty?  Trust your honesty? Trust your smartass cleverness to strike deals with the Borgs without getting eaten alive? </b></p>

<p><small>(No, I've not not saved local copies of the two websites in case they silently change them. And no, I've not scrutinized the respective texts of both copies for diffs, to see if each comma and semicolon is at the same place... Why should I do the job of a paid journalist? The likes of <a href="">BP</a>, <a href="">SJVN</a> or <a href="">MJF</a>, who are much better in such kind of research?)</small></p>

<!--break-->