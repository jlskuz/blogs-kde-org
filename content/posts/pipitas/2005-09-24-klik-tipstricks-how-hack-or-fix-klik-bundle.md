---
title:   "klik tips+tricks: how to hack (or fix) a klik bundle"
date:    2005-09-24
authors:
  - pipitas
slug:    klik-tipstricks-how-hack-or-fix-klik-bundle
---
<p>Time to reveal a few more of the little tricks around klik...</p>

<p>This time it is <i>"How do I fix a klik AppDir bundle if it does not work the way I want it to work?"</i> </p>

<p>Easy. Follow these steps to troubleshoot a problematic klik .cmg file. (We assume, that your Kernel is in fact cramfs and loop-mount enabled; this is surely the case, if you succeeded before to run one of the simpler klik applications, like <A href="klik://xvier">klik://xvier</a>, and only suffer from a problem with certain .cmg files).</p>

<p>We'll use as an example <A href="http://edu.kde.org/kalzium/index.php">"Kalzium"</A>, the cool chemistry application (<A href="http://edu.kde.org/kalzium/screenshots.php">screenshots</A>) by <A href="http://www.livejournal.com/users/cniehaus/">Carsten Niehaus</A>, from the KDE Education module. (I do not say the bundle needs a fix --  it works fine for me; but maybe someone wants to update it manually to KDE-3.5, so that it contains the latest and greates improvements Carsten made?).</p>
<code>
STEP  1:  install the klik client: "wget klik.atekon.de/client/install -O - | sh" 
STEP  2:  klik-copy and -run the existing kalzium bundle: "[alt]+[f2]"; "klik://kalzium"
STEP  3:  find the "kalzium.cmg" in your ${HOME}/Desktop/ directory
STEP  4:  unpack it: "cd ${HOME}/Desktop/; /sbin/fsck.cramfs -x kalzium/ kalzium.cmg"
STEP  5:  go into the new "kalzium" directory and explore it; find the "wrapper" script, and the "/usr/" subdir, and the "/opt" symlink, and the rest.
STEP  6:  run the bundle from inside its subdirectory: "sh wrapper" does the trick
STEP  7:  exchange all binaries and libs for newer ones
STEP  8:  test the changes in the "kalzium" subdirectory first, by running the app from there: "cd kalzium; sh -x wrapper"
STEP  9:  fix problems you encounter, by repeating STEPS 5-7 as often as needed
STEP 10:  pack the new bundle: "cd ${HOME}/Desktop/; /sbin/fsck.cramfs kalzium/ kalzium-new.cmg"
STEP 11:  run the new bundle. Either just click on it from Konqui, or use the commandline start: "${HOME}/.zAppRun ${HOME}/Desktop/kalzium-new.cmg"
STEP 12:  enjoy!
</code>
<dl>
<dt>Step 1</dt>
<dd>The IP of <A href="http://klik.atekon.de/">klik.atekon.de</A> is <A href="http://134.169.172.48/">134.169.172.48</A> in case the DNS problem is still prevalent. It is advisible to re-install the klik client from time to time. The reason is: klik is a development in full swing. We add bugfixes and improvements (like handling of new distros), and this will only make it to you if you let the client be re-installed onto your system. The way to install the klik client is this: hit <tt>"[alt]+[f2]"</tt>; type <tt>"wget klik.atekon.de/client/install -O - | sh"</tt>; hit <tt>"Enter"</tt>. This install script is not saved onto your harddisk (only its output is), but executed on the fly by piping its content into a shell (the <tt> "| sh" </tt> part). If you want to have a look at the install script itself (it is only 15 kByte), modify the above sequence like this: <tt>"[alt]+[f2]"</tt>; type <tt>"wget klik.atekon.de/client/install"</tt>; hit <tt>"Enter"</tt>. This will save (and not execute) the file "install". As was already outlined in my <A href="http://dot.kde.org/1126867980/">initial Dot article</A>, the install procedure creates two files in your home directory: <i>.zAppRun</i> and <i>.klik</i>, plus a few <i>.desktop</i>, <i>.protocol</i> and <i>.hidden</i> files.</dd>

<dt>STEP 2</dt>
<dd>You know by now how to klik-copy and -run the existing kalzium bundle, yes? Just do <tt>"[alt]+[f2]"</tt>; <A href="klik://kalzium">"klik://kalzium"</A>. The "klik://kalzium" fake URL in the meanwhile does not just work for KDE and Konqueror -- Firefox and elinks are now supported too. What the "klik://kalzium" action then will do is this: it contacts the <A href="http://klik.atekon.de/">klik server</A> to ask for "kalzium"; the klik server sends back the <A href="http://klik.atekon.de/apt/?package=kalzium">Kalzium "recipe"</A> containing a list of Debian packages (Kalzium plus dependencies) to download, plus the commands to unpack the Debian packages, to stuff them into a directory structure, to spice the soup with additional symlinks and environment variables (<tt>$PATH</tt> and <tt>$LD_LIBRARY_PATH</tt>) come to mind), to arrange them in a way so that the correct directory structure is the result, and finally to compress them into a cramfs filesystem image.</dd>

<dt>STEP 3</dt> 
<dd>Once the Kalzium is started, you can find the file "kalzium.cmg" in your <i>${HOME}/Desktop/</i> directory. This .cmg is a compressed image containing a compressed file system (much like the ISO image that fills the complete Knoppix or Kanotix CD) which holds all binaries, libraries, icons and helper files that the Kalzium application needs to run. If you do not like the default locations where klik stores your new klik-able applications, you can freely move the kalzium.cmg file to other locations; it will still run from there.</dd>

<dt>STEP 4</dt> 
<dd>Unpack the bundle: <tt>"cd ${HOME}/Desktop/; /sbin/fsck.cramfs -x kalzium/ kalzium.cmg"</tt>. This creates the <i>"kalzium/"</i> sub directory, where you can now browse the contents of the bundle. You could browse the bundle by mounting it, or by running it (which mounts it too) -- but then it would be read-only, and you could not make changes to it.</dd>

<dt>STEP 5</dt> 
<dd>Now go into this "kalzium" directory and explore it. Find the <tt>"wrapper"</tt> script, and have a look at it. Find the <i>"usr/"</i> and <i>"opt/"</i> subdirs, and take note of their contents (symlinks as well as real files). </dd>

<dt>STEP 6</dt> 
<dd>Did I tell you yet, that you can run the application from the subdirectory? Try this: <tt>"cd kalzium; sh wrapper"</tt>. For more output (possibly good for debugging): <tt>"sh -x wrapper"</tt>.</dd>

<dt>STEP 7</dt> 
<dd>You may be one the Kalzium developers: this means you do not like the old version of your app so much that is represented by the downloaded kalzium.cmg. OK, go ahead, exchange all binaries and libs and icons and documents in that subdirectory for newer ones!</dd>

<dt>STEP 8</dt> 
<dd>When you are done with the modifications: test the changes inside the <i>"kalzium"</i> subdirectory first, by running again the app via the wrapper script: "<tt>cd kalzium; sh -x wrapper"</tt>.</dd>

<dt>STEP 9</dt> 
<dd>Fix any problems you encounter; repeat STEPS 5-8 as often as needed</dd>

<dt>STEP 10</dt> 
<dd>Last, pack the new bundle into a compressed image file: <tt>"cd ${HOME}/Desktop/; /sbin/fsck.cramfs kalzium/ kalzium-new.cmg"</tt></dd>

<dt>STEP 11</dt> 
<dd>Run the new bundle. Either just click on it from Konqui, or use the commandline start: <tt>"${HOME}/.zAppRun ${HOME}/Desktop/kalzium-new.cmg"</tt></dd>

<dt>STEP 12</dt> 
<dd>enjoy!</dd>
</dl>

<p>As you can see, modifying klik bundles is pretty easy. If you want help bring the current 4.000+ klik packages and recipes into really good shape, join us at channel #klik on <A href="http://Freenode.net/">Freenode</A>, or <A href="https://mail.kde.org/mailman/listinfo/klik">subscribe</A> to the <A href="mailto:klik@kde.org">klik@kde.org</A> mailing list.</p>

<!--break-->