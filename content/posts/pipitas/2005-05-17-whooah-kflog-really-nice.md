---
title:   "Whooah -- KFlog is really nice!"
date:    2005-05-17
authors:
  - pipitas
slug:    whooah-kflog-really-nice
---
Aaron's <a href="http://aseigo.blogspot.com/2005/05/andr-somers_16.html">last blog entry</a> was a real eye-opener to me.<br>
I didn't know <a href="http://www.kflog/org/">KFlog</a> before.<br>
I'll tell a friend of mine about it, who "has to" use Linux at work (and he likes it) but who uses MS Windows at home... guess why? He is <a href="http://www.glidingmagazine.com/PhotoGallery/1040.jpg">a glider/sailplane pilot</a>, and told me there is no feasible software on Linux to use for his hobby.<br>
The fact that <i>"KFLog is the only flight analyser program available for Linux to be recognized by the FAI IGC"</i> will surely help to convince him. (<a href="http://www.fai.org/">FAI</a> is the <I>F&eacute;d&eacute;ration A&eacute;ronautique Internationale</I>, and <a href="http://www.fai.org/gliding/">IGC</a> its <I>International Gliding Commission</i>.)<br>
I wonder if KFlog could be useful for paragliding too? I'll find out, sometime....<br>
<!--break-->