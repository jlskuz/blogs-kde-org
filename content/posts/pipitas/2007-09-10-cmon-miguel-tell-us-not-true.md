---
title:   "C'mon, Miguel... tell us this is not true!"
date:    2007-09-10
authors:
  - pipitas
slug:    cmon-miguel-tell-us-not-true
---
<p>Today I experienced two moments of bewilderment, the second one mixed with dismay.  At first, when I googled for something unrelated, on one of the returns I saw a forum post where someone said "Icaza himself says that OOXML is superb". Well, first I was amazed, then I shrugged, and wrote it off as a troll, and continued with my other tasks. Two hours later I remembered again.</p>

<p>So I googled once more. This time simply for <a href="http://www.google.com/search?q=ooxml+superb">OOXML +superb</a>. And sure enough, I <a href="http://groups.google.com/group/tiraniaorg-blog-comments/msg/57a3560ebfed98a4?dmode=source">found this</a> (may also be found <a href="http://groups.google.com/group/tiraniaorg-blog-comments/browse_thread/thread/2a07b8b50038d8c8/d582162af2d63d57">here</a>). </p>

<p>The Google group where this quote (which is only 5 days old) is from, is setup by Miguel himself, specifically to provide a feedback forum for his blog. Here is the quote. The emphasize was added by me:</p><dl><dt>&nbsp;</dt><dd><p><b><i>OOXML is a superb standard and yet, it has been FUDed so badly</i> by its competitors that serious people believe that there is something fundamentally wrong with it.  This is at a time when OOXML as a spec is in much better shape than any other spec on that space.</p>

<p>Besides, it is always <i>better to have two implementations and then standardize than trying to standardize a single implementation</i>.</p> </dd></dl></b>
<!--break-->
<b></b>
<p>Come on, Miguel! Please tell us this is not what you said. This must be a forgery. Google must have f+cked up with its archive. Microsoft hackers must have cracked the hosting server. Or your email account, and they posted under your name. Or you didn't mean it. You had a terrible headache that Wednesday night. You thought it's April Fool's Day, and it was a good joke. You just wanted to test if it gets noticed.</p>

<p>Whatever. <b>Just tell us that it is not what you really think about OOXML.</b></p>

<p><b>Update:</b> <small>So it's true. It <b><i>IS</i></b> his opinion, and it is not misrepresented. -- I'm speechless. Returning to the base office after a long workday spent on a customers' site, not only do I find my blog entry having been <a href="http://linux.slashdot.org/article.pl?sid=07/09/10/2343256">submitted by someone to Slashdot</a> and to <a href="http://digg.com/linux_unix/Miguel_de_Icaza_Gnome_creator_linux_developer_and_OOXML_supporter">Digg</a>, but also Miguel has now personally confirmed above quote in <a href="http://slashdot.org/comments.pl?sid=293507&cid=20547277">a long Slasdot comment</a>. If you want to read all of his arguments, be sure to search for <i>'miguel (7116)'</i> &nbsp; <A href="http://linux.slashdot.org/comments.pl?sid=293507&threshold=0&commentsort=0&mode=nested&startat=0&pid=0">on</A> &nbsp; <A href="http://linux.slashdot.org/comments.pl?sid=293507&threshold=0&commentsort=0&mode=nested&startat=75&pid=0">each</A> &nbsp; <A href="http://linux.slashdot.org/comments.pl?sid=293507&threshold=0&commentsort=0&mode=nested&startat=150&pid=0">of</A> &nbsp; <A href="http://linux.slashdot.org/comments.pl?sid=293507&threshold=0&commentsort=0&mode=nested&startat=225&pid=0">the</A> &nbsp; <A href="http://linux.slashdot.org/comments.pl?sid=293507&threshold=0&commentsort=0&mode=nested&startat=300&pid=0">expanded</A> &nbsp; <A href="http://linux.slashdot.org/comments.pl?sid=293507&threshold=0&commentsort=0&mode=nested&startat=375&pid=0">Slashdot</A> &nbsp; <A href="http://linux.slashdot.org/comments.pl?sid=293507&threshold=0&commentsort=0&mode=nested&startat=450&pid=0">pages</A>. I'm tired now, and I myself haven't yet read all of his reasonings (and probably never will). I'm also speechless... (oh, I said so already...).</small></p>
