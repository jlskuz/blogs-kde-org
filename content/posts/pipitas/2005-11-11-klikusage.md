---
title:   "klik://usage"
date:    2005-11-11
authors:
  - pipitas
slug:    klikusage
---
<p><A href="http://klik.atekon.de/wiki/index.php/User's_FAQ">klik User's FAQ</A> continued. Today the "Usage" section:</p>

<TABLE width="95%" cellspacing="2" border="1" cellpadding="2" align="center" bgcolor="#d3d3d3">
  <tbody>
    <tr>
      <td>


<b>Usage</b>

<p>
<b>Q: </b><i>Where does klik store the newly installed applications?</i>
<b>A:</b> All applications are stored on your desktop. In the file system, this is the directory $HOME/Desktop.</p>

<p>
<b>Q: </b><i>Why does it put all downloaded .cmgs onto the Desktop?</i>
<b>A:</b> This is to make it more easy and more obvious to users where the new klik application ended up.</p>

<p>
<b>Q: </b><i>Can I move the .cmg files away from my Desktop? </i>
<b>A:</b> Yes. The .cmg files will run from any place in your local file system.</p>

<p>
<b>Q: </b><i>What do I need to do to install and run an application via klik?</i>
<b>A:</b> Just klik on a "klik://appname"-type of link.</p>
<p>
<b>Q: </b><i>Is there a commandline client for klik?</i>
<b>A:</b> Yes. You can run ".klik" from the command line too: try "$HOME/.klik klik://xvier" or simply "$HOME/.klik xvier".</p>

<p>
<b>Q: </b><i>Can't you make the klik application more verbose?</i>
<b>A:</b> Yes we can. However, we want to make it as fast and as little confusing to "newbie" and "grandma" users too. It is an art to find the right balance, and maybe it will always be subject to objections by some groups of users, however we do it.</p>

<p>
<b>Q: </b><i>Why does it download xvier again and again each time I click on <a href="klik://xvier">klik://xvier</a> ?</i>
<b>A:</b> This is by design. Just don't click the same klik:// link twice. To run a klik application already present on your system, there are other means.</p>

<p>
<b>Q: </b><i>How can I run a klik application already present on my system?</i>
<b>A:</b> Just click (or double-click, depending on your settings) onto the <appname>.cmg file.</p>

<p>
<b>Q: </b><i>Can i run the klik application from the command line too?</i>
<b>A:</b> Yes. Use "$HOME/.zAppRun /path/to/appname.cmg", or "$HOME/.zAppRun -x /path/to/appname.cmg" for a more verbose output.</p>

<p>
<b>Q: </b><i>I see most klik applications are for GUIs. Are there non-GUI ones too?</i>
<b>A:</b> Yes. Try <a href="klik://scli">klik://scli</a> for an example.</p>

<p>
<b>Q: </b><i>What are the main use cases for klik?</i>
<b>A:</b> We see two main purposes for klik: First, use klik to add missing applications to Linux running from live CD systems. Second, use klik to quickly evaluate (or beta-test) new versions of a certain software without endangering your current system library setup. </p>

<p>
<b>Q: </b><i>Do you advice to leave the "old-school" package management systems like RPM or APT behind, and replace it with klik?</i>
<b>A:</b> No, not at all! klik is not a replacement for your established package manager, but merely a good complement. Use klik to evaluate new software. Install it permanently, using your system's package manager once you are convinced of a new application or new version.</p>

<p>
<b>Q: </b><i>Can I use klik to run applications permanently?</i>
<b>A:</b> Yes.</p>

<p>
<b>Q: </b><i>Can I use klik to run applications from a USB stick?</i>
<b>A:</b> Yes.</p>

<p>
<b>Q: </b><i>Can I use klik to run applications from a CD?</i>
<b>A:</b> Yes.</p>

<p>
<b>Q: </b><i>Does klik work for GUI applications only?</i>
<b>A:</b> No. There are some commandline utilities that work with klik too.</p>

<p>
<b>Q: </b><i>Are there some "rules of thumb" for the type of applications that work with klik best?</i>
<b>A:</b> All simple standalone GUI programs should work "out of the box".</p>

<p>
<b>Q: </b><i>Can you tell which type of applications are not expected to work with klik?</i>
<b>A:</b> Complex applications with lots of dependencies are more difficult to get working via klik. Examples are integrated development environments (IDEs) (like KDevelop, eric3 or gambas2), applications requiring databases (like Kexi), programs which need another service running (like amarok, which needs a sound server), or servers which generally need root privileges to start or require access to a TCP/IP-UDP port below 1024 (CUPS, BIND, Samba, Apache,...). </p>

<p>
<b>Q: </b><i>Is it possible to make the more complex applications work via klik?</i>
<b>A:</b> Yes, it is possible. However -- we just need time to figure it out. Your help is welcome, though.</p>

</td>
    </tr>
  </tbody>
</TABLE>

<p>Tomorrow's topics: <i>"klik and the Base System"</i> and <i>"klik and Package Management"</i>. I've now completed about 50 out of 105 questions; my notes for tomorrow list another 10. Submit yours to me -- preferably with the answers provided, please.</p>
<!--break-->