---
title:   "My openSUSE-10.2 adventure starts..."
date:    2007-04-10
authors:
  - pipitas
slug:    my-opensuse-102-adventure-starts
---
<p>The notebook mentioned in my last blog: yesterday it got an openSUSE-10.2 installation completed. </p>

<p>What tempted me most in favor of openSUSE, was that I had read about their "build service", and that they offer regularly updated KDE4 snapshot RPMs from there, which possibly can save me lots of compile time while I familiarize myself again what's current with KDE developments.</p>

<p>However, KDE4 is not yet installed. </p>

<p>I first had to become familiar with smart, and the dozens of "channels" leading to repositories + sub-repositories in the build service. Cool as the build service in principle is -- the way that its "accessibility" is organized makes it much less useful than it could be. </p>

<p>It took me many, many hours to search for the specific packages and package updates I wanted (mainly printing- and fonts-related stuff). There is no easy-to-use interface to the build service's "fruits" for the end user (and I do not thing of "Aunt Tillie"-type of end users, but people like myself who have by now quite some experience about how to administer their own system, and that of colleagues and customers as well). </p>

<p>Had I not discovered one good soul's <a href="http://benjiweber.co.uk:8080/webpin/">openSUSE package search web page</a> (an extremely cool and useful service!), I would not have found half the non-standard (or updated) packages that I have now installed on top of the 10.2 DVD.</p>

<p>So -- thanks a lot, "benjiweber"; your great work just saved me a lot of time (and Novell should reward you for doing their job)!</p>
<br>
<hr><p><b>Update/Addendum:</b></p> I found <a href="http://blogs.kde.org/node/2761">Beineri's hint</a> regarding the build service <a href="http://software.opensuse.org/download/repositories/KDE:/Backports/openSUSE_10.2/repodata/latest-feed.xml">latest changes news feed</a> quite interesting. What a pitty that I had to use Firefox to see its contents in a recognizeable way; Konqui 3.5.6 (as shipped by the build service) could not display the link properly....

<!--break-->