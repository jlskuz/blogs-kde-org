---
title:   "Hello, Fellow Planet SUSE Bloggers :)"
date:    2005-09-15
authors:
  - pipitas
slug:    hello-fellow-planet-suse-bloggers
---
<a href="http://planetsuse.org/">planetsuse.org</a> is now syndicating my blog. Thanks <a href="http://rubberturnip.org.uk/">James</a>, for taking my feed to your part of our world. -- Hello, fellow planetSUSE bloggers  :) 

Maybe it was <a href="https://bugzilla.novell.com/show_bug.cgi?id=116309">this rather interesting bug</a> that helped proof that I have somehow some relationship with the Green Geeko Distro.

I'm just wondering why they have not yet accepted <a href="https://blogs.kde.org/blog/457">Beineri</a> into that circle, with him being even an employee in Nuremberg now, packaging KDE: did he not yet fullfill his initial "You-must-first-build-1000-RPMs-before-we-let-you-in" newbie quota?   ;-P
<!--break-->