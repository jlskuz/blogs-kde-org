---
title:   "klik User's FAQ: \"Installation\" and \"File Format\" chapters"
date:    2005-11-07
authors:
  - pipitas
slug:    klik-users-faq-installation-and-file-format-chapters
---
<p>Work on the <A href="http://klik.atekon.de/wiki/index.php/User's_FAQ">klik User's FAQ</A> continued. This morning I completed the "Installation" section. This evening followed the "File Format" one. The plan to add at least one new section each day during the next two weeks so far is met. It will be hard to stay on target, because lots of distractions are scheduled already, and multiple days where I'm not sure to be able to be online. -- Today's local preview:</p>

<TABLE width="95%" cellspacing="2" border="1" cellpadding="2" align="center" bgcolor="#d3d3d3">
  <tbody>
    <tr>
      <td>
<b>Installation</b>

<p>
<b>Q: </b><i>How much effort is it to install the klik client on my system?</i>
<b>A:</b> Less than 20 kByte of download; usually less than 20 seconds of effort.</p>

<p>
<b>Q: </b><i>What is the procedure to install the klik client?</i>
<b>A:</b> Type "<tt>wget klik.atekon.de/client/install -O -|sh</tt>" into a terminal window (or mini-cli); follow the instructions.</p>

<p>
<b>Q: </b><i>What files does a klik client installation put into my system?</i>
<b>A:</b> Into your <i>$HOME</i> it puts 2 scripts, <i>.klik</i> and <i>.zAppRun</i>. Into /etc/fstab it adds 7 additional mountpoints (for this action the klik installer needs root privileges once). Into your <i>$HOME/.kde/</i> (or variations thereof) it puts a few <i>*.protocol</i>, <i>*.desktop</i>, <i>.directory</i> and assorted files that help handle the klik applications.</p>

<p>
<b>Q: </b><i>Does klik need root privileges to run its applications?</i>
<b>A:</b> No. klik is designed for use without requiring root privileges. (It needs root only once, to add 7 lines to /etc/fstab, which allow usermode loopmounting of klik .cmg files).</p>

<p>
<b>Q: </b><i>Is there an uninstall script for the klik client?</i>
<b>A:</b> Yes. Type "<tt>wget klik.atekon.de/client/uninstall -O -|sh</tt>" to run it. However, it is still very simple and not yet widely tested. Type "<tt>wget klik.atekon.de/client/uninstall</tt>" to just investigate it inside an editor.</p>

<p>
<b>Q: </b><i>Is there an uninstall script for the klik-ed applications (the .cmg files)?</i>
<b>A:</b> No. But, assuming you have all your .cmg files somewhere in your $HOME directory, you could run this command to find them all: "<tt>find $HOME -name \*.cmg |xargs ls -l</tt>". To delete them all, replace the <i>"ls -l"</i> with an <i>"rm -rf"</i>.</p>

<b>.cmg File Format</b>

<p>
<b>Q: </b><i>What is the file format of the klik applications?</i>
<b>A:</b> All klik applications are embedded into a file with the extension ".cmg". The .cmg is a compressed file system image (similar to an ISO image that you can burn onto a CD). </p>

<p>
<b>Q: </b><i>Which type of files does the .cmg image include?</i>
<b>A:</b> The .cmg image includes binaries, directly required libraries and other files needed by the application.</p>

<p>
<b>Q: </b><i>What does the .cmg extension stand for?</i>
<b>A:</b> Compressed iMaGe.</p>
</td>
    </tr>
  </tbody>
</TABLE>
<p><b>Tomorrow's topics</b>:  Scope, Usage. I'm now done 13 out of 105 questions; my notes for tomorrow lists another 13 by now. Submit the ones you are interested in, please.</p>

<!--break-->