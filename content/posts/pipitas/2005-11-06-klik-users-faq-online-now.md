---
title:   "klik User's FAQ online now"
date:    2005-11-06
authors:
  - pipitas
slug:    klik-users-faq-online-now
---
<p>I'm working on getting the <A href="http://klik.atekon.de/wiki/index.php/User's_FAQ">klik User's FAQ</A> complete. The plan is to add at least one new section each day during the next two weeks. Today, I completed "Basics" and put it up together with probono's "Which are the supported operating systems?" -- So here is the local preview:</p>


<TABLE width="95%" cellspacing="2" border="1" cellpadding="2" align="center" bgcolor="#d3d3d3">
  <tbody>
    <tr>
      <td>

<b>Basics</b>

<p>
<b>Q: </b><i>What do I need to use klik?</i>
<b>A:</b> A Linux distribution with the klik client installed.</p>

<p>
<b>Q: </b><i>Are there other requirements?</i>
<b>A:</b> Yes. Your Kernel must support the <i>"cramfs"</i> file system. Your <i>/etc/fstab</i> needs entries which allow users without root privileges to <i>loopmount</i> cramfs image files.</p>

<p>
<b>Q: </b><i>Are there Linux distros which do not support cramfs by default?</i>
<b>A:</b> We came across Gentoo and Mandrake/Mandriva users who had to install a new Kernel in order to run klik applications.</p>

<p>
<b>Q: </b><i>Are there Linux distros which ship the klik client pre-installed?</i>
<b>A:</b> Yes. Knoppix, Kanotix, openSUSE/SLICK-enhanced and CPX-MINI.</p>

</td>
    </tr>
  </tbody>
</TABLE>


<p>Tomorrow's topic: klik client installation. While I have already noted about 105 questions, maybe the one that plagues you is still missing. Send it in, we'll deal with it.</p>

<!--break-->