---
title:   "klik2 at FOSDEM 2008 -- klik2 now starts handling non-GUI/CLI applications"
date:    2007-12-23
authors:
  - pipitas
slug:    klik2-fosdem-2008-klik2-now-starts-handling-non-guicli-applications
---
<p>Now that OpenOffice.org does make <A href="http://www.linux-community.de/Neues/story?storyid=24318">some</A> <A href="http://portableapps.com/apps/office/openoffice_portable">splashes</A> in the IT press for the sole achievement of having created a "portable" version that can run from an USB stick (on Windows only, that is) -- isn't it time for <A href="http://klik.atekon.de/wiki/index.php/Architecture">klik</A> to get ready for gaining its own share of public fame sometime soon? That's because klik does not only turn OpenOffice.org, but many thousand Linux applications into "PortableApps". And does not need painstakingly recompiling portable binaries from modified source code, one by one. But will re-utilize the marvellous work and special knowledge of all the dedicated Debian, RPM and Slackware packaging heroes out there and repackage 95% of its supported klik bundles fully automatically, including dependency resolution... </p>

<p>Have I ever mentioned that <A href="http://www.fosdem.org/2008/schedule/events/260">klik will be featured</A> as one of 3 (invited) talks in the  <a href="http://www.fosdem.org/2008/">FOSDEM 2008</a> main track about <A href="http://www.fosdem.org/2008/schedule/tracks/packaging">"Packaging"</A>? (The other two talks will be given by the Conary and the PackageKit developers.)</p> 

<p>probono and myself will be <a href="http://klik.atekon.de/presentation/">presenting</a> and showing off the (then) state of <A href="http://klik.atekon.de/wiki/index.php/Klik2">klik2 development</A>, which will (hopefully) have reached a major <A href="http://code.google.com/p/klikclient/">milestone</A> by February.</p>

<p>Last evening I re-tested how well (or not) <A href="http://code.google.com/p/klikclient/source">klik2</A> does deal with applications that have <i>no GUI at all</i>. Though general CLI support is not even part of our preliminary February milestone plan, that one does start to work already pretty well.</p>

<p>Even though I finally did find an ipcalc package on the openSUSE build service repository, my personal coolness factor for running a Debian-born, klik-ified ipcalc on my (oldish) openSUSE-10.2 notebook feels considerably higher.</p>

<p>I hit <i>[ctrl]+[alt]+[f2]</i> to go to a non-GUI console and started:</p>

<pre>   klik get ipcalc</pre>

<p>The process to download and run the XML-based (and largely Zero-Install compatible!) <A href="http://code.google.com/p/klikclient/source">klik2</A> recipe, which in turn fetched the  ipcalc_0.41-1_all.deb from the official Debian repository, and converted this single ingredient into our .cmg format that suits klik's "1 application == 1 file" principle took less than 12 seconds.</p>

<p>As usual, the resulting .cmg image file was placed into the $HOME/Desktop folder. I tried to run it to see if it worked:</p>

<pre>   klik run $HOME/Desktop/ipcalc_0.41-1.cmg</pre>

<p>which brought up the usage output. Next, I ran a calculation for an existing customer's site that I had done earlier the day, but manually/mentally, since the notebook didn't have an ipcalc RPM installed (also, I hadn't found the one in the build service repository yet). This time in a KDE Konsole, to be able to provide a screenshot proof to you:</p>

<pre>   klik run $HOME/Desktop/ipcalc_0.41-1.cmg 10.49.46.146/255.255.255.192</pre>

[image:3165 align="center" width=500 height=366 hspace=6 vspace=4 border=0 class="showonplanet"] 

<p>Now that this worked so nicely, I moved the .cmg to my standard folder of working klik bundles (BTW, which standard naming convention for the klik .cmg files would you prefer: bundle? container? image? archive? AppFilesystem? capsule? packet? package? BoxedApp? PortableApp? What else can you come up with?).</p>

<p>Then I created an alias in my ~/.bashrc file:</p>

<pre>   alias ipcalc='klik run ~/Desktop/klik2-work/ipcalc_0.41-1.cmg'</pre>

<p>Works like a charm. To recognize the difference from a "normally" installed ipcalc running, you'd need to know exactly what to look for.</p>

<p>Kudos to Jason "killerkiwi" Taylor and Lionel Tricon for doing most of the recent work to make the current klik2 state of development a reality!</p>

<!--break-->