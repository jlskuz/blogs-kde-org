---
title:   "Ascension"
date:    2005-04-16
authors:
  - canllaith
slug:    ascension
---
Today was my second glider flight. I try not to mention the first one. The discovery that I was lactose intolerant at 3000ft after drinking an inhabitual hot chocolate on my way to the airfield was fairly traumatic for both myself and the pilot.

Today went much more smoothly. We were up for 15 minutes and just floated gently back and forth along a nearby ridgeline. I took a few photographs toward the end and a short video clip of the landing (Including authentic violent camera jerking as the wheel hits the runway =). I'm hoping I can get used to this enough to photograph scenery from way up high without getting queasy looking through the viewfinder. 

I'm starting to think I might be interested in learning to fly one of these things.

<img src="http://www.hoult.org/~canllaith/blog/waiting_with_excitement.jpg">
<img src="http://www.hoult.org/~canllaith/blog/in_the_air.jpg">
<img src="http://www.hoult.org/~canllaith/blog/coming_down.jpg">

<!--break-->