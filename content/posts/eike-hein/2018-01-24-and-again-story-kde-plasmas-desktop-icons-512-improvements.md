---
title:   "Off and On Again: The story of KDE Plasma's desktop icons; 5.12 improvements"
date:    2018-01-24
authors:
  - eike hein
slug:    and-again-story-kde-plasmas-desktop-icons-512-improvements
---
<center><a href="https://i.imgur.com/Pk1fC8u.png"><img src="https://i.imgur.com/Qy4F4Qz.png" alt="Desktop icons in Plasma 5.12 LTS Beta" /></a><br /><small><i>Desktop icons in Plasma 5.12 LTS Beta (Click to enlarge)</i></small><br /><br /></center>

<a href="https://didrocks.fr/2018/01/23/welcome-to-the-ubuntu-bionic-age-nautilus-a-lts-and-desktop-icons/">Recent news</a> in the Linux desktop community recall an interesting time in Plasma's history: Release 4.1 in 2008, Plasma's second release ever, that time we (in)famously <a href="http://aseigo.blogspot.kr/2008/05/no-more-desktop-icons-in-41.html">abandoned desktop icons</a> (sneak preview: they came back).

Of course we never <i>really</i> abandoned them. Instead, in 4.1 we initially debuted the <i>Folder View</i> technology, which powers most of the ways to browse file locations on a Plasma desktop. Folder View gives you folder widgets on the desktop, folder popups on your panels - and yes, desktop icons, which always remained a supported option. An option we, crucially, did decide to tick default-off at the time. Instead we chose to place a folder widget on the default desktop, in part to reinforce the then-new widget-oriented ways of doing things in Plasma, things older KDE desktops just couldn't do.

<b>The Awkward Years</b>

A telling sign in hindsight, many distributions reneged on our decision and turned icons on for their users anyway. And yet we had decided to throw the switch upstream; what next?

A period of research and experimentation followed. With all that newly freed-up screen real estate and a new modular architecture, we looked into alternatives for what a device homescreen could be. The PC during this time was in a mood to diversify as well, with new form factors popping up in stores. Some of our experiments took off - the <a href="https://www.youtube.com/watch?v=9VCX6S-ndKA"><i>Search and Launch</i></a> interface we debuted alongside the Plasma Netbook spin in 4.5 directly inspired the popular <i>Application Dashboard</i> fullscreen overlay we introduced in Plasma 5.4. Others, like the <a href="https://www.youtube.com/watch?v=8IiSTBTDBas"><i>Newspaper</i> view</a>, failed to find much of an audience.

<center><a href="https://i.imgur.com/nNLUybp.png"><img src="https://i.imgur.com/2Kap4ba.png" alt="Application Dashboard in Plasma 5.12 LTS Beta" /></a><br /><small><i>The Application Dashboard overlay has its origins in the icons-off adventure (Click to enlarge)</i></small><br /><br /></center>

Ultimately, though this period was productive in many ways, we didn't hit upon a clearly-better new homescreen. Elsewhere meanwhile, on a parallel track, the icon homescreen UI metaphor unexpectedly bounced back and grew stronger. Touchscreen handsets introduced a whole new generation of computer users to - essentially - desktop icons. In the following years we saw user numbers and familiarity with homescreen icons increase, not decrease.

<b>The Lights are Back on and the Doors are Open</b>

During the Plasma 5.10 dev cycle, we did a lot of <a href="https://blogs.kde.org/2017/03/01/plasma-510-folder-view-default-desktop-mode">polish work</a> on the desktop icons experience. We then decided that it was time to stop hiding desktop icons support behind a config option: All things considered, the previous default was just not serving the majority of our users well. It had to change.

We still don't <i>place</i> any icons on the desktop by default. (Many distributions do - but they always did for all that time.) Those who enjoy the calm and tranquility of an empty desktop or don't want icons to get in the way of widgets were not impacted by this move. But drop a file or add an app to the desktop, and you now get an icon again, with full support for all of the powerful features KDE's desktops have always offered when dealing in files and links. For the many users who rely on desktop icons, this is a welcome reprieve from having to fiddle around post-install.

<b>Icons of the Future</b>

In the upcoming Plasma 5.12 LTS release, desktop icons are getting even better. We've done a <i>truckload</i> of work on improving the experience with multiple monitors, across which icons can be moved freely again, along with gracefully handling monitor hot plug/unplug. Performance and latency improvements, the key theme to 5.12 in general, have continued where 5.10+ left off, with the desktop reflecting file operations now faster than before.

We've worked though many of the most-reported feature requests and pain points for desktop icons throughout 2017, but we're not done yet. Folder View development continues in 2018 with more outstanding user requests on the horizon, so feel free to get in touch. 

Check out the <a href="https://www.kde.org/announcements/plasma-5.11.95.php">beta</a> now and let us know what else you want out of desktop icons after 5.12!