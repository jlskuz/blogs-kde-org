---
title:   "KDE Plasma Desktop 4.11's new Task Manager"
date:    2013-07-29
authors:
  - eike hein
slug:    kde-plasma-desktop-411s-new-task-manager
---
One of the <a href="http://www.kde.org/announcements/announce-4.11-rc2.php">many things</a> to look forward to in the impending KDE Plasma 4.11 release is a new version of the default Task Manager applet, which had its front-facing bits rewritten from scratch, along with additional support work and improvements in the underlying library.

The motivation for the rewrite was two-fold: Address many long-standing and gnarly bugs the old applet was suffering from, and to pull the Task Manager forward into the <a href="http://en.wikipedia.org/wiki/QML">QML</a> era, making it ready to transition to Plasma 2. Visual and interaction changes were to be kept to a minimum for now, focusing on a regression-free port, but a leaner and meaner codebase along with QML's designed-in flexibility should make a much better foundation for future experiments.

Let's take a look at some of the improvements over the old applet :).

<b><br />Side bar: The art of putting tasks on screen</b>

<center><img src="http://www.eikehein.com/kde/blog/411taskmanager/taskmanager.png" alt="The 4.11 Task Manager" /><br /><small><i>There's a lot going on here.</i></small><br /><br /></center>

Laying out a task manager is complicated business. Task items have preferred minimum and maximum sizes in both dimensions, derived from the current theme and font settings. This size range information factors into calculating when to break into additional rows (or columns), provided the available space and user settings allow for it. Layout also behaves akin to justified text layout, dividing available space between tasks evenly even when wrapping into multiple rows - but not allowing them to grow beyond their preferred maximum size.

The addition of launcher items complicates things further, making it necessary to carefully negotiate the peaceful graphical co-existence of two different types of items with different preferred sizes, throwing the easy assumption that all items are going to be the same out the window.

Layout also doesn't just have its internal concerns to worry about, but some external ones as well: It has to keep track of when it considers the Task Manager to be full (again drawing on the preferred size range information) and communicate that to the machinery that will try to opportunistically group windows to conserve space. It also has to generate useful size hints for the applet, allowing for things like Plasma's auto-growing/shrinking panels to work correctly.

And then all of that has to work well in both horizontal and vertical orientations, with some subtle differences to tune things for both cases.

<b><br />Layout: A lot better now</b>

Unfortunately, the old applet didn't do so well on mastering the above. Its layout code suffered from many, many bugs, ranging from just making bad placement decisions to getting stuck mid-animation to causing hangs due to infinite loops. Consistency between horizontal and vertical layout wasn't very good, with support for vertical orientation introduced via copious special-casing and being perennially out of sync with the better-maintained horizontal codepaths.

Layout is also involved with reordering tasks by Drag-and-Drop (if sorting is set to "Manual"), and in the old applet, would occasionally compute incorrect indices for the reinsertion of dragged tasks, particularly at the end of rows and when right-to-left locales like Arabic and Hebrew were used. DND across multiple rows was also a bit crashy.

Many of these bugs only surfaced in somewhat unusual configurations, with the general theme being that new layout features had been added without enough attention being paid to how they would interact with all of the permutations allowed by the feature matrix, not just the most common cases. An example would be combining the presence of launchers with multiple rows, illustrated here:

<center><img src="http://www.eikehein.com/kde/blog/411taskmanager/launchers_multirow_old.png" alt="Layout bug" /><br /><small><i>Layout gone bad: Launchers larger than tasks ...</i></small><br /><br /></center>

<center><img src="http://www.eikehein.com/kde/blog/411taskmanager/launchers_multirow_new.png" alt="Layout fixed" /><br /><small><i>And now it's good.</i></small><br /><br /></center>

All of these bugs should hopefully be resolved by the new version's layout engine.

<b><br />Consistent interactions</b>

Not just layout behaves more consistently across features and configurations now, interacting with the Task Manager does, too. For example, the popup for window groups now supports switching between tasks via the mouse wheel and reordering tasks by dragging (again, provided sorting is set to "Manual"), just as the primary UI does.

Speaking of those group popups: Up until now, they weren't smart enough to adjust their size as windows came and went or window titles changed, and if windows set very long titles for themselves, the resulting popups could sometimes show up with unreasonably large sizes. They're much smarter now, handling these situations properly.

<center><img src="http://www.eikehein.com/kde/blog/411taskmanager/grouppopup.png" alt="Group popup" /><br /><small><i>Group popups learned many new tricks.</i></small><br /><br /></center>

<b><br />Visual fixes and polish</b>

Task buttons have frames (depending on your theme, they come in less or more varieties), and those frames aren't purely decorative - they're used to communicate important state like the identity of the active window. The old applet had several bugs in the code handling button frames, sometimes causing it to claim multiple windows as active or forgetting to reset a frame after a task context menu had been used. These kinds of bugs are gone now.

Task buttons also (well, usually - if layout thinks it has enough space for them!) have text labels with nifty-looking shadows behind them - which the old applet sometimes forgot to re-render for text changes, namely when the font settings were changed. Poof, gone as well.

Themes are also generally applied more correctly now, e.g. taking frame margins into account when making decisions on the size of elements inside them.

<b><br />Playing nice with the rest of the Workspace</b>

The Task Manager is also responsible for communicating task item positions to the window manager, so the window manager can know the destination for the minimize animation of a window. The old applet sometimes dropped the ball on this duty, causing KWin to animate windows minimizing into the center of the screen, or into outdated positions. Once more I'm happy to report the new version should take this obligation a lot more seriously.

<b><br />Configuration handling</b>

The old applet would occasionally be reluctant to apply changed settings immediately, e.g. toggling the "Force row settings" option wouldn't take effect unless the value for the "Maximum rows" setting was changed at the same time. Its successor suffers from no such compunctions.

<b><br />Performance</b>

QML is kind to us here, and it's not even the shiny OpenGL-powered Qt 5 version yet. Animations are generally a bit smoother across the board, and specifically the throbber/spinner animation used in place of the icon on task startup notification items sports a much higher frame rate than before.

<center><img src="http://www.eikehein.com/kde/blog/411taskmanager/throbber.png" alt="Startup notification throbber" /><br /><small><i>It may be hard to tell from a still image, but it's spinning a </i>lot<i> more smoothly now.</i></small><br /><br /></center>

<b><br />There's also some bad news (with a side dish of glimmers-of-hope)</b>

Sadly, a few features didn't make it into the new version - yet, at least.

One is the ability to drag tasks from the Task Manager onto the pager, a very nifty way to move windows between virtual desktops. That this wasn't retained is down to a limitation in the components we implemented to teach QML Drag-and-Drop support; I've proposed a patch to address it in Plasma 2 that may even be back-portable to KDE 4, so stay tuned for 4.11.1 possibly addressing this.

The other two are support for manual grouping and uncollapsing groups into an inline representation in the primary UI. Both seem to have relatively few users, partly due to unfriendly interaction design: Figuring out how to actually manually group windows generally took an internet search to learn about the necessary keyboard modifier. That doesn't make the regressions any less regrettable, of course, but on the other hand we will try to bring these back in much improved incarnations in Plasma 2 (or perhaps sooner). It's part of the reason why they didn't make it yet - both features had some fundamentally bad aspects to their current realization, and burdening the new codebase with their complexity was thoroughly unattractive from a cost/gain perspective. We'll do better.

<b><br />Future plans</b>

The new QML-powered Task Manager is here to stay - this is the codebase we're taking into Plasma 2, delivering on our intentions to make the transition to KDE 5 a more iterative, less disruptive one. The regressions mentioned above imply some open to-dos, and beyond that we'll be investigating new features and ideas, such as allowing apps to do more with the context menus of their launcher and task items.

Meanwhile, the move to Wayland will cause some further work as well, below the visual level this time, adapting the underlying library to KWin becoming the new authoritative source for window data.

I'm hoping you'll enjoy the new 4.11 Task Manager. If you've got any fancy ideas: Throw them my way in the comments!

<small><i>PS.: A few of the things described here took final form quite late. If you have a problem with the Task Manager in one of the betas or RCs, take the above as an update on what you can expect from the final release.</i></small>