---
title:   "Homerun 1.2.0"
date:    2014-01-29
authors:
  - eike hein
slug:    homerun-120
---
Monday saw the release of version 1.2.0 of Homerun, now a collection of launcher interfaces for Plasma Workspaces, powered by a common foundation. If you're already familiar with, or even a happy user of Homerun this description of it might make you raise an eyebrow, so let's take a look at what's new in this version.

<b><br />Homerun Kicker</b>

<center><img src="http://www.eikehein.com/kde/blog/homerun120/homerun_kicker_1.png" alt="Homerun Kicker" /><br /><small><i>Tada.</i></small><br /><br /></center>

The main addition in Homerun 1.2.0 is a second interface built atop Homerun's collection of data sources, the <i>Homerun Kicker</i> launcher menu shown above. Unlike the first Homerun interface, which is designed for use on the full screen or desktop background and meant to be both mouse- and finger-friendly (you can check it out <a href="http://www.youtube.com/watch?v=cdM_006z_U4">here</a> if you're new to Homerun or just need a memory boost), Homerun Kicker is a more traditional launcher menu design optimized for efficient use by mouse or touchscreen when placed on a panel.

The use of traditional, cascading popup menus is complemented by a sidebar strip in which application favorites and items related to power and session handling may be placed. Both types of items can be added, removed and reordered at will via mouse and menus, much like in the bigger, older brother.

It also has search, which - also a previously known Homerun feature - mines several different sources of data for your query. Unlike in the larger interface, however, results in Homerun Kicker are shown in dynamically created columns, one for each source:

<center><img src="http://www.eikehein.com/kde/blog/homerun120/homerun_kicker_2.png" alt="Searching in Homerun Kicker" /><br /><small><i>Mmmmm. All that data.</i></small><br /><br /></center>

Homerun Kicker looks and should feel simple, but has a bunch of fairly neat things going on under the hood to achieve that goal. Optimization for efficient mouse use starts with the chosen layout, but doesn't end there: The handling of mouse input is smart enough to, for example, treat a diagonal move into a sub-menu differently from vertical movement, to avoid accidentally switching to a different category when only briefly grazing menu items. The result is a menu that hopefully feels solid, dependable and hassle-free.

Keyboard aficionados have no need to feel to feel left out, either: Arrow keys and other keyboard commands work as you'd expect from a menu. Upon opening Homerun Kicker keyboard input focus is placed on the search field, so the search-and-hit-return workflow familiar to Homerun users is supported here as well.

Another feature worth mentioning is support for setting a custom image as the launcher button. The image can be non-square, enabling greater visual variety and a wider mouse click target.

<b><br />What else is new?</b>

With both Homerun interfaces built on the same underlying framework, several of the new things in this release pop up in both of them. Acute eyes may have already spotted a "Recent Applications" entry in the first Homerun Kicker screenshot - this is backed by a new Homerun source that can of course also be placed on tabs in the screen-spanning Homerun interface. The same goes for "Power / Session"; having a combined listing of buttons many users mentally group together had been a popular user wish in the past.

The original Homerun interface is most often used as a fullscreen launcher, but thanks to the versatility of the Plasma Workspaces architecture can also be placed on the desktop background, where it is very useful in a tablet or hybrid laptop setting. Some users felt the Plasma Desktop Toolbox getting in their way in this usage mode. It's now hidden by default, but can be toggled on and off from the configuration menu.

There's also the usual collection of minor behavior improvements and bug fixes. One particularly nasty bug could lead to the wrong apps being launched when using the "All Installed Applications" source with a particular combination of sidebar filtering and a search query - that's fixed now.

<b><br />Future plans</b>

Homerun Kicker is included here as a first version that lacks some of the greater power known from its big brother. In particular, there's no graphical way to change the list and ordering of Homerun sources in the menu yet (though there's one big knob to disable the integration of non-apps data into search results if you don't want it). This is one obvious avenue for future versions to explore.

Those future versions may already be powered by Plasma Next by then - porting to Plasma Next (and therefore Qt 5 and Qt Quick 2) is on the immediate todo as well. The Plasma 1 version will continue to be supported with improvements and fixes until that transition is complete, however.

Some of the work put into Homerun Kicker and the experience gathered with it may also benefit the Plasma Next effort down the road. First reactions from the development period and the users of distributions that have already included Homerun Kicker in their default configuration indicate there's definitely an audience for a traditional menu option that integrates nicely with workspace theming, unlike the classic launcher menu widget currently bundled with Plasma Desktop. Replacing that widget with a derivative of the work accomplished here is one option that's being discussed at the moment.

<b><br />Closing notes</b>

Welp, that's it! Go <a href="http://download.kde.org/stable/homerun/src/homerun-1.2.0.tar.xz">grab the tarball</a> or poke your favorite distro for an updated package. After checking out the goods consider providing your feedback. There's a <a href="irc://chat.freenode.net/kde-homerun>#kde-homerun</a> IRC channel on <a hred="http:/www.freenode.net">freenode</a> you can stop by, and of course bugs as well as wishes can be reported at <a href="https://bugs.kde.org/">KDE Bugzilla</a>.