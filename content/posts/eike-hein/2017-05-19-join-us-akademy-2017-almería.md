---
title:   "Join us at Akademy 2017 in Almería!"
date:    2017-05-19
authors:
  - eike hein
slug:    join-us-akademy-2017-almería
---
This July KDE's user and developer community is once again going to come together at <a href=''https://akademy.kde.org/2017">Akademy</a>, our largest annual gathering. 

I'm going there this year as well, and you'll even be able to catch me on stage giving a talk on <a href="https://conf.kde.org/en/akademy2017/public/events/359"><i>Input Methods in Plasma 5</i></a>. Here's the talk abstract to hopefully whet your appetite:

<hr />

<blockquote><i>An overview over the How and Why of Input Methods support (including examples of international writing systems, emoji and word completion) in Plasma on both X11 and Wayland, its current status and challenges, and the work ahead of us.</i>

Text input is the foundational means of human-computer interaction: We configure or systems, program them, and express ourselves through them by writing. Input Methods help us along by converting hardware events into text - complex conversion being a requirement for many international writing systems, new writing systems such as emoji, and at the heart of assistive text technologies such as word completion and spell-checking.

This talk will illustrate the application areas for Input Methods by example, presenting short introductions to several international writing systems as well as emoji input. It will explain why solid Input Methods support is vital to KDE's goal of inclusivity and how Input Methods can make the act of writing easier for all of us.

It will consolidate input from the Input Methods development and user community to provide a detailed overview over the current Input Methods technical architecture and user experience in Plasma, as well as free systems in general. It will dive into existing pain points and present both ongoing work and plans to address them.</blockquote>

<hr />

This will actually be the first time I'm giving a presentation at Akademy! It's a topic close to my heart, and I hope I can do a decent job conveying a snaphot of all the great and important work people are doing in this area to your eyes and ears.

See you there!

<a href="https://akademy.kde.org/2017"><img src="https://community.kde.org/images.community/5/51/Akademy2017_banner.png" title="I'm going to Akademy! banner image" /></a>