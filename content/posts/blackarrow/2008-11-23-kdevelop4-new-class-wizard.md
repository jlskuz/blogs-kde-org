---
title:   "KDevelop4: New Class Wizard"
date:    2008-11-23
authors:
  - blackarrow
slug:    kdevelop4-new-class-wizard
---
I've been pretty inactive in development recently for a few reasons (I got engaged, then I had a nasty fall from my road bike due to a tyre blowout).  I was impressed by the great work that David Nolden continues to do for kdevelop, particularly the behind-the-scenes improvements which make using the program bearable.  However as we know it's the new features which are the most 'bloggable'...

The new class wizard is currently pretty basic, but in a few hours I managed to link up the dialog with the definition-use chain, and the results are pretty impressive (for the amount of effort involved):

<img src="http://members.optusnet.com.au/hamish.rodda/override.jpg" class="showonplanet">

Here you can see a list of super classes which you're overriding, and the virtual functions which each class contains.  The idea is then you can check those functions which you want to implement / override, and kdevelop will create the entry in the new class and the stub in the implementation file.  This code is also mostly language-independent, although I need to speak to David about a few details there. BTW, it's not committed yet, pending a bit more polishing.<!--break-->