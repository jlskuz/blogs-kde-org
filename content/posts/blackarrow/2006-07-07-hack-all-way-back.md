---
title:   "The Hack all the way Back"
date:    2006-07-07
authors:
  - blackarrow
slug:    hack-all-way-back
---
So, for me the KDE four core meeting in Trysil has finished today.  It was an amazing experience, meeting people for the first time and seeing what we could do when we concentrated (most) all of our efforts on improving KDE4.

I'll blog a bit more about what happened at the meeting later, but now I'm waiting in London's Heathrow airport waiting to get on a longhaul flight for 24 hours to Melbourne via Singapore.  I decided to make the most of my time on the way back, hacking as much as I could.  So far, I've done some programming on the bus, at Norway's airport (both while having dinner and while waiting for the flight at the terminal), in the plane while we sat on the tarmac for 30 minutes waiting for flight clearance, and again in the air - it's no surprise that my 2 batteries were completely exhausted at the end of that (although amazingly they held up pretty well considering).

I'm working on kdevelop's parser, and more specifically on the definition-use chain logic.  Roberto Raggi's code is simply amazing, or perhaps I think so because I don't have a formal IT education.  A little background: Roberto has already written a comprehensive and fast preprocessor and parser which delivers a very detailed AST (abstract syntax tree).  There is some hacky support for a code model, but it really needs replacing.  Thus earlier this year Roberto started on a new type system (framework ready, not much logic) and definition-use parser (some framework)

At Trysil, Roberto visited for a day and we discussed what was needed to be done for kdevelop4, as unfortunately Roberto has very little to no time to help these days.  We'd had similar discussions online, but the power of face-to-face meeting is not to be underestimated.

I've managed so far to read and understand Roberto's latest code, and have now completed the definition-use chain parser, and made stubs for the definition-use chain data structures, and a new class which is aimed at texteditor integration  (the texteditor integration class I'm particularly proud of, because it was a mess before).

Once this is finished, I will hopefully move on to fleshing out the type system.  Then, all that's left to do is to bring it all into the ui, with a model for code completion; and refactoring, highlighting, and navigation support.  A long way off yet, but I personally think that this has the potential to make a much stronger case for developers switching to katepart/kdevelop.

Well, my first battery is recharging quickly, so I should get back to the hacking while I still have AC ;)  The only other problem is I've been assigned a non-laptop-friendly seat on the plane, so here's hoping that they might take pity on me after the flight closes (but I doubt it...).

Signing off from Heathrow, Hamish.<!--break-->