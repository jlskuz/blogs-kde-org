---
title:   "KDE in Lisbon"
date:    2006-08-31
authors:
  - dario massarin
slug:    kde-lisbon
---
Well, it happened that this year I went in Lisbon for my summer holidays (ah, btw, it was a great fun) and walking along one of the main streets, just near the well known Rossio (Dom Pedro IV) square, it also happened that I got into a currency exchange office where I found something like 4 or 6 computers (I don't remember precisely) that were used as an internet point for tourists. This is what I saw:
[image:2303 align=center size=preview]

Just great :)

<!--break-->