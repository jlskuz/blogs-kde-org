---
title:   "KGet is saying: \"Test me!!\""
date:    2009-12-24
authors:
  - dario massarin
slug:    kget-saying-test-me
---
Well.. Come on guys! <b>You can do better than this!</b> You say why? Simply because, apart from a very serious bug related to the download of files .torrent, .meta4 and .metalink (that we have been lucky enough to find out) and a Nepomuk crash (already fixed in trunk), <b>we don't have any report right now reporting any crash or uncorrect behaviour</b> of kget.. You sure about this? So, please, <b>Test it</b> a little bit more, so that the next release will be perfect for you! Ehy! I'm talking also to all people that tried kget some time ago but dropped it saying: "aaaaaaah.. still unstable..". Could you give our baby another chance? ;)
<!--break-->
