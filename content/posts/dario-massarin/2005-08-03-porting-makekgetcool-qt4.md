---
title:   "Porting make_kget_cool to qt4"
date:    2005-08-03
authors:
  - dario massarin
slug:    porting-makekgetcool-qt4
---
So yesterday I finally switched the make_kget_cool to qt4 and some hours ago I finished to compile and install kde4. Wow! That wasn't hard as I thought.
I started my work on a new sidebar that in the future will show nice animations effects when downloading files. 

Stay tuned!! 