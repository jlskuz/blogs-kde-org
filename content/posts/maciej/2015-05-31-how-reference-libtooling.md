---
title:   "How to reference libTooling"
date:    2015-05-31
authors:
  - maciej
slug:    how-reference-libtooling
---
Being accustomed to use of big frameworks like Qt, libraries like Boost, or smaller like OpenSSL it can be astonishing how difficult can be referencing external (but still native) library from C++ code.
<!-- break -->

All of libraries mentioned above come with fairly good CMake support making it more or less difficult to use after reading some (or all) of available documentation. With Clang we have two problems here:
<ul>
<li> No FindClang.cmake module
<li> No forward source compatibility
</ul>

The first problem seems be easy to solve. Probably some of You have written some FindXXX.cmake module to locate library shipped without accompanying Find... CMake module. In most cases it is very straight forward task. Here it was even simpler: FindClang.cmake was already prepared and needed only small extension to find a few more libraries.

The second. I would say we cannot help that. I decided to focus on one version of library for now (and track changes in future adding support for never versions). If we have library but in other version - compilation of the whole module will be skipped. Quite miserable but safe for build process. We have also unstable ABI here (not so surprising). In our case quite small update of Clang may destroy tooling because of ABI breakage.

Because of this inconvenience, after some discussion (this is all about use of libTooling in KDevelop) we decided to make connection between client of libTooling (my GSoC) and KDevelop loose. We take into consideration the fact that tools binary may be destroyed without our knowledge. Having these tools linked with kdev-clang would make the whole plugin unusable (broken ABI would scream at time dynamic loader tries to resolve a DSO making the whole plugin loading fail). We will establish this connection at runtime when we have a chance to handle failure (disable refactorings, but allow the rest of plugin to work). This is not the most convenient way of accessing external functionality...

But that's not all. As it came out, Clang libraries in some systems (like openSuSE) are distributed in form of shared libraries, but in other (like Gentoo) as static libraries. Static libraries does not carry dependency information. These should be provided from external source... They are not. But that's still not all. The most surprising (i meet this problem the first time) is that ordering of parameters for linker (including position of my code in parameter list) affect result. In some cases yielding linker failure. This ordering must be controlled through CMake build system.

Other notes:
<ul>
<li>Empty DSO linked (statically) with "average" debug libTooling setup has 18MiB (with 60 LLVM/Clang static libraries and 5 shared, AMD64 Gentoo "-O2")
<li>There is FindLLVM.cmake provided by upstream. It doesn't contain Clang libraries
</ul>