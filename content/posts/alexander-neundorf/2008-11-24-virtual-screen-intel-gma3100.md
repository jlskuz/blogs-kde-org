---
title:   "Virtual screen with Intel GMA3100 ?"
date:    2008-11-24
authors:
  - alexander neundorf
slug:    virtual-screen-intel-gma3100
---
Last week I upgraded my development machine, from a AMD Athlon XP 2000+ to an Intel based one, featuring GMA3100-based onboard graphics.
Everything's working smoothly that far, I have only one issue: "Virtual" keyword in the "Screen" section seems to be ignored.
This was working since my very first Linux installation in 1996, so I'm <b>really</b> used to having a big virtual screen.
This is on Slackware 12.1, using xf86-video-intel 2.2.1.
Any ideas ?

Alex
