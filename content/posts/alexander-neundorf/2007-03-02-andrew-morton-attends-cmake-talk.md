---
title:   "Andrew Morton attends CMake talk"
date:    2007-03-02
authors:
  - alexander neundorf
slug:    andrew-morton-attends-cmake-talk
---
So this year I was the first time at FOSDEM in Brussels. Brussels is a <i>very</i> nice city, I didn't expect this. It reminded me on Paris, it has the same french flair and beautiful architecture.
But I digress. The exhibition at FOSDEM was smaller than e.g. the german LinuxTag, but there were much more talks. I myself gave two of them: <i>CMake and friends</i>, where I gave a quick overview over CMake (the buildsystem), CTest + Dart (Unit testing and Continuous Integration) and CPack (packaging). It was only a 15 minutes Lightning Talk so I couldn't go to deeply into details. 
<!--break-->
But, as the headline says, Andrew Morton was among the audience.
Maybe he just kept sitting where he was when my talk started, but anyway, it feels good to have kernel hacker #2 in the audience ;-)
So will we see the kernel switching to CMake anytime soon ? ;-) (actually I don't think so)

Then on Sunday I had a talk about <a href="http://ecos.sourceware.org">eCos</a>, a free realtime operating system. The audience in the <i>embedded</i>-room was as during the other talks too very skilled and technical, most of them probably also working with embedded software during their day to day jobs. I even met some people from the eCos mailing lists there :-)
The slides for both talks will be later available online at the FOSDEM site.

So, actually I had planned to help at the KDE booth, but they were always good staffed and did a really good job :-)

While all this sounds as if it wouldn't be KDE-related, IMO promoting CMake is something which will hopefully help KDE (and Linux in general) a lot:
<ul>
<li>You know, we had Arts, nobody else adopted it, it died.
<li>We have DCOP since KDE 2.0, nobody else adopted it, it will be replaced by DBUS in KDE4.
<li>We had unsermake (a bit), nobody else used it, so it died.
</ul>

I don't want that the same happens with the buildsystem. Autotools are so complex you actually can't expect e.g. from students who are starting to learn C or C++ that they also learn how to deal with m4 + Makefile syntax + libtool + autoconf + automake. The same is true e.g. for Windows developers used to MSVC.  CMake helps with that and is IMO really the best choice there is for building software, also better than e.g. the built-in buildsystems in IDEs like XCode, MSVC, or any other IDE. It even makes sense for Windows-only software, if you just want to build it with different compilers.

So among others I talked to Iago Toral Quiroga from our Gnome friends, and it seems the average Gnome developer likes autotools as much as the average KDE developer liked it. So maybe there's a chance... :-)

Right now I'm preparing my talk for the <a href="http://chemnitzer.linux-tage.de/2007/info/">Chemnitzer Linuxtage</a>, which is always a nice event, and the week after that Cebit will follow. Anybody in Hannover or around who'd like to help us a bit ?
We could still need some booth people, and some "catering" would also be nice :-)
So if you always wanted to help KDE but are no programmer, that's your chance !
Just let <a href="mailto:neundorf AT kde DOT org">me know</a> !

Alex

