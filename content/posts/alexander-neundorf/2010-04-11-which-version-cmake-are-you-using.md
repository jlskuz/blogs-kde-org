---
title:   "Which version of CMake are you using ?"
date:    2010-04-11
authors:
  - alexander neundorf
slug:    which-version-cmake-are-you-using
---
Hi,

KDE 4.2, 4.3 and 4.4 all required at least CMake 2.6.2.
I'd like to know how much "pain" it would be to increase this requirement for KDE 4.5 to CMake 2.6.3.

In order to find out, please participate in this poll: 
<a href="http://blogs.kde.org/node/4198">http://blogs.kde.org/node/4198</a>

Thanks
Alex
