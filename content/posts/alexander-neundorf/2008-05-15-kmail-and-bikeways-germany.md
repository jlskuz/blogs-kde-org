---
title:   "KMail and bikeways in Germany"
date:    2008-05-15
authors:
  - alexander neundorf
slug:    kmail-and-bikeways-germany
---
So this blog title is "KMail and bikeways in Germany". You may wonder what they have in common. You may wonder whether there will be an eloquent Aaron-style nice little story behind that.
<!--break-->
But sorry, no, I have to disappoint you. At least from my point of view kmail and bikeways in Germany have absolutely nothing in common, two completely unrelated things, no surprising lovely details they share.

So, to get to the point: what I wanted to say for a long time: congratulations to KMail ! I use it since years and it always just works. And one IMO <b>huge</b> achievement: I have thousands of emails in my mail folders, and it starts really really fast, basically immediately. Sorting or searching the mail folders - instantly. Greak work ! Not all applications manage to do that.

Now to the sad part, the german bikeways. No, I won't complain that there are no or too few bikeways in Germany. More the opposite. There are too many, or, too many unusable bikeways in Germany. I mean, bikeways could be a nice thing. Not so here in Germany. The trend here is to put bikeways on the sidewalk together with the pedestrians. Does that make sense ? Does it make sense to have me riding with let's say 25 to 35 km/h through the pedestrians ? I don't think so. I also don't think that would be especially safe, neither for me nor for the pedestrians. But, well, in these cases, one could just ignore the bikeways on the sidewalks.

But now the really annoying effect of that is, that car drivers have come to expect that bikers belong on the sidewalk to the pedestrians: "This is my street, get off !".
I mean, it's not enough for them to honk at you, some of them go so far they lower their window just to scream that at you as biker.
Even if there is no bikeway visible, just sidewalks. 
How to put it, I'd suggest to split bikers into two groups: sporty bikers, i.e. faster than 20 km/h and comfy bikers, slower than that. Consider the sporty bikers like motor bikes, and the comfy bikers like pedestrians.
IMO this would make a lot of sense. If I'm going relatively fast, e.g. at 30, I much rather prefer to share the street with the cars at 50, than to mess around between pedestrians moving at 5 km/h. At 30 I also don't want to circle around trees on sidewalks, go up and down the sidewalks at each crossing and also drivers don't expect somebody almost as fast as them selves on the sidewalks on crossings. OTOH if I'm going slow all that is ok.

So my proposal: either split bikers into two groups, or much better <b>build bikeways as part of the street, not as part of the sidewalk</b>, so we get accepted again on streets.
(children biking on sidewalks is ok of course)

Alex
