---
title:   "LinuxQuestions.org Awards 2005 results :-)"
date:    2006-03-07
authors:
  - alexander neundorf
slug:    linuxquestionsorg-awards-2005-results
---
Hiya,

the results of the <a href="http://www.linuxquestions.org/questions/forumdisplay.php?f=69">2005 LinuxQuestions.org Members Choice Award</a> are there.
Although the announcement: "Among the winners are Ubuntu, Firefox, MySQL and OpenOffice.org." doesn't sound too good for KDE, here's a (only slightly) closer look at the result :-)

The most surprising result: <b>Slackware</b> almost won the "Distro of the year" poll :-)
Distro: 1.) Ubuntu (19.5), 2.) Slackware (19.0)

And here are the results (except shell, database, live CD, security, game):

KDE won:

<i>Desktop</i>: 1. <b>KDE <i>(64.9)</i></b>, 2. Gnome (25.7)
<i>WebDev</i>: 1. <b>Quanta</b> (44.3) 2. Nvu (24.9)
<i>Audio</i>: 1. <b>amarok</b> (41.9) 2. xmms (28.9)
<i>File manager</i>: 1. <b>Konqueror</b> (51.3) 2. Nautilus (15.9)

KDE came second place in: 

<i>IDE</i>: 1. Eclipse (32.0) 2. <b>KDevelop <i>(31.4)</i></b> 
Note how KDevelop lost only by 0.6 percent to Eclipse :-)

<i>Editor</i>: 1. Vi (38.0) 2. <b>Kate</b> (22.5) 3. Emacs (8.7)
Well, although Vi won with a big margin, there also a big gap between Kate and Emacs. And from these three Kate is actually the only option for non-geeks.

<i>Office</i>: 1. OOo (84.9) 2. <b>KOffice</b> (11.7)
<i>Browser</i>: 1. Firefox (71.9), 2. <b>Konqueror</b> (11.0)
<i>Mail</i>: 1. Thunderbird (51.7) 2. <b>KMail</b> (21.5)

The three winners above all have their origins in commercially developed applications, so when they became Open Source, they had a head start compared to the KDE apps, which were developed  from scratch by the community :-)

<i>IM</i>: 1. Gaim (52.4) 2. <b>Kopete</b> (23.4)
<i>Window manager</i>: 1. FluxBox (27.1) 2. <b>KWin</b> (18.1)

And here KDE didn't score that much:

Video: 1.) mplayer (47.0) 2.) Xine (30.2)
Graphics: 1.) gimp (62.0) 2.) Inkscape (11.8)

Alex
