---
title:   "Four things that rock !"
date:    2006-11-14
authors:
  - alexander neundorf
slug:    four-things-rock
---
So, in the last days there have been several things which made me think "This rocks !".
Wanna know what ? So here we go, without any special order.
<!--break-->

<ul><li>Sun releases <a href="http://lwn.net/Articles/208977/">Java under the GPL</a> !</li></ul>


You know what ? This rocks !
I have never been especially fond of Java. If you want fast code, write it in a language which is compiled to machine code, like, well, C++ or C. Additionally I always had the impression that Java is almost like C++, just that you cannot do everything with it you can do with C++ (templates, operator overloading, multiple inheritance, ...).
But OTOH if you don't care that much about execution speed but things like reduced development time and portability (without recompiling), then why stop halfway instead of going the whole way and use a scripting language (preferably one that rocks ;-), to stay on-topic) like, let me think a minute, like Ruby ?
You get even shorter development times, you get dont-compile-run-everywhere and you can do things with the introspection features you can't do with any compiled language I know of.
Ok, said that, I think releasing Java under the GPL is really great from Sun. You know, there is something called Mono, which is a free implementation of the attempt from Microsoft to push Java out of the market with C# and its CLI.
So, if we want a free bytecode-compiled language, Java <b>had</b> the issue that it wasn't really free, only free-as-in-beer. And Mono has to play catchup with the stuff MS develops, and there's the potential that it might carry patent problems.
So, now that Java is free, what has changed ? We have a free bytecode-compiled language, maintained by the main development team, i.e. no clone or fork, and since these guys released it theirselves under the GPL they probably won't sue anybody for violating any patents.
So, who still needs Mono since November 13th, 2006 ?


<ul><li>One more thing that rocks is the EU !</li></ul>


Yes, the European Union. Seriously. Within the EU you can simply travel from one country to the other, in many countries you can use the same money, if you want to study somewhere else but within the EU, no problem, no visum or anything required. You want to work somewhere else ? Same thing. You just start to notice if you look how to do such things if you want to go outside the EU, like e.g. Australia, or let's say even the USA. November 9th has just passed, the day the border between the eastern and western part of Germany was opened in 1989. Before this day I wouldn't have been able to even visit these countries at all.
So, despite the bureaucracy the EU might bring, it a really cool thing !


<ul><li>Next thing that rocks: the <a href="http://www.odfalliance.org/">OpenDocumentFormat</a> !</li></ul>


Ok, we all know that storing your data in an openly documented standardized file format is a good thing to do and actually everything else is an unbearable situation. I mean, how many businesses, administrations etc. are storing their data in a black-box-binary-blob file format ? Anybody thinks that's good or at least acceptable ?
But this is what I already knew for a long time. So, now last week I had an ODF document written by my girlfriend with <a href="http://www.openoffice.org/">OpenOffice.org 2</a>, and she wanted me to have a look at it. So I clicked on it in Konqueror and it opened. I started reading and editing here and there but after some minutes I wondered why OOo did look so different today ?
Guess what ? I wasn't editing it in OOo, but Konqy had opened the document in <a href="http://www.koffice.org/kword/">KWord</a> and I didn't even notice. Isn't that cool ? Never before I have been able to edit one text document with different word processors without having to import it in one of them and when saving being warned that it will loose some formatting because it isn't the native file format. 
Nothing of that with ODF, it simply worked ! :-)
But as if this wasn't enough, I had just downloaded the first beta of <a href="http://www.softmaker.com/english/ofl_en.htm"> Textmaker 2006</a> a few days before, and since they claimed to support ODF too, I just thought I'll try loading the same document with Textmaker. 
And it did really work !
To put it in other words: I was able to use three different word processing applications, all of them available under Linux, FreeBSD and Windows and Mac OS X (KWord starting with KDE4), to work on the same document without any problems !


<ul><li>Last but not least, and maybe some of you already expected it, <a href="http://www.cmake.org">CMake</a> rocks !</li></ul>


I was just doing some bugfixing on KDE 3, when I thought I would need to test some behaviour of QString. So I could have simply put the test code in the library I was just working on (kio_audiocd), compile it, install it and then watch the output. Better would be to write just a small testcase. Writing a small testcase for a Qt3 app ? How do I compile it ? Using autotools ? Writing anything which is small with autotools as buildtool ? To me this sounds like a contradiction in itself.
But then I thought "Wasn't there something called CMake ?"
And yes, fortunately CMake was installed on my box ;-), so I could just go ahead, write a small main.cpp which just consisted of maybe 15 lines of code, accompanied by this tiny CMakeLists.txt:

<pre>
   find_package(Qt3 REQUIRED)
   include_directories(${QT_INCLUDE_DIR})
   add_executable(hello main.cpp)
   target_link_libraries(hello ${QT_LIBRARIES})
</pre>

which means: find Qt3 on my system, add the Qt3 include directory to the compiler include path, build an executable from the file main.cpp and link it to the Qt libs. Did this, entered "cmake .", ran make, and voila, there was my small testcase application. 
This has never been so easy.
Anybody thinks things might get more complicated when trying to do the same for KDE 3 ?
If so, you are wrong. 
Next day I was looking at a buglet in libkio, when I thought "I should write a small test to see how that function of KURL exactly behaves."
So I wrote some lines of test code and a small CMakeLists.txt:

<pre>
   find_package(KDE3 REQUIRED)
   include_directories(${KDE3_INCLUDE_DIR} ${QT_INCLUDE_DIR})
   link_directories(${KDE3_LIB_DIR})
   add_executable(hello main.cpp)
   target_link_libraries(hello ${QT_AND_KDECORE_LIBS})
</pre>

Not that much different from above, is it ?
Find KDE3 on the system, add the include dirs, now also add the KDE3 library directory to the linker search path, build the executable and link it to the libs. That's all.
Has building software ever been easier ?

Of course there are a lot of more things which rock, but I think these are enough just for today :-)



