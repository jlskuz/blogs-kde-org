---
title:   "News from buildsystem land"
date:    2010-02-23
authors:
  - alexander neundorf
slug:    news-buildsystem-land
---
I haven't blogged about my development activities for some time now...
So here come some news.

We released KDE SC 4.4.
There were no really big new features in this release buildsystem-wise. Nevertheless it was enough work. PolicyKit support has been added, which was quite some work, and all the new Strigi/Soprano/Nepomuk/Raptor/... stuff, which is still quite confusing for me, and which broke our buildsystem during the 4.4 cycle for a few weeks. But we got that sorted out too :-)

For KDE 4.4.x we still require version CMake 2.6.2 or above, as we do since KDE 4.2.0. This required some effort, to make sure no "code" which requires newer versions gets added somewhere. Right now CMake is at version 2.8.0, and <a href="http://www.cmake.org/pipermail/cmake/2010-February/035185.html">2.8.1</a> is in its RC-cycle. Please give it a try, to make sure we get a release which works without problems with the KDE codebase.

In the current 4.5 cycle we may increase the required version of CMake, so we can use the newer features again. I see basically two options: upgrade from 2.6.2 to 2.6.3, which would give us parentheses in if()-expressions, and better finding of FooConfig.cmake files.
Or directly go to the most current release, 2.8.1, or maybe 2.8.2, so we can stay with this again for a few release cycles.

With KDE requiring CMake 2.6.2, and KDE being available not only on Linux, but also other operating systems like FreeBSD, Solaris, OSX and Windows, it becomes important that we can ensure KDE trunk keeps building and running on all these platforms. Luckily, CMake comes together with CTest, which is a tool to drive testing, continuous and nightly builds.
We are currently in the process of setting a CDash dashboard for the KDE modules up at <a href=http://my.cdash.org">http://my.cdash.org</a>, which provided for free by <a href="http://www.kitware.com">Kitware</a>. E.g. here you can see the dashboard for kdelibs trunk: <a href="http://my.cdash.org/index.php?project=kdelibs&date=2010-02-23">http://my.cdash.org/index.php?project=kdelibs&date=2010-02-23</a>

This is not yet finished. I'm still working on making it easier to set such nightly builds up and for a project of the size of KDE we need finegrained notification emails. E.g. the maintainer of kcalc in <a href="http://my.cdash.org/index.php?project=kdeutils&date=2010-02-23">kdeutils</a> may not be interested in problems in other parts of kdeutils, like e.g. okteta. To help with this there is support for subprojects in CTest and CDash, but I'm still not sure how we can make good use of that in KDE, it seems currently still quite some manual work is required. So we may need some modifications in CMake/CTest/CDash to make it easily usable in KDE. Documentation for using subprojects can be found <a href="http://www.kitware.com/products/archive/kitware_quarterly0709.pdf">here</a>, the <a href="http://www.kitware.com/products/thesource.html">"The Source"</a> edition from July last year. 
If you are interested in helping, come and join us on the kde-buildsystem@kde.org mailing list :-)

In KDE itself, Harald Fernengel started to work on cross-compiling support for KDE. This work is still in its early phase, but we may well have a cross-compilable KDE 4.5 release :-) ...or at least some parts.
CMake <a href="http://www.itk.org/Wiki/CMake_Cross_Compiling">supports cross compiling</a> since its 2.6.0 release. When making a software cross-compilable, you have to keep several things in mind:
<ul>
<li>you cannot run the executables you build, so avoid configure checks which need to run an executable.
<li> the same is true for running foo-config executables to query package information. This will in general not work when cross-compiling.
<li>when writing custom commands or targets, keep in mind that you cannot run the executables you just built. These helper executables may have to be imported from a native build.
<li>you may have to check not only for one system, but two systems. CMAKE_SYSTEM_NAME gives you the system name of the target platform, CMAKE_HOST_SYSTEM_NAME gives you the system name of the build host.
</ul>
Implementing this in KDE is quite some work. I think it may be even more work to keep cross compiling working then. I guess for that we will <b>really</b> need good working nightly builds.

In CMake itself, I'm currently working on adding support for the IAR compiler, which is a compiler for embedded systems. For added fun, the IAR compiler for ARM and the IAR compiler for AVR seem to be two completely unrelated toolchains, using different command line options, linkers and file formats (the AVR compiler uses some proprietary r90 object file format, haven't found any documentation on this yet).
That said, the IAR ARM compiler is already working, and I'm working on the AVR compiler now.

Beside that, since a few weeks I own a Symbian mobile phone. So, guess what...<br>
Yes, I'm also working on adding support for Symbian to CMake (... in order to be able to build Qt apps for my mobile phone).
That's actually more work than I expected. The thing is, Symbian comes with its own set of weird build tools (named abld and more scripts), which generate Makefiles finally. Symbian seems to consist of many many small libraries, and I didn't find a good overview documentation yet. Also, the different versions of Symbian also seem to differe slightly in required compiler options and definitions. 
Currently I am able to compile and link "Hello world" for Symbian, but in order to be able to install it on the device, I need to create a sis-file from it, using more strange Symbian tools. But I think we'll get this working too :-)

So, that's it for now.
Alex
