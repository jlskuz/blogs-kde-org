---
title:   "Need a feature or fix in cmake ?"
date:    2006-08-10
authors:
  - alexander neundorf
slug:    need-feature-or-fix-cmake
---
Hi,

well, imagine you find something in cmake which doesn't work or there is something missing.
Now, luckily enough, cmake is Free Software, BSD licensed. So, you can just have a look at the sources, write a patch and send it to the cmake developers at cmake@cmake.org .

This is probably a better approach than writing wrapper scripts which hide your issue.
So, feel free to start submitting patches :-)

Bye
Alex
