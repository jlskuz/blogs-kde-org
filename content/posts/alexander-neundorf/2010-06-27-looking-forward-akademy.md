---
title:   "Looking forward to Akademy :-)"
date:    2010-06-27
authors:
  - alexander neundorf
slug:    looking-forward-akademy
---
Hi,

this year I'll be again at Akademy, for me it will be the third time after Dublin and Mechelen. I'm already looking forward to meet all you KDE guys again :-)

Another thing: on Tuesday, there'll be a BoF about Continuous and Nightly/Daily building and testing of KDE on the various supported platforms: <a href="http://community.kde.org/Events/Akademy/2010/Tuesday">http://community.kde.org/Events/Akademy/2010/Tuesday</a>.

Originally I submitted this for a full talk, but I "only" got a BoF. If you are interested in keeping KDE working on the more "exotic" platforms (i.e. not Linux) or in non-standard configurations, please join.

Now because I'm lazy, here's the abstract for it:

Since the release of KDE 4.0.0 KDE supports a growing number of platforms.
While KDE 3 was basically UNIX-only, with KDE4 we now also support natively MS Windows 
and Apple OSX, and mobile platforms like MeeGo and maybe also Symbian may be
supported in the future.
While it is one thing to get KDE or some part of KDE working on a specific
platform, once this is done, it needs a lot of continuos work to keep this working.
This is especially true since for these "minor" platforms there is typically 
only a small group of developers, which are not able to check everything every day.
This is not only true for the mentioned "exotic" platforms, but basically for
anything which is not Linux+gcc, e.g. Solaris with the Sun Studio compiler, or FreeBSD, etc.

One way to help with keeping KDE working everywhere are regular Nightly builds
and testing on all platforms.
KDE4 is built with CMake, which comes with tight integration of CDash quality dashboards
via CTest. CDash a is an open source, web-based software testing server. It aggregates 
and displays the results of builds and test runs submitted from clients 
running different operating systems and compilers, located around the world. 
This approach of distributed testing fits very well with the 
distributed way KDE is developed.

Since early this year work has been going on to set up CDash dashboards for all
KDE modules at <href="http://my.cdash.org">http://my.cdash.org</a>. This work is now mostly done and the
dashboards are already collecting contributions.

In the talk I will 
<ul>
<li>give an overview over CTest and CDash in general
<li>explain how it is integrated with KDE
<li>show how to use the dashboards to ensure your application or platform stays working
<li>show how to set up Nightly builds to contribute to the dashboard
<li>give an overview over the current status and open issues.
</ul>

<i>Who should attend ?</i>
<ul>
<li>everybody who cares about KDE quality
<li>developers who are working on supporting KDE on an "exotic" platform
<li>application or module maintainers who want t make sure their application or module
   is always working everywhere
<li>people who have some spare hardware or virtual machines which could be used for running nightly builds

Alex
