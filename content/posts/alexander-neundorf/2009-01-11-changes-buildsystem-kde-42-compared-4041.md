---
title:   "Changes in the buildsystem of KDE 4.2 compared to 4.0/4.1"
date:    2009-01-11
authors:
  - alexander neundorf
slug:    changes-buildsystem-kde-42-compared-4041
---
Hi,

in KDE 4.2 we have quite a few changes regarding the buildsystem compared to 4.0/4.1. Also we require at least CMake 2.6.2 now, which alone brings a lot of new features.

So, if you want to know what has changed, IOW, if you are a 
KDE developer or a packager or somebody writing about it, you can find a (hopefully) full list on TechBase:
<a href="http://techbase.kde.org/Development/CMake_KDE_4_2">Changes in the buildsystem with KDE 4.2</a>.

Alex
