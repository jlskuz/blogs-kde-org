---
title:   "FOSDEM - Day 2"
date:    2007-02-25
authors:
  - bart coppens
slug:    fosdem-day-2
---
Unfortunately FOSDEM is over already, it was a lot of fun. Today started with me getting there at the much too early hour of 08:55, meaning I'm very tired today ;) This morning I mostly was in the Crossdesktop session of the GNOME room. It started off with a pretty impressive demo of Metisse, which apparently is some kind of X hack that looks and works really well. Some impressive features were the customized UI and the 'flipping' away of the screens on top of a lower screen when you select text from it. Next was Jos van den Oever's talk about Strigi internals, which I think will be very neat in KDE4. I tried my best to get a picture of him in action, but unfortunately it was too dark for the small camera I borrowed from Jos Poortvliet; hopefully the GNOME people took a pic, that would be nice. After that was a talk about Wasabi: a proposal for a standard on stuff like desktop search engines and things a user can query.

At noon I walked around the FOSDEM area a bit, talking to people at some booths, talking to some KDE people, playing around with a demo model of a OLPC that someone brought along, all very fun. The afternoon sessions started with my Krita talk, which I thought went pretty well. The things I showed all worked all right, even the small live demo of KWord 2's Flake shapes, so I'm very happy about that as well. There also were some people from the GIMP asking interesting questions: unfortunately they left after the talk, and I had to leave as well for the GDB talk, so we didn't get a chance to talk afterwards. Oh well.

After my talk, the room was getting packed completely full for Jos Poortvliet's KDE4 talk. It was very nice to see so many people interested in KDE4, let's hope we can give them something! But like I said, I wasn't actually at that talk: I went to the GDB talk. The concept is very cool: kernel tracepoints from within gdb. You can get 'live' (not quite, but as good as it gets) debug data from the kernel gdb is running in. Very very nifty thing. After that I went to the KDE room to talk with some KDE people, and then to see Sebastian's KDE e.V. talk. After that my day ended with the 'Beyond GPLv3' talk. Probably just in time, because I was very very tired after 2 days of FOSDEM.

All in all it was a very fun event for me, and I can't wait to meet all those people again at next year's FOSDEM :) <!--break-->