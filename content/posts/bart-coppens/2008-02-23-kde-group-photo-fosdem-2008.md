---
title:   "KDE Group Photo at FOSDEM 2008"
date:    2008-02-23
authors:
  - bart coppens
slug:    kde-group-photo-fosdem-2008
---
So, not only is it FOSDEM this weekend, I also organized a group photo for the KDE related people who are here. We went to the small field of grass in front of the KDE devroom. Luckily, the sun just started breaking through the clouds, so there was nice lighting (unfortunately I forgot to switch my ISO back from 1600, so it's a bit noisy nonetheless).
Here's a very small version (WiFi is a bit slow here, so no big version yet)
<a href="http://blogs.kde.org/node/3291"><img src="https://blogs.kde.org/files/images//IMG_1673_small.preview.jpg" /></a><!--break-->