---
title:   "Comet Holmes"
date:    2007-10-30
authors:
  - bart coppens
slug:    comet-holmes
---
Once again, there's a wonderful treat for people who want to take a look outside at night. Less than a year ago, I was pleasantly surprised to be able to see <a href="http://blogs.kde.org/node/2609">comet McNaught</a> with the naked eye. This time, it is <a href="http://en.wikipedia.org/wiki/Comet_Holmes">comet Holmes</a> giving us a surprise show by suddenly brightening! More details are all over the net, like <a href="http://www.skyandtelescope.com/observing/home/10775326.html">here</a>.

Just like last time, the weather in Belgium sucked these past few days. Today the clouds finally opened somewhat at night (not completely, but still), so it was only a few moments ago that I was able to have a real look. And I was once again not disappointed at all. :) I was able to see the little fuzzy bulb with the naked eye just fine. Looking at it through binoculars is pretty amazing, even though it does look more like a blob rather than the stereotypical head-with-long-tail. If you're near the northern hemisphere, you might want to try and have a look as well.

As for the final KDE-hook: it appears the comet is also findable in everybody's favourite KDE-Edu application KStars, so if you can't figure out where the comet can be found by use of the sky charts in the above links, you could try to find it using KStars :)<!--break-->