---
title:   "Pictures of Akademy 2008"
date:    2008-08-13
authors:
  - bart coppens
slug:    pictures-akademy-2008
---
So, 3.5 days of Akademy apparently means a lot of pictures being taken, I'm pretty sure everyone around has noticed this ;) Now I personally took a lot of pictures (again, most of you will have noticed that), which means that I also have a lot of data. Since I'm not really in the mood to upload 2.1G of data (at least, that's the current count), I'm not going to upload all of that somewhere (especially since a lot of those are actually out of focus, moved, generally blurry, bad, etc). If any of you think I made a nice picture of you, though, feel free to contact me and I'll see if I can actually find any such picture to mail you ;)

Now a some of pictures of me actually already online by virtue of being used in other <a href="http://blogs.kde.org/node/3604">people's</a> <a href="http://blauzahl.livejournal.com/8907.html">blogposts</a> and <a href="http://dot.kde.org/1218497374/">dot stories</a>.

Still, here's some more (some of them slightly cropped) pictures, hopefully somewhat complementary to the ones already on the web.
Dot hero Danny promoting his favourite drink:
<img src="http://www.bartcoppens.be/blogimages/IMG_3797.JPG" />

Here we see Matthew herding the people back to the talks using Adriaan's whip/riding crop:
<img src="http://www.bartcoppens.be/blogimages/IMG_4120.JPG" />

Obviously Cliff did not think Matthew intimidated his audience enough, so he had to borrow Adriaan's riding crop as well to do this himself.
<img src="http://www.bartcoppens.be/blogimages/IMG_4123.JPG" />

Here's an intense Amarok debugging session:
<img src="http://www.bartcoppens.be/blogimages/IMG_4168.JPG" />

It's amazing how much more sportive Akademy seems to be, compared to last year. Not only do people regularly play table tennis, but they even made good use of the lunch break in the KDE e.V. meeting to play some frisbee. Here's Leo and Seb playing frisbee:
<img src="http://www.bartcoppens.be/blogimages/IMG_4331.JPG" />
<img src="http://www.bartcoppens.be/blogimages/IMG_4339.JPG" />
And here's Aaron and Chani playing table tennis against 2 other people during the eV break (apparently tennis can be very funny indeed!):
<img src="http://www.bartcoppens.be/blogimages/IMG_4364.JPG" />

Lydia looking at me suspiciously:
<img src="http://www.bartcoppens.be/blogimages/IMG_4439.JPG" />

And we finish with the renewed El Presidente who is using the stylus of his new N810 as a baton to conduct the KDE Orchestra:
<img src="http://www.bartcoppens.be/blogimages/IMG_4515.JPG" />

It's too bad I don't think I'll be able to take much more pictures, since I unfortunately can't be there on Wednesday (I agreed to go somewhere else, in retrospect that was not the brightest idea ever), on Thursday I'm not on the boat trip, and on Friday most people will either be leaving or will already have left. :(
<!--break-->