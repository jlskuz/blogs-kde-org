---
title:   "FOSDEM 2011"
date:    2011-01-30
authors:
  - bart coppens
slug:    fosdem-2011
---
Less than a week to go for <a href="http://fosdem.org/2011/">FOSDEM 2011</a> next weekend! Just so that you don't forget: this year we have 2 days of <a href="http://fosdem.org/2011/schedule/track/crossdesktop_devroom">(Cross)Desktop-related talks in their own devroom</a>, but be sure to also look at the <a href="http://fosdem.org/2011/schedule/tracks">entire schedule</a> since there's a lot of interesting stuff in the other tracks as well (including some KDE-related ones). Hope to catch a glimpse of you then :) Especially for the <a href="http://fosdem.org/2011/schedule/event/crossdesktopgrouppicture">group picture on Saturday afternoon</a><!--break-->