---
title:   "Change of venue"
date:    2008-10-01
authors:
  - bart coppens
slug:    change-venue
---
For those few of you interested (is there anyone?): I've started a new <a href="http://www.bartcoppens.be/blog/">blog</a>. If this one is any measure, it will be a pretty dead one, but I think it's the thought that counts =)