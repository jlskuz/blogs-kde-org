---
title:   "Almost perfect"
date:    2005-12-15
authors:
  - krake
slug:    almost-perfect
---
After spending some time on implementing a DBUS proxy object, i.e the service object's peer on the client side, I finally found the 100% CPU usage problem I mentioned in my <a href="http://blogs.kde.org/node/1673">last blog</a>

The <a href="http://doc.trolltech.com/3.3/qsocketnotifier.html">QSocketNotifier</a> instance for writing operations is enabled by default, but the <a href="http://dbus.freedesktop.org/doc/api/html/group__DBusWatch.html">DBusWatch</a> it is handling isn't!
<b>"...Note that when a watch is added, it may not be enabled...."</b>
Pah!

So a simple query for the watch's state and matching setEnabled() call on the notifier later the problem was gone and I have an almost complete Qt3 backport of Harald's Qt4 based classes.

The only thing not working that I am currently aware of is an async reply call directly followed by sync reply call.
Somehow the async reply isn't either received or not processed.
