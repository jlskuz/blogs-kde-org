---
title:   "KDE PIM Meeting 2008"
date:    2008-02-03
authors:
  - krake
slug:    kde-pim-meeting-2008
---
I am sitting here in a Cafe on Münster Airport after an exciting weekend at the KDE PIM meeting in Osnabrück.

Being a first-timer I was really looking forward to it and I wasn't disappointed.
Although my travelling to the event turned out to be quite adventurous because my connection flight from Frankfurt to Münster got cancelled due to strong winds and the airline had to re-booked me on a train ride for the rest of the journey.

Incidentally this happend to be the same train Volker arrived on, so I didn't have to find my way to the hotel but could just follow Volker who has been here a couple of times before.

We immediately started hacking after arriving at the Intevation office.
More and more people arrived and shortly we had a full blown Hackathon going, which continued on Saturday (only interupted by a couple of hours planning discussions in the afternoon) and was still going on when I had to leave to catch the bus to the airport.

I think the largest single group were the people working on Akonadi related things:

- Volker working on server and library code
- Tom on (AFAIK) mail related issues
- Tobias and Sebastian on "search" (the word is just way to limited to describe it precisely, borrowing from our friends at the Amarok project I'd rather name it "Rediscover your PIM data")
- myself working on the compatability adapters I blogged about last time

The others were (again AFAIK) mainly working on their respective applications so they'll probably better write about it themselves.

Thomas, who was sitting right next to me, was busy working on KMail. Obviously so busy that Ingo managed to get him off-guard and transfered maintainer ship of KMail to him.
Congratulations to both of you ;)
<!--break-->