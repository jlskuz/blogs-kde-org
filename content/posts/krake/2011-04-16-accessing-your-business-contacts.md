---
title:   "Accessing your business contacts"
date:    2011-04-16
authors:
  - krake
slug:    accessing-your-business-contacts
---
Companies often store their customer information in databases managed by customer relation management tools.

<a href="http://www.sugarcrm.com">SugarCRM</a> is one such popular (and open source) system, built on the extremly wide spread AMP stack (Apache HTTP, MySQL, PHP). Therefore its main user interface is web based, i.e. accessible through standard web browsers and thus also relatively platform independent.

Additional to that SugarCRM is also capable of being a web service, allowing interaction with programs of any sort through passing requests and responses formatted as XML over HTTP.
The technology deployed for this is called <a href="http://en.wikipedia.org/wiki/SOAP">SOAP</a>.

Building a SOAP client with just XML parsing libraries would be a daunting task, so there are many SOAP frameworks which help with that. Now, as KDE developers we would want to use such a framework which is as Qt-like as possible, ideally one being built with Qt.

Under these requirments our champion is <a href="http://kdab.com/kd-soap">KD SOAP</a>, which is not only a Qt based library capable of talking SOAP, it also includes a code generator tools which will take a web service's API description file and generate C++/Qt proxy code for working with the web service as if it would be a local object in your application.
For the developers among this blog's readers, you can think of this as an equivalent to using qdbusxml2cpp for D-Bus services.

When we talk about contacts in the context of KDE we of course imply Akonadi, or more specifically having an Akonadi resource for accessing contact data on a certain backend.

KD SOAP enabled me to quickly develop such a resource for accessing SugarCRM addressbook, which I have now put into <a href="https://projects.kde.org/projects/playground/pim/akonadi-sugarcrm/repository">KDE's git</a>.
It is still lacking polish such as KWallet integration, but is can already be used to view, edit, add and delete SugarCRM contacts through KDE addressbook interfaces.

Like all KDAB's add-on products to Qt and like Qt itself, KD SOAP is available both under Free Software (GPL) license as well as under a commercial license.
The SugarCRM resource is Free Software and licenced under GPL2 and GPL3.

The following are two screenshots showing sample contact data in both SugarCRM's native web interface as well as in KDE's native addressbook:

<a href="http://blogs.kde.org/node/4410?size=_original"><img src="https://blogs.kde.org/files/images/sugarcrm-browser_1.preview.png" alt="SugarCRM contacts in a browser" title="SugarCRM contacts in a browser"  class="image image-preview " width="640" height="295" /></a>
<a href="http://blogs.kde.org/node/4411?size=_original"><img src="https://blogs.kde.org/files/images/sugarcrm-kaddressbook_0.preview.png" alt="SugarCRM contacts in KDE&#039;s addressbook" title="SugarCRM contacts in KDE&#039;s addressbook"  class="image image-preview " width="640" height="377" /></a>
<!--break-->
