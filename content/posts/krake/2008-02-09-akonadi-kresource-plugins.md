---
title:   "Akonadi KResource plugins"
date:    2008-02-09
authors:
  - krake
slug:    akonadi-kresource-plugins
---
I just moved the two Akonadi KResource "bridges", i.e. implementations of the KResource plugins for contacts and calendars based on Akonadi (<a href="http://api.kde.org/4.0-api/kdepimlibs-apidocs/kabc/html/classKABC_1_1Resource.html">KABC::Resource</a> and <a href="http://api.kde.org/4.0-api/kdepimlibs-apidocs/kcal/html/classKCal_1_1ResourceCalendar.html">KCal::ResourceCalendar</a> respectively), to <a href="http://websvn.kde.org/kde/trunk/KDE/kdepim/kresources/akonadi/">kdepim/kresources</a>.

If you are intersted in testing them, you need an Akonadi setup with at least one resource for the respective data type, e.g. an Akonadi vCard resource for testing the kabc plugin.
Run akonadiconsole to check and/or add such resources and it would probably be wise not use valuable data :)

Adding the bridge plugin to an application works just like adding any of the already existing ones, e.g. for the kabc resource you can fire up kaddressbook and then use its menu "Settings -> Show Extensions -> Address Books" to get a list of activated contact resource plugins.

Then click on "Add..." and you should see the resource named "Akonadi" right on top. Adding it (or clicking on "Edit..." later on) will show you a list of Akonadi contact collections. Select the one you want the resource plugin to access and click "OK".

Works pretty much the same for the kcal plugin in KOrganizer.

Btw, if anyone knows what I am doing wrong in the resource configuration widget, i.e. why it doesn't correctly pre-select the collection on "Edit", please let me know.

If you are an application developer using <a href="http://api.kde.org/4.0-api/kdepimlibs-apidocs/kabc/html/classKABC_1_1StdAddressBook.html">KABC::StdAddressBook</a> or the KCal equivalent, you'll probably need to edit the respective stdrc config file to make the Akonadi resource the default.

Since my experience is mostly KABC related, I'd like to ask especially developers using the KCal APIs for feedback.

But now, back to watching the Atlantis ISS docking on <a href="http://www.nasa.gov/multimedia/nasatv/index.html">NASA TV</a>
<!--break-->
