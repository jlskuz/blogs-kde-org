---
title:   "Brazilian overlords"
date:    2009-04-08
authors:
  - krake
slug:    brazilian-overlords
---
In case you thought that reading about the <a href="http://dot.kde.org/2009/04/06/pim-hackers-boost-akonadi-future">Akonadi developer sprint</a> on the dot gave you an all encompassing overview of stuff happening around Akonadi, you forgot our brazilian friends.

Adenilson Cavalcanti, also <a href="http://commit-digest.org/issues/2009-02-22/">featured</a> in one of Danny's excellent commit digests, has been working like a mad man (or a genious, thin line and all that ;-) ) on the Akonadi resources for Google's data services.

Since there is no business like show business, we hereby proudly present you, straight from the source, exclusive premium footage of these resources in action.

Video of Akonadi working with Google <a href="http://savago.blip.tv/file/1972911/">Contacts</a>

Video of Akonadi working with Google <a href="http://blip.tv/file/1972974/">Calendar</a>

(Videos available in Flash and OGG Theora, embedded and downloadable thanks to the excellent <a href="http://blip.tv">blip.tv</a> service)
<!--break-->
