---
title:   "I'm leaving on a jet plane"
date:    2006-09-30
authors:
  - krake
slug:    im-leaving-jet-plane
---
don't know when I'll have net access again :)

So I am leaving Dublin and this years aKademy and I am so glad I attended.
There were so many things to learn, so many people to meet and so much fun to have.

The things to learn about KDE and what great things other people are currently working on is to much to list. I just say that with all the BoFs I attended I had a full week conference, not just two days.

I also learned that Dutch people rule!
They can communicate fluently in more languages than other people can only hope to have even heard about, they do all those imporant non-technical stuff like <a href="http://www.spreadkde.org/">marketing</a> and they are really supportive for my quest for better user support ;)

However, the most important thing I learned at aKademy is something I learned about myself.
While having been a KDE contributor for years, I always felt a bit disconnected from the project. I always assumed that it was the result of only contributing very few code.

But I was so wrong!
As contributors we are peers. We gain the respect of other contributors through our commitment and quality of work.
But at meetings like aKademy we are more than that: we are friends who gain appreciation through friendlyness and openess!

I am a bit sad that I didn't manage to say good bye to all my new friends either yesterday evening or this morning, but I'm looking forward to get a chance to correct this at the next "never-ever-miss-it" meeting.

A special thank you goes to my long time friends Eva und Wolfgang, who not only took me from the airport to the city on last friday and back again today, but also let me stay a night at their place (including the offer to stay for the whole week!) and invited me for lunch twice and once for dinner.
<!--break-->