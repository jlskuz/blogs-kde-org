---
title:   "Improving applications' desktop integration"
date:    2006-11-12
authors:
  - krake
slug:    improving-applications-desktop-integration
---
Application which do not "belong" to any of the free desktop projects, i.e. which do not use their respective development platform, are usually lacking integration features.

However, most of have options to configure helper applications for certain tasks, e.g. handling HTTP URLs, opening files of certain MIME types, etc.

Common use cases of this are:
<ul>
<li> setting a mail client to handle <i>mailto</i> links in Firefox
<li> setting a browser to handle <i>http</i> and <i>https</i> links in Thunderbird
<li> setting a browser and mail client for Google Earth
<li> setting a mail client for OpenOffice.org
</ul>

There are quite some documents available on the Internet how to do this, usually taking one handler application as an example.

Since any change to your desktop configuration implies that you also have to change all this separate configurations, you might have been looking for a better way to do this and, fortunately, there is!

Using the Portland initiative's <a href="http://portland.freedesktop.org/wiki/XdgUtils">xdg-utils</a> adapter scripts, you can now change your desktop's configuration and all those third party applications will follow those changes automatically!

<dl>
<dt><b>Setting a mail client to handle mailto links in Firefox</b></dt>
<dd>

Type <b><tt>about:config</tt></b> into the browser's URL input field and then type <b><tt>mailto</tt></b> into the filter input field of the configuration browser.

If there is an entry called <b><tt>network.protocol-handler.app.mailto</tt></b> right-click it and choose <b><tt>Modify</tt></b>

If there is no such entry yet, right-click anywhere on the list and choose <b><tt>New</tt></b>, then <b><tt>String</tt></b> and then type <b><tt>network.protocol-handler.app.mailto</tt></b>.

In both cases, i.e. either after choosing <b><tt>Modify</tt></b> or after entering the new key, type <b><tt>xdg-email</tt></b> and click on <b><tt>OK</tt></b>
</dd>

<dt><b>Setting a browser to handle <i>http</i> and <i>https</i> links in Thunderbird</b></dt>
<dd>

Since Thunderbird is also a product of the Mozilla Foundation, the steps are pretty similar.
Instead of typing <b><tt>about:config</tt></b> as in the Firefox setup, you can navigate to the configuration browser through the menus. Menu <b><tt>Edit</tt></b>, then <b><tt>Preferences</tt></b> and then on the <b><tt>Advanced</tt></b> tab click on <b><tt>Config Editor...</tt></b>

The entries you want to create or modify here are <b><tt>network.protocol-handler.app.http</tt></b> and <b><tt>network.protocol-handler.app.https</tt></b>, their value in both cases should be set to <b><tt>xdg-open</tt></b>

<dt><b>Setting a browser and mail client for Google Earth</b></dt>
<dd>

Google Earth is a bit more complex, since it does not (yet) allow to treat <b><tt>http</tt></b> and <b><tt>mailto</tt></b> differently.

A quick solution is to set <b><tt>$BROWSER</tt></b> to <b><tt>xdg-open</tt></b> before launching the program, e.g. by editing the <i>googleearth</i> start script and putting the following line after the export for LD_LIBRARY_PATH:
<code>
export BROWSER=xdg-open
</code>

A better, yet more complex, solution, would be to set the variable to point to a script which delegates <b><tt>mailto</tt></b> URIs to <b><tt>xdg-email</tt></b> and everything else to <b><tt>xdg-open</tt></b>, but the simple solution should work well enough.
</dd>

<dt><b>Setting a mail client for OpenOffice.org</b></dt>
<dd>

Theoretically, OpenOffice.org offers a configuration option for an email client in its settings dialog. Menu <b><tt>Tools</tt></b>, then <b><tt>Options...</tt></b>. Expand the category <b><tt>Internet</tt></b> and click on <b><tt>E-mail</tt></b>

However, changing this to xdg-email does not work, or at least it fails on my system :(
I tried with and without full path to the <i>xdg-email</i> executable, but OOo seems to be expecting something else.

Searching through the file belonging to the OOo package, I found a script called <i>open-url</i>. Maybe it can be modified to delegate to the xdg-utils adapters.

If anyone gets this to work, please let me know!
</dd>
</dl>
<!--break-->
