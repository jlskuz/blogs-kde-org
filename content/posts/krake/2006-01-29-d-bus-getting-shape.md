---
title:   "D-BUS getting into shape"
date:    2006-01-29
authors:
  - krake
slug:    d-bus-getting-shape
---
The last few days saw a couple of threads on the dbus mailinglist about topics we KDE users got used to love in DCOP.
For example how to start browsing for interfaces on applications connected to the bus, in D-BUS terminology called <a href="http://dbus.freedesktop.org/doc/dbus-specification.html#introspection-format">introspection</a>, how to know which session busses are currently active for a user and how to work with users session busses from the system bus.

The last two features are known to DCOP commandline client users as
<pre>
dcop --all-sessions
</pre>
and
<pre>
dcop --all-users --all-sessions
</pre>
where especially the second form is very nice to have in administrative scripts to inform the users about stuff going on, for example telling them via KNotify that the system is about to reboot and they better save or be sorry, or to save and logout all KDE sessions for those users who ignore you anyway :)

And as a personal note to the introspection thingy: if you have introspectable objects you are supposed to <a href="http://www.englishbreakfastnetwork.org/apidocs/apidox-playground/work-apidocs/dbus-qt4-qt3backport/html/classQDBusConnection.html#a16">register</a> one as a startingpoint at the <a href="http://www.englishbreakfastnetwork.org/apidocs/apidox-playground/work-apidocs/dbus-qt4-qt3backport/html/dbusconventions.html#dbusconventions-objectpath">object path</a> "/" (the root of the path namespace), which is unfortunately not documented yet. (The Qt3 bindings API DOX do, but they in turn do not provide an easy way to generate introspection data yet)
<!--break-->
