---
title:   "Another challenge: support"
date:    2006-03-11
authors:
  - krake
slug:    another-challenge-support
---
Beside being a developer I consider myself part of the KDE support team.

I have been doing developer and end user support for a couple of years now, for the last two years almost exlusively (read no development).

Bram has issued a <a href="http://bram85.blogspot.com/2006/03/reminder-our-forum.html">reminder</a> that KDE has a web forum for user support, asking other KDE constributors to come over and help.

I have to admit that I haven't visited it since my "return" to development in December and the forum seems to be quite a mess, e.g. people (multi-) posting and not moderators to move the threads into the correct subsection (my moderator rights must have been removed, possibly a side effect of being a <a href="/www.qtcentre.org">QtCentre</a> admin :-/)

Anyway, unchanged from my more active times the Kopete section is still the one most badly needing more supporters.
Just in case you haven't heard about this section yet: I call it the Kopete section even if it is officially named <a href="http://www.kde-forum.org/board.php?boardid=34">Internet & Network</a> because about 90% of the topics are questions on Kopete.

I am afraid our support capabilities are not keeping up with our market share gains. In the earlier days we had major developers like Waldo Bastian or Ingo Klöcker answering question on KDE's user mailinglists, but the increasing workload for them as developers made them unavailable for that task.

As a result we see users moving their questions onto developer lists.

So I'd like to ask all KDE contributors that when you are asked by someone about possibilities to help KDE, don't forget to mention "support" as one of the options.

If you ask yourself "How on earth can one volunteer to do support? The IT sectors nightmare job?", let me give you a few hints:
<!--break-->
<ul>
<li>In contrast to paid for support work you don't have to do it. If you don't feel like answering a particular question or take a leave for any number of days, it's your decision
<li>You don't need to know everything, community based support combines the knowledge of all supporters
<li>While it helps to be a developer for tracking down stuff like file-only configuration options or hard coded limitations, most user questions are about the programs usual functionality
<li>Your knowledge about KDE increases when you test possible solutions or experiment with settings you haven't used yourself yet
<li>You learn about features you didn't even know existed, perhaps because they were introduced in a later version of a program or there is a new one for the same task you are handling differently
<li>It is rewarding to know you are part of a solution that makes users happy
<li>It can be rewarding to compete with other supporters, e.g. answering a tad bit faster then the other one :)
</ul>

and my favorite

<ul>
<li>While other possibilities of contributing to KDE usually require that your spare time is available in blocks, e.g. an hour or more, support can be done in quantities of minutes, a single e-mail is quickly written.
</ul>

In case you want to give it a try, I'd ask you to consider a few recommendations:
<ul>
<li>If you know something is answered in an online documentation, e.g. a FAQ, don't just tell the user to read it, provide a link to it. Just because you can find it doesn't necessarly mean he or she can. Same goes for googling, any kind of search depends on the quality of the keywords you search for!
<li>If a user gets angry or demands his/her problems are solved ASAP, drop it! There is no point in getting yourself frustrated.
</ul>
