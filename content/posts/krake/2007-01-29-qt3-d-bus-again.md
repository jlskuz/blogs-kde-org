---
title:   "Qt3 D-Bus again"
date:    2007-01-29
authors:
  - krake
slug:    qt3-d-bus-again
---
After almost a year of I am working on the Qt3 D-Bus bindings again.

The past week I have been fiercely working on getting them in shape for the requirement of the D-Bus based DAPI implementation on KDE3.

Since working with bindings is quite boring I decided to write a code generator that creates nice wrapper code for D-Bus interfaces (service and client side).

This really got me into hacker mood, so aside from the benefit of easier debugging later on I had the necessary commitment for free :)

Today I have been working on support for maps (dictionaries in D-Bus speak), but I think I should have concentrated on QString as key first and not writing code for all primitives right away.

If you want to see something in action, check this <a href="http://lists.freedesktop.org/archives/portland/2007-January/000938.html">mailinglist posting</a> by me for further links.

Since I am a bit of a KDE PIM guy, I especially like the address book section :)
<!--break-->

Btw, how about a "D-Bus" tag for kdedeveloper.org's blog system? ;)