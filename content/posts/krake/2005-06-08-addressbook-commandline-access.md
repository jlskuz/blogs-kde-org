---
title:   "Addressbook commandline access"
date:    2005-06-08
authors:
  - krake
slug:    addressbook-commandline-access
---
While preparing for <a href="http://blogs.kde.org/node/view/1089">my talk</a> on KDE commandline scripting I discovered a couple of new commandline clients.

So afterwards I continued to look for possibilities of accessing KDE from the commandline.
After a while I thought it would be nice if one could access the KDE addressbook like <a href="http://pim.kde.org/components/konsolekalendar.php">konsolekalendar</a> allows to access KOrganizer's data.

At first I missed <a href="http://websvn.kde.org/trunk/KDE/kdepim/kabc/kabc2mutt/">kabc2mutt</a> and started to code my own client program: <a href="http://www.sbox.tugraz.at/home/v/voyager/kabcclient/kabcclient-0.5.tar.bz2">kabcclient</a> (however it now also features a kabc2mutt compatability mode :))

Features:
<ul>
<li>List whole addressbook</li>
<li>Search in addressbook</li>
<li>Add entries to addressbook</li>
<li>Remove entries from addressbook</li>
</ul>

It is pretty flexible how it interprets input (from commandline or stdin) and how it formats its output, thus allowing it to work like kabc2mutt if called by that name.

Usage examples can be found in the <a href="http://www.sbox.tugraz.at/home/v/voyager/kabcclient/README">README file</a>