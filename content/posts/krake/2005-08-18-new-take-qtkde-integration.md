---
title:   "New take on Qt/KDE integration"
date:    2005-08-18
authors:
  - krake
slug:    new-take-qtkde-integration
---
I have blogged about QDS, my Qt desktop integration library, almost exactly a year ago.<br>
Unfortunately I didn't have much time back then due to serving at the Red Cross
(instead of serving in the military; Zivildienst for the German speakers).<p>

However, as the <a href="http://www.sbox.tugraz.at/home/v/voyager/qds/index.html">QDS part of my webspace</a> got quite some hits after mentioning QDS on the Dot,
I decided to have a look at it again.<p>

While my original goal was to provide the best flexibility, I figured that the
required additional complexity was an obstacle for developers wanting to try it out.<br>
So I restructed it a bit to achieve the new goal: <b>require as few changed to a Qt program
as possible<b>:<p>
<!--break-->

Consider you have this main function in your network aware Qt application:
<code>
#include <qapplication.h>
#include <qnetwork.h>
#include "mywindow.h>

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    qInitNetworkProtocols();
    
    MyWindow w;

    app.setMainWidget(&w);
    w.show();

    return app.exec();
}
</code>

Now, if we change that to use QDS in order to enable KIO based access on KDE, we get this:
<code>
#include <qapplication.h>
#include "qds/qds.h"
#include "mywindow.h>

int main(int argc, char** argv)
{
    QApplication* app = QDS::createApplication(argc, argv);
    
    MyWindow w;

    app->setMainWidget(&w);
    w.show();

    int ret = app->exec();

    delete app;

    return ret;
}
</code>

The createApplication function in the QDS namespace is a convenience wrapper around a
a couple of calls on the QDS main class, the service factory.<br>
The Unix implementation of this class checks if there is a plugin name specified on
the commandline (commandline switch --qds=pluginame), if this fails it checks the
configuration file (.qt/qdsrc, section General, key Plugin) and if this fails as well
it will try some kind of desktop detection (currently decides to use the "kde" plugin
if KDE_FULL_SESSION is present in the application's environment and "true").<p>

The KDE plugin as of QDS release 0.3, implements only the Network service, i.e.
providing QNetworkProtocol implementations through KIO.<br>
A network aware Qt application, for example one which uses QUrlOperator, automagically
gets access to the protocols implemented in KIO slaves if the plugin could be
loaded successfully, but has still access to the three protocols Qt implements itself
in case the loading failed, thus being equivalent to calling qInitNetworkProtocols().<p>

An simple example application in the QDS source package demonstrates this.<br>
Having just built libqds and the application, it will be capable of doing this:<br>
<pre>
#> qdsexample http://www.kde.org/ kde.html
</pre>
downloading the index of www.kde.org to the local file kde.html using Qt's HTTP
implementation.<p>

After building and installing the KDE plugin (and probably setting LD_LIBRARY_PATH
accordingly), the above example should transfer the HTML code using kio_http<br>
(if you want to use the commandline option to specify the KDE plugin, provide the option
as the third parameter. The application is always taking argv[1] as the source URL and
argv[2] as the target file)
