---
title:   "Akonadi porting explained"
date:    2009-12-03
authors:
  - krake
slug:    akonadi-porting-explained
---
For quite some time almost every blog by a KDE PIM developer is about Akonadi in one for or the other, often about "Akonadi porting" or "porting to Akonadi".

Akonadi itself can already be difficult to explain, combined with "porting" it probably has only meaning left if you are a developer.

The thing anyone else will be able to extract are delays.
Delays in the sense that we, the developers working on KDE PIM, are enthusiastic about our plans on what we want to achieve for a certain KDE release, only to be disappointed that we didn't get as far as we originally hoped for.

For example an initial guess was to have KAddressBook and KOrganizer ported in time for the 4.3 release, but lack of resources meant we didn't even get to start working on KOrganizer in any significant form and the new KAddressBook would lack some features.
So KAddressBook is held back and finished for 4.4, KOrganizer is still being worked on.

So what is all this porting business actually about and why does it take so long?

Imagine a situation where you want to cook something (well, you can imagine anything els of course, but it will greatly improve the understanding of the following example if you restrict your fantasy to cooking for now).

After you've decided what you want to cook, you'll have to get your ingredients, which usually means going out for shopping. When you get home, probably after quite some time and travelled distance, you'll have to prepare these ingredients (wash, peel, chop, etc.) and then start the actual cooking.

That works pretty well, millions of people do between that once in a while and every day.
So you meet other cooks and discover that while you have individual styles for preparing ingredients and the cooking process, you all despise the shopping chore, especially when shops are crowed, roads are jammed, weather is nasty, and so on.

On one of those lazy sunday afternoons you read about outsourcing and immediately convince your fellow cooks to outsource shopping to a shopping specialist.
You snicker at the thought of this poor fellow having to brave the nasty weather, travel the jammed roads, wait in line at stores, while you comfortably wait for the devlivery.

However, you discover that you have to adjust your approach to cooking to accomodate for this for of ingredient acquisition.
With your initial approach you knew when you would be doing what for how long (not counting unavoidable delays during shopping).
With your new approach you don't anything between placing your order and the arrival of the goods. Sure, you could be waiting at the door step, but that gets boring after a while. You can spend some time preparing everything you already have at home, but usually that won't keep you busy long enough. So you do something else instead, maybe reading Planet KDE and learning that those lazy developers have balantly copied your outsourcing idea :)

Switching to a different reality there is this cook called KMail (in this reality cooks are, for an unknown reason, actually called "mail user agents").
An absolut expert in preparing an ingredient called "message" (weird reality, I know), peeling of envelopes, chopping into pieces (jokingly called "multi parts"), etc. You know, the usual cooking stuff, just done expertly.

But again the shopping for "messages" at shops called "servers", with owners of sometimes questionable character, low quality offerings and so on, is making KMail's life unpleasent.
So of course outsourcing that to a shopping specialist (in this reality, weird as it is, called "Akonadi") is the way to go.

Unfortunately our cook KMail discovers, while adjusting to the different cooking approach, that all these years of shopping had resulted in acquiring certain habits (in another reality, the author of this blog has the habit of not using shopping lists because eventually he'll recognize things to buy at the shop anyway).

Habits that need to be unlearned or replaced by habits which fit the new situation better. Habits our cook might not have been aware of or vowed never to think about again.
Fortunately there is a group of motivational trainers (having the surprisingly sensible name "KDE PIM Developers") who will help to overcome those.

It takes time, but it is totally worth it.
<!--break-->
