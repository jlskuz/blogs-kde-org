---
title:   "April: an eventful month"
date:    2009-05-02
authors:
  - krake
slug:    april-eventful-month
---
The past month had quite some cool things in store.

It started with the <a href="http://dot.kde.org/2009/04/06/pim-hackers-boost-akonadi-future">Akonadi developer sprint</a> in Berlin, Germany, where we got quite some work done, especially regarding mails.

This was followed by the general ranking period for this year's Google Summer of Code proposals and we were delighted to see that the Akonadi related ones did exceptionally well.

Three of them are mentored by developers from the KDE PIM team: <a href="http://socghop.appspot.com/student_proposal/review/google/gsoc2009/stefanek/t123843773479">Akonadi Filtering Framework</a>, <a href="http://socghop.appspot.com/student_proposal/review/google/gsoc2009/exit/t123844646927">Global Outbox for Akonadi</a> and <a href="http://socghop.appspot.com/student_proposal/review/google/gsoc2009/saschpe/t123849664087">SyncML Agent for Akonadi</a>.
I am proud to be a mentor again, for the fifth consecutive year, my second time for Akonadi/KDE PIM.

Two more Akonadi related proposals which got accepted are <a href="http://socghop.appspot.com/student_proposal/review/google/gsoc2009/edulix/t123851315382">Bookmarks with Akonadi and Nepomuk</a> and <a href="http://socghop.appspot.com/student_proposal/review/google/gsoc2009/roide/t123833272786">Conversation Logging Framework</a>.

Later on that month our local Linux and FLOSS event, <a href="http://www.linuxtage.at">Grazer Linuxtage 2009</a> succeeded far beyond our (as in the organising team, which I am a part of ) expectations.

<img src="https://blogs.kde.org/files/images/kde-linuxtage-2009.jpg" alt="Konqi and Kevin talking with Clifford Wolf at Grazer Linuxtage 2009" title="Konqi and Kevin talking with Clifford Wolf at Grazer Linuxtage 2009"  class="image image-_original " width="800" height="533" />

Konqi and I managed to present KDE for the better part of the day, basically only interrupted by time I gave my talk about way of participating in FLOSS projects.
I'd say the feedback I've got on both accounts was quite positive, though it would have been nice to have a stronger presence at such events.

But as I already mentioned in one of my earlier posts, Austrians lack the necessary community spirit or at least the ones participating in desktop oriented projects are (OpenOffice.org was mainly represented by folks from Germany, GNOME didn't show up at all).

Anyways, shortly after the event I achieved enought experience points to gain another level.
I am still trying to figure out which additional skills I've got, however telekinesis seems not to be included. Again. *sigh*

Incidentally the month ended as it began: me being in Nuremberg :)

Back home again I have to make sure I get all my refactoring done in time for the upcoming message freeze.
<!--break-->
