---
title:   "Fairy tales"
date:    2006-05-12
authors:
  - krake
slug:    fairy-tales
---
Once upon a time, I believed in <a href="http://en.wikipedia.org/wiki/Brothers_Grimm">fairy tales</a>.

Later, during my software engineering education, I came across a fairy tale called "capabilties detection".

The tale's main content was that it would be possible for a software to query for available capabilities in some kind of backend.
It told about a mythical protocol called X11, which would allow an even more mythical software called an X11 client, to query an almost unbelievably mythical thing called an X11 server for the availability of what the tale called extensions.

The tale then went on to make the audience believe that the "X11 client" could offer different sets fo features depending on the available "extensions".

A different variant of the tale told about a mythical API called OpenGL, if I remember correctly after all those years it had something to do with three dimensional visualisation.
Anyway, the tale's variant claimed that an OpenGL using application could magically detect which features of OpenGL were available and enable/disable options in its user inferface.

Unfortunately fairy tales do not last forever. :(
Yesterday I learned that the only way to support differently sophisticated backends was to <a href="http://www.jonobacon.org/viewcomments.php?id=687">"...bow to the lowest common denominator in the backends.."</a>

Ah, well, I am sure I'll find a new fairy tale to believe in soon enough.
<!--break-->
