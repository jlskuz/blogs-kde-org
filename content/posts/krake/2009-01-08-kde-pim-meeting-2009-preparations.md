---
title:   "KDE PIM Meeting 2009 Preparations"
date:    2009-01-08
authors:
  - krake
slug:    kde-pim-meeting-2009-preparations
---
The following weekend will see this year's instance of the famous KDE PIM meeting in Osnabrück, Germany, as usual gratiously hosted by <a href="http://www.intevation.de/">Intevation</a> at their shiny new office.

I managed to convince the company I am currently working for that I can attend it and still make it to the final part of our site acceptance test in Nürnberg.
Actually, since I am already in Germany, it will require less traveling than going <a href="http://blogs.kde.org/node/3801">there and back again</a> from Graz.

As I am preparing for my journey tomorrow, I receive confirmation that <a href="http://bugs.freedesktop.org/show_bug.cgi?id=15711">my request</a> for Akonadi project infrastructure at freedesktop.org as finally been closed as agreed upon with the fd.o site-wranglers.
So if you are one of those <a href="http://blogs.kde.org/node/3777">wondering</a> about the state of that request, lean back at ease that for now all parts of the Akonadi related development services will remain in the capable hands of KDE's sysadmins.

In case you are wondering about the "agreed upon" part above: freedesktop.org project hosting is intended to offer projects a reliable, Free Software based hosting solution in case they cannot easily be accomodated by the world class service provided by major community projects such as GNOME or KDE.

The Akonadi project is obviously not in need of that since we know and value high quality of the infrastructure provided by KDE, however it was still necessary to file a request at freedesktop.org to publicly demonstrate that neither do we as developers object to any other solution if some interested third party would have required that nor does our software have any KDE dependencies in case any interested third party would have been afraid of that.

So if you are a KDE developer who's been holding back because you thought that you would have to apply for a new account, I am afraid we removed that excuse ;)

And if you are a new developer or working on some other Free Software project, nothing has changed either.
In both cases developers with proven track record of reasonable participation in this kind of project will easily get accounts if the intend to contribute regularily to Akonadi and new developers are always encouraged to earn such trust the usual way (i.e. sending patches, improve patches on feedback, etc).

Anyway, back to packing.
<!--break-->
