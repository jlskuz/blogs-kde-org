---
title:   "Travelling: the odd(?) lucky guy"
date:    2007-07-11
authors:
  - krake
slug:    travelling-odd-lucky-guy
---
With all these blogs about how awful people's travelling experience has been from/to aKademy, I'd thought I blog a bit about mine.

In short: my journey turned out better than expected.

It started a bit like the other stories, i.e. the plane from Graz to Vienna was held at Graz airport for 40 minutes due to heavy traffic in the area around Vienna. Since my connection flight was expected to leave one hour after my original arrival, I would have still had enough time to reach it in any case.

Turned out the plane from Vienna to London/Heathrow had also been delayed by about the same amount of time, mostly due to waiting for connecting flights to arrive :)

At Heathrow the delay was already about one hour, but since I had wisely planned about three hours layerover in London, I still had enough time to do my presentation preprations while having a huge cup of Cappuchino.

While waiting for the flight to Glasgow, I met up with Aaron, who incidentally got rebooked to exactly the same flight :)
He even managed to get us seated next to each other, so I enjoyed the flight very much.

In Glasgow we met a bunch of other KDE folks, so the task of getting into the city and finding the hostel has been a nice experience instead of taking a costly taxi or having to stop at every corner to check a map.

For the journey from aKademy back home I checked the Wiki if there were any others leaving about the same time and, being lucky again, there were two guys from Finland (Louai Al-Khanji and Teemu Rytilahty) leaving about one hour earlier.

So we shared a taxi to Glasgow International around mignight (just to be sure, extended security and all) and - drum roll - found out that there is yet another KDE hacker already waiting there: Andrew Manson.

As an afterthough, I probably should have bought a lottery ticket, because not only does yet another KDE devleoper arrive (Ivan Cukic), no, he also is on the same flight as myself <b>and also</b> on the flight from London to Munich!

So instead of travelling all alone for hours, I enjoyed great company :)
<!--break-->
