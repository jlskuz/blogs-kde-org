---
title:   "Akonadi Sprint, Final Day"
date:    2010-05-16
authors:
  - krake
slug:    akonadi-sprint-final-day
---
As you most likely have already read <a href="http://www.omat.nl/2010/05/13/akonadi-meeting-here-i-come-mailody-is-dead/">on</a> <a href="http://www.omat.nl/2010/05/13/accountwizard/">various</a> <a href="http://www.omat.nl/2010/05/15/akonadi-meeting-day-2-hacking-accountwizard-continued/">other</a> <a href="http://www.omat.nl/2010/05/16/akonadi-meeting-day-3-productivity-is-amazing/">blogs</a> we had one of our Akonadi sprints for the past couple of days.

Most of the time I've been working on Akonadi support for a KMail mail store, basically a local directory where mails are stored in a combination of nested Maildir directories and MBox files.
We already had support for Maildir and MBox in respective resources, but this "mixed mode", interleaved with highly KMail specific index files, made it necessary to either have some way of importing all this data and metadata or to use it directly similar to KMail.

Conceptionally my preference would have been an import process, but of course factors like requiring (at least temporarily) close to twice the storage space, make the direct usage more viable for the upcoming releases.

So after a couple of days of feverish hacking, we've now got a "Mixed Maildir" resource, capable of operating on a local folder tree created by KMail, including read-only support for message flags ("read", important", etc.).

Today I wanted to work on something different, so I started with an idea I had when brainstorming for possible GSOC/SOK ideas.

Akonadi is service based infrastructure, so there is a bunch of processes working together to satisfy the users' PIM needs.
Since these processes, called Agents ("if you ever see an Agent, run!"), work behind the user session's scene, their status is kind of hidden as well.

While it is of course possible to show the status of e.g. resource for a specific data type in the applications handling that data type, say showing status of IMAP resources in KMail, there are use cases for having access to that information even when non of these applications are running.

Probably the best way to do that on a KDE Plasma Desktop Workspace is to have our status information available through a Plasma Data Engine. So I created one:

<div class="content"><img src="https://blogs.kde.org/files/images/engineexplorer.png" alt="Akonadi Agent Data Engine shown in Plasma Engine Explorer" title="Akonadi Agent Data Engine shown in Plasma Engine Explorer"  class="image image-_original " width="810" height="538" />
<p>Plasma Engine Explorer showing a list of Akonadi Agents, one of them expanded to show the agent's details.</p>
</div>


Akonadi Agents can be controlled through their D-Bus interfaces, e.g. asked to show their configuration UI or in the case of resource agents (data connectors) to (re-)fetch data from their respective backends.

Plasma has matching technology for this use case a well, Plasma Services. So I created one of these as well:

<div class="content"><img src="https://blogs.kde.org/files/images/serviceactions.png" alt="Akonadi Agent Service Actions" title="Akonadi Agent Service Actions"  class="image image-_original " width="645" height="325" />
<p>Plasma Engine Explorer showing the list of actions the Akonadi Agent Service provides</p>
</div>


So the obviously missing piece is a Plasma Widget for actually displaying the data and providing the action UI for the service.
The code for data engine and service is in <a href="http://websvn.kde.org/trunk/playground/base/plasma/dataengines/akonadiagents/">KDE's SVN</a>, so if you are the kind of person with elite Plasma UI skills, its all yours.
<!--break-->
