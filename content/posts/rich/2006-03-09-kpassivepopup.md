---
title:   "KPassivePopup"
date:    2006-03-09
authors:
  - rich
slug:    kpassivepopup
---
Well, after a long break I've finally had a chance to work on KDE again. As a start, I've been trying to get <a href="http://www.englishbreakfastnetwork.org/apidocs/apidox-kde-4.0/kdelibs-apidocs/kdeui/html/classKPassivePopup.html">KPassivePopup</a> into a sane state in the KDE 4 libraries. The first step was moving all the internals of the class into the internal data class, which was a bit of a pain but pretty easy. To make things more interesting, it turns out that the API that worked fine on X11, is actually ambiguous on win32 and macos X due to different definitions of WId in Qt, the fix was to remove the ambiguous constructors (which I'd planned anyway as part of making a more Qt4-style API). The question moving forward is if this class needs to allow subclasses to define new visual representations (probably via a decorator of some kind), or if it's better to say there are the following presentation types and that's it. Does anyone who's using the class have any thoughts on this?

