---
title:   "Back from Glasgow"
date:    2007-07-10
authors:
  - rich
slug:    back-glasgow
---
Well, I'm back from Glasgow and have now almost recovered. The conference was great, and I'd like to thank all the organising team for their efforts. For me things were quite productive, with some nice steps forward in my QtScript code (my bindings are now dynamically loaded plugins for example) and lots of useful discussions about topics from improving the library facilities for scripts. I also managed to make a start on a plasma applet container that lets you write applets in Javascript.

Isn't it nice to be able to blog again :-)

EDIT: I forgot to say, my slides and the video of my talk are <a href ="http://conference2007.kde.org/conference/talks/36.php">online now</a> if anyone wants to see them.
<!--break-->