---
title:   "Plasma Scripting"
date:    2007-07-29
authors:
  - rich
slug:    plasma-scripting
---
I've made some decent progress in the scripting support for plasma today with the addition of the ability to access QPainter, QTimer and QFont from scripts. I've also improved a few other bits of the code. The result is that I've now been able to reimplement a functioning version of the plasma analog clock applet in Javascript. There's obviously more to be done, but I think this shows that things are progressing pretty well and along the right lines. The clock looks just like the C++ one (except I turned on the standard background so it can be distinguished as being the js one).

<img src="http://xmelegance.org/devel/script-applet3.png">
<!--break-->