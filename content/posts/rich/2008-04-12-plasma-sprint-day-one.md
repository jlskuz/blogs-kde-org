---
title:   "Plasma Sprint Day One"
date:    2008-04-12
authors:
  - rich
slug:    plasma-sprint-day-one
---
After a day's work at the Plasma sprint, there's already quite a lot of news
to report. After a lot of trawling through log files, I was able to fix the
problem that was preventing the Plasma binding plugin from loading. In the end
it was something simple (as usual) namely that the method that allows the
plugin to load was not being compiled into the module since it was missing
from the generated .pri file. Once this was fixed, it was simply a matter of
changing the name of the extension we load from 'qt.plasma' to
'org.kde.plasma' to match a fix in the generator and we had a successfully
loading set of bindings.
<p>
Lots of thanks are due to Kent Hansen of Troll Tech
for this - not only did he write the binding generator and the type system for
the Qt bindings, he also fixed up the one for plasma. Oh, and if that wasn't
enough he's even figured out the bug that has been preventing the generated
code from having anti-aliasing enabled.
<p>
In order to test that these bindings worked properly, I wanted to create a
small demo that exercised the script engine code, the Qt bindings and the
plasma bindings in the same plasmoid. My initial attempt worked ok, but didn't
look very good (though it had the advantage of requiring only a tiny amount of
code). Instead, I'll show a slightly more complex example that looks prettier.

<center>
<img src="http://xmelegance.org/devel/plasma-bindings-simple-demo.png">
</center>

Obviously, you can't see the animation in the screenshot, but this shows four
squares spinning around a common axis at different speeds and changing
colour. Ok, it's not the best piece of graphic design you've ever seen but it
does illustrate that the crucial facilities are in place.
<p>
In addition to the coding, we also spent some time today going over the
results Seele obtained from her interviews with some of the developers. We
were looking at the user populations we are currently supporting well, but
more importantly at those that we could potentially encourage to adopt KDE and
Plasma to consider how we can improve the facilities we offer for them
too. Before anyone gets hot under the collar, this doesn't mean ripping out
functionality to 'dumb down' the interface, in fact a lot of the suggestions
were more about how we can improve the interface consistency.
<p>
Following this discussion we did some more coding and also broke down into
smaller groups so we could continue to plan for our inevitable world
domination.
<!--break-->