---
title:   "ADSL from tomorrow, look out XPath and KJSEmbed here I come"
date:    2004-03-02
authors:
  - rich
slug:    adsl-tomorrow-look-out-xpath-and-kjsembed-here-i-come
---
My ISP confirmed last night that my ADSL should be working from tomorrow, so I'll be getting back to work on XPath and KJSEmbed. Hurray!
