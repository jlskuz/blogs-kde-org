---
title:   "KJSEmbed 4, small steps"
date:    2006-03-30
authors:
  - rich
slug:    kjsembed-4-small-steps
---
I finally got some time to spend on KDE last weekend, so on Sunday I started working on a new binding generator for KJSEmbed on Qt 4. In the KDE 3 version I used Doxygen to generate XML output then processed that with XSLT (using xsltproc) to produce the C++ code for the bindings. Unfortunately this provded to be a bit too inflexible, and difficult for other developers (who weren't familiar with XSLT) to work on. The new code uses KJSEmbed's DOM bindings to process the Doxygen output and seems to be a lot easier to work with - [mX] was making improvements within a few hours of the first commits. This also means we might be able to make the bindings self-hosted in KDE 4 which would be nice.

When I get a bit more time (this weekend hopefully!) I plan to try to get this code pushed a bit further - thanks to [mX] it can already mostly do value types, just objects and scalars to go!
<!--break-->


