---
title:   "Advertising"
date:    2008-09-29
authors:
  - rich
slug:    advertising
---
Just watched a very clever advert for the new iphone. 

<ul>
<li>It's a manual, it shows you how to download (and buy) new apps.
<li>It's very short, just one feature
<li>User ends up having fun - playing game, and then call from a guy
<li>Interupting you in the middle of the game is presented as a feature!
<li>Slogan about this changing everything (not sure how it's supposed to do so)
</ul>

The advert seems well put together, but I wonder if we couldn't create something similar regarding KDE features?
<!--break-->
