---
title:   "Setting things up to get core dumps"
date:    2009-03-27
authors:
  - dfaure
slug:    setting-things-get-core-dumps
---
Drkonqi is nice and friendly, but some apps (e.g. kded or krunner) have a crash handler so that they restart instead of annoying the user with crash dialogs.

As developers, we want to see backtraces of crashes in all cases; and to be in gdb in order to be able to print variables etc. Solution: asking for core dumps. That part is easy:

<pre>
export KDE_DEBUG=1
ulimit -c unlimited
</pre>

You should put that somewhere that is read during KDE startup, for instance ~/.kde4/env/debug.sh

But then after a while you find core dumps everywhere in your source and build dirs, and you need some "find all core dumps and delete them" scripts when your disk is full. Not convenient. Instead, I like to set it up like Mac OSX does: with a directory dedicated to core dumps. Fortunately, the linux kernel can be set up like that too. As root, do the following:

<pre>
mkdir /cores
chmod 777 /cores
</pre>

and at the bottom of /etc/rc.local or any other script run by root during the boot sequence:

<pre>
# Let's have the core files in /cores
echo /cores/core > /proc/sys/kernel/core_pattern
echo 1 > /proc/sys/kernel/core_uses_pid
</pre>

Now when an application crashes you'll get a file like /cores/core.14030, which you can use with `gdb krunner /cores/core.14030`   (random example... I swear).

Which leaves one question open: if I go in /cores and I see 5 core files, how do I know which app generated them? You can use the file(1) command, if it's recent enough or old enough (at some point there was a bug, which I sent a patch for some time ago).

<pre>
$ file core.*
core.14372: ELF 64-bit LSB core file x86-64, version 1 (SYSV), SVR4-style, from './resourcetest'
core.16472: ELF 64-bit LSB core file x86-64, version 1 (SYSV), SVR4-style, from '/d/kde/build/t/kdelibs/khtml/tests/khtmlparttest'
</pre>