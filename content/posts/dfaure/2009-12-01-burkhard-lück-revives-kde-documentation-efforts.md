---
title:   "Burkhard Lück revives KDE documentation efforts"
date:    2009-12-01
authors:
  - dfaure
slug:    burkhard-lück-revives-kde-documentation-efforts
---
I just have to say it, I'm thoroughly impressed by Burkhard Lück's determination to improve the user-documentation of KDE applications (particularly the kdebase ones). This is much needed, a huge undertaking, and probably also not very rewarding work. This being said, I'm sure he would appreciate some help -- if you can speak English (you can, otherwise you wouldn't be reading this) and want to contribute to the KDE documentation, subscribe to the kde-doc-english mailing-list :)