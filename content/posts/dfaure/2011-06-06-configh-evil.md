---
title:   "config.h is evil"
date:    2011-06-06
authors:
  - dfaure
slug:    configh-evil
---
We often need to check for a system function or library availability, like HAVE_XZ_SUPPORT, and we put that into a cmake-generated file like config.h (much better than -DFOO because changing that requires recompiling everything).

That's fine. However a single config.h at the toplevel of kdelibs is not modular; every change requires recompiling everything, and at some point we would like to split up the libraries to make their adoption easier by other people.

So the idea -- which was started long ago, before kde-4.0 -- is to split that up into one (or more) generated file(s) per library. Modularity "FTW", as kids say these days.
<!--break-->
If you would like to help with that work, it's quite easy, see <a href="http://commits.kde.org/kdelibs/fe7cbd8bbbabaf13480653749021760545ea75ac">this commit</a> for an example, or <a href="http://commits.kde.org/kdelibs/6815222a658f7e2aca47cd1dcd3920e21066157a">this commit</a> for another one (as you can see, this might require duplicating some one-liner checks in some cases, but that's fine, it's the price of modularization).

If you're more adventurous, do it like kdelibs/kdecore/compression, which uses #cmakedefine01 instead of #cmakedefine, in order to always define the symbol (to 0 or 1) in the header file, and then port the code to use #if instead of #ifdef. This is much nicer in fact, because if you forget to include the right <config-foo.h> header, then gcc will warn about it. With ifdef it would just never compile the code in question...

Well, there you go, kdelibs is all yours, I'll go to other issues now :)