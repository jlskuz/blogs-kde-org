---
title:   "Installing OpenSUSE on Thinkpad P50"
date:    2016-05-03
authors:
  - dfaure
slug:    installing-opensuse-thinkpad-p50
---
The Lenovo Thinkpad P50 is quite a nifty laptop. However installing Linux required some digging around, so I'm writing this up for others to stumble upon it when looking for answers to similar issues.

* It is very important to change the BIOS/UEFI setting from "Legacy and then UEFI" to "UEFI only". Otherwise, installing Linux from a USB key boots in Legacy mode, and after 2 hours of installation, the OpenSUSE installer finishes with the very imprecise "an error occured during boot loader installation". After reading http://www.rodsbooks.com/linux-uefi/ I changed the setting to UEFI only, re-did the installation, and voilà, the bootloader installs just fine. Compatibility is sometimes more trouble than not :-)

* Hardware consideration: since it uses a M.2 SSD (small form factor) and doesn't provide the necessary (Lenovo-specific?) cable to plug a 2.5" SSD, my initial idea of replacing the SSD with a bigger one failed. Should have bought another M.2 SSD instead.

* Grub2 in graphical mode was horribly slow (maybe due to high DPI, too many pixels to paint?). I changed /etc/default/grub to say GRUB_TERMINAL=console, and then ran `grub2-mkconfig -o /boot/grub2/grub.cfg` and `grub2-install`. Good old menu in text mode, much faster.

* The Wifi didn't work at all, because OpenSuSE Leap 42.1 includes kernel 4.1.12, but proper support for "Intel 8260" with subsystem 0130 (see lspci -vv) was only added in kernel 4.2 (see http://askubuntu.com/questions/693109/intel-wireless-8260-unclaimed-network for the one-liner change to the kernel sources, but I didn't feel like recompiling the kernel...). So I switched to OpenSuSE Tumbleweed which has kernel 4.5. Magic, the wifi works now.

* Remaining issue: when asking for an encrypted LVM group, the installer didn't create an unencrypted /boot partition; it still boots fine but I have to enter the crypto password twice (once for GRUB itself, and once for booting linux). I wonder why grub can't just pass along the now-unencrypted device or something, dunno how that works exactly. Makes me wonder if there isn't a way to give grub everything it needs in the small EFI FAT partition...

Now moving on to compiling my own Qt5, KF5, Plasma5 :-)