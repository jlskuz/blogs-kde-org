---
title:   "Kubuntu Jaunty (xorg 1.6) doesn't like ATI Radeon cards"
date:    2009-04-25
authors:
  - dfaure
slug:    kubuntu-jaunty-xorg-16-doesnt-ati-radeon-cards
---
Hearing that kubuntu jaunty was out, I upgraded two machines today.
My wife's machine, so she can finally use KDE 4.2, and my own desktop machine. On her machine, NVidia card, no problem at all.
On my machine, ATI Radeon X1300, after the upgrade, X would always just show some red dots on the top of the screen, and then the machine would hang (no keyboard, no ssh, nothing except reboot).
I tried every possible driver in xorg.conf, no difference. So this isn't a driver problem, but an Xorg problem.
Other people on #kubuntu (e.g. "chx") reported the same issue.

So... I recompiled the xorg packages from intrepid (i.e. X.org 1.5.2) so that they can be used on jaunty. Long and painful, but it solved the problem. Proof that the problem is really X.org 1.6.0.

In case anyone is interested:  http://www.davidfaure.fr/2009/xorg_debs.tar.bz2

Quick instructions: download, unpack, then
<code>sudo dpkg -r xserver-xorg-video-all xserver-xorg-input-all</code>
<code>sudo dpkg -i *.deb</code>.

If you see apt-get -f install removing a bunch of video drivers, that's fine; I didn't recompile them all, only a few.