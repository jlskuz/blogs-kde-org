---
title:   "Linux (and Windows) on a new HP Pavilion laptop"
date:    2011-06-11
authors:
  - dfaure
slug:    linux-and-windows-new-hp-pavilion-laptop
---
I got a HP Pavilion g6 for my birthday -- this must be the first time I have a personal laptop, all my previous ones having been bought by KDAB (or by IBM as KDE sponsoring, for the very first one).

Main purpose: Windows music software. But also reading KDE email of course. So I installed linux (openSUSE, to try it out for the first time).

First finding: creating partitions in Windows 7 must be done with care: if you have 4 physical partitions, trying to create a 5th partition will NOT switch one of them to logical as it should, instead it converts the whole partition table to 'dynamic', which is apparently not supported by grub nor linux. Thank you Microsoft for not using standard partition tables, and for not providing the 'convert back to basic partition table' feature. Fortunately, others solved that problem: downloading and using 'EASEUS Partition Master' enabled me to undo this and create logical partitions.

Second surprise: copying 13000 files (4 GB) from a DVD to the hard-disk is extremely slow in Windows 7: 10 hours! Well, estimated. I lost patience after 2 hours and rebooted. Doing the same operation on Linux: 7 minutes. Amazing.

KDE 4 is nicely integrated in OpenSUSE, and as a user I like the large coverage of functionality offered by YaST2. Setting up KDE from scratch reminded me that a 'disable all sounds' button is really missing in the 'configure notifications' dialog box, doing it by hand for every system event is rather tedious :-)
We also have some work to do on making network printer detection as automated as Windows does.
