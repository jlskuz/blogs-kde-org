---
title:   "Ark Junior Jobs"
date:    2010-01-02
authors:
  - rakuco
slug:    ark-junior-jobs
---
So let's talk about what's going on in the Ark land.

Some time ago, Lydia <a href="http://blog.lydiapintscher.de/2009/11/23/lets-start-small-junior">blogged</a> about raising the number of bug reports marked as junior jobs on Bugzilla. I've done my part, and currently there are <a href="https://bugs.kde.org/buglist.cgi?keywords=junior-jobs&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&cmdtype=doit&product=ark">12 Ark bug reports</a> listed as <a href="https://bugs.kde.org/buglist.cgi?keywords=junior-jobs&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&cmdtype=doit">junior jobs</a>.

If you have some (little) experience with C++ and Qt, fixing a junior job is a very good way to become part of the KDE community: junior jobs are usually easy to fix, you delve into the art of reading and understanding other people's code and, of course, you make KDE better with your contributions! What's more, you get the feeling that you're also part of our community.

Despite being about 12 years old, Ark, KDE's file archiver, does not have a huge code base, and it's fairly easy to understand its source. Most of the bugs marked as junior jobs involve writing support for more file formats (such as RPM), improving the error messages displayed to the user or implementing some easy wishlist items. But that's not all: as I write this blog post, there are <a href="https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&cmdtype=doit&product=ark">67 open Ark bug reports</a>: only one of them is a more serious crash, and I'm sure there are many other report there which are not marked as junior jobs but are fairly easy to fix or implement.

Take a look at our bug reports, choose one or more you'd like to fix. You can then attach the patches to the reports themselves, mail them to the kde-utils-devel mailing list or send them directly to me. And remember that if you ever get in trouble, there's plenty of places you can look for help: <a href="http://techbase.kde.org">TechBase</a>, the <a href="http://api.kde.org">API reference</a>, <a href="http://lxr.kde.org">LXR</a>, the kde-devel mailing list or the #kde-devel IRC channel on Freenode.

Happy hacking! :)
<!--break-->
