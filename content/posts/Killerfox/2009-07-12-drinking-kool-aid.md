---
title:   "Drinking the 'K'ool-aid"
date:    2009-07-12
authors:
  - Killerfox
slug:    drinking-kool-aid
---
This past week has been quite interesting to say the least. Being at GCDS has been very interesting and educational. I got to know quite a few KDE(and some OSS) people, to chat and work with. It was pretty hard to start taking to people (most people are really shy in person), but after you get to know some people you know everything is for real(and they like to party too!).

I am by now used to being around a lot of geeks (pretty much everyone at school is a geek). However being around so many people that share the same ideas about freedom and OSS feels quite... different. At first I felt like an outsider looking at a 'k'ult. But by the end of it I felt that I was truly a part of it.

So cheers to everyone! active members, and lurkers alike!
