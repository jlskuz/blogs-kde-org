---
title:   "PyKDE5 Status"
date:    2014-08-10
authors:
  - simon edwards
slug:    pykde5-status
---
I've been steadily chipping away at creating Python bindings for the many libraries which make up KF5. The current list of KF5 libraries which have bindings is:

<ul>
<li>karchive</li>
<li>kcoreaddons</li>
<li>kguiaddons</li>
<li>kitemmodels</li>
<li>kitemviews</li>
<li>kplotting</li>
<li>kwidgetsaddons</li>
<li>solid</li>
<li>sonnet</li>
</ul>

This is most of tier 1. The biggest omission at the moment is kconfig. Some small libraries like kcodecs and kwindowsystem don't have bindings because the functionality is either not applicable or already present in Python's standard library. As I mentioned at the start, I'm 'chipping' away at the big pile of KF5 libraries. There are still many left. If you see a library in KF5 which you really want bindings for before the rest of KF5, then you can email me and I'll try to give it high priority.

Also note that only Python 3 is supported. It doesn't make sense to support legacy Python versions like 2, especially when developers need to do a port anyway from PyKDE4 to PyKDE5.

Another important reason is that it costs more time and effort to support more configurations. It is no secret to anyone who has followed PyKDE4 development and support just a little bit, will have noticed that my time for KDE is very limited. The situation isn't likely to improve, in fact in a couple months it should get worse if all goes to plan. :-) I see that some people have stepped in to fill the void and fix some of the build and installation problems they have encountered (hi Luca!). This is great and I encourage people to get involved where possible. The hardest part is getting a working dev environment set up, deep C++ knowledge isn't really needed (I certainly don't have deep C++ knowledge!).

The KDE project page for PyKDE5 and links to the git repository can be found here: <a href="https://projects.kde.org/projects/kde/kdebindings/python/pykde5">https://projects.kde.org/projects/kde/kdebindings/python/pykde5</a>

There is currently no separate release or tag, it is all on trunk. It is best to build and install your own version of Python 3 somewhere and then build and install SIP and PyQt5 with using that Python 3 installation.
<!--break-->
