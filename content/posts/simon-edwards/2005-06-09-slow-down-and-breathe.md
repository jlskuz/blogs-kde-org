---
title:   "Slow down, and breathe"
date:    2005-06-09
authors:
  - simon edwards
slug:    slow-down-and-breathe
---
I figured that now was as good a time as any to jump on the blogwagon. My timing couldn't be worse though, everything is happening at the same time:
<!--break-->
<ul>
<li>New house! Got the keys this week. It's new as in "just been built" and hence is certainly not ready to move into. The whole inside needs to be finished off over the course of the next month (read: painting, light fittings etc and most importantly: deciding network topology and pulling Cat5 through walls :-) ). oh, something also needs to be done with the matching sandpits at the front and back of the house.</li>
<li>Bought an Athlon64 3000+ CPU, motherboard and 512Mb RAM for &euro;229, new of course. An impulse buy really. I was kind of planning to buy some hardware after house was ready but just couldn't help myself when I saw this advertised. Mind you I'm now running on an underclocked Athlon 1600 (~1300 stooopid motherboard) which I got off e-bay. Don't really know when I'm going to get a chance to install it in place of the P2.
<li>Pushed out version 0.3.0 of <a href="http://www.simonzone.com/software/pykdeextensions/">PyKDE Extensions</a>. It is software for supporting the creation of KDE programs using Python. Adds things like build and installation system, i18n support, application templates, better Qt-Designer integration and now support for writing KControl modules. (Just added in 0.2.0).</li>
<li>Trying to work on <a href="http://www.simonzone.com/software/guidance/">Guidance</a> when I can, or at least trying to make sure that Sebas can work on it, since he's got the time right now.</li>
</ul>
The good news is that in July I should have plenty of time for things.