---
title:   "Python ready to go in KDE 4.1"
date:    2008-07-10
authors:
  - simon edwards
slug:    python-ready-go-kde-41
---
Thank $DEITY for feature freezes. It's only after the bulk of the KDE libraries are frozen that bindings people can come into action and franticly update everything before the RCs and the final release of a shiny new version of KDE. It's not much time and it only takes a last minute update to one of the C++ headers in the KDE break the bindings build. (That is the risk you run when you use almost <b>every</b> part of an API). But I can report that Python support is ready for 4.1. yippie! \o/

In the last month I've updated PyKDE4 for KDE 4.1. Incorporating all of the changes to the KDE libraries into the Python bindings and generally making sure that things are working. Jim Bublitz's tutorials, documentation and example code is also in the kdebindings module. In the process I've also created Python bindings for the DNSSD module, knewstuff, soprano, phonon (KDE's version), nepomuk and akonadi. There aren't many APIs in kdelibs and kdepimlibs which aren't covered with Python binding support. Incidentally, if you're a Python developer and you see an API somewhere in KDE which isn't covered but you would like to use, then get in touch and I'll see what I can do.

My next targets in Python bindings support are kcmodule / systemsettings modules, and other plugin mechanisms. I've got almost-ready-for-beta ports of my older utilities Guidedog (network connection sharing) and Guarddog (firewall). Ported to KDE 4 and Python, which should make they much easier for other people to hack on, not to mention debug. (I'll take an exception and a real stacktrace over a segfault any day.) And hopefully sooner instead of later I'll be able to make a start on a new version of Guidance administration tools for KDE 4.

<!--break-->
