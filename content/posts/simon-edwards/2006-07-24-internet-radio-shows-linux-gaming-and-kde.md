---
title:   "(Internet) Radio shows: Linux, gaming and KDE"
date:    2006-07-24
authors:
  - simon edwards
slug:    internet-radio-shows-linux-gaming-and-kde
---
I'm a glutton when it comes to radio shows. I just can't get enough. And thankfully there is a lot of high quality and entertaining radio being produced out there in internetland. So in this post I want to talk about and give some links to some of my favourites and some of the more interesting episodes.

First up is Game Radio ( http://www.hwhq.com/ ). C4 and Kobar put out a well produced show every couple of weeks covering gaming, movies and anything else they feel like discussing. The show is mostly about Windows based games although they do cover the consoles and hand-helds. Episode #124 is definitely worth checking out for us Linux/KDE types. Around 23 minutes into #124, C4 gives an account of his experience trying out Vista beta 2, which is worth listening too. But the most interesting secton starts at about 51 minutes where they discuss their experiments running games on Linux. It is always interesting (and educational!) to hear what the impression non-hardcore Linux people have from Linux.

Game Radio also covers the Nintendo DS and DS Lite. Speaking of which, I grabbed myself one a couple weeks ago. It is just damn fun. ;-) This leads us on to Dual Screen Radio ( http://www.dualscreenradio.com/ ) where Shane and Chris talk about all things DS. This show is also well produced and edited. 

The Linux Link Tech Show ( http://www.tllts.org/ ), produced by member of the  Lehigh Valley Linux User Group, covers Linux and FOSS. The idle banter can get a bit on the long side IHMO (more editing here would help), but this show is really all about the very good interviews. Each week they interview someone from the FOSS community. Some of the more interesting recent interviews are episode 133 with one of the Linspire people, and episode 127 with Rick Timmis from Adaptive Linux Solutions in the UK.

Finally, there is also LUG Radio ( http://www.lugradio.org/ ), coming out of the UK. LUG Radio also has a lot of good interviews and discussions/heated debates about issues in the FOSS community. I found the interview with Timothy Miller from the Open Graphics card project in season 3 episode 11, to be very interesting. LUG Radio recently covered the big GNOME developer conference Guadec, and interviewed a lot of interesting people there. In the last episode (#57) of their Guadec coverage they have some interesting comments about the future of GNOME and KDE.

happy listening.
