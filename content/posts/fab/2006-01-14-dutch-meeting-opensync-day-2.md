---
title:   "Dutch Meeting OpenSync: Day 2"
date:    2006-01-14
authors:
  - fab
slug:    dutch-meeting-opensync-day-2
---
<p><A href="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/day2/00080.jpg"><IMG src="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/day2/scaled/00080.jpg"  width="267" height="200" align="right" border="0"></A>Day 2 of the <p href="http://www.opensync.org/wiki/dmo/">Dutch Meeting OpenSync</A> started today. You will find some pictures as links in the text. </p>

<p>These hackers seem to have plenty of energy. Also on pictures it seems that eating is the main theme at this meeting. As long as we don't have something like 'Action Coding' you will have to do with <A href="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/day2/scaled/00082.jpg">these kind</A> <A href="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/day2/scaled/00011.jpg">of</A> pictures.</p>

<p>In my previous blog I forgot to mention one person who has been very supportive of this initiative. His name is <A href="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/day2/scaled/00007.jpg">Jeroen Visser</A>, an amiable guy who does open source consulting at IBM Netherlands. It seems that Frank moved <A href="http://fzwart.l3e.nl/">his blog</A> to some space of his own and meanwhile he posted another informative entry (it could be that some DNS servers aren't yet aware of the move of the blog).</p>

<p>The Dutch press has noticed the event as well and  Webwereld has a <A href="http://www.webwereld.nl/articles/39310">news snippet</A> on the meeting (<A href="http://www.opensync.org/wiki/dmo/webwereld/">English translation</A>). It is always nice when mainstream press writes about innovative projects like <A href="http://www.opensync.org/">OpenSync</A>. </p>


<p>Bart Knubben, a colleague of mine who works as an advisor on open source software for the Dutch government also paid us a visit. It was nice as we had some discussion about OpenSync, the opportunities of cross platform open source software and the current state of open source groupware.</p>

 <p>Today we <A href="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/day2/scaled/00077.jpg">helped</A> <A href="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/day2/scaled/00083.jpg">ourselves</A> with an Senseo coffee machine as we are not able to find a working coffee machine in this IBM building. BTW <A href="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/day2/scaled/00012.jpg">that's</A> a cool shirt Tobias is wearing.</p>


<p>As a final remark ... I hope that the people of KDE-NL will be blogging as well about the <A href="http://www.kde.nl/agenda/2006-01-14-nieuwjaarsbijeenkomst/">KDE New Years Meeting</A> and what is happening there. I heard a lot of new people signed up for this meeting. Great job guys.</p>

<!--break-->

