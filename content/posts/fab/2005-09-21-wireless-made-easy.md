---
title:   "Wireless made easy?"
date:    2005-09-21
authors:
  - fab
slug:    wireless-made-easy
---
Today I was asked on IRC (#kde-nl) if we have a decent wifi manager in KDE. <a href="http://kwifimanager.sourceforge.net/">KWifiManager</a> does not work for him (hi rikva) as it has no support for WPA. To be honest I think we do not have a decent wifi manager in KDE currently. On <a href="http://www.kde-apps.org">KDE-apps.org</a> you can find quite some tools. Some of them are really aging like <a href="http://www.linux.com/article.pl?sid=05/09/13/1914255">KWirelessMonitor</a> and <a href="http://www.kde-apps.org/content/show.php?content=9827">KWlanInfo</a>. When digging even further on KDE-apps.org you can find <a href="http://www.kde-apps.org/content/show.php?content=13858">KiFi</a>. 

Luckily we also have the wonderful <a href="http://www.kde-apps.org/content/show.php?content=21832">Wireless Assistant</a> and the informative <a href="http://www.kde-apps.org/content/show.php?content=28637">PyWireless</a>. And <a href="http://www.kde-apps.org/content/show.php?content=12956">KNemo</a>? Does it know how to manage wireless networks? Nope. However there exists a nifty <a href="http://www.kde-apps.org/content/show.php?content=17627">random WEP/WPA Key Generator</a>. 

Then I read <a href="http://www.linux.com/article.pl?sid=05/09/13/1914255">this article</a> about <a href="http://people.redhat.com/dcbw/NetworkManager/">Network Manager</a>. 

<i>You can click on your network of choice if you need to select a specific one. Otherwise the open network with the best signal strength is automatically selected. If you manually select a network, your network configuration will be statically configured for that network and you may have to go into your network settings program to erase it when you're done.</i>

Woow ... that sounds great. Do we have such a thing in KDE? If so can anyone point me to it please?<!--break-->