---
title:   "Dutch Meeting OpenSync: Enjoying garlic and beer!"
date:    2006-01-15
authors:
  - fab
slug:    dutch-meeting-opensync-enjoying-garlic-and-beer
---
<p>So yesterday we (Armin, Tobias, Cornelius, Frank and me) went into Amsterdam to have some dinner. So after some lingering we decided to try our luck in a greek restaurant. We had some nice chats about the KDE community, about <A href="http://blogs.kde.org/node/1734">Akonadi</A> the new Pim Storage Service for KDE4 and new exciting programming languages. Also it seems that <A href="http://blogs.kde.org/node/1465">one of my gripes</A> with the KDE desktop is being addressed in <A href="http://blogs.kde.org/node/1685">SUSE Linux 10.1</A>.</p>

<p align="center"><A href="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/day2/00005.jpg"><IMG src="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/day2/scaled/00005.jpg" border="0"></A></p>

<p align="center"><A href="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/day2/00009.jpg"><IMG src="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/day2/scaled/00009.jpg" border="0"></A></p>

<p>After consuming some beer and too much garlic we left the restaurant for a little walk on <A href="http://www.channels.nl/leidseplein.html">Leidseplein</A> where there is always a lively atmosphere. I hope Tobias will send me some pictures as well.</p>

<p align="center"><A href="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/day2/00010-a.jpg"><IMG src="http://www.xs4all.nl/~leintje/stuff/fotos/dmo/day2/scaled/00010-a.jpg" border="0"></A></p>



<p>Something else now. As <A href="http://www.fosdem.org/2006">FOSDEM2006</A> is approaching Cornelius told me he will be present at FOSDEM in Brusselsl (man this guy is travelling a lot) for the OpenSUSE project. If you plan to go to FOSDEM (I think you should), then be sure to check out the <A href="http://wiki.kde.org/tiki-index.php?page=fosdem2006">Wiki</A>. <A href="http://vizzzion.org/">Sebas</A> and <A href="http://people.fruitsalad.org/adridg/bobulate/">Adriaan</A>, our friends from <A href="http://www.kde.nl/">KDE-NL</A>,  are organizing the KDE presence this year.</p>


<p>Great <A href="http://people.fruitsalad.org/adridg/bobulate/index.php?/archives/132-We-are-out-of-beer.html">to</A> <A href="http://braincore.blogspot.com/2006/01/kde-new-year.html">hear</A> that the <A href="http://www.kde.nl/agenda/2006-01-14-nieuwjaarsbijeenkomst/">KDE New Years Meeting</A> was again very succesful. In my opinion the KDE Dutchies should not stop with this tradition. It gives a good starting point for discussing the activities and plans for this year. I hope to see some pictures soon.</p>

<p>Well I should be leaving in a bit for Day 3 of the <A href="http://www.opensync.org/wiki/dmo/">Dutch OpenSync Meeting</A> at Amsterdam. No doubt I will blog a bit about it this afternoon.</p>

<!--break-->



