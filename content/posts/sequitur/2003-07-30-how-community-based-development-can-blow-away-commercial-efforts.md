---
title:   "How community based development can blow away commercial efforts"
date:    2003-07-30
authors:
  - sequitur
slug:    how-community-based-development-can-blow-away-commercial-efforts
---
Hi everyone,
<br>It's fun to have this platform for developers and so I thought I'd put forward
an idea I've been trying to promote for some time that seems to not get much
traction in the community. Often I get a request that reads like <i>can you add
auto completion support for [your favorite scripting language here]?</i> I
dilligently reply <i>"I don't actually use that language and we have a policy
that these must be done by people who use them and can test them. However Quanta
is set up so that users can add a language using XML."</i> Now, please allow me
some small degree of indulgence here... but isn't it weird that given these facts
<ul>
  <li>The program was free</li>
  <li>XML should be part of a web developer's toolbox and no big deal</li>
  <li>The idea of <i>"community"</i> means you'd think people would be both
  proud and anxious to do their part</li>
</ul>
Yet it is maybe one in four or less that will finally step up and put a few
hours into what is essentially making a tool their own. I'm not really surprised,
but I can't say I'm still not dissapointed.
<br><br>
Here's what really gets me. I talk about having the best tool anywhere for web
work and I think a lot of people are excited about that. We have a handful of
C++ developers actively working on Quanta and the vast majority of the code is
written by Andras. More is gradually happening in community involvement and I'm
working on my C++ skills for various reasons, not the least of which is that I
feel I need to because Andras is so swamped. Then there are some people that
feel that even being at the bottom of the Bugzilla top 100 and all we do, that
we still have not offered enough support because we don't have a feature supported
that is used by less than 5% of developers. I'm not complaining, but I am making
a point here.
<br><br>
<b>How do we beat larger and more established developer teams?</b> Granted KDE
gives us advantages, but that only goes so far. <b>What if we could turn this
around and exceed the development efforts of any commercial product?!</b> In fact
this has been our plan, to involve the community in making templates, adding
language support, doing scripts, creating Kommander dialogs... <b>Quanta is
so extensible that a major upgrade could be done with no C++ at all!</b> This
would be a dangerous strategy for commercial ventures. <b><i>What if we
involved the community on that level?</i></b>
<br><br>
Clearly this is the key. I figure maybe 1 in 10 to 1 in 20 people who use Quanta
would actually download and compile the source. We ran 35,000 downloads on a
major version in the 2x incarnations on Sourceforge. That would mean 350,000 to
700,000 users almost two years ago before we became part of KDE. Here's a question
for every user... <b>"Do you see yourself more as a member of a community or a
consumer, based on your mentality and actions?"</b> It's an interesting question.
I believe the biggest challenge of community developed software is that people
are still operating on consumer conditioning in a community environment. If you
observe many critical comments I'm sure you'll agree. The really good news is
this... <b><i>If we have over a million users and only 1 in 10,000 "get it" we
can easily produce the most amazing tool ever...</i></b> but right now only a handful
of people get it. I regret to say that <b><i>not 1 in 100,000 are on board
with this yet</i></b>, but I think we're doing a little better than 1 in 1,000,000.
<br><br>
I remember thinking it was special to be one in a million, but I think we must
be missing some special people out there who just haven't heard yet. How about you?
Don't know C++? Neither did I, but you don't have to. Don't have time? Who does?
We all have 24 hours in a day and we all have to make time. Last year I got called
for jury duty. Did I have time? No. It's just part of being a citizen in a
community. <i>What will you do for your software community this year?</i>