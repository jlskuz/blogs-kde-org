---
title:   "KDEPIM Help Wanted"
date:    2009-05-12
authors:
  - awinterz
slug:    kdepim-help-wanted
---
While most core kdepim folks are knee-deep (neck-deep?) working on Akonadi and Akonadi migration issues, the bug reports and feature requests continue rolling-in at brisk pace for Kontact, KMail, KOrganizer, KAddressbook, Akregator, KTimeTracker, KJots and friends.

We PIMsters are a small group -- and we need fresh talent+brains+muscle.

So consider this my aperiodic plea for help.  

If you have skills and time and love C++/Qt/KDE development, we'd be happy to have you join us.  Feel free to drop by #kontact on irc.freenode.org or drop an email to kde-pim@kde.org.

PS. Please don't forget that we gladly accept patches in the KDEPIM group on reviewboard.kde.org.  See <a href="http://techbase.kde.org/Contribute/Send_Patches">Contribute/Send Patches</a> on Techbase for more info.

 



