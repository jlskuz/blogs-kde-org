---
title:   "KDE 4.4.5 is scheduled"
date:    2010-06-01
authors:
  - awinterz
slug:    kde-445-scheduled
---
A short note.  

The KDE Release Team has decided to make a KDE SC 4.4.5 release.  So please remember to continue backporting your bug fixes into the 4.4 branch.


<u>The Schedule</u>
<b>June 24th, 2010: Tag KDE 4.4.5
June 29th, 2010: Release KDE 4.4.5</b>


