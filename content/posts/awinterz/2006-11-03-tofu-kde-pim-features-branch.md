---
title:   "TOFU in KDE-PIM Features Branch"
date:    2006-11-03
authors:
  - awinterz
slug:    tofu-kde-pim-features-branch
---
Yesterday Pradeepto added the TOFU (an acronym for something in German) feature to KMail in the KDE-PIM Features Branch.   TOFU provides top-posting -- see the "Insert signatures above quoted text" option in KMail's Composer configuration -- as well as composer window edit options for: Prepend Signature; Append Signature; and Insert Signature at Cursor Position.

As usual, you will find this and other neat new features in $SVN/branches/work/kdepim-3.5.5+

Sorry there are no pre-built packages available for this branch.  You will need to compile and install this code yourself.

Feedback and testers needed.  Please send your comments to kmail-devel@kde.org or directly to me.
