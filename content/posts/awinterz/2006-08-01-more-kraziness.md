---
title:   "More Kraziness"
date:    2006-08-01
authors:
  - awinterz
slug:    more-kraziness
---
We have a new "pass-by-value" check in Krazy written by Andreas.  It is the first plugin written in C++ (and not perl).  And it requires Qt4.  So, we had to do a bit of tweaking of things on the EBN.  Seems to be working well. 
<p>

<a href="http://www.englishbreakfastnetwork.org/krazy/index.php">Check out your favorite code's kraziness</a>.

<p>
On the APIDOX front:  I'm thinking about re-writing the doxygen.sh script in kdelibs/doc/api.  Possibly in perl.
I think this one script is trying to be too many things to too many people.  And I'd really like our APIDOX to look more like the <a href="http://doc.trolltech.com/4.1/qstring.html">Qt4 doc (QString)</a>.  I like how Qt puts the argument description in-line with the method description, and doesn't clutter things up with separate @param and @return stuff.