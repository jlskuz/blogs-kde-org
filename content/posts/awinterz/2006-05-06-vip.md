---
title:   "VIP"
date:    2006-05-06
authors:
  - awinterz
slug:    vip
---
First, if anyone over at planetkde is reading this... I can't seem to get clee's attention.  I need him to change the URL of my blog there.

<p>
Now that I've been mentioned in <a href="http://aseigo.blogspot.com">Aaron's blog</a> I feel like a VIP (Very Important Person/Programmer).  Yes, the APIDOX error reporting is now all beautified.  I wrote some perl that takes the doxygen error logs and converts into our defacto standard html report format.  Hope this helps.

<p>
And while your over at <a href="http://www.englishbreakfastnetwork.org">The EBN</a>, don't forget to check out what the "<a href="http://www.englishbreakfastnetwork.org/krazy/index.php">Krazy Code Checker</a>" thinks about your favorite source code.  I wrote the krazy tool too.. with lots of inspiration from Ben Meyer's <a href="http://www.icefox.net/kde/tests">testscripts</a>.  Contact me if you want to help write some <i>krazy</i> code checking plugins (in perl, python, ruby, ...)
