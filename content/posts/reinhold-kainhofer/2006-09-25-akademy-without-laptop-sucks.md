---
title:   "aKademy without a laptop sucks..."
date:    2006-09-25
authors:
  - reinhold kainhofer
slug:    akademy-without-laptop-sucks
---
About a week ago, my laptop broke -- completely broke in the sense that not only does the machine not work any longer, it even trashed my whole harddisk. Now I'm at aKademy without a laptop and still one whole afternoon of the KDE e.V. general assembly left to sit through... Oh, how much I envy all those developers sitting in there with their laptops, hacking on KDE stuff!

Fortunately I brought my book about Austrian criminal law with me to entertain me through lengthy boring discussions.
<!--break-->