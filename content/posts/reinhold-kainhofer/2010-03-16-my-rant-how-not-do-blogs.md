---
title:   "My rant: How not to do blogs..."
date:    2010-03-16
authors:
  - reinhold kainhofer
slug:    my-rant-how-not-do-blogs
---
Apparently, with my original posting here, I stepped on several people's toes. I'm sorry for that, and for this reason, I've simply removed this content (and also because - as some of you pointed out - some of the things I mentioned were not Plasma's fault, but workarounds or bugs in other areas, although to me as a user they appeared on Plasma).

On the other hand, my frustration with KDE is growing... I find my self more and more replacing KDE applications with GNOME applications, simply because the KDE application does not fulfill my needs or is buggy, while the GNOME application provides the fundamentals that I need.
When KDE 4.0 came out, I accepted the excuse that some things simply needed the time to be ironed out. However, now at KDE 4.4, my biggest problems are still there: <ul>
<li>Printing in Konqueror is just not working so I have to use Firefox (and yes, I posted messages to bug reports), <li>KDE's printer dialog does not seem to remember any settings, and it also messes up CUPS in other applications <li>VPN does not work in knetworkmanager so I have to use nm-applet (and knm does not display the list of available networks), <li>Booklet printing is missing altogether so I have to use Acroread to print my students' seminar works and other papers, <li>printing landscape slides 2-up doesn't work in Okular so I have to use Acroread to print handouts for the students, <li>KMail regularly decides to eat my inbox or my calendar, <li>My bluetooth headset is working only with gnome-bluetooth and gnome-volume-control-applet<li> etc.</ul> (And just for the record, for most of these problems I either filed bug reports or talked to the maintainers on IRC, who told me mostly, that it's not their application to blame, but some underlying framework and I could do nothing about it...)