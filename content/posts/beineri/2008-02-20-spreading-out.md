---
title:   "Spreading Out"
date:    2008-02-20
authors:
  - beineri
slug:    spreading-out
---
It's this time of the year when all the events seem to happen at almost the same time: the upcoming week-end Dirk and Will will be at <a href="http://fosdem.org/2008/">FOSDEM</a> and give a talk "<a href="http://fosdem.org/2008/schedule/events/opensuse_kde4">KDE 4 on openSUSE 11</a>" on Sunday morning in the <a href="http://fosdem.org/2008/schedule/devroom/opensuse">openSUSE Developer Room</a>. The week-end after I will be at the openSUSE booth of <a href="http://chemnitzer.linux-tage.de/2008/info/">Chemnitzer Linux-Tage</a>. The week after is <a href="http://www.cebit.de/">CebIT</a>, visit the openSUSE counters at the Novell booth and check whether Martin presents KDE or something else. I plan to do a control visit on some secret day too. ;-) Also in March, <a href="http://www.novell.com/brainshare/">Novell BrainShare</a> happens where Zonker and Adrian will offer the suits an introduction to KDE4 (<a href="https://www.novellbrainshare.com/slc2008/scheduler/catalog/catalog.jsp">session IO140</a>). :-)<!--break-->
