---
title:   "\"There must be a final stroke!\""
date:    2004-07-17
authors:
  - beineri
slug:    there-must-be-final-stroke
---
I want to tell you a true story which happened about half a year ago but is still in my mind. As you perhaps know I maintain <a href="http://developer.kde.org/build/konstruct/">Konstruct</a>. Sometimes it happens that an archive disappears from its specified download location, often the cause is a new version release and the old version being deleted. This breaks Konstruct's installation process. Usually I notice this very quick because I read application announcement sites like <a href="http://kde-apps.org/">kde-apps.org</a>.<br>
There is an author who unregularly but then within days releases several new versions of an application which is included within Konstruct. He immediately deletes older versions and doesn't announce a new release. So I mailed him and kindly asked to keep older versions a bit or at least announce new versions on eg freshmeat.net so I have a chance to fix it quickly.<br>
He replied with a lengthy mail listing what users asked him during the last 3 years (documentation also in other formats, porting to KDE 3, making it compile with gcc 3 etc.) - and he did all this. A release would keep him busy 5 hours now, he says, to test it against KDE2, KDE3, gcc2, gcc3 and different cases of optional dependencies. He concluded that he doesn't see this effort reflected in the users' reactions and declined either of my both requests.<br>
Isn't it strange that he sacrifices 5 hours for a release (not counting the days to enhance the application before!) but then refuses to announce it with some minutes efforts so others can benefit from the new features or bugfixes because "there must be a final stroke for my efforts"? And not deleting the old releases would not even have required him to spend more time...<br>
My solution for this problem is simple: Download one copy from his website and upload it to another permanent place for Konstruct users. I'm curious if this has a measuarable impact (by Konstruct users installing meta/everything) on his download statistics but I don't dare to ask as my last mail to him only triggered a response "You are at my PERSONA NON GRATA list." He for sure has a problem with criticism.
<!--break-->