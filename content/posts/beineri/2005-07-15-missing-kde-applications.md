---
title:   "Missing KDE Applications"
date:    2005-07-15
authors:
  - beineri
slug:    missing-kde-applications
---
I'm happy about the <a href="http://www.kde-apps.org/">whole bunch of applications</a> which are nowadays available for people's daily tasks: Whether you want to <a href="http://amarok.kde.org/">play music</a>, <a href="http://www.k3b.org/">burn CDs</a>, <a href="http://konversation.kde.org/">chat</a>, <a href="http://kopete.kde.org/">send instant messages</a>, <a href="http://akregator.sourceforge.net/">read RSS feeds</a>, <a href="http://www.digikam.org/">manage your photos</a>, <a hreF="http://kmymoney2.sourceforge.net/">keep your money together</a>, <a href="http://freehackers.org/~tnagy/kdissert/">draw a mindmap</a> or <a href="http://ktorrent.pwsp.net/">download a Torrent</a> - there is a KDE-based and nicely integrated solution available.

But for some small tasks an application is still missing and I am not aware of work on them:
<ul>
<li>a <a href="http://en.wikipedia.org/wiki/Podcast">Podcast</a> client with integrated Bittorrent client and directory browser like <a href="http://ipodder.sourceforge.net/">iPodder</a> (but without having xmms hardcoded!) would be nice. With support to enclosure all media types (animation, video!). Not sure if it makes sense to add all this to Akregator.</li>
<li>a stream directory browser like <a href="http://www.nongnu.org/streamtuner/">streamtuner</a> with support for SHOUTcast, Live365, Xiph.org and so on. This could be <a href="http://bugs.kde.org/show_bug.cgi?id=97515">integrated into amaroK</a> but then some people may prefer a stand-alone version for use with their favorite player.</li>
<li>an offline blog client like <a href="http://www.dropline.net/drivel/">Drivel</a> with support for popular interfaces like LiveJournal, Blogger, MovableType, Advogato, and Atom journals. For me the web interface to my blog is sufficient but your flavor may differ.</li>
</ul>

If you miss some other application feel free to comment and maybe some bored developer without an idea what project to start next reads it and will start to work on it. :-)
<!--break-->
