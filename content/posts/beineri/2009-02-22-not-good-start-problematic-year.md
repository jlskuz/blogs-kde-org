---
title:   "Not a Good Start Into a Problematic Year"
date:    2009-02-22
authors:
  - beineri
slug:    not-good-start-problematic-year
---
Like some other [open]SUSE developers I was casted and am now forced to look for a new day job. It could have happened in better economic times for sure. :-(

Pointers to new interesting job positions are gladly accepted. Bonus points the more they have to do with Open Source, Linux, Qt and KDE.