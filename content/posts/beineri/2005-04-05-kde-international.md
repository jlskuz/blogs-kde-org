---
title:   "KDE International"
date:    2005-04-05
authors:
  - beineri
slug:    kde-international
---
KDE is not only <a href="http://worldwide.kde.org/">developed worldwide</a> and <a href="http://i18n.kde.org/teams/">being translated into over 75 languages</a> making it the most and broadest translated Free Software graphical desktop environment. <a href="http://www.kde.org/international/">kde.org/international</a> shows that the KDE community also consists of many, mostly non-English speaking, local groups worldwide who promote KDE in their country or region (like <a href="http://www.kde.cl">KDE Chile</a> at the <a href="http://dot.kde.org/1112582895/">Latin America Free Software Install Festival</a> recently) and help KDE users in their native language.

To get an impression of their activities (or to make at least an educated guess about them :-)) visit for example <a href="http://www.kdelatino.org/">KDE Latino</a>, <a href="http://kde.linuxdby.com/">KDE China</a>, <a href="http://www.kde.ru/">KDE.RU</a>, the <a href="http://www.kde.gr.jp/">Japanese KDE User Group</a>, <a href="http://www.kde.org.tr/">KDE TÜRK