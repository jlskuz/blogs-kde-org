---
title:   "openSUSE News Milestone"
date:    2007-12-08
authors:
  - beineri
slug:    opensuse-news-milestone
---
I just noticed the internal statistics of <a href="http://news.opensuse.org/">openSUSE News</a> saying "There are currently 200 posts and 1,800 comments, contained within 17 categories" (posts include calendar entries). Thanks to everyone contributing to make its start such a success. :-) And I can still not understand why some other distro portals don't dare to allow user comments.