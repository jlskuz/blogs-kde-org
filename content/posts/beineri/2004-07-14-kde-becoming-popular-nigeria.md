---
title:   "KDE Becoming Popular in Nigeria?"
date:    2004-07-14
authors:
  - beineri
slug:    kde-becoming-popular-nigeria
---
The KDE <a href="http://events.kde.org/info/kastle/">conference of last year</a> took place in Nové Hrady, Czech Republic and there were attendees from almost everywhere, apart from Africa perhaps. At <a href="http://conference2004.kde.org/">this year's conference</a> (expect the conferences schedules to be announced real soon now!) in Ludwigsburg, Germany this may be different: The registration shows a great amount of registrants from Nigeria together with requests for invitation letters. It's great to see KDE being such successful there und being used even in ministries. Or is it just because of Germany?
<!--break-->