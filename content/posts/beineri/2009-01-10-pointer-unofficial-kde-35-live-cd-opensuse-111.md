---
title:   "Pointer: Unofficial KDE 3.5 Live CD for openSUSE 11.1"
date:    2009-01-10
authors:
  - beineri
slug:    pointer-unofficial-kde-35-live-cd-opensuse-111
---
openSUSE 11.1 Live CDs and USB images featuring KDE 3.5 are now available for download. <a href="http://news.opensuse.org/2009/01/09/unofficial-kde-35-live-cd-for-opensuse-111/">openSUSE News has the full story</a>.