---
title:   "Malaga Day 1"
date:    2005-08-26
authors:
  - beineri
slug:    malaga-day-1
---
For most people day one will be the first day of the user conference, for me and the e.V. members it is Friday. As previously written I didn't sleep well last night, not much more than 3 hours. Went to the breakfast area where the interesting game started to order your drink (water, orange juice or coffee) and what you want to have on your bread (tomato or jelly) because the waitress doesn't speak any word English and most of us no word Spanish. If you want to eat more you have to pay (and succeed to order it).

Drove with a larger crowd to the nice public beach of Malaga for sun bathing and swimming in the Mediterranean. After some hours clouds appeared and sun disappeared. But we wanted to drive back to attend the KDE e.V. meeting in the afternoon anyway.

But before several of us visited the near-by super supermarket. In my stupidity I bought a "6 bottles a 2 liter" offer which I then had to carry to the student residence with sun at max peak. Ate what I bought, took a shower to then make the walk to the university. It took 30 min and it was very hot and the sun burned like hell. Arrived some minutes after scheduled start of the KDE e.V. meeting at 4pm.

The meeting was two part with nice lunch in the cafeteria in-between and continued until 11pm. No Wifi available in the lecture hall and only few power plugs. Finally had time to visit the computer lab afterwards to post my blog of last night. Bad news is that the computer labs are closing at midnight. No time left now, I will have to upload first pictures tomorrow.
<!--break-->
