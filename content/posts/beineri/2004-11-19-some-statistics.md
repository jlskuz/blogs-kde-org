---
title:   "Some Statistics"
date:    2004-11-19
authors:
  - beineri
slug:    some-statistics
---
On one side:

<ul>
<li>In October 1050 distinct people have filed bug or wish reports on bugs.kde.org.</li>
<li>In the six months from May to October 2004 there were 4635 distinct reporters.</li>
<li>During the last 180 days (from today) 8340 bug and 3202 wish reports were filed.</li>
</ul>

On the other side:

<ul>
<li>In November until today 279 distinct cvs accounts were used.</li>
<li>90 contributors used their account last in October, 55 in September, 36 in August, 29 in July, 18 in June
and 22 in May.<br>
That are marginal over 500 contributors during the last six months.</li>
<li>During the last 180 days (from today) 7327 bug and 2675 wishes reports were closed.</li>
</ul>

Now you can calculate the average bug and wish load of a KDE contributor. :-)
<!--break-->