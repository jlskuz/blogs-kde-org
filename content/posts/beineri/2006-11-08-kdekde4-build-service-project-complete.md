---
title:   "KDE:KDE4 Build Service Project Complete"
date:    2006-11-08
authors:
  - beineri
slug:    kdekde4-build-service-project-complete
---
During the last days I updated the existing packages, added the until now missing KDE modules to the <a href="http://software.opensuse.org/download/repositories/KDE:/KDE4/">KDE:KDE4 project</a> of the <a href="http://en.opensuse.org/Build_Service">openSUSE build service</a> and fiddled a bit until all packages built successfully for <a href="http://software.opensuse.org/download/repositories/KDE:/KDE4/SUSE_Linux_10.0/repodata/">SUSE Linux 10.0</a>, <a href="http://software.opensuse.org/download/repositories/KDE:/KDE4/SUSE_Linux_10.1/repodata/">SUSE Linux 10.1</a> and <a href="http://software.opensuse.org/download/repositories/KDE:/KDE4/SUSE_Factory/repodata/">Factory</a>. I will make no promises if they work :-) - they are just snapshots of last Sunday's SVN. I think we will update regularly to newer SVN versions to ease development/porting work or just for the curious who are eager to try if they can see progress on the way to KDE 4.
<!--break-->