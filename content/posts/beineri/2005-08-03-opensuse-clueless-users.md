---
title:   "OpenSUSE & Clueless Users"
date:    2005-08-03
authors:
  - beineri
slug:    opensuse-clueless-users
---
<img align=right class="showonplanet" src="http://lists.opensuse.org//pics/OpenSUSE-small.png">
Congratulations to <a href="http://www.novell.com/">Novell</a> for deciding, according to <a href="http://news.com.com/2100-7344_3-5816079.html">several</a> <a href="http://www.eweek.com/article2/0,1895,1843097,00.asp">reports</a>, to open the development process for <a href="http://www.novell.com/products/linuxprofessional/">SUSE Linux</a> as "OpenSUSE". Also for planning to spread it as widely as possible while keeping the boxed retail version with manuals/support. Looking forward to next week to play with a public SUSE Linux 10.0 Beta.

Reading the comments about it the "criticising but uninformed" user type caught my eye again: those guys who once tried something or picked up an argument which is out-dated today but keep posting it because they are experts. In the case of KDE we know the "not based on Free Software licensed library", "KDE is slow" etc. people. In case of SUSE people state it would not be Open Source or Free Software (referring to YaST, it's GPL since last year) or not freely available for download (FTP installations exist for years) or as ISO images (also not true for last releases) or no <a href="http://susefaq.sourceforge.net/">single</a> <a href="http://www.suseforums.net/">community</a> <a href="http://www.susewiki.org/">support</a> or <a href="http://packman.links2linux.org/">package</a> <a href="http://www.usr-local-bin.org/rpms/">site</a> would exist. Lord, give them some clue!
<!--break-->