---
title:   "SUSE Linux Enterprise Desktop 10"
date:    2006-03-09
authors:
  - beineri
slug:    suse-linux-enterprise-desktop-10
---
Novell today <a href="http://www.novell.com/news/press/item.jsp?contentid=687fc977d6cd9010VgnVCM10000024f64189____">announced SLED 10</a> at CeBIT. Apparently there is some confusion caused by <a href="http://www.osnews.com/comment.php?news_id=13921">bad coverage</a>: it's the successor of Novell Linux Desktop 9, a rebranded NLD 10 - if you like to say so - which gives you the choice to use either KDE or GNOME. As Nat Friedman <a href="http://golem.de/0603/43914.html">stated to some press</a> you will not lose functionality when you choose to use KDE: every desktop's applications will run on the other desktop, OpenOffice.org will <a href="http://kde.openoffice.org/">integrate into both</a> equally, <a href="http://blogs.kde.org/node/1820">desktop search</a> is available under both and Xgl/Compiz (if supported on your graphic card) is desktop-agnostic too. Also the KDE desktop inherits from SUSE Linux 10.1 nice stuff developed at SUSE like <a href="http://en.opensuse.org/Projects_KPowersave">kpowersave</a> and <a href="http://blog.nouse.net/?p=71">knetworkmanager</a> which other distributions maybe will only adopt in their <a href="https://wiki.kubuntu.org/KubuntuFutureIdeas">next but one release</a>.<!--break-->