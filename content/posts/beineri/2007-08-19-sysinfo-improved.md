---
title:   "sysinfo:/ Improved"
date:    2007-08-19
authors:
  - beineri
slug:    sysinfo-improved
---
Other distributions certainly like to pick up stuff that Novell/SUSE are developing (and have been using for long). Recent examples include <a href="https://launchpad.net/ubuntu/+spec/apparmor">Ubuntu</a> and <a href="http://wiki.mandriva.com/en/Releases/Mandriva/2008.0/Notes#AppArmor">Mandriva</a> starting to integrate AppArmor or <a href="http://oss.oracle.com/projects/yast/">Oracle porting YaST to their Enterprise Linux and RHEL</a>. On a smaller scale, the <a href="http://jerrad.tuxfamily.org/kiosysinfo_eng.html">sysinfo:/ KIO slave has been improved</a> and packaged for several distributions.

Dirk had a look at it and integrated the most interesting changes into the sysinfo:/ of openSUSE 10.3 including swap space, CPU cores, CPU temperature display and those nice bars showing free disk space (even colored depending on the state):

[image:2944 size=preview hspace=100]

Additionally sysinfo:/ has been split from the kdebase3-SUSE package into an own package so that you can exchange/upgrade it with some other version easily.
<!--break-->