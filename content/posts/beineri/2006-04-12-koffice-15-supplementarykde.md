---
title:   "KOffice 1.5 in supplementary/KDE"
date:    2006-04-12
authors:
  - beineri
slug:    koffice-15-supplementarykde
---
Congratulations to everone involved for the <a href="http://www.koffice.org/tours/1.5/">KOffice 1.5</a> <a href="http://www.koffice.org/announcements/announce-1.5.php">release</a> and especially <a href="http://www.koffice.org/krita/">Krita</a> - finally a usable painting and image editing application with professional color support:

<a href="http://koffice.org"><img src="http://koffice.org/pics/koffice1.5_v4-500.jpg" /></a></p>

You can find rpms for it for SUSE Linux 9.2 to 10.0 in the <a href="http://www.novell.com/linux/download/linuks/">supplementary/KDE YaST repository</a>. For <a href="http://en.opensuse.org/Development_Version">SUSE Linux 10.1</a> <a href="http://download.kde.org/stable/koffice-1.5.0/SuSE/">packages jump over to ftp.kde.org</a> as no &quot;supplementary&quot; distribution for 10.1 is set up yet. If you encounter crashes, ftp.kde.org hosts debuginfo rpms for 9.3 and up.
<!--break-->