---
title:   "/pub/suse/ <arch> /supplementary/ KDE/update_for_10.1"
date:    2006-05-15
authors:
  - beineri
slug:    pubsuse-supplementary-kdeupdatefor101
---
<a href="ftp://ftp.suse.com/pub/suse/i386/supplementary/KDE/update_for_10.1">ftp.suse.com</a> and <a href="ftp://ftp-1.gwdg.de/pub/suse/i386/supplementary/KDE">first</a> <a href="http://www.novell.com/products/suselinux/downloads/ftp/int_mirrors.html">mirrors</a> now carry the "<a href="http://www.novell.com/linux/download/linuks/">KDE supplementary</a>" package repository for <a href="http://en.opensuse.org/Product_Highlights">SUSE Linux 10.1</a>.

"supplementary" is one of the unsupported playgrounds of SUSE packagers for providing newer application versions like KDE 3.5.2 with KOffice 1.5.1 for past distribution releases. See <a href="http://en.opensuse.org/Additional_YaST_Package_Repositories">this wiki page</a> for a list of other YaST repositories, most of them provided by the <a href="http://en.opensuse.org/">openSUSE community</a>, and read <a href="http://www.novell.com/coolsolutions/feature/16592.html">this fine tutorial</a> how to add them.
<!--break-->