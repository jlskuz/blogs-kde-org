---
title:   "Cute Harmony: Qt goes LGPL"
date:    2009-01-14
authors:
  - beineri
slug:    cute-harmony-qt-goes-lgpl
---
Thanks Nokia! It will be really interesting how this will impact the Qt and KDE communities, desktop, embedded, mobile, cross-desktop collaboration. Looking forward to a friendly competition on technical merits only. :-)

<ul>
<li><a href="http://www.qtsoftware.com/about/news/lgpl-license-option-added-to-qt">Nokia Press Release: LGPL License Option Added to Qt</a></li>
<li><a href="http://dot.kde.org/1231920504/">KDE News: Qt Everywhere: 4.5 To Be Relicensed As LGPL</a></li>
<li><a href="http://arstechnica.com/news.ars/post/20090114-nokia-qt-lgpl-switch-huge-win-for-cross-platform-development.html">Ars Technica: Nokia Qt LGPL switch huge win for cross-platform development</li>
<li><a href="http://www.qtsoftware.com/about/licensing/frequently-asked-questions">QtSoftware.com: Frequently Asked Questions about Licensing</a></li>
<li><a href="http://www.youtube.com/watch?v=IsTIIQocSqs&eurl=http://osnews.com/story/20769/Nokia_To_Add_LGPL_to_Qt_Licensing_Model&feature=player_embedded">YouTube: Nokia to License Qt Under LGPL</a></li>
</ul>
<!--break-->
