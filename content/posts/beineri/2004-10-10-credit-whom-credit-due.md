---
title:   "Credit to Whom Credit is Due"
date:    2004-10-10
authors:
  - beineri
slug:    credit-whom-credit-due
---
Learned yesterday on IRC that people still think that the <a href="http://www.kde.org/people/credits/">KDE Credits</a> page is access restricted. That's wrong, everyone with a CVS account can update it (cvs co www/people/credits/). If you don't have an account read the paragraph at its bottom.

On a related note, the directory with the change logs is also not access restricted, so please run "cvs co www/announcements/changelogs/changelog3_3to3_3_1.php" and add your bugfixes like Annma ideally did. :-)
<!--break-->