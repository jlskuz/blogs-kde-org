---
title:   "Smarter YaST Control Center for openSUSE 10.2"
date:    2006-12-26
authors:
  - beineri
slug:    smarter-yast-control-center-opensuse-102
---
If you're annoyed by the openSUSE 10.2 YaST Control Center not remembering its last size (but starting always too small/with one column) as me, <a href="http://software.opensuse.org/download/home:/Beineri/openSUSE_10.2/">my home project</a> in the openSUSE Build Service has a yast2-control-center package which does.<!--break-->