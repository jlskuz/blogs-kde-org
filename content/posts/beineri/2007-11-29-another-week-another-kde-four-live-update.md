---
title:   "Another Week, Another KDE Four Live Update"
date:    2007-11-29
authors:
  - beineri
slug:    another-week-another-kde-four-live-update
---
The KDE 4.0 RC 1 version of the <a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a> CD <a href="http://distrowatch.com/?newsid=04603">generated publicity</a>, was downloaded over 10000 times within the first days and finally <a href="http://wire.dattitu.de/archives/2007/11/21/KDE-4.0-RC1-being-scared.html">scared Dirk</a>. Thanks go to openSUSE project for jumping in and providing powerful torrent seeders during the later hours. :-)

Today's new version with 3.96.2 snapshot packages has a two weeks newer code base although being released only one week later (the release candidate was tagged one week before publication). And expecting the worst, we have a torrent available from the first minute. Also for the first time, all published openSUSE 10.3 updates are included within the CD so if you decide to install it you don't have much to update. 

Have a lot of fun... :-)
<!--break-->