---
title:   "A Nice Christmas Gift by Canonical?"
date:    2007-12-23
authors:
  - beineri
slug:    nice-christmas-gift-canonical
---
The week saw <a href="https://lists.ubuntu.com/archives/kubuntu-devel/2007-December/002066.html">this Kubuntu announcement</a> being posted which mixes two news, reasons the one with the other and left some users imo rather confused as shown by <a href="http://wadejolson.wordpress.com/2007/12/22/quick-hits-3/">this user comment</a>: "<i>IMHO this is a nice present by the Kubuntu Community and Canonical to the KDE Community</i>". So what was announced?

Kubuntu 8.04 will ship KDE 4.0 as option/on an alternative CD (read KDE 3.5 will exist in Kubuntu 8.04 too). I guess this "news" does not surprise anyone. Also that the Kubuntu <b>community</b> developers want to focus on doing cool new stuff.

The real news is that Kubuntu 8.04 will be, unlike Ubuntu 8.04, no LTS release. LTS stands for "Long Term Support", meaning 3 years support on the desktop, and is a label given to certain releases by the commercial company behind Ubuntu called <b>Canonical</b>.

This is real bummer! So the next Kubuntu LTS we will see, if ever at all, will not appear before 2010. This leaves users/customers who eg planned to update from previous Kubuntu LTS release with a rather big gap by surprise. Ubuntu 8.04 LTS will happen as expected, just the KDE packages on it will have only 18 months support. This means also the KDE 3.5 packages! Would Canonical have the burden to maintain KDE 3.5 alone for further 3 years? No, as eg the KDE 3.5 as contained in SUSE Linux Enterprise Desktop 10 will be supported by Novell until 2011 (extended support even until 2013).

How the decision to support KDE 4.0 shall relate to not giving support (which basically is about critical/security bug fixes) for KDE 3.5 is a mystery to me. One is done by the community, while LTS is said to be a Canonical only thing. My guess is that from the community nobody will work on support at latest starting month 7 (when the next release is out) anyway.

Am I the only one who sees a growing discrepancy between Mark Shuttleworth, Patron of KDE as individual and who promised last year at LinuxTag to not treat KDE second class to GNOME, and what his company Canonical is actually doing?
<!--break-->