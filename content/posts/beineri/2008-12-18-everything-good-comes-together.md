---
title:   "Everything Good Comes Together..."
date:    2008-12-18
authors:
  - beineri
slug:    everything-good-comes-together
---
What an exciting day:

<ul>
<li>The openSUSE team has an early christmas gift in the form of the second distribution release this year: <a href="http://news.opensuse.org/2008/12/18/opensuse-111-released/">openSUSE 11.1</a> - the best distro with the best KDE desktop<b>s</b>.<br><a href="http://software.opensuse.org/">Download it</a> within the first 24 hours to enjoy the Akamai speed afterburner!</li>
<li>Also <a href="http://www.kde.org/announcements/announce-4.2-beta2.php">KDE 4.2 Beta 2</a> gets released today. Packages for openSUSE are available in the <a href="http://download.opensuse.org/repositories/KDE:/KDE4:/UNSTABLE://Desktop/">KDE:KDE4:UNSTABLE:Desktop</a> Build Service repositories.</li>
<li>Together this results in a new <a href="http://home.kde.org/~binner/kde-four-live/">KDE Four Live</a> release - an installable KDE 4.2 Beta 2 Live-CD based on openSUSE 11.1.</a>
</li>
</ul>

Have a lot of fun... :-)

<a href="http://en.opensuse.org/openSUSE_11.1"><img hspace=20 src="http://counter.opensuse.org/11.1/" /></a>
<!--break-->
