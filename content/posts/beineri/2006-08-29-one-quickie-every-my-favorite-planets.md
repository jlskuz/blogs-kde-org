---
title:   "One Quickie for Every of My Favorite Planets"
date:    2006-08-29
authors:
  - beineri
slug:    one-quickie-every-my-favorite-planets
---
I created KDE4.x/ application directories in <a href="ftp://upload.kde.org/incoming/">ftp://upload.kde.org</a>'s incoming/ directory. The first application it was used for was <a href="http://www.okular.org/">okular</a>, an universal document viewer for KDE 4 based on <a href="http://kpdf.kde.org/">KPDF</a>, with <a href="http://tsdgeos.blogspot.com/2006/08/okular-snapshot-released.html">a snapshot preview</a> requiring the recent <a href="http://dot.kde.org/1155935483/">Krash release</a>.

The German <a href="http://www.heise.de/ct/">computer magazine c't</a> published a special edition &quot;Linux&quot; accompanied by a DVD containing the full SUSE Linux 10.1 with a 70 thousand run. That was obviously not enough as they now announced that they will produce another 25 thousand. 95 thousand is an impressive ratio to the 380 thousand print run which the main magazine has (<a href="https://www.heise.de/kiosk/special/ct/06/05/">the special edition</a> costs 8,50 Euro including free shipping within Europe).
<!--break-->