---
title:   "openSUSE Build Service on Steroids"
date:    2007-06-26
authors:
  - beineri
slug:    opensuse-build-service-steroids
---
Thanks to a yet to be announced generous sponsor (also the first non-Novell sponsor to the openSUSE project) is the <a href="http://build.opensuse.org/">openSUSE Build Service</a> running like on steroids since last week: over 120 CPUs are now available in the build host farm. And the new hosts' disc systems are also very fast. That's enough power to rebuild the openSUSE:Factory project within a few hours. And of course it's nice for packagers: no longer waiting for your builds to start. At least I as heavy user don't remember having to wait for it anymore the whole last week. :-)

And last, taken from the statistics page: "There are now 668 projects, 8706 packages, 2142 repositories and 631 confirmed users."
<!--break-->