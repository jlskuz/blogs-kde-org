---
title:   "openSUSE 10.3 Released"
date:    2007-10-04
authors:
  - beineri
slug:    opensuse-103-released
---
Today, after ten months of work, <a href="http://news.opensuse.org/?p=400">openSUSE has 10.3 been released</a>. :-)

<a href="http://farm2.static.flickr.com/1423/1484339302_abf7f62a78_o_d.png"><img src="http://farm2.static.flickr.com/1227/1484344418_4d1c11e609_o.png" height="213" width="278"/></a>

<a href="http://news.opensuse.org/?p=125">Francis</a> did an excellent coverage of everything new so let me just relist his <a href="http://news.opensuse.org/?cat=17">Sneak Peeks</a>:

<ul>
<li><a href="http://news.opensuse.org/?p=104">Greatly Improved Boot Time</a>, with Stephan Kulow</li>
<li><a href="http://news.opensuse.org/?p=133">1-Click Install</a>, with Benjamin Weber</li>
<li><a href="http://news.opensuse.org/?p=153">New Package Management</a>, with Duncan Mac-Vicar Prett</li>
<li><a href="http://news.opensuse.org/?p=167">Compiz and Compiz Fusion</a>, with Matthias Hopf and Jigish Gohil</li>
<li><a href="http://news.opensuse.org/?p=219">KDE 4</a>, with Dirk Müller</li>
<li><a href="http://news.opensuse.org/?p=264">SUSE-Polished GNOME 2.20</a>, with JP Rosevear</li>

<li><a href="http://news.opensuse.org/?p=325">1-CD Installation &#038; Multimedia support</a>, with Michael Löffler</li>
<li><a href="http://news.opensuse.org/?p=371">Virtualisation</a>, with Frank Kohler</li>
<li><a href="http://news.opensuse.org/?p=341">A Plethora of Improvements</a>, with Andreas Jaeger</li>
</ul>

<a href="http://en.opensuse.org/Screenshots/openSUSE_10.3">More screenshots</a> and <a href="http://en.opensuse.org/Product_Highlights/10.3">more detailed version list</a> are available in the wiki. <a href="http://software.opensuse.org/">Get it here</a>.

Not all work is done though: the installable Live-CDs didn't qualify for a release today - we will release them maybe next week. The most annoying bugs now showing up will have to be fixed via online updates. And of course we will provide newer packages of KDE 4.0 as its development progresses.
<!--break-->