---
title:   "The Three Weeks Newsreel"
date:    2007-07-12
authors:
  - beineri
slug:    three-weeks-newsreel
---
A quick roundup what happened during the last three weeks:
<ul>
<li>It started with the <a href="http://idea.opensuse.org/">Novell OPS Hack Week</a>: I continued one project I started already previously and started three others. <a href="http://idea.opensuse.org/content/ideas/improve-krpmview">One got finished</a> and about two I will blog once they <a href="http://idea.opensuse.org/content/ideas/opensuse-news-portal">go online</a>/are <a href="http://idea.opensuse.org/content/ideas/kde-textcompletion-history-editor">finished and in our KDE packages</a>.</li>
<li>What's better than hack week? Double hack week! Novell Hack Week ended already Thursday evening for me as I departed to <a href="http://akademy.kde.org/">Akademy</a> on Friday morning. After the conference during the week-end was the next week filled with the KDE e.V. general assembly (congratulation to <a href="http://en.opensuse.org/User:Kfreitag">Klaas</a> getting elected into the board), BoF sessions and a coding marathon (don't miss the groupphoto plasmoid in playground SVN!).</li>
<li>During Akademy running <a href="http://dot.kde.org/1183569837/">KDE 4.0 Alpha 2</a> was finally released.</li>
<li>Of course we had <a href="http://en.opensuse.org/KDE4">packages for openSUSE</a> and a <a href="http://home.kde.org/~binner/kde-four-live/">Live-CD with them</a>. <a href="http://thelins.se/index.php?title=KDE_4_Alpha">This article</a> contains some screenshot of it.</li>
<li>Also during Akademy the news went public that <a href="http://dot.kde.org/1183806862/">Novell had become "Patron of KDE"</a> - as first distributor. Thanks to Nat and everyone else who supported it.</li>
<li>After playing with Wordpress the last weeks, a welcome change: Drupal, to get <a href="http://blogs.kde.org/node/2871">kdedevelopers.org going again</a>.</li>
<li>Pleased with the openSUSE schedule to bring all new openSUSE frontends, sites and skins online just in time for openSUSE's second birthday which incidentally coincides with LWE San Francisco. SUSE will have much to celebrate there!</li>
</ul>
Look forward to more often and smaller blogs in the future again. :-)
<!--break-->