---
title:   "Novell Open Audio: SUSE Linux 10.1 Unleashed"
date:    2006-05-20
authors:
  - beineri
slug:    novell-open-audio-suse-linux-101-unleashed
---
I think this was not yet mentioned on <a href="http://planetsuse.org/">Planet SUSE</a>, at least <a href="http://reverendted.blogspot.com/">Ted</a> didn't blog about it as he was on vacation: <a href="http://www.novell.com/company/podcasts/about.html">Novell Open Audio</a>, the company's own podcast show, had a long <a href="http://www.novell.com/podcast/Detailpage.jsp?id=60">episode about the SUSE Linux 10.1 release</a> last week. Not Michael Loeffler as advertized but Martin Lasarsch (notlocalhorst) was interviewed for almost half an hour about the <a href="http://en.opensuse.org/Product_Highlights">cool stuff in SUSE Linux 10.1</a>.<!--break-->