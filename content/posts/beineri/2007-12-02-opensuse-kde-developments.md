---
title:   " openSUSE KDE Developments"
date:    2007-12-02
authors:
  - beineri
slug:    opensuse-kde-developments
---
I want to summarize some as yet unmentioned changes which we have done during the last weeks:

<ul>
<li>The <a href="http://en.opensuse.org/KDE">KDE Wiki page</a> has been spiffed up and should be now a good portal to inform everyone quickly about everything worth knowing about KDE on openSUSE.</li>
<li>This includes better packaging documentation like a <a href="http://en.opensuse.org/KDE/Packaging/Cookbook">KDE spec file cookbook</a> and <a href="http://en.opensuse.org/Packaging/SUSE_Macros/KDE4_Macros">description of used macros</a>.</li>
<li>It also mentions a long <a href="http://en.opensuse.org/KDE/Challenges">list of challenges</a> we face - any help to accomplish as much of it as possible until next openSUSE release is welcome.</li>
<li>We started to have biweekly <a href="http://en.opensuse.org/KDE/Meetings">scheduled meetings</a> on our IRC channel, the next one will be on 12th December.</li>
<li>The <a href="http://en.opensuse.org/Packaging/Packaging_Day">Packaging Days</a> this weekend <a href="http://en.opensuse.org/Talk:Packaging/Packaging_Day">resulted</a> on the KDE part in a fixed kitchensync package and new packages in the build service for Qtpfsgui, Misfit Model 3D and KCometen3.
<li>Dirk has posted a <a href="http://lists.opensuse.org/opensuse-kde/2007-11/msg00031.html">proposal how to restructure our build service repositories</a> - looking forward to your feedback!
<li>Improved KDE:KDE4 packaging, removed packaging bugs and made it more maintainable.</li>
<li>Our KDE4 extragear snapshot packages have been broken down to application packages and include as of today rsibreak, kpager, kaider, ksig, kmldonkey, ktorrent, kftpgrabber, kwlan, amarok, kplayer, kaudiocreator, kmid, kfax, kgrab, kuickshow, digikam, kgraphviewer, kcoloredit, kpovmodeler, kphotoalbum and ligature.</li>
<li>KOffice2 packaging has been improved and should show up in Factory soon.</li>
</ul>

That was it already. :-)
<!--break-->