---
title:   "24 KDE projects approved for Google's Summer of Code!"
date:    2005-06-25
authors:
  - zogje
slug:    24-kde-projects-approved-googles-summer-code
---
Thank you Google! It's quite exciting to see so many people with so many new ideas for making KDE better. Best of luck to all 24 winning contestants!