---
title:   "Rant of the day (2/4)"
date:    2004-04-03
authors:
  - zogje
slug:    rant-day-24
---
Since I'm not so good in thinking of innovative titles I have decided to stick with the current one. In turn, that obliges me to rant a little. The subject of my rant today is kwallet. Thanks to kwallet I can now use secure passwords such as "@&fR(e6FSdkf%" and "n#hkghgHGJkjf^$z" which would otherwise be impossible to remember. This is good. The bad part comes when you start to rely on this. The password for my blog account is equally cryptic as the two examples above (still good), but kwallet <b>REFUSES</b> to remember my password for the login page. I have a somewhat of a love-hate relationship with the thing anyway since it <b>DOES</b> insist on popping up on every Groklaw article that I read.

My previous rant has been very productive. Geiseri was so kind to add a new "Personal" category to the blog-machine which I will be happily using from now on.

The New Project is well on schedule. Tomorrow I expect to complete phase 1. Sometimes circumstances create a very disturbing situation, to a certain degree you can try to fight that, but at one point you will need to concede and accept that your goals may be unrealistic given the circumstances. Instead of fighting the situation it is better to adjust to the new reality. The New Project is all about accommodating to this new reality.

Did not have a very productive day today on the other front. The URLButton in kicker was especially depressing. It is hardly used in kicker. Until this week it was only used when you drag a URL to the panel. However, dragging a .desktop link that points to URL to the panel didn't work, so I fixed that. That appears to have broken old kicker settings that still used URLButton to execute .desktop files. And now I am torn between changing these old settings to use the more appropriate ServiceButton instead, or changing URLButton so that it always creates a Link-style .desktop file for storing the URL. The advantage of the latter would be that you could edit the URL icon via the properties RMB option. On the other hand it doesn't make sense to use an URLButton for an application-style .desktop file as the old kicker settings did. But if I migrate these to ServiceButtons then I need to make sure that the .desktop file isn't in fact a link-style .desktop file. All this is hardly very important, which makes it all the more annoying. KSim also pissed me off majorly today because I couldn't find why it refused to show the "unhide" button. Without "unhide" the "hide" option becomes somewhat of a "hide for the rest of the session" button.

On to pressing matters: Will I watch tonight's GP F1 Qualifying live or tommorrow morning? Decisions.. decisions... I'm a big fan of Pablo, I try to be supportive of Kimmi as well but he needs to do a whole lot better if he wants to make the championship a bit exciting.