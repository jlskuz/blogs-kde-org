---
title:   "What every KDE developer should know...."
date:    2006-06-28
authors:
  - zogje
slug:    what-every-kde-developer-should-know
---
You look at the code of this new KDE application and you immediately notice several of the mistakes that you made in your first KDE application as well. Sounds familar?

Then it's time to share your experience at this years aKademy. Tell the world how to write KDE applications the right way. There are still two days left to <a href="http://conference2006.kde.org/conference/call.php">submit a presentation proposal</a> for this years aKademy. It's a 5 minute job: <a href="http://portland.freedesktop.org/xdg-utils-1.0beta1/xdg-email.html">xdg-email </a> <a href="mailto:akademy-talks-2006@kde.org?subject=My%20aKademy%20presentation%20about%20...&body=I%20would%20like%20to%20give%20a%20presentation%20at%20this%20years%20Akademy%20about...">akademy-talks-2006@kde.org</a> --subject 'My aKademy presentation about ...' --body 'I would like to give a presentation at this years aKademy about ...' You only need to fill in the blanks. Be a KDE Hero(m/f) and do it now.

Suggestions:
* Getting on top: Things you need to know about focus stealing prevention. 
* Out of the way DCOP, here comes the DBUS.
* Writing KDED modules that don't suck.
* Supporting KIOSK, what you can do to keep Sysadmins happy.
* Make fast, 10 tips to stay in the left lane of the digital autobahn. Common patterns to avoid that make aplications slow.
* ...
<!--break-->
[Updated: Fixed link to call for papers]