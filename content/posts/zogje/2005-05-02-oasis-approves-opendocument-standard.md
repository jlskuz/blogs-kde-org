---
title:   "OASIS Approves OpenDocument Standard"
date:    2005-05-02
authors:
  - zogje
slug:    oasis-approves-opendocument-standard
---
The month of May has started with a big milestone: the <a href="http://lists.oasis-open.org/archives/tc-announce/200505/msg00001.html">approval of the OpenDocument 1.0 specification</a> as an OASIS standard. Part of the credits for that go to our very own David Faure who has done a great amount of hard work in the OASIS OpenDocument Technical Commitee. The importance of a truly open file format for office documents can not be understated and the OASIS OpenDocument standard helps to bring competition back to office applications. I expect the OASIS OpenDocument standard to make headlines in the near future as it has been eagerly awaited by governments around the world. 

With OASIS OpenDocument the user gets the rights to his own documents back. Instead of being forced into the upgrade-cycle of that single vendor for nothing more than the privilege of continued access to your own documents, the OASIS OpenDocument standard will allow you to actually select your office application based on good old-fashioned criteria like features, stability and price. When you do that you may want to take a critical look at the upcoming KOffice 1.4. Thanks to its native OASIS OpenDocument support KOffice 1.4 is without doubt the most appealling KOffice ever.