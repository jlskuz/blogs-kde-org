---
title:   "On the virtues of a common configuration system"
date:    2005-04-11
authors:
  - zogje
slug:    virtues-common-configuration-system
---
Yesterday, <a href="http://aseigo.bddf.ca/images/aseigo.png">Aaron</a> made <a href="http://aseigo.blogspot.com/2005/04/stupidity-of-dconf.html">some interesting</a> comments on the unified configuration system that has been discussed on the <a href="http://lists.freedesktop.org/archives/xdg/">xdg-list</a> over the last few weeks. I think some of his comments are spot on, unfortunately some others seem to be of the more paranoid hallucinogenic kind. I'm not into mushrooms myself so I will concentrate on the productive parts.

Let me start by pointing out that the discussion on the xdg-list is lively and ongoing. In my view there is an overall concensus among the participants of the discussion that it would be beneficial to have a common configuration system in place on Linux, what keeps the discussion going is the question how such configuration system should look like and how to get there. If you think you have anything to contribute in this area, by all means, <a href="http://lists.freedesktop.org/mailman/listinfo/xdg">join the fun</a>.

For the sake of discussion this future configuration system has been dubbed D-Conf. Aaron describes D-Conf as vapourware, which is true of course, the challenge though is to model the vapour into a shape of our liking and then put in some honest work, sweat and tears to turn vapour into reality. Not all the participants in the discussion have the same amount of experience with configuration systems so it is important to make sure that the vapour doesn't end up like some bad trip, that would be a shame because such a system, once implemented, would be unlikely to find adoption in either KDE or Gnome.

In his blog entry, Aaron characterizes D-Conf as an effort to create the <i>One True Unix Config File Format</i>. I think that misses the point. Although file formats have certainly been discussed, they are not terribly interesting. What is interesting, is to have a <i>One True Unix Configuration System</i>. One true way to access and manage configuration settings. There is already <i>One True KDE Configuration System</i>, it is called KConfig. It uses an enhanced .ini-style storage format and was initially designed to support multiple different storage format. Although hardly anyone bothered over the years to come up with an alternative storage format, it should still be relative straight forward to add one. There is also the <i>One True Gnome Configuration System</i> which is called G-Conf. It uses an XML-based storage format and was designed to support multiple different storage formats. When you have a configuration system like KConfig or G-Conf in place and have applications using it, the storage format isn't that big of a deal... when you need something else, it's easy enough to add a different storage format. The internal design of the system is important though. It would, for example, be easy to add an XML backend to KConfig, but due to the internal design of KConfig it would be impossible at the moment to let applications take full advantage of the extra flexibility offered by XML.

Aaron makes two very interesting point when he writes <i>"people need to realize that a new configuration system should do more and be better than what's there. so actually talk to the people making these systems."</i> The first part can't be stressed enough, it makes no sense to design a new system if it doesn't do a better job than existing systems. The second part follows from the first because without the expertise of the the people that worked on the existing systems you are bound to repeat the failures instead of the successes. Luckily both Havoc, the lead developer of G-Conf, and me, maintainer of KConfig, are subscribed to the xdg list :-)

Havoc has outlined <a href="http://www.gnome.org/projects/gconf/plans.html">here</a> what he thinks is currently lacking
in G-Conf.

At the KConfig side I am aware of the following wishes from KDE developers:
* Change notification
* Nested groups (if only to make khotkeysrc readable for humans)
And a recurring wish from KDE users:
* LDAP storage backend (or any remote storage)

If there are other things that you miss in KConfig don't hesitate to let me know.

I think that D-Conf creates a great opportunity to work together and improve both G-Conf and KConfig. Especially because the plans for G-Conf will make G-Conf more like KConfig, and in order to implement the above wishlist for KConfig, we will need to make KConfig more like G-Conf. I think we can create D-Conf that solves the needs of both G-Conf and KConfig and at the same time establish the elusive <i>One True Unix Configuration System</i>

Although I think the journey to D-Conf land will be an exciting journey I realize that many people are very happy with KDE's configuration system today and it is absolutely essential to keep it that way. That means that for D-Conf to be a success KDE developers must be able to continue to use KConfig as they do today and it means that KDE users must be able to continue to use their carefully tuned configuration as they do today. I think it's possible to do that and I hope that KDE developers will join me on this journey.
