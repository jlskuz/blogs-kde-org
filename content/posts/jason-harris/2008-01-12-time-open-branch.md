---
title:   "Time to open the Branch"
date:    2008-01-12
authors:
  - jason harris
slug:    time-open-branch
---
With the <a href="http://www.kde.org/announcements/4.0/">release of KDE 4.0</a>, it's time to start maintaining both trunk and branch versions of the code, so that bugfixes can be backported to the branch.

If you use the <a href="http://techbase.kde.org/index.php?title=Getting_Started/Increased_Productivity_in_KDE4_with_Scripts#Bash">cs/cb/cmakekde</a> scripts to manage your build environment, it's fairly easy to switch the context of these between branch and trunk.  
<!--break-->
I am using the following scripts (I have trunk in $HOME/kde/src and branch in $HOME/kde/src_4.0):

<code>
#!/bin/bash
# branch: switch build environment to the 4.0 branch
export KDE_BUILD=$HOME/kde/build_4.0
export KDE_SRC=$HOME/kde/src_4.0
export OBJ_REPLACEMENT="s#$KDE_SRC_4_0#$KDE_BUILD_4_0#"
</code>

<code>
#!/bin/bash
# trunk: switch build environment to trunk
export KDE_BUILD=$HOME/kde/build
export KDE_SRC=$HOME/kde/src
export OBJ_REPLACEMENT="s#$KDE_SRC#$KDE_BUILD#"
</code>

Seems to work so far.

Oh, and since all the cool kids are doing it:<br>
<img src="http://static.kdenews.org/jr/kde-4.0-banner.png">
