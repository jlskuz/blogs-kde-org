---
title:   "I'm not going to make it..."
date:    2004-07-31
authors:
  - jason harris
slug:    im-not-going-make-it
---
Whenever there's a KDE release, I like to draw up a "QA list" for KStars that essentially lists all of the program's expected behaviors (it's quite a long list).  I then go through and systematically check each one.  I typically turn up a good number of lurking insects this way, and the current release is <a href="http://lists.kde.org/?l=kstars-devel&m=109102970800374&w=2">no exception</a>.

Unfortunately, I am just not going to have time to fix these problems before the 3.3.0 release.  I am currently out of the country, and am not getting back home until Aug 4th.  I feel kind of bad about it, because for every release up until this one, I had solved all known KStars bugs. :(

This release cycle seemed much shorter than previous ones.  Or maybe I was just busier with work this time.  Oh well, I guess there's always 3.3.1, right?
