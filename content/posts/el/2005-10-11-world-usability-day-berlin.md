---
title:   "World Usability Day @ Berlin"
date:    2005-10-11
authors:
  - el
slug:    world-usability-day-berlin
---
[image:1534 align=center width=300 class=showonplanet]

Our site for the Berlin event in the scope of the <a href="http://www.worldusabilityday.org"> World Usability Day</a> on November 3rd is up:

<a href="http://www.usabilitytag.de">www.usabilitytag.de</a>
Layout and Design by tina and holehan.

Programm details will follow in 7-10 days.



We are also listed on the official UPA sites:

<ul>

<li><a href="http://www.worldusabilityday.de/berlin/index.html">Berlin @ the German WUD site</a></li>

<li><a href="http://worldusabilityday.org/taxonomy/term/131">Berlin @ the International WUD site</a></li>

</ul>

<!--break-->
