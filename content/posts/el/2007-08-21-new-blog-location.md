---
title:   "New blog location"
date:    2007-08-21
authors:
  - el
slug:    new-blog-location
---

This blog moved to <a href="http://ellen.reitmayr.net/index.php/blog">my private website</a> 
(<a href="http://ellen.reitmayr.net/index.php/feed">feed</a>)!

