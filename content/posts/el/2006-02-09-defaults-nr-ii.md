---
title:   "Defaults Nr. II"
date:    2006-02-09
authors:
  - el
slug:    defaults-nr-ii
---
Let's have a look at font settings:

<ul>

<li>In <b>Konqueror</b>, pressing Defaults resets the fonts to the current Control Center Default font.</li>

<li>In <b>Konversation</b>, pressing Defaults resets the fonts to the Control Center Default at Konversation's startup. This is not necessarily the current one.
Imagine you change the Control Center Default, but Konversation and some other apps have not adopted the settings. You go to each of them and change the font settings manually as the Defaults button has no effect.
A lot of work - and all for nothing: Restarting the X Server would have done the same job.</li>

<li>In <b>KMail</b>, you either use custom colours or you do not (the latter will set the current Control Center Defaults). 
The Defaults button, however, has no effect. So there is no way to base a new colour scheme on the Default, once you've changed it.</li>

</ul>

Resume:  Three applications, two different types to visualise the font settings, and three different behaviours to reset to the defaults.

<!--break-->

