---
title:   "UI Guidelines for Content Management Systems @ Berlin OSU Meeting"
date:    2005-09-21
authors:
  - el
slug:    ui-guidelines-content-management-systems-berlin-osu-meeting
---
Today, we'll have another Open Software Usability meeting in Berlin, 20.00, at <a href="http://www.relevantive.de">relevantive</a>.

Today's topics:

<ol>

<li><b>World Usability Day</b><br> On November 3rd, it's the World Usability Day. We are planning an event in Berlin which will mainly address Open Source Usability. We have to discuss organisational things, such as finding a room, who volunteers, what talks and workshops to offer.<br><br></li>

<li><b>CMS Usability Guidelines</b><br>Once more, we will address Content Management Systems in order to specify User Interface Guidelines for CMS in the long run. After inspecting <a href="http://www.typo3.com">Typo3</a> a few weeks ago, we'll have a look at <a href="http://www.mambocms.com">Mambo</a> today and we'll set up relevant chapters of the future guidelines.<br>
On OpenUsability, there is a <a href="http://openusability.org/projects/cms-uig">project</a> for this purpose as well as a <a href="http://wiki.openusability.org/cms-uig/index.php/Main_Page">wiki</a>.<br><br>
</li>

<li><b>CMS for HTML Protoypes</b><br>Furthermore, we want to clarify if CMS can be used to quickly produce HTML prototypes and click dummies which can be used for usability testing. <br><br></li>

</ul>

Hope to see some of the Berlin guys there :)

<!--break-->


