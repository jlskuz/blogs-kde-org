---
title:   "User survey: What types of removable drives do you use?"
date:    2006-11-28
authors:
  - el
slug:    user-survey-what-types-removable-drives-do-you-use
---
For the next generation of KDE's file manager, we want a better handling of removable drives such as USB sticks, USB and firewire drives, CDs and DVDs, ZIP (does anybody still use this??) and flash memory. We set up a short survey to answer the questions that bother us most: How many removable drives are usually connected to a computer and what are they used for?

Please support us by filling in the survey (it only takes a few minutes): 

<a href="http://test.openusability.org/UCCASS/survey.php?sid=37">[ Start survey ]</a>

<!--break-->