---
title:   "Continiuous Integration server for KDE(PIM) - Dead"
date:    2011-08-09
authors:
  - tnyblom
slug:    continiuous-integration-server-kdepim-dead
---
Unfortunately my server decided to die today... I've managed to get the most important stuff up again but the Jenkins instance is toast.

That is the actual Jenkins server is alive but there is no resources for the build slave, so for all intents it's dead.

Until some kind force enables me to replace the server or it can be hosted at the EBN, no more CI for PIM.