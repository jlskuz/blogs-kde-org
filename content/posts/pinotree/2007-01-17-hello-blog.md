---
title:   "Hello blog!"
date:    2007-01-17
authors:
  - pinotree
slug:    hello-blog
---
Here we are.
Some people told me a blog would have been a nice idea, so let's try this adventure, I'm sure it will be fun :-)

<!--break-->
I'll talk about my KDE development, about okular, and of course about any other thing that I might want to talk about. It's my blog, isn't it? ;-) Of course, don't expect too much from me, as I'm not a so great talker like many other people around ;-)
As a first entry, I'd like to thank the fantastic KDE community and all the people in it: the support you can get is great - thanks!