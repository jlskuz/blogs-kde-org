---
title:   "A new sidebar"
date:    2007-08-06
authors:
  - pinotree
slug:    new-sidebar
---
Like many other things, <a href="http://okular.kde.org">okular</a> used the sidebar <a href="http://kpdf.kde.org">KPDF</a> had, adding tabs with new stuff (like the Review pane and the Bookmarks panel).

Now, the problem is that the implementation for these sidebars was all but a real solution. In the past, and especially yesterday, our usability expert Florian pointed me the issues of it.

For example...
<!--break-->
<ul type="disc">
  <li>Too much space taken by the tabs of the toolbox (thus, less space for the real contents of the tabs)</li>
  <li>"jumping" tabs - that means, the "label" of each tab had a different position, depending on the open ones (of course, not for the first)</li>
  <li>No way to change the active tab of the toolbox using the keyboard (that is what was also reported by the KPDF's <a href="http://bugs.kde.org/show_bug.cgi?id=132152">bug 132152</a></li>
  <li>not really good-looking (not a real issue, just a taste that many people made me notice)</li>
</ul>

[image:2921 align=right]
So, Florian suggested me a new possible solution, and asked me if it would have been doable. I said "why not?", and instantly started working on it. Luckly, it was not really difficult, and the result is what is see on the right. Nice, isn't it? :)

This new approach solves basically almost all the "cons" of the old sidebar, and introduces some new thing:
<ul type="disc">
  <li>Every tab have the full window height for its contents</li>
  <li>You can activate each tab from a well known position, and using also the keyboard</li>
  <li>You can close the active tab with no need to close the way to select one<br />
    This means that you can click on the icon of the current tab to close it, leaving more room for the contents area, without closing the tabbar!</li>
  <li>A better look for it (sometimes is due, isn't it? ;) </li>
</ul>
The only "regression" the new sidebar introduces wrt the old one, is that the only way to distinguish every tab is looking at its icon, while with the toolbox there was also the name next to the icon. Partially solved by providing the name as tooltip of each icon.

Still some graphical glitch is left (like the color for the disabled tabs), but they are easy to be solved, and I hope to fix them in time for KDE 4.0 beta2.

As usual, comments, problems and ideas are always welcome!