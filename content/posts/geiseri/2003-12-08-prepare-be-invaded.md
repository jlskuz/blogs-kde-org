---
title:   "Prepare to be invaded!"
date:    2003-12-08
authors:
  - geiseri
slug:    prepare-be-invaded
---
Okay so it was a cold Sunday morning, and I just couldn't seem to make it out of bed.  So with my warm Powerbook, I hacked out this little cvs:[kdebindings/kjsembed/docs/examples/invaders/invaders.js|nugget]: [image:260,middle]
<!--break-->
So now I can waste the next few hours playing space invaders ;)  This is also a neat example of how to do keyboard and mouse events.  It also shows that kjsembed can be fast enough to do real work.