---
title:   "A Model with a View"
date:    2004-05-18
authors:
  - geiseri
slug:    model-view
---
Well Alexander promised me that he would give me a QVariant editor widget tomorrow, so I am psyched.  I grew annoyed with Qts qt:QSQLPropertyMap and decided to deviate some with my own take on the bugger. Not that its TTs fault here because I am departing from the realm of SQL. I ended up with a more complex property map, but this also gave me some added bonuses.
<!--break-->
I decided not to store pointers in the map first of all.  Since the widget knows its children, it can query those from a string, and since properties can be accessed via strings, i store those too in a Key object.  Then I create a qt:QMap of these Keys and qt:QVariants.  This is cool because unlike the SQL property map, you can change the property you want to query and also have multiple values extracted from the same widget,only different properties.

Now the cool part of all of this will come when I can get the variant editor from Alexander.  At that point, i can auto-generate GUIs on the fly from the property map.<br>  [image:468]
<br>
I created a special key for this one called the AutoKey.  This one is special that it can store things like a "pretty label" in the key, or auto-generate one if it was not provided.  This is nice because for some data entry instances, layout is not important, you just need to edit data.

Lastly I created an adaptor class that will then allow my KStorable::Objects to be manipulated by these data aware forms.....  the power grows ;)