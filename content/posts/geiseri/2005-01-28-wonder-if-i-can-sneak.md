---
title:   "Wonder if i can sneak this in...."
date:    2005-01-28
authors:
  - geiseri
slug:    wonder-if-i-can-sneak
---
So due to Debian's insistance on messing with packages and KDE's fragile docs build system, I found myself in #kde-docs yesterday.  Man either I'm a sucker for kiwi women or I just hate life.

4 hours later I now have a xlst style sheet that will extract useful data from KConfigXT kcfg files and generate a wrong, but promising dockbook pate.  The net result is that we can actually autogenerate documentation about applications configurable options.

Now comes the hard part.  To get developers to document their kcfg files.  Developers fill in your <label> and <whatsthis> tags.  A) because they are used to populate the whatsthis and tooltip data in the UI, B) it will make applications like KConfigEditor useful and C) it will make these docs damn sweet.

Developers seem unable or unwilling to write formal documentation but they seem very able and willing to fill in whats this and tooltip data in their Qt UI forms.  If they do the same in the KConfigXT kcfg files then we can get more documentation for free. 

Either way, now to see if I can sneak this into KDE 3.4 ;)  I know there will more than likely be a KDE 3.5, but it would be nice to have this sooner than later.
