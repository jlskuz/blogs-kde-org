---
title:   "girls 'ill do that to you...."
date:    2004-10-08
authors:
  - geiseri
slug:    girls-ill-do-you
---
So they say necessity is the mother of all invention... personally I tend to believe its the affections of a woman ;)

Basicly it started last week, my wife is working on her masters in EE, and found out that the lab at work didn't have Matlab installed for his DSP class.  So I told her about GNU Octave http://www.octave.org and she was for the most part thrilled.

The rub is that its a VERY unixy application, full of odd consoles, that don't play well with others and commands that are not always clear.  So not to leave her high and dry I started on a GUI wrapper for Octave. http://www.geiseri.com/kdevelop/console.png and http://www.geiseri.com/kdevelop/embedded_input.png are the progress so far.  I have the embedding of the engine done, and interaction with the input and help systems complete.  My next step is to make the plot() function a bit more friendly. Once I have solved that issue I can start with an IDE...  but thats another blog.

Yeah, and I snuck flowers into her office for our anniversary. I am pretty sure that either she forgot, or she figures I forgot, since I have not heard her talk about it for some weeks.  Either way, gold star for me. :)
 