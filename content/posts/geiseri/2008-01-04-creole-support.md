---
title:   "Creole support"
date:    2008-01-04
authors:
  - geiseri
slug:    creole-support
---
So I have been working on a Wiki parser in Qt and so far have most of the basic cases working.  I tried two approaches one with a series of regexps and the other using a tokenizing parser.

Well the regexp one gave some very nice quick results but the corner cases proved too much for QRegExp and my utter lack of regexp-fu.  Next was to try a tokenizing parser.  This was amusing since I have not touched parsers in almost 10 years now, so it was mostly a refresher course.  So far this has been slower progress, but things are more reliable and tests seem to be passing and staying passing.   With the regexp approach things seemed kinda fragile.  Now I am utterly useless with lex/yacc so I am still doing it by hand, but I have encapsulated things so that at a later date the grammar portion can be cleaned up and the rest can remain unchanged.  Ideally when things take off here I can get more people interested who know more about parsers.

Once the XHTML backend is done then I can start on the QTextDocument backend.  One nice thing about this approach is that one can basically provide a WYSIWYG wiki editor to any QTextEditor widget with one line of code. Damn it feels good to be a KDE Developer ;)