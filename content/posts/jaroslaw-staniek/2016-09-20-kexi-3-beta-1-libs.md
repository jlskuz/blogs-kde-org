---
title:   "Kexi 3 Beta 1 + libs"
date:    2016-09-20
authors:
  - jaroslaw staniek
slug:    kexi-3-beta-1-libs
---
Cool, Kexi 3.0.0 Beta 1 and related libraries have landed, ready for testing and packaging. Please find downloads and change logs at <a href="https://community.kde.org/Kexi/Releases/3#3.0.0_Beta_1">https://community.kde.org/Kexi/Releases/3#3.0.0_Beta_1</a>

As usual report bug and wishes are welcome, details at <a href="https://community.kde.org/Calligra/File_a_bug_or_wish">https://community.kde.org/Calligra/File_a_bug_or_wish</a>.

This time the release got automated a bit more. Changelogs generated with some bash coding can be now easily pasted on the wiki, and mediawiki templates {{bug}}, {{commit}}, {{diff}} work also for your project, on any wiki page. What are your favourite changelog tools? Maybe Phabricator would offer something ultimate like https://secure.phabricator.com/w/changelog/? 

Later we would move some content from wiki to semi-static generated pages on kexi-project.org. So this obsolete page would become more like marble.kde.org or kate-editor.org.

I know, I know. it's hard to try without examples and tutorials. Stay tuned.  For now, API docs sit at at <a href="https://api.kde.org/playground-api/libs-apidocs">https://api.kde.org/playground-api/libs-apidocs</a>, a temporary location.

Have fun!

<img src="/sites/blogs.kde.org/files/dbserver3.png">