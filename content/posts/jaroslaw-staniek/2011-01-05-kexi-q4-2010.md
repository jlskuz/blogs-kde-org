---
title:   "Kexi in Q4 2010"
date:    2011-01-05
authors:
  - jaroslaw staniek
slug:    kexi-q4-2010
---
Short story: In the last day of 2011 Kexi reached the 2.3 release as the other KOffice applications.
Versions 2.4 and newer will be developed distributed within the <a href="http://dot.kde.org/2010/12/06/kde-announces-calligra-suite">Calligra</a> suite, meaning it stays within the team formed by the same awesome, welcoming community. 

What changed? Calligra widens the scope of the activities to two main things: offering components for 3rd-party developers and enabling deep customizations of the code base, so new applications can be developed. Desktop versions, as before, are the main incarnations of the Calligra applications.

<br>
<a href="http://www.calligra-suite.org"><img src="http://kexi-project.org/pics/ads/calligra/calligra-ad.png"/></a>
<br>

This process has started spontaneously long ago and Kexi is a part of it too. One thing is Kexi Mobile subproject, already active within Kexi as you can see below. Another is the high-level database handling library <a href="https://projects.kde.org/projects/playground/libs/predicate">Predicate</a>, successor of KexiDB. Predicate found its way into <a href="https://projects.kde.org/projects/playground/libs/predicate">KDE Playground's git</a> as Qt-only component with wider scope than QtSQL (e.g. you can also define/create databases). Because of the origins of the Predicate project, I will usually write about it in the same blog entries that are devoted to Kexi.
<!--break-->
A short log (based on <a href="http://identi.ca/kexi">identi.ca notes</a>):

<ul>
<li>(October) Nokia donated <a href="http://www.piggz.co.uk/index.php?page=blogentry&id=56">maemo-powered N900 phone to Kexi developer Adam Pigg for development of Kexi Mobile. Thanks Suresh!</a></li>
<li>(November) News from Predicate front (successor of Kexi database abstraction layer): development of brand new PostgreSQL driver started</li>
<li>(November) Work is ongoing on the next version of Kexi (2.4), with behind the scenes stuff like implementing more models and views. Also porting drag/drop handling to Qt4 style. Not exciting, but necessary! (Adam).</li>
<li>Do you know you can submit bug reports for Kexi at bugs.kde.org? A few hints <a href="http://annma.blogspot.com/2010/11/bug-reporting-2-how-to-write-good.html">here</a>.</li>
<li>(December) <a href="https://projects.kde.org/projects/playground/libs/predicate">Predicate goes git!</a></li>
<li><a href="http://dot.kde.org/2010/12/06/kde-announces-calligra-suite">Kexi is now part of the Calligra Suite, KOffice successor!</a></li>
<li>Large changes in Predicate: libpq-based driver, *natively* escaped strings for unmatched security, code at <a href="https://projects.kde.org/projects/playground/libs/predicate">projects.kde.org</a>. Status at <a href="http://community.kde.org/Predicate/Drivers">http://community.kde.org/Predicate/Drivers</a>.</li>
<li><a href="http://www.piggz.co.uk/index.php?page=blogentry&id=61">Prototype of Kexi Mobile Reports works</a><br>

<br><a href="http://kexi-project.org/pics/2.4/alpha1/kexi-2.4-mobile-reports.png"><img src="http://kexi-project.org/pics/2.4/alpha1/kexi-2.4-mobile-reports_sm.png"/></a><br><br></li>
<li><a href="http://www.calligra-suite.org/changelogs/koffice-2-3-0-changelog/#Kexi">What's new in !Kexi 2.3?</a></li>
<li><a href="http://www.calligra-suite.org/news/koffice-2-3-0-released/">Kexi 2.3 released!</a> This is the last big release within KOffice, 2.4 will be Calligra.<br><br><a href="http://www.koffice.org/wp-content/uploads/2010/09/kexi-2.3-new-csv-export.png"><img src="http://www.koffice.org/wp-content/uploads/2010/09/kexi-2.3-new-csv-export-490x410.png"/></a></li>
</ul>

It's worth to mention that the Development Home Page for Predicate is <a href="http://community.kde.org/Predicate">http://community.kde.org/Predicate</a> and for Kexi is <a href="http://community.kde.org/Kexi">http://community.kde.org/Kexi</a>. I am also contributing to the mother of these wikis, <a href="http://community.kde.org/Calligra">http://community.kde.org/Calligra</a> to make sure you have access the most current information about the development in case you'd like to join us.

<br><br>
And as always: <a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&product=kexi&long_desc_type=allwordssubstr&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&keywords_type=allwords&keywords=&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&bug_severity=wishlist&emailtype1=substring&email1=&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=&chfieldto=Now&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&known_name=Kexi+wishes&query_based_on=Kexi+wishes&field0-0-0=noop&type0-0-0=noop&value0-0-0=">The list of current wishes for Kexi</a>. Add yours! There is no easier way to contribute!

<br><br>
<a href="http://www.calligra-suite.org/kexi/"><img src="http://kexi-project.org/pics/ads/kexi-calligra-ad.png"></a>