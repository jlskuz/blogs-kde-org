---
title:   "Elegance #4: tab bar uncluttered"
date:    2010-09-29
authors:
  - jaroslaw staniek
slug:    elegance-4-tab-bar-uncluttered
---
In <a href="http://blogs.kde.org/node/4268">previous</a> <a href="http://blogs.kde.org/node/4269">"elegance"</a> <a href="http://blogs.kde.org/node/4312">entries</a> I presented just ideas, without implementation. Readers obsessed in the area of look&feel may remember the proposal for <a href="http://blogs.kde.org/node/4104">uncluttering tab widget when used in side pane</a>.
<!--break-->
Any specific fix would be just for Oxygen style, so it is still unclear if starting to code this makes much sense. But. As a low-hanging fruit today I called this code on my tab widget of Kexi side pane:

<pre>tabWidget->setDocumentMode(true);</pre>

As for so cheap fix the result looks quite nice to me. Also presented result for two other styles. I think it's good hint for similar UIs. Also works with Qt-only apps.

<img src="http://kexi-project.org/pics/2.3/0/tabbar_setDocumentMode.png"/>