---
title:   "Google Code-in experience - Kexi"
date:    2012-12-19
authors:
  - jaroslaw staniek
slug:    google-code-experience-kexi
---
KDE is taking part in Google Code-in (GCI) this year once again. It's a contest to bring 13 to 17 year-olds closer to Free Software. 

One of the tasks I proposed for Kexi was <a href="http://community.kde.org/Kexi/Junior_Jobs/Add_d-pointers">Adding d-pointers to the code</a>. Because of its size I split the challenge into two GCI tasks for truly motivated students. One part has been taken by <a href="www.google-melange.com/gci/task/view/google/gci2012/7971213">Shou Ya</a> and second by <a href="http://www.google-melange.com/gci/task/view/google/gci2012/8009216">Andrew Inishev</a>. Both parts have been finished successfully, the patches (2 * 10 thousands of lines) are already in the Calligra master repository.

Andrew writes on his <a href="http://inish777.blogspot.ru/2012/12/about-google-code-in-2012.html">blog</a>:
<b>"<i>[..] It was hard for me at beginning because I am beginner in C++ and I didn't know about d-pointers before,  but when mentor of this task explained me how to use d-pointers right, task became to be easier. And I must to mention that he is very patient, if keep in mind my inattention when modify code. </i></b>

<b><i>Collaboration with KDE community is very useful for me (and, may be, for community :-) because I am getting experience and skills which will be useful in my future work. For example, experience of using version-control systems and build systems (which are important skills in collaborative development), experience in C++, etc.</i>"</b>

Another student tries to <a href="http://www.google-melange.com/gci/task/view/google/gci2012/7957214">find UI issues while running Kexi under non-KDE desktops</a>. This particular task expresses commitment of Kexi to be highly portable software that adapts to various environments, what is I think in line with the rest of the Qt and KDE world.

Are you interested in contributing to Kexi or other Calligra applications? We've made <a href="http://community.kde.org/Calligra/First_Contact">first steps really easy</a> and you can have a lot of fun too!