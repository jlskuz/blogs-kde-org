---
title:   "Cool features in Kexi 2.9.8"
date:    2015-10-11
authors:
  - jaroslaw staniek
slug:    cool-features-kexi-298
---
This week <a href="https://www.calligra.org/kexi">database apps builder</a> Kexi that competes with MS Access and Filemaker has been <a href="https://www.calligra.org/news/calligra-2-9-8-released">released with cool new features</a>. 

Version 2.9.8 finally fixes world's issues with SQL! 22 typical scalar functions now work <b>portably</b> across SQLite, MySQL, PostgreSQL. No other general purpose software I know does this.

Here's the list and it will be growing:
<img src="http://kexi-project.org/pics/2.9/kexi-2.9.8-sql_functions_list.jpg">

To understand how the compatibility is achieved see this <a href="https://community.kde.org/Kexi/Plugins/Queries/SQL_Functions#Typical_Scalar_Functions">openly available document</a>.

The new Kexi speaks your language too. It checks correctness of your SQL:

<a href="http://kexi-project.org/pics/2.9/kexi-2.9.8-sql-functions.jpg"><img src="http://kexi-project.org/pics/2.9/kexi-2.9.8-sql-functions_sm.jpg"></a>

If you type error in your statement Kexi carefully explains what's wrong:

<a href="http://kexi-project.org/pics/2.9/kexi-2.9.8-sql-functions2.jpg"><img src="http://kexi-project.org/pics/2.9/kexi-2.9.8-sql-functions2_sm.jpg"></a>

Great tool no matter if you're power users crunching data or a student willing to learn some SQL technology.

In addition, this year the technology will be available as a KDE framework called <a href="https://community.kde.org/KDb">KDb</a> to everyone.

PS: What you see on the screen is somehow a common denominator of various query languages, called <i>KexiSQL</i>. Having it, means that you can more freely migrate your software across backends. One day JavaScript functions would be available too.

PS2: MSSQL support for the functions was also analyzed, just currently it's out of scope.

We're also looking for new contributors. Not only programmers.
Promote Kexi on <a href="https://twitter.com/kexi_project">Twitter</a>, <a href="https://www.facebook.com/kexi.project">Facebook</a> and blogs. Users can request new features or fixes for Kexi via <a href="https://www.bountysource.com/teams/kexi">Bountysource</a>.