---
title:   "Elegance #2"
date:    2010-07-05
authors:
  - jaroslaw staniek
slug:    elegance-2
---
Proposed by chrome developers - two entries and submenu are removed. Reduces mouse clicks and movements.

<img src="http://img205.imageshack.us/img205/4370/chromeunifiedmenu.png"/>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 