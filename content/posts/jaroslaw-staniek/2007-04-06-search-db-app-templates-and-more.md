---
title:   "Search, DB App Templates and More"
date:    2007-04-06
authors:
  - jaroslaw staniek
slug:    search-db-app-templates-and-more
---
<img src="http://kexi-project.org/pics/1.1.3/templates_m.png" align="right" border="0"/>There were at least 17 internal test releases since December 2006 in my company before so called 2007.1 release emerged (in user-oriented naming scheme). Dealing with customers is going pretty well - people (including those from the USA) have inexpensive tech support and extensions on request. 

All this happens in parallel with preparing the KDE 4 version, 2.0.

<!--break-->

As usual -- during and/or after the iteration -- KDE community can enjoy new changes pretty quickly. Yesterday I've committed a <a href="http://lists.kde.org/?l=kde-commits&w=2&r=1&s=kexi&q=b">number of features </a> to KDE SVN. This will make 1.1.3 release somewhere in this quarter. 

You can now use simple database templates, provided -- no surprise -- by copying an original file to a selected destination; optionally a predefined form or table can be automatically opened after creating the new database, so user is not entirely lost. This feature is subject to extensions, and so far it looks like better tool for publishing databases on KDE New Stuff/kde-files because users do not overwrite original database --  there's always a GUI-driven way to "start" with the same template again.

<img src="http://kexi-project.org/pics/1.1.3/search_window_m.png" align="right" border="0"/>Another long standing TODO addressed in the release is "Find" window that can be used over tabular views, query results and forms. It's already pretty advanced internally as well as from user's point of view - most usable options are already there, including options for cntrolling area and direction of search. Searching is passed through relationships, so -- if you have, say, a dictionary-table of countries -- you can search by country name.

I know you like screenshots, and perhaps screencasts even more, so enjoy them:

<ul>
<li><a href="http://kexi.pl/en/News/Kexi_Releases/Kexi_2007.1" target="_blank">screenshots</a></li>
<li><a href="http://kexi-project.org/pics/1.1.3/kexi_db_templates/" target="_blank">a screencast</a> (Flash, 5 MB)</li>
<li><a href="http://kexi-project.org/pics/1.1.3/kexi_db_templates/images.pdf">example PDF document with images</a></li>
</ul>
