---
title:   "Elegance #3: Opinions vs Data"
date:    2010-08-17
authors:
  - jaroslaw staniek
slug:    elegance-3-opinions-vs-data
---
Follow up of the discussion about new UI elements: <a href="http://ignorethecode.net/blog/2010/08/13/opinions_vs__data/">"it may look weird" first-look opinions vs positive results of usability testing</a>. GMail has removed "select all/select none/..." buttons with single combo box for exactly one reason: elegance or UI uncluttering.

<a href="http://ignorethecode.net/blog/2010/08/13/opinions_vs__data/"><img src="https://kexi-project.org/pics/blog/2010/elegance-3-opinions-vs-data/checkboxpopup2.png"/></a>

How do you like it? Feel free to share your comments below after a few days (preferably using the UI e.g. in gmail).
