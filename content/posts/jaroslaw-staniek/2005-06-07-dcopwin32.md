---
title:   "DCOP/win32"
date:    2005-06-07
authors:
  - jaroslaw staniek
slug:    dcopwin32
---
Finally, DCOP Client/Server implementation for win32 found its way to KDElibs, thanks to great contribution from Andreas Roth (aroth at arsoft-online . com).

On the screenshot: 'testdcop' application communicating with dcopserver.

<a href="http://www.kdelibs.com/shots/dcop_win32.png"><img src="http://www.kdelibs.com/shots/dcop_win32.png" width="50%"/></a>

The communication is built on <a href="http://websvn.kde.org/*checkout*/trunk/KDE/kdelibs/win/qeventloopex.h?content-type=text%2Fplain">QEventLoopEx</a> class.
