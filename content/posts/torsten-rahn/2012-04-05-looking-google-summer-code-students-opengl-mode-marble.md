---
title:   "Looking for Google Summer of Code students: OpenGL mode for Marble"
date:    2012-04-05
authors:
  - torsten rahn
slug:    looking-google-summer-code-students-opengl-mode-marble
---
<p><a href="http://code.google.com/intl/de-DE/soc/">Google Summer of Code</a> application deadline is just a bit more than 24 hours away and we are still looking for highly motivated students to work on a <a href="http://edu.kde.org/marble/">Marble Virtual Globe</a> project this summer. Tomorrow is a holiday in many countries, so you might still have some time for the application. Make sure you file your application not later than tomorrow at 19:00 UTC.

<img src="http://devel-home.kde.org/~tackat/gsoc2012/gsoc-shirts.jpg">

<p>The last project topic for GSoC that I'll cover is "OpenGL support for Marble":

<p>You might ask: Why OpenGL? Doesn't Marble make use of OpenGL already? No it doesn't. Marble currently uses it's own software rendering to provide the different projections. Both texture and vector data are fully rendered in software by default.  
<p> 
<p>This works reasonably fast on most devices, but compromises must be made with respect to the map quality. OpenGL, however, offers higher-quality results, is more state-of-the-art and perhaps helps to reduce power consumption on mobile devices. It therefore makes sense to introduce an OpenGL mode in addition to the sotware rendering mode in Marble (the user should be able to choose between software rendering or OpenGL).
<p>The main task is to refactor the Marble codebase such that both the software rendering and the future OpenGL code share as much code as possible. In particular, visibility control (which objects should be rendered according to the current view parameters) should be factored out into separate classes, such that they can be reused in the OpenGL mode.
<p>Initially the primary focus will be about replicating the current feature set and behavior of Marble. So the globe would still be browsed in looking top-down. Later on one could extend Marble and introduce bird-view, camera flights and "real" mountains and "real" 3D buildings.
<p>Bernhard Beschow created an initial prototype of the OpenGL mode in an experimental branch already. You can look up more details about this prototype in his <a href="http://shentey.wordpress.com/2010/11/10/marble-meets-opengl/">blog</a>. Here is the video and a screenshot:

<p><a href="http://blip.tv/shenteys-show/opengl-in-marble-4372679">Marble - OpenGL Prototype</a>

<img src="http://devel-home.kde.org/~tackat/gsoc2012/marble_opengl.jpg" width="480" height="360">

<p>Since then Bernhard has partially prepared the current Marble master branch for inclusion of OpenGL. But there are still lots of missing bits and pieces in order to have the actual introduction of OpenGL inside Marble. 

<p> Expected Results for this project:

<ul>
<li>duty: refactored code that separates rendering from visibility management
<li>"icing": an OpenGL mode 
</ul>

<p><b>If you are interested in this wonderful project then you should act quickly: Deadline for applications is on Friday, April 6th, 2012m 19:00 UTC.</b> Apart from the <a href="http://www.google-melange.com/document/show/gsoc_program/google/gsoc2012/faqs#student_application_looks">usual GSoC student application guidelines</a> your application should:
<ul>
<li>describe the benefit of the feature from a user's perspective (including self-created mock-ups and screenshots)
<li>provide a rough technical explanation in your own words what the project will be about.
<li>state why you are the best person to master this project.
</ul>
<p>So if you are a student then we are looking forward to your application! Don't hesitate to <a href="edu.kde.org/marble/support.php">ask</a> us any questions.
<!--break-->
