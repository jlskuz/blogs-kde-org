---
title:   "Marble C++ Tutorial Part 1"
date:    2010-08-03
authors:
  - torsten rahn
slug:    marble-c-tutorial-part-1
---
<p>In a few hours KDE 4.5 will be released. And together with it the new version of our Virtual Globe and Map Widget <a href="http://edu.kde.org/marble">Marble</a>. The new release <b>Marble 0.10.0</b> will bring lots of additional features (Routing, Tile-Bulk-Download, Multiple Layers, initial WMS support and a lot more) - many of them related to OpenStreetMap.
<p>Of course development on the next release has started already: Dennis Nienhüser and Niko Sams have added <a href="http://nienhueser.de/blog/?p=137">Worldwide and Offline Routing support</a> to Marble using Gosmore and Routino.
<p>In other news the spanish Linux Magazine has published a really nice <a href="http://www.linux-magazine.es/issue/60/072-075_MarbleLM60.pdf">article</a> by Tim Schürmann about Marble the application: "<i><a href="http://www.linux-magazine.es/issue/60/072-075_MarbleLM60.pdf">¡MARBILLOSO!</a> Marble se ha convertido en una alternativa interesante a Google Earth, y funciona sin conexión a Internet. <a href="...</a></i>". If you are interested in more news about Marble then join us and feel welcome in our <a href="http://www.facebook.com/group.php?gid=346064806033">Marble Facebook Group</a>!
<br>
<p>But Marble is more than an application: You can use the MarbleWidget and its framework in your own application to display map data! There are many applications which are doing this already. See our <a href="http://techbase.kde.org/Projects/Marble/MarbleUsedBy">success story page</a> for more information. And today we start to show how to do it. All you need for our Tutorial is a little bit of C++ and basic Qt knowledge:
<p>

<p><b>Hello Marble!</b></p><br>
<p>The API of the Marble library allows for a very easy integration of a <b>map widget</b> into your application. 

Let's prove that with a tiny <b>Hello world</b>-like example: Qt beginners might want to have a look at the <a href="http://doc.trolltech.com/widgets-tutorial.html">Qt Widgets Tutorial</a> to learn more about the details of the code. But this is probably not necessary. For a start we just create a <a href="http://doc.trolltech.com/qapplication.html">QApplication</a> object and a <a href="http://api.kde.org/4.x-api/kdeedu-apidocs/marble/html/classMarble_1_1MarbleWidget.html">MarbleWidget</a> object which serves as a window. 
<p>By default the MarbleWidget uses the ''Atlas'' map theme. However for our first example we choose to display streets. So we set the maptheme id to 
<a href="http://www.openstreetmap.org">OpenStreetMap</a>. Then we call <a href="http://doc.trolltech.com/qwidget.html#show">QWidget::show()</a> to show the map widget and we call <a href="http://doc.trolltech.com/qapplication.html#exec">QApplication::exec()</a> to start the application's event loop. That's all!
<p>
<code>

#include <QtGui/QApplication>
#include <marble/MarbleWidget.h>

using namespace Marble;

int main(int argc, char** argv)
{
    QApplication app(argc,argv);

    // Create a Marble QWidget without a parent
    MarbleWidget *mapWidget = new MarbleWidget();

    // Load the OpenStreetMap map
    mapWidget->setMapThemeId("earth/openstreetmap/openstreetmap.dgml");

    mapWidget->show();

    return app.exec();
}

</code>
<p>Copy and paste the code above into a text editor. Then save it as <tt>my_marble.cpp</tt> and compile it by entering the folling command on the command line:
<p>
<code>
 g++ -I /usr/include/qt4/ -o my_marble my_marble.cpp -lmarblewidget -lQtGui
</code>
<p>If things go fine, execute <tt>./my_marble</tt> and you end up with a fully usable OpenStreetMap application: 
<p>
<br><img src="http://developer.kde.org/~tackat/marble_tutorial/lesson1.png" class="showonplanet" />

<p>That's all for today. In our next chapter we'll show how to create a basic weather map with Marble. We'll also show how to change basic properties of the map widget. So stay tuned. If you need help join us on our mailing list <a href="http://edu.kde.org/marble/getinvolved.php">marble-devel@kde.org or on #marble</a> (IRC on Freenode). 

<p><b>Tip</b></p><br>
<p>Here's a little checklist to tackle some problems that might arise when compiling the code above:
<ul>
<li> You need Qt and <b>Marble development packages</b> (or comparable SVN installations)
<li> If ''Qt headers'' are not installed in <b>/usr/include/qt4</b> on your system, change the path in the g++ call above accordingly.
<li> Likewise, <b>add -I /path/to/marble/headers</b> if they're not to be found in /usr/include
</ul>
<p><b>Note</b></p><br>
<p>If you provide maps in your application please check the <b>Terms of Use</b> of the map material. The map material that is shipped with Marble is licensed <i>in the spirit of Free Software</i>. This usually means at least that the authors should be credited and that the license is mentioned.
<p>E.g. for <i>OpenStreetMap</i> the license is <a href="http://creativecommons.org/license/by-sa/2.0">CC-BY-SA</a>. Other map data shipped with Marble is either public domain or licensed in the spirit of the BSD license.
<p>
<!--break-->
