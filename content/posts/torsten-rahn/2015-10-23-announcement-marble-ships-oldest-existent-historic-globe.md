---
title:   "Announcement: Marble ships the oldest existent historic  Globe"
date:    2015-10-23
authors:
  - torsten rahn
slug:    announcement-marble-ships-oldest-existent-historic-globe
---
<p>Today I have the pleasure to announce that <a href="http://marble.kde.org">Marble</a> is the first popular virtual globe that ships and visualizes the <a href="https://en.wikipedia.org/wiki/Erdapfel">Behaim Globe</a>. <i>The Behaim Globe is the oldest surviving terrestrial globe on earth</i>. It was created between 1492 and 1493 - yes at the same time when <a href="https://en.wikipedia.org/wiki/Voyages_of_Christopher_Columbus">Christopher Columbus</a> made his first voyage towards the west and "discovered" America. This fact makes the Behaim Globe very special and also subject to scientific research: It documents European cartography during that era and it's probably the only historic globe which completely lacks the American continent. 
<a href="https://play.google.com/store/apps/details?id=org.kde.marble.behaim"><img src="http://developer.kde.org/~tackat/behaim/behaim0.png" class="showonplanet" /></a>
<p>These days the Behaim globe can be <a href="http://objektkatalog.gnm.de/objekt/WI1826">visited</a> in the <a href="http://www.gnm.de/en/">Germanisches Nationalmuseum</a> in Nuremberg, Germany. The Germanisches Nationalmuseum (GNM) has kindly granted the Marble project permission to release the photo scan material of the Behaim Globe under the <a href="https://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA 3.0 license</a> and they have supported us in bringing it for the first time to the users of a widely deployed virtual globe: Marble. Right now our users can immediately download the Behaim Globe from inside Marble by entering File->Download Maps (or download it via our <a href=" https://marble.kde.org/maps-4.5.php">maps download website</a>.). 
<br>
<div>
<p>
<a href="http://developer.kde.org/~tackat/behaim/behaim1.jpg"><img src="http://developer.kde.org/~tackat/behaim/behaim1_thumb.jpg" class="showonplanet" /></a>
</p>
<p>
<a href="http://developer.kde.org/~tackat/behaim/behaim3.jpg"><img src="http://developer.kde.org/~tackat/behaim/behaim3_thumb.jpg" class="showonplanet" /></a>
</p>
</div>
<br>
<div>
<p>Starting with the next Marble release scheduled for December 2015 the Behaim Globe map theme will become a regular part of the map themes that are shipped with the Marble installation package by default.

<p>In addition the Marble project released a special Behaim Marble version in the <a href="https://play.google.com/store/apps/details?id=org.kde.marble.behaim">Google Play Store</a>. So users of Android devices –  like smartphones and tablets – can enjoy the Behaim Globe, too! This also marks the first public release of a Marble based application on Android devices.
<p>The Behaim map theme for Marble was created as part of the master thesis (Diplomarbeit) <a href="http://wisski.cs.fau.de/behaim/sites/default/files/poussami.3D-Modellierung_Behaim-Globus_Marble.pdf">3D Modelling of the Behaim Globe using Marble</a> by Halimatou Poussami. The map theme allows to pan and zoom the whole Behaim Globe - curiously the whole globe is almost fully covered with detailed inscriptions in early modern German. Via checkboxes in the legend tab inside Marble our users can also overlay today's accurate coastlines. This allows to compare the Behaim cartography with today's known actual coastlines. Quite obviously the Behaim map depicts the continents stretched in longitude. So the creators of the Behaim Globe have probably based their globe on an earth radius value that was too small. 
<br>
<div>
<p>
<a href="http://developer.kde.org/~tackat/behaim/behaim2.jpg"><img src="http://developer.kde.org/~tackat/behaim/behaim2_thumb.jpg" class="showonplanet" /></a>
</p>
</div>
<p>The photographic material of the Behaim Globe is based on digital scans made by the <a href="https://www.fau.de/">Friedrich Alexander University (FAU)</a> in Erlangen-Nuremberg and the <a href="https://www.tuwien.ac.at/en/tuwien_home/">IPF TU Wien</a>. These scans were performed using polarized light. This is also part of the reason for the vibrant colors that you can see in the map theme - visually the colors of the Behaim globe are much more subdued. During the past centuries the Behaim Globe has been subject to several "restoration attempts" and "editing", which also resulted e.g. in text changes. Therefore today's scientific research also focuses on the Behaim globe as a <a href="https://en.wikipedia.org/wiki/Palimpsest">palimpsest</a>. Using Marble's legend tab our users can compare the photomaterial of the Behaim Globe with facsimile drawings from 1853 and 1908 which also reveals differences.
<br>
<div>
<p><a href="http://developer.kde.org/~tackat/behaim/behaim4.png"><img src="http://developer.kde.org/~tackat/behaim/behaim4_thumb.png" class="showonplanet" /></a>
</p>
</div>
<br>
<p>The Marble Team would like to thank the <a href="http://www.gnm.de/en/">Germanisches Nationalmuseum</a> for its decision to release the Behaim imagery under the CC BY-SA 3.0 license. In particular we'd like to thank <i>Dr. Thomas Eser (GNM)</i>, <i>Prof. Dr. Günther Görz (FAU)</i> and <i>Halimatou Poussami</i> for their active support in bringing the Behaim Globe to our Marble users and to the public!
<p>   
<br>
<p><a href="http://developer.kde.org/~tackat/behaim/behaim6.png"><img src="http://developer.kde.org/~tackat/behaim/behaim6_thumb.png" class="showonplanet" /></a>
</p>
<p>
<a href="https://play.google.com/store/apps/details?id=org.kde.marble.behaim">
  <img alt="Get it on Google Play"
       src="https://developer.android.com/images/brand/en_generic_rgb_wo_45.png" />
</a>
</p>
<!--break-->
