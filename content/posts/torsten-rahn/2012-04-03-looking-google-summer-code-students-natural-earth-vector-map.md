---
title:   "Looking for Google Summer of Code students: Natural Earth Vector Map"
date:    2012-04-03
authors:
  - torsten rahn
slug:    looking-google-summer-code-students-natural-earth-vector-map
---
<p><a href="http://code.google.com/intl/de-DE/soc/">Google Summer of Code</a> application deadline is near and we are still looking for highly motivated students to work on a <a href="http://edu.kde.org/marble/">Marble Virtual Globe</a> project this summer.

<img src="http://devel-home.kde.org/~tackat/gsoc2012/gsoc-shirts.jpg">

<p>This is a follow-up of yesterday's blog about <a href="http://blogs.kde.org/node/4555">OpenStreetMap vector rendering with tiling support</a>. 

<p>Today I'd like to describe another important feature that we'd like to see covered as a GSoC project. In terms of technology this project is very similar to the "OpenStreetMap vector rendering with tiling support" topic. In fact if you applied for that project already then you might want to apply for this one as well if you want to increase your chances:

<p>The topographic "Atlas" map is the oldest map theme featured by our Marble Virtual Globe. The original aim behind the Atlas map was to create a quite detailed map from very little data. The data would get shipped together with the Marble application for offline usage.

<a href="http://edu.kde.org/marble/screenshots/generic/marble-turkey_thumb.png"><img src="http://edu.kde.org/marble/screenshots/generic/marble-turkey_thumb.png"></a>

<p>The <a href="http://blogs.kde.org/node/3272">result was a combination of a small set</a> of vector data (for coastlines and country borders) and grayscale JPGs (for the color-coded elevation model and for the hillshading). 


<p>The small set of vector data is still based on the ancient <a href="http://www.ngdc.noaa.gov/ecosys/cdroms/ged_iib/datasets/b14/mw.htm">Pospeschil Micro World Data Bank II ("MWDB II")</a> dataset which was originally created in the 70ies/80ies and received its last update 20 years ago.

<p>The GSoC project "Natural Earth Vector Map" is about creating a next generation "Atlas" map: It would be based on the <a href="http://www.naturalearthdata.com/">Natural Earth Data</a> project.

<p>In opposite to the current approach the whole map would be based on vectors. The Natural Earth Data website provides all kinds of <a href="http://www.naturalearthdata.com/features/">feature data</a> for this use case.

<p>The basic data is available in <a href="http://en.wikipedia.org/wiki/Shapefile">ESRI Shapefile format</a>. Just recently Thibaut Gridel added initial support in Marble for Shapefile rendering (using <a href="http://shapelib.maptools.org">libshp</a>). 

<p>The aim of this project would be to provide a new Atlas map based on the Natural Earth vector data that is

<ul>
<li>very space efficient (so the shapefile format might not be the ultimate solution).
<li>has a basic version of the data provided together with the application (similar to the current Atlas map)
<li>allows loading of further vector data on demand online with no user interaction (read the OSM vector blog and think vector tiles).
<li>and shows all kinds of topographic features in a map that is nice to look at.
</ul>

<p>The good news about this project: John Layt has created a <a href="http://techbase.kde.org/Projects/Marble/NaturalEarth">master plan</a> that describes all the challenges of this project in detail.   

<p>If you are interested in this project then you should act quickly: <b>Deadline for applications is on Friday, April 6th, 2012.</b> Apart from the <a href="http://www.google-melange.com/document/show/gsoc_program/google/gsoc2012/faqs#student_application_looks">usual GSoC student application guidelines</a> your application should:
<ul>
<li>describe the benefit of the project from a user's perspective (including self-created mock-ups and screenshots)
<li>provide a rough technical explanation in your own words what the project will be about.
<li>state why you are the best person to master this project.
</ul>
<p>So if you are a student then we are looking forward to your application! Don't hesitate to <a href="edu.kde.org/marble/support.php">ask</a> us any questions.
<!--break-->
