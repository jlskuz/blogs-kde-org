---
title:   "KOrganizer incident dialogs"
date:    2005-06-14
authors:
  - cwoelz
slug:    korganizer-incident-dialogs
---
<p>While documenting KOrganizer incident dialogs, I was bugged about some small details. In the spirit of doing something about it instead of complaining,  created some mockups to express these ideas.<p>

<p>I noticed that you can't select the resource to save the event in the dialog. It seems an important feature to me. Also, the incident description is buried in the end of the dialog. It is hard to know it is even the place to write the description, as there is no label. Here is the original image (correction: this is not the original image, this is a previous mockup. The original image does not have a "Description:" label above the details edit box):</p>

[image:1155]

<p>I moved the description up, and added the resource drop-down at the bottom. Nothing too fancy. The result is here:</p>

[image:1156]

<p>I know that from a task-oriented perspective, adding a description is probably a less common task than setting the time, so the time may deserve to be above the description. But then again, the user can (and should, because it is easier) create the event directly on the calendar main view, so the time is already set. And having where to set the calendar / resource to save the incident avoids displaying that pop up for <b>every</b> incident you save (when you have more than one resource available.</p>

<p>Now to the Attendees tab: the Address Book should be the main source of address right? But here on this dialog, where can you find access to the Address Book? There, at the bottom, hidden under the name "Select Adressees...".</p>

[image:1157]

<p>Since adding by hand one by one of the attendees does not seems to be the most practical way to fill the list, here is a mockup with the address book button renamed and moved up:</p>

[image:1158]

<p>That's all for the moment, but there is more to come.</p>

<!--break-->