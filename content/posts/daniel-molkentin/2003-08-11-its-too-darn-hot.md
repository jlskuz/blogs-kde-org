---
title:   "It's too darn hot..."
date:    2003-08-11
authors:
  - daniel molkentin
slug:    its-too-darn-hot
---
Finally I found enough topics to annoy you with a new blog entry, so here we go:

The last days were definitely too hot. Consequently I enjoyed most of the day in swimming pools or similar and didn't get to do much coding. At least I got some administrativa done, namely publishing the final keylist for the Nove Hrady Keysigning Party and in the evenings when it cooled down I got XMLGUI for KOrganizer RMB menus basically working. It still needs tweaking, but it works, yay! 

The 'tweaking' part seems to be harder than I expected though. There are some things with XMLGUI that are too complicated (namely merging entries not coming from an external rc file), but tronical offered t help me extending the XMLGUI API to simplify things. But it's really worth it, since we'll get rid of quite some code dupes in KOrganizer that way.

But other delayed tasks now strike back: I still need to finish my papers for Nove Hrady and do some slides (while I am writing this, my poor CPU is busy compiling KOffice).