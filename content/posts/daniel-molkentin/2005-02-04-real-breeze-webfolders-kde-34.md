---
title:   "A real breeze: WebFolders with KDE 3.4"
date:    2005-02-04
authors:
  - daniel molkentin
slug:    real-breeze-webfolders-kde-34
---
After I tried to discover what's behind Konquerors new shiny Introduction page, I discovered a feature that I want to share: Konqueror now knows the metaphor of WebFolders, similar to Windows, but actually supporting more protocols. (Warning: Flash movie ahead :))

Accessing WebDAV folders itself is no news and has been working for a long time. However, this was not done in an exactly userfriendly manner. This, the remote:/ slave allows for the creation of WebFolders.

Lucky enough, I remembered that one of my email providers also provides Data Storage via WebDAV, so I tried that.

The following <a href="http://developer.kde.org/~danimo/screenies/webdavfolder.html">little flash movie</a> shows exactly that: The creation of a WebFolder via a wizard, which is (imho) even easier to handle than WebFolder creation on Windows. Note how the new "GMX" entry is generated before the new Konqueror instance starts to open the location.

You might notices two things: For one, I didn't have to provide username or password. This is because I stored the credentials in kwallet on an earlier ("manual") visit to the site. The other issue you might notice is that umlauts are broken on folder listing. I investigated that and found that GMX is sending escaped umlauts valid for Latin 1 (aka ISO-8859-1), but at the same time specifies UTF-8 as character encoding, which is bound to fail. Bug report to GMX is out since 24 hours, no response yet...