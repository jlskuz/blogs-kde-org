---
title:   "Passing Back the Ball"
date:    2005-07-14
authors:
  - daniel molkentin
slug:    passing-back-ball
---
It seemed I made a wish of some people from the usability folks come true. Presenting: my first approach of a CollapsibleWidget!

The same dialog with two filled and one empty collapsable item on Linux, OS X and Windows:

<a href="http://developer.kde.org/~danimo/screenies/collapsible1.png" border="0"> <img src="http://developer.kde.org/~danimo/screenies/collapsible1.png" class="showonplanet" /></a>
<a href="http://developer.kde.org/~danimo/screenies/collapsible_mac.png" border="0"> <img src="http://developer.kde.org/~danimo/screenies/collapsible_mac.png" class="showonplanet" /></a>
<a href="http://developer.kde.org/~danimo/screenies/collapsible_win.png" border="0"> <img src="http://developer.kde.org/~danimo/screenies/collapsible_win.png" class="showonplanet" /></a>

Interesting to see that there are so many differences in the layouting among the different platforms -- odd. But I will sort them out eventually. On the positive side of things, this allows us to finally get rid of the tab-desert in most kde applications and separate common settings from rarely used ones.

This is the part of the usability guys: It's your turn again :).

PS: Whoever finds typos may keep them ;).
<!--break-->