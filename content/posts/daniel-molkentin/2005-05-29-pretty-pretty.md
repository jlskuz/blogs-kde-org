---
title:   "Pretty pretty"
date:    2005-05-29
authors:
  - daniel molkentin
slug:    pretty-pretty
---
<img src="http://developer.kde.org/~danimo/screenies/korg_fancy_preview.png" border=no class="showonplanet" align="right" size="50%"/>
After we decided to merge some improvements from KOrganizer/PI yesterday I finished to check-in the new timebar last night. It looks a lot better now than the old one, which basically only showed hour and seconds at the same font size. Those with 12 hour display will see an am/pm indicator instead of "00".

In other news: Mosquitos really suck... my blood. It's really painful. Fortunately a drug store was open today, probably because this part of Holland is close to Belgium, where shops are also open on sundays. I could get some treatment for my stiches/bites, which caused me a fairly sleepless night. 

Anyway, things are a lot better now, but I'd still prefer the literal pain in the butt over this pain. Hopefully my feet will somewhat recover in time, because tomorrow I will talk about Kolab and Kontact at Holland Open in Amsterdam as a substitute for Till. I'll join Will, who is going to talk about Kopete (and was also a victim of the mosquitos, I start to see a pattern here ;-). I hope to be back in Germany by tuesday.
<!--break-->