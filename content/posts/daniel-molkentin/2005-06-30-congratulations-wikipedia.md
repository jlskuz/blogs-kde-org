---
title:   "Congratulations, Wikipedia!"
date:    2005-06-30
authors:
  - daniel molkentin
slug:    congratulations-wikipedia
---
<a href="http://www.wikipedia.org">Wikipedia</a> has just won the most important prices  german journalism in two categories -- the <a href="http://www.grimme-online-award.de/">Grimme Online Awards</a>. I am proud that we have <a href="http://dot.kde.org/1119552379/">teamed up</a> with such a cool project. I wish I could contribute more time to Wikipedia. Another price went to <a href="http://www.bildblog.de">BILDblog</a>, one of the most important watchblogs that I know. Congratulations to you all!
<!--break--> 