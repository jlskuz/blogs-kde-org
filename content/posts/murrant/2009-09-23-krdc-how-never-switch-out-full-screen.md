---
title:   "Krdc, How to Never Switch Out of Full Screen"
date:    2009-09-23
authors:
  - murrant
slug:    krdc-how-never-switch-out-full-screen
---
This is what full screen KRDC will look like in KDE SC 4.4:

<a href="http://picasaweb.google.com/lh/photo/2GTG334kwudPAbQmukN61w?feat=embedwebsite"><img src="http://lh3.ggpht.com/_X8cIVFCZFY4/SrmI8RTo7BI/AAAAAAAAAG4/JTYT5UVs45Y/s800/krdc_snapshot_fullscreen.png" /></a>
<small>Jam-packed with new features.</small>

Hi, now that I have your attention, let me introduce myself.  I'm Tony Murray and like many SysAdmins on Linux, I use KRDC on a daily basis and I am often connected to several servers simultaneously.  KRDC is one of my most vital tools.  I have some form of coding skills, mostly being schooled in Java, but there are many fundamental differences between C++ and Java so it is a hard learning experience.  I have contributed some small bits of code to Plasma and the nowplaying applet (which I believe is almost obsolete).  Then I finally got up the courage to work on something I've been wanting to tweak for a long time, KRDC.  I love it so, but there have been many small pain points in using it to do my SysAdmin duties.

It all started out with a small bug that I was occasionally hitting.  I started reading through the code and also some C++/Qt books/apidocs to help me know what I'm doing better.  After much learning, I finally had a patch to commit, I presented it to Urs Wolfer, the current maintainer of KRDC, and he happily asked if I could commit it.  So commit I did, then my appetite got larger.

One of my most wanted changes is the ability to switch sessions while I was in full screen mode, so I set out to try to make that happen.  After a couple of weeks of casual hacking, I had something working and I posted to be reviewed.  Urs was happy to see it and encouraged me to continue.  After creating this, I realized that there were a few things about the gui design that were holding it back from acting like I thought it should and some things that were making the code overly complicated.

I started thinking about what needed to happen to make things work better and before I knew it I was working on a redesign of the way new connections are handled in KRDC.  After many posts to the KDE Usability board and conversations with Urs and others, we finally settled on a new design.  Here is what the mock-up looked like:
<a href="http://picasaweb.google.com/lh/photo/AHDG3zmpMWM4knw_If_1pA?feat=embedwebsite"><img src="http://lh6.ggpht.com/_X8cIVFCZFY4/SrmQyMbU83I/AAAAAAAAAHw/bPlDxPdAhbE/s400/krdc%20idea5.png" /></a>

Urs was pretty excited about it and coded up an initial version in a few days, I worked on moving the Zercoconf stuff into the dockwidget.  After several bug shakedowns, Urs finally committed his work to change the flow for new connections.  I then started on my work with the full screen switching.  Because I had it in mind while doing the new connection redesign I also added a button to allow creating new connections while in full screen.  This added some complications as there was no synchronization between the KTabWidget that holds all of the RemoteViews and the KComboBox which allows you to switch while in full screen.  So you could end up with some inconsistent situations.  After debating a bit, I decided to create a model for the KTabWidget which I could supply to the KComboBox and then it would be done by Qt for me.

Most of the big changes are behind us and we will be focusing on bug-fixing and polishing.  I have plenty of small changes that in my mind to take KRDC to the next level and even more changes that I hope will come together for KDE SC 4.5 or later.  One thing I'm imagining is something like Firefox's Awesome Bar.

<b>I hope you guys enjoy the new features and as always with new features please test and report bugs so we can have a great 4.4 release. :)</b>

Click through to see more screen shots of the new stuff in action...
<!--break-->
<a href="http://picasaweb.google.com/lh/photo/qz5Dmh5E7u8V9rXNr35jtg?feat=embedwebsite"><img src="http://lh3.ggpht.com/_X8cIVFCZFY4/SrmI8c3YGmI/AAAAAAAAAG8/qXBwkWjxjzc/s400/krdc_snapshot_initialwindow1.png" /></a>
KRDC, after opening with the "Always show tab bar" option disabled.

<a href="http://picasaweb.google.com/lh/photo/RADx4xkxeRhO7HZur0ioPQ?feat=embedwebsite"><img src="http://lh3.ggpht.com/_X8cIVFCZFY4/SrmI8jMPYsI/AAAAAAAAAHA/gXiWNTHEhEc/s400/krdc_snapshot_singleconnection.png" /></a>
Connected to a single remote computer.

<a href="http://picasaweb.google.com/lh/photo/3PQ9djHWo1Tj0V0MGlrF-A?feat=embedwebsite"><img src="http://lh3.ggpht.com/_X8cIVFCZFY4/SrmI8tkztgI/AAAAAAAAAHE/r3Qv368ea-s/s400/krdc_snapshot_secondconnection.png" /></a>
Clicked the new connection button and using the filter.

<a href="http://picasaweb.google.com/lh/photo/YptsorNE7tmnPnEdU251bA?feat=embedwebsite"><img src="http://lh4.ggpht.com/_X8cIVFCZFY4/SrmI86H_NEI/AAAAAAAAAHI/90u0_2FUb34/s400/krdc_snapshot_secondconnection1.png" /></a>
With a second connection open.

<a href="http://picasaweb.google.com/lh/photo/eUDxjJyZediqMdm5xU6yMg?feat=embedwebsite"><img src="http://lh4.ggpht.com/_X8cIVFCZFY4/SrmU-c5dhpI/AAAAAAAAAII/XXBy9erMDWU/s400/krdc_snapshot_fullscreen1.png" /></a>
And finally in full screen mode.