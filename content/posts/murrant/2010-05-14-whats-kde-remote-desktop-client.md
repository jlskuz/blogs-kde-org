---
title:   "Whats up in KDE Remote Desktop Client?"
date:    2010-05-14
authors:
  - murrant
slug:    whats-kde-remote-desktop-client
---
KDE SC 4.5 is coming up around the bend and I'm posting about some of the new exciting (to somebody I hope) features for KRDC.  For KRDC 4.4 we introduced <a href="http://blogs.kde.org/node/4065">a new gui layout</a>.  I have been away from the keyboard for awhile and finally have been able to hammer out some bugfixes (<a href="http://bugs.kde.org/show_bug.cgi?id=235223">1</a>,<a href="http://bugs.kde.org/show_bug.cgi?id=232228">2</a>,<a href="http://bugs.kde.org/show_bug.cgi?id=208845">3</a>,<a href="http://websvn.kde.org/?view=revision&revision=1120422">4</a>) for those new features as well as some older bugs to both 4.5 and 4.4.3 (for the most part).

Well what are these new features I'm talking about?  Well for starters I've taken that drab list of connections in the center of KRDC and made it much more useful by adding statistics and other information.  You can sort your list by these different pieces of information and it will save your sort column/order for the next time you open it so you can keep it sorted the way you like.  Lets take a look:

<a href="http://picasaweb.google.com/lh/photo/yNFgLu1q1ai5l23Mo0MOqQ?feat=embedwebsite"><img src="http://lh4.ggpht.com/_X8cIVFCZFY4/S-zHmCQX51I/AAAAAAAAAK4/BshMrC7e1EM/s800/krdc_screenshot1.png" /></a>

I thought it would be fun to show KRDC with some non-default settings.  From left to right, we have a bookmark icon that you can use to toggle favorites, the name of the connection (user modifiable), last connection date/time, the number of times you connected this remote connection, when the connection was created, and how this entry came to be listed here (current sources are History, Bookmarks, and Zeroconf if supported).  Now most of this isn't useful till you've been collecting data for awhile, but I cheated, planning ahead I included the data collection for this into 4.4 so when you open 4.5, you should already have a pretty interesting view.

We've also added a right click menu so you can access several useful items right from this screen:

<a href="http://picasaweb.google.com/lh/photo/tMWxkdXmOQwsTnuwZvgz-Q?feat=embedwebsite"><img src="http://lh4.ggpht.com/_X8cIVFCZFY4/S-zHmaPe6EI/AAAAAAAAAK8/rUW1icN2Oxg/s400/krdc_screenshot2.png" /></a>

While I was fixing a window resizing bug for 4.5, I added some nice functionality for VNC users.

<a href="http://picasaweb.google.com/lh/photo/kj9LuFHhOwqn5fzOcgCStA?feat=embedwebsite"><img src="http://lh3.ggpht.com/_X8cIVFCZFY4/S-zHmi6sAPI/AAAAAAAAALA/QvjSR_qpyWU/s400/krdc_screenshot3.png" /></a>

Now you can have a VNC connection open up scaled to a specific resolution every time.

Another thing I changed while working on the resizing bugs was the behavior when opening a new connection.  One of the most annoying problems I've had with KRDC is it always seems to be maximized because it will try to resize the window to fit, then I click the fullscreen button anyway.  Upon leaving fullscreen, I still have a maximized window covering up everything else.  I thought of a way around this, we assume that if the desktop and the remote connection size match, the user wants to go into fullscreen mode.  So with 4.5, it works something like this: open KRDC it is a nice compact window, open a connection and select the current screen resolution, the window immediately goes fullscreen, click the fullscreen button and it switches back to a nice small window again.  So is this a good change or a bad change, please let me know, feedback is great!

Unfortunately, during the 4.5 cycle I haven't had much time to hack, so these features will have to be pushed back:
 * In Progress: Connection status / reconnect screen
 * TODO: Unified fancy connection input line

Feel free to contribute, I'm pretty easy to get a hold of ;)  There have been several string changes, so thanks to all the translators.  And finally, my apologies to any one having issues with KRDC crashing when closing VNC, it appears the VNC thread is not exiting sometimes and as of right now it is out of my expertize to fix it.

I hope you enjoy the new changes, I have been happy with them in my own usage.  Till next time.
<!--break-->
