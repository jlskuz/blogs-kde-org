---
title:   "KDE Is Bloated"
date:    2005-11-09
authors:
  - stefan teleman
slug:    kde-bloated
---
so, KDE suffers from bloat, over-engineering and user confusion. apparently.

personally, i never thought that to be the case until now, but, there are others who seem to hold these beliefs to be self-evident. i respect everyone's opinions and beliefs, even though they may be different than, or opposite to, my own.

i want to help. really, i do. therefore, i have spent some time thinking about an alternative to this over-engineering bloatness problem in KDE. luckily, i think i found a solution: DOS.

please let me explain. let's start first by enumerating KDE's most blatant defects:
1. Bloat
2. Too many configuration options
3. Over-engineered
4. Confusing to the average user

this list of shortcomings seems incomplete. to wit, i shall add my own personal complaint about KDE:

5. Too many blue icons and buttons

i want this blog entry to be an objective, professional and knowledgeable comparative analysis of KDE's weaknesses vs. DOS' strenghts. therefore, i will now enumerate DOS' overwhelming superiority over KDE, in the areas mentioned above:

1. No bloat, runs in 64K. beat that if you can.
2. No configuration options. if you disagree, name one.
3. Under-engineered (i think you'll agree)
4. Very clear and concise for any user (you only need to know about C:\, and everything is command line)

and, most importantly:

5. No blue icons or buttons.

if there still is any doubt about DOS' superiority over KDE, please allow me to describe a few typical KDE user situations. for example, in KDE, if you want to get to a shell command line, you have to start Konsole. Konsole has blue icons. therefore, Konsole is Bad. Bad Konsole makes for a Bad user experience. what's even worse, Konsole comes with a Blue Background. that is also Bad. another example: in DOS, every directory on your hard drive starts at C:\. easy enough, clear, intuitive, concise, no confusion possible. that is a Good Thing. compare that with Konqueror, or Krusader. again, Blue Icons for folders. Bad. Blue buttons at the top. Bad. there is no C:\, and all these Blue Folders are arranged in this weird tree-like shape. Bad.

Conclusion: DOS is better than KDE. QED.

i live in a Blue State. Bad.











 

