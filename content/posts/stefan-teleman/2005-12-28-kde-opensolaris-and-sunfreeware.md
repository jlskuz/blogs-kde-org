---
title:   "KDE at OpenSolaris and SunFreeware"
date:    2005-12-28
authors:
  - stefan teleman
slug:    kde-opensolaris-and-sunfreeware
---
KDE now has a Community at <a href="http://www.opensolaris.org/os/community/desktop/communities/kde/">OpenSolaris</a>. And Steve Christensen had made KDE available for download from <a href="http://www.sunfreeware.com/">SunFreeware</a> as well.

It all happened yesterday, which was my birthday.

I got some nice birthday presents this year. :-)

