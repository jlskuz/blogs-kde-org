---
title:   "not your average geek"
date:    2009-10-29
authors:
  - till
slug:    not-your-average-geek
---
On a related (to my other blog post today) note, while I'm giving credit where credit is due: my personal KDE hero at the moment is Anne Wilson, who has been helping KDEPIM users for years on our lists and at meetings and has been a voice of reason, courtesy, constructive feedback and positiveness that makes a huge difference in the atmosphere of our community. I much admire her work with the documentation team (userbase, anyone?) and the community working group and ever since I first met her in person (in Glasgow, I think) I have been impressed by the fearless and all embracing manner in which she has found her way amongst us weirdos and become a gentle, well respected leader and wrangler of geeks. I don't know when exactly it is, but happy 70th birthday, Anne, all the best from us PIMsters, we thank you and look forward to many more Akademy meetings with you.
<!--break-->
