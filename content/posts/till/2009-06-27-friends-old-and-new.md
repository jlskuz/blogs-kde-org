---
title:   "Friends, old and new"
date:    2009-06-27
authors:
  - till
slug:    friends-old-and-new
---
I'm currently sitting at a table in the still empty Qt Software / KDAB booth, listening to the awesome KDE Linuxtag team get the KDE / Amarok / Kubuntu presence behind me ready for another day. Throughout all of Linuxtag they have been, and will continue to be, proudly showcasing what we have collectively already achieved and helping new contributors make their way into our community so we can do even greater things in the future, with their help. Today the conference program features a KDE track, full of diverse and interesting presentations for a wide range of audiences. Claudia, Luca and their team have done an amazing job getting this conference presence and the many talks lined up. KDE is again making a very good impression, I think. Yeah, us! :)<br><br>The joking and chatter behind me has reminded me how much I'm looking forward to the Gran Canaria Desktop Summit next week. It will be great to catch up with everyone, like every year, but unlike every year, this time I'll also be able to catch up with friends from outside the usual KDE circles, since many friends from Gnome and related projects will be there. I'm sure this co-located event will be awesome and will bring our two communities, which share so many of their core goals and ideals, even closer together. I'm going there a few days early to do some hanging out on the beach, diving and general R&R. Good times.<br><br>But now I need to get going, the KDE track starts in a few minutes and my presentation on transitioning from Qt/KDE 3 to 4 as a developer is the second one.
<br>
<img src=http://amarok.kde.org/blog/uploads/gcds_summit_badge.serendipityThumb.png>
<!--break-->
