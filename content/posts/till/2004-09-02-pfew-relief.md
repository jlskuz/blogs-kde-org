---
title:   "Pfew, this is a relief."
date:    2004-09-02
authors:
  - till
slug:    pfew-relief
---
Just a quick completely KDE unrelated personal note: My wife's heart surgery was very successfull. They found out what was wrong and were able to completely fix it. Hats off to the debugging skills of these electrophysiological cardiologists, the amount of information they can read out of a few meters of EEG output paper is astonishing. Everyone is confident that she will be better than ever and free of heart related troubles for the foreseeable future. Thanks to everyone who sent good wishes and words of encouragement, they were very much appreciated. :)